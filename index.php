<?php
// Version
define('VERSION', '2.3.0.2');
if (isset($_SERVER['REMOTE_ADDR'])) {
    $ip = $_SERVER['REMOTE_ADDR'];
} else {
    $ip = '';
}
if((isset($ip) && $ip && ($ip == '222.255.115.196'))){
  ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
} else {
  ini_set('display_errors', 0);
}
//ini_set('display_errors', 1);
//
// Configuration
if (is_file('config.php')) {
	require_once('config.php');
}

// Install
if (!defined('DIR_APPLICATION')) {
	header('Location: install/index.php');
	exit;
}
// Startup
require_once(DIR_SYSTEM . 'startup.php');

start('catalog');