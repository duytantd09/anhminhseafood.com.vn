<?php
// HTTP
define('HTTP_SERVER', 'https://www.anhminhseafood.com.vn/');
define('HTTP_IMAGE', 'https://www.anhminhseafood.com.vn/image/');

// HTTPS
define('HTTP_SERVER_WMCC', 'https://www.anhminhseafood.com.vn/');
define('HTTPS_SERVER', 'https://www.anhminhseafood.com.vn/');
define('HTTPS_IMAGE', 'https://www.anhminhseafood.com.vn/image/');
// DIR
define('DIR_APPLICATION', '/home/khachhang/web/anhminhseafood.com.vn/public_html/catalog/');
define('DIR_SYSTEM', '/home/khachhang/web/anhminhseafood.com.vn/public_html/system/');
define('DIR_IMAGE', '/home/khachhang/web/anhminhseafood.com.vn/public_html/image/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/theme/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_SYSTEM . 'storage/cache/');
define('DIR_DOWNLOAD', DIR_SYSTEM . 'storage/download/');
define('DIR_LOGS', DIR_SYSTEM . 'storage/logs/');
define('DIR_MODIFICATION', DIR_SYSTEM . 'storage/modification/');
define('DIR_UPLOAD', DIR_SYSTEM . 'storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'khachhang_food');
define('DB_PASSWORD', 'ZLiqIZCT37');
define('DB_DATABASE', 'khachhang_food');
define('DB_PORT', '3306');
define('DB_PREFIX', 'tgq_');