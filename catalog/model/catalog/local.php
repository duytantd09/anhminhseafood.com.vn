<?php
class ModelCatalogLocal extends Model
{
	public function getLocals($parent_id = 0)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "local c LEFT JOIN " . DB_PREFIX . "local_description cd ON (c.local_id = cd.local_id) LEFT JOIN " . DB_PREFIX . "local_to_store c2s ON (c.local_id = c2s.local_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");


		return $query->rows;
	}

	public function getLocalByLocaltions($local_id)
	{
		$data = array();
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX ."location_to_local WHERE local_id = '". (int)$local_id ."'");
		foreach ($query->rows as $row) {
			$data[] = $row['location_id'];
		}
		return $data;
	}
}