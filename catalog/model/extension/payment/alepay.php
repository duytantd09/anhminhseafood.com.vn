<?php

class ModelExtensionPaymentAlepay extends Model {
    private $logFileName = "alepay.log";
    private $_alepay;

    public function getMethod($address, $total) {
        $this->load->language('extension/payment/alepay');

        /*$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int) $this->config->get('payment_alepay_geo_zone_id') . "' AND country_id = '" . (int) $address['country_id'] . "' AND (zone_id = '" . (int) $address['zone_id'] . "' OR zone_id = '0')");

        if (!$this->config->get('payment_alepay_geo_zone_id')) {
            $status = true;
        } elseif ($query->num_rows) {
            $status = true;
        } else {
            $status = false;
        }*/
        $status = true;
        $method_data = array();

        if ($status) {
            $method_data = array(
                'code' => 'alepay',
                'title' => $this->language->get('text_title'),
                'terms' => '',
                'sort_order' => $this->config->get('payment_alepay_sort_order')
            );
        }

        return $method_data;
    }


    function pagePay($order_data, $config) { 
        $log = new Log($this->logFileName);
        $log->write("-------- Thanh toan qua Alepay (Ma HD: ".$order_data['order_id'].") ---------");
        
        //$this->load->library('alepayapi');
        
        $this->_alepay = new AlepayAPI(null, array(
            'apiKey' => $config['public_key'],
            'encryptKey' => $config['encrypt_key'],
            'checksumKey' => $config['checksum_key'],
            'env' => $config['env']
        ));
        
        // kiem tra da ket noi token chua
        $token = '';
        $user_id = $this->customer->getId();
        if ($user_id) { // da login
            $user_id = (int) $user_id;
            $log->write("Khach hang da dang nhap voi ID: ".$user_id);
            $query = $this->db->query("SELECT token FROM ".DB_PREFIX."alepay_token WHERE user_id = ".$this->db->escape($user_id));
            if ($query->num_rows) { // da ket noi
                $token = $query->rows[0]['token'];
                $log->write("Khach hang da ket noi the voi token: ".$token);
            } else { // chua ket noi
                $log->write("Khach hang chua ket noi the");
            }
        } else { // chua login
            $log->write("Khach hang ko dang nhap");
        }
        
        // Huy ket noi the neu khach hang chon phuong thuc ko phai token
        if ($token && $config['alepay_method'] !== '3') {
            $this->_alepay->cancelCardLink($token);
            $this->db->query("DELETE FROM ".DB_PREFIX."alepay_token WHERE user_id = ".$this->db->escape($user_id));
            $token = '';
            $log->write("Khach hang da lien ket the nhung ko chon thanh toan token nen huy lien ket");
        }
        
        if ($token && $config['alepay_method'] === '3') { // Neu khach hang da ket noi the va tiep tuc chon thanh toan token
            $data = $this->_alepay->createTokenizationPaymentData($token);
            $url = $this->_alepay->baseURL[$this->_alepay->env] . $this->_alepay->URI['tokenizationPayment'];
            $log->write("Khach hang da lien ket the va tiep tuc chon thanh toan token");
        } else { // Chua lien ket the
            $data = $this->_alepay->createCheckoutData();
            $data['checkoutType'] = $config['alepay_method'];
            $data['buyerAddress'] = $order_data['payment_address_1'] ? $order_data['payment_address_1'] : $order_data['payment_address_2'];
            $data['buyerAddress'].= $order_data['payment_city'] ? $order_data['payment_city'] : $order_data['payment_zone'];
            $data['buyerCity'] = $order_data['payment_country'];
            $data['buyerCountry'] = 'Viet Nam';
            $data['buyerEmail'] = $order_data['email'];
            $data['buyerName'] = trim($order_data['payment_firstname'] . ' ' . $order_data['payment_lastname']);
            $data['buyerPhone'] = $order_data['telephone'];
            
            if ($config['alepay_method'] === '3') { // KH chon thanh toan token
                $data['checkoutType'] = '1';
                $data['merchantSideUserId'] = $user_id;
                $data['buyerPostalCode'] = $order_data['payment_postcode'];
                $data['buyerState'] = $order_data['payment_zone'] ? $order_data['payment_zone'] : 'VN';
                $data['isCardLink'] = true;
                $log->write("Khach hang chon thanh toan token");
            } else {
                $log->write("Khach hang chon thanh toan ".($config['alepay_method'] === '1' ? "thuong" : "tra gop"));
            }
            $url = $this->_alepay->baseURL[$this->_alepay->env] . $this->_alepay->URI['requestPayment'];
        }

        $data['orderCode'] = $order_data['order_id'];
        $data['orderDescription'] = $order_data['comment'] ? $order_data['comment'] : $data['orderCode'];
        $data['amount'] = $order_data['total'];
        $data['totalItem'] = $this->cart->countProducts();
        $data['currency'] = $config['currency'] ? $config['currency'] : $order_data['currency_code'];
        $data['returnUrl'] = $this->url->link('extension/payment/alepay/callback');
        $data['cancelUrl'] = $this->url->link('quickcheckout/checkout');
        $data['language'] = $config['language'];
        
        return array(
            $this->_alepay, 
            $this->_alepay->sendRequestToAlepay($data, $url.'?lang=' . $config['language'])
        );
        
    }
	function updateOrder($data, $config) { 
		$log = new Log('alepay.log');
		$alepay = new AlepayAPI(null, array(
            'apiKey' => $config['public_key'],
            'encryptKey' => $config['encrypt_key'],
            'checksumKey' => $config['checksum_key'],
            'env' => $config['env']
        ));
		$status = $config['status'];
		$user_id = $this->customer->getId();
		$alepay_data = json_decode($alepay->decryptCallbackData(trim($data), $alepay->publicKey));
		if ($alepay_data->errorCode === '000') {
			if (isset($alepay_data->data->alepayToken)) {
				$token = $alepay_data->data->alepayToken;
				$query = $this->db->query("SELECT id FROM ".DB_PREFIX."alepay_token WHERE user_id = ".$this->db->escape($user_id)." AND token = '".$this->db->escape($token)."'");
				if ($query->num_rows) { // da ket noi

				} else { // chua ket noi
					$this->db->query("INSERT INTO ".DB_PREFIX."alepay_token SET token = '".$this->db->escape($token)."', user_id = ".$this->db->escape($user_id).", time = '".$this->db->escape(date('Y-m-d H:i:s'))."'");
					$log->write("Cap nhat token: ".$token);
				}
				$info = json_decode($alepay->getTransactionInfo($alepay_data->data->transactionCode));
				$this->load->model('checkout/order');
				$order_id = $info -> orderCode;
				$this->model_checkout_order->addOrderHistory($order_id, $status);
			}
			else{
				$info = json_decode($alepay->getTransactionInfo($alepay_data->data));
				$this->load->model('checkout/order');
				$order_id = $info -> orderCode;
				//echo $order_id .' <br/>'; 
				//echo $status;
				//die;
				$this->model_checkout_order->addOrderHistory($order_id, $status);
			}
			$log->write("Thanh toan thanh cong");
		} else {
			$log->write("Thanh toan that bai errorCode: ".$alepay_data->errorCode);
		}
	
	}

}
