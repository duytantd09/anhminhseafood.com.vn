<?php
class ModelExtensionTotalShippingzone extends Model {
	public function getTotal($total) {
		$this->load->language('extension/total/shipping_zone');
		// $this->session->data['shipping_zone'] = 100;
		if (isset($this->session->data['shipping_zone'])) {
			$shipping_cost = $this->session->data['shipping_zone'];
		} else{
			$shipping_cost = 0;
			$this->session->data['shipping_zone'] = 0;
		}

		$total['totals'][] = array(
			'code'       => 'shipping_zone',
			'title'      => $this->language->get('text_shipping_zone'),
			'value'      => $shipping_cost,
			'sort_order' => $this->config->get('shipping_zone_sort_order')
		);

		// $total += $shipping_cost;
		
		$total['total'] += $shipping_cost;

	}

	public function setcost($cost) {
		unset($this->session->data['shipping_zone']);
		$this->session->data['shipping_zone'] = $cost;
		return 'ok';
		
		
	}

}