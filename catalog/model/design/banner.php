<?php
class ModelDesignBanner extends Model {
	public function getBanner($banner_id) {
		$banner = array();
		$banner = $this->cache->get('banners_' . $banner_id . '_' . (int)$this->config->get('config_language_id'));  
        if (!$banner) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "banner b LEFT JOIN " . DB_PREFIX . "banner_image bi ON (b.banner_id = bi.banner_id) WHERE b.banner_id = '" . (int)$banner_id . "' AND b.status = '1' AND bi.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY bi.sort_order ASC");
			$banner =  $query->rows;
			$this->cache->set('banners_' . $banner_id . '_' . (int)$this->config->get('config_language_id'), $banner);
		}
		return $banner;
	}
}