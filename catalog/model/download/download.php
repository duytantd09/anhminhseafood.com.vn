<?php
class ModelDownloadDownload extends Model {
	public function getDownload($download_id) {
        $query = $this->db->query("SELECT d.filename, d.mask FROM " . DB_PREFIX . "download d WHERE d.download_id = '" . (int)$download_id . "'");

        return $query->row;
	}

	public function getDownloads($start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 20;
		}
        
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "download d LEFT JOIN " . DB_PREFIX . "download_description dd ON (d.download_id = dd.download_id) WHERE dd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY d.date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

        return $query->rows;
	}

	public function getTotalDownloads() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "download` d");

        return $query->row['total'];
	}
}