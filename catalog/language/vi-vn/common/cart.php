<?php
// Text
$_['text_items']    = '%s sản phẩm - %s';
$_['text_empty']    = 'Giỏ hàng của bạn chưa có sản phẩm nào!';
$_['text_cart']     = 'Xem giỏ hàng';
$_['text_checkout'] = 'Thanh toán';

//$_['text_items'] = '<i class="fa fa-shopping-cart" aria-hidden="true"></i><span class="count">%s</span> <span class="total contrast_font mobile_hide">%s</span><span class="cart-text">GIỎ HÀNG</span>';
$_['text_items'] = '<span class="count">%s</span> <span class="total contrast_font mobile_hide">%s</span><span class="cart-text">GIỎ HÀNG</span>';
			
$_['text_recurring']  = 'Hồ sơ thanh toán';
$_['text_heading_title'] = 'Giỏ hàng';