<?php
// Mobile menu text
$_['tlptech_text_mobile_menu'] 	= 'Danh mục';

// Category pages
$_['text_category_expire'] 		 = 'Expires in:';
$_['text_category_stock_quantity']  = 'Hurry! only %s item(s) left!';


// Product page
$_['text_special_price'] 	 	= 'Giá khuyến mãi:';
$_['text_old_price']  			= 'Giá cũ:';
$_['text_you_save']  			= 'Tiết kiệm:';
$_['text_expire'] 		 		= 'Hết hạn vào ngày:';
$_['text_stock_quantity']  		= 'Hurry! only %s item(s) left!';
$_['text_items_sold']      		= '%s item(s) already purchased!';
