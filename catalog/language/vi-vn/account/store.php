<?php
// Heading
$_['heading_title']      = 'Thông tin cửa hàng của tôi';

// Text
$_['text_account']       = 'Cửa hàng';
$_['text_edit']          = 'Chỉnh sửa thông tin';
$_['text_store_details']  = 'Chi tiết cửa hàng';
$_['text_success']       = 'Thành công: Cửa hàng của bạn đã được cập nhật thành công.';
$_['text_product']		 = 'Sản Phẩm';
$_['text_profile']		 = 'Giới thiệu';
$_['text_contact']		 = 'Liên hệ';
$_['text_blog']		 = 'Tin tức';

// Entry
$_['entry_name']                       = 'Tên cửa hàng';
$_['entry_owner']                      = 'Chủ cửa hàng';
$_['entry_address']                    = 'Địa chỉ';
$_['entry_geocode']                    = 'Vị trí';
$_['entry_email']                      = 'E-Mail';
$_['entry_telephone']                  = 'Điện thoại';
$_['entry_fax']                        = 'Fax';
$_['entry_image']                      = 'Hình ảnh';
$_['entry_open']                       = 'Thời gian mở cửa';
$_['entry_comment']                    = 'Bình luận';
$_['entry_location']                   = 'Vị trí cửa hàng';
$_['entry_meta_title']                 = 'Tiêu đề Meta';
$_['entry_meta_description']           = 'Mô tả Meta Tag ';
$_['entry_meta_keyword']               = 'Từ khóa Meta Tag';
$_['entry_country']                    = 'Tỉnh/Thành Phố';
$_['entry_zone']                       = 'Tỉnh / Thành phố';
$_['entry_keyword']						= 'Từ khóa SEO';

$_['help_name'] 					= 'Tên cửa hàng được hiển thị trên các sản phẩm của bạn.';

// Error
$_['error_exists']       = 'Cảnh báo : Địa chỉ E -Mail đã được đăng ký!';
$_['error_name']    = 'Tên phải được từ 1 đến 32 ký tự!';
$_['error_owner']     = 'Chủ đầu tư phải có từ 1 đến 32 ký tự!';
$_['error_email']        = 'E -Mail Địa chỉ không có vẻ hợp lệ!';
$_['error_telephone']    = 'Điện thoại phải có từ 3 đến 32 ký tự!';
$_['error_warning'] = 'Bạn không thể thay đổi các thông tin!';
