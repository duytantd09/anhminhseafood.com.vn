<?php
// Heading
$_['heading_title']   = 'Xác thực tài khoản';

// Text
$_['text_account']    = 'Tài khoản';
$_['text_verifried']  = 'Mã xác thực';
$_['text_code']      = 'Nhập mã kết hợp với tài khoản của bạn. Mã xác nhận sẽ được gửi đến địa chỉ e-mail của bạn, bạn đã đăng ký với trang web.';
$_['text_success']    = 'Một email với một mã xác nhận đã được gửi địa chỉ email của bạn.';
$_['text_verifried_success'] = 'Xác thực thành công. Cảm ơn bạn đã đăng kí thành viên!';

// Entry
$_['entry_verifried']     = 'Mã xác thực';

$_['button_verifried']   = 'Xác thực ngay';
$_['button_send_mail']   = 'Yêu cầu mã xác thực';

// Error
$_['error_verifried']     = 'Cảnh báo: Mã xác thực không được tìm thấy trong hồ sơ của chúng tôi, xin vui lòng thử lại!';

