<?php
// Heading
$_['heading_title']      = 'Tài khoản';

// Text
$_['text_account']       = 'Tài khoản';
$_['text_my_account']    = 'Tài khoản';
$_['text_my_orders']     = 'Đơn hàng của tôi';
$_['text_my_newsletter'] = 'Thư thông báo';
$_['text_edit']          = 'Chỉnh sửa thông tin tài khoản của bạn';
$_['text_password']      = 'Thay đổi mật khẩu của bạn';
$_['text_address']       = 'Sửa đổi sổ địa chỉ của bạn';
$_['text_wishlist']      = 'Sửa đổi danh sách mong muốn của bạn';
$_['text_order']         = 'Xem lịch sử đơn hàng của bạn';
$_['text_download']      = 'Tải về';
$_['text_reward']        = 'Điểm thưởng của bạn';
$_['text_return']        = 'Xem yêu cầu trả hàng của bạn';
$_['text_transaction']   = 'Giao dịch của bạn';
$_['text_newsletter']    = 'Đăng ký / Hủy đăng ký nhận bản tin';
$_['text_recurring']     = 'Thanh toán định kỳ';
$_['text_transactions']  = 'Giao dịch';
$_['text_filemanager']   = 'Quản lý tập tin';
$_['text_orders']   	 = 'Đơn hàng của cửa hàng';
$_['text_information']   = 'Thông tin cửa hàng';
$_['text_products']		 = 'Quản lý sản phẩm';
$_['text_banner']		 = 'Banner';

$_['text_blog'] = 'Blog';
$_['text_article']		 = 'Quản lý bài viết';
$_['text_blog_comment']		 = 'Bình luận';
