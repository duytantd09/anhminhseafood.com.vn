<?php
// Heading
$_['heading_title']				= 'Cảm ơn bạn đã mua sắm với %s .... ';

// Text
$_['text_title']				= 'Thẻ tín dụng / Thẻ ghi nợ (PayPoint)';
$_['text_response']				= 'Phản hồi từ PayPoint:';
$_['text_success']				= '... thanh toán của bạn đã được nhận thành công.';
$_['text_success_wait']			= '<b><span style="color: #FF0000">Vui lòng chờ...</span></b> trong khi chúng tôi hoàn tất xử lý đơn đặt hàng của bạn.<br>Nếu trình duyệt của bạn không tự động chuyển trong 10 giây, vui lòng bấm vào <a href="%s">đây</a>.';
$_['text_failure']				= '... Thanh toán của bạn đã bị hủy bỏ!';
$_['text_failure_wait']			= '<b><span style="color: #FF0000">Vui lòng chờ...</span></b><br>Nếu trình duyệt của bạn không tự động chuyển trong 10 giây, vui lòng bấm vào <a href="%s"> đây</a>.';