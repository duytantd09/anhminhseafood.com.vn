<?php
// Text
$_['text_title']				= 'Thẻ tín dụng hoặc Thẻ ghi nợ';
$_['text_credit_card']			= 'Chi tiết thẻ tín dụng';
$_['text_wait']					= 'Vui lòng đợi!';

// Entry
$_['entry_cc_number']			= 'Số thẻ';
$_['entry_cc_name']				= 'Tên chủ thẻ';
$_['entry_cc_expire_date']		= 'Ngày hết hạn thẻ';
$_['entry_cc_cvv2']				= 'Mã bảo vệ thẻ (CVV2)';

// Help
$_['help_start_date']			= '(nếu có)';
$_['help_issue']				= '(chỉ đối với thẻ Maestro và Solo)';

// Text
$_['text_result']				= 'Kết quả: ';
$_['text_approval_code']		= 'Mã số phê duyệt: ';
$_['text_reference_number']		= 'Tham khảo: ';
$_['text_card_number_ref']		= '4 chữ số cuối của thẻ : xxxx ';
$_['text_card_brand']			= 'Thẻ nhãn hiệu: ';
$_['text_response_code']		= 'Mã phản hồi: ';
$_['text_fault']				= 'Thông báo lỗi: ';
$_['text_error']				= 'Thông báo lỗi: ';
$_['text_avs']					= 'Xác minh địa chỉ: ';
$_['text_address_ppx']			= 'Không có dữ liệu địa chỉ cung cấp hoặc địa chỉ không được chọn bởi nhà phát hành thẻ';
$_['text_address_yyy']			= 'Nhà phát hành thẻ xác nhận rằng tên đường và mã bưu điện khớp với hồ sơ của họ';
$_['text_address_yna']			= 'Nhà phát hành thẻ xác nhận rằng tên đường phù hợp với hồ sơ của họ nhưng mã bưu điện không phù hợp';
$_['text_address_nyz']			= 'Nhà phát hành thẻ xác nhận rằng mã bưu chính phù hợp với hồ sơ của họ nhưng tên đường không phù hợp';
$_['text_address_nnn']			= 'Tên đường và mã bưu điện không khớp với các hồ sơ của nhà phát hành thẻ';
$_['text_address_ypx']			= 'Nhà phát hành thẻ xác nhận rằng tên đường phù hợp với hồ sơ của họ. Họ không kiểm tra mã bưu chính';
$_['text_address_pyx']			= 'Nhà phát hành thẻ xác nhận mã bưu điện đó phù hợp với hồ sơ của họ. Họ không kiểm tra trên đường';
$_['text_address_xxu']			= 'Nhà phát hành thẻ đã không kiểm tra thông tin AVS';
$_['text_card_code_verify']		= 'Mã bảo vệ: ';
$_['text_card_code_m']			= 'Mã bảo mật thẻ phù hợp';
$_['text_card_code_n']			= 'Mã bảo vệ thẻ không khớp';
$_['text_card_code_p']			= 'Không được xử lý';
$_['text_card_code_s']			= 'Người bán đã cho rằng mã bảo vệ thẻ không có trên thẻ';
$_['text_card_code_u']			= 'Nhà phát hành không được chứng nhận và/hoặc đã không cung cấp khóa mã hóa';
$_['text_card_code_x']			= 'Không nhận được phản hồi từ công ty phát hành thẻ tín dụng';
$_['text_card_code_blank']		= 'Một phản hồi không có thông tin xác nhận rằng không có mã được gửi đi và có dấu hiệu là mã không có trên thẻ.';
$_['text_card_accepted']		= 'Thẻ được chấp nhận: ';
$_['text_card_type_m']			= 'Mastercard';
$_['text_card_type_v']			= 'Visa (Credit/Debit/Electron/Delta)';
$_['text_card_type_c']			= 'Diners';
$_['text_card_type_a']			= 'American Express';
$_['text_card_type_ma']			= 'Maestro';
$_['text_card_new']				= 'Thẻ mới';
$_['text_response_proc_code']	= 'Mã vi xử lý: ';
$_['text_response_ref']			= 'Số REF: ';

// Error
$_['error_card_number']			= 'Vui lòng kiểm tra số thẻ của bạn là hợp lệ';
$_['error_card_name']			= 'Vui lòng kiểm tra tên chủ thẻ là hợp lệ';
$_['error_card_cvv']			= 'Vui lòng kiểm tra mã bảo vệ thẻ CVV2 là hợp lệ';
$_['error_failed']				= 'Không thể xử lý thanh toán của bạn, vui lòng liên hệ với người bán';