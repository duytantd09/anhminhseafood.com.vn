<?php
// Heading
$_['text_title']				= 'Tín dụng / Thẻ ghi nợ (Realex)';

// Button
$_['button_confirm']			= 'Xác nhận';

// Entry
$_['entry_cc_type']				= 'Loại thẻ';

// Text
$_['text_success']				= 'Thanh toán của bạn đã được ủy quyền.';
$_['text_decline']				= 'Thanh toán của bạn đã không thành công';
$_['text_bank_error']			= 'Đã có lỗi khi xử lý yêu cầu của bạn với ngân hàng.';
$_['text_generic_error']		= 'Đã có lỗi khi xử lý yêu cầu của bạn.';
$_['text_hash_failed']			= 'Hàm băm kiểm tra thất bại. Vui lòng không cố gắng thanh toán đơn hàng của bạn một lần nữa và trạng thái thanh toán của đơn hàng sẽ được đặt là không rõ. Vui lòng liên hệ với người bán.';
$_['text_link']					= 'Vui lòng bấm vào <a href="%s">đây</a> để tiếp tục';
$_['text_select_card']			= 'Vui lòng chọn loại thẻ của bạn';
$_['text_result']				= 'Auth kết quả';
$_['text_message']				= 'Tin nhắn';
$_['text_cvn_result']			= 'Kết quả CVN';
$_['text_avs_postcode']			= 'AVS mã bưu chính';
$_['text_avs_address']			= 'AVS địa chỉ';
$_['text_eci']					= 'Kết quả ECI (3D an toàn)';
$_['text_tss']					= 'TSS kết quả';
$_['text_order_ref']			= 'Tham khảo đơn hàng';
$_['text_timestamp']			= 'Dấu thời gian';
$_['text_card_type']			= 'Loại thẻ';
$_['text_card_digits']			= 'Số thẻ';
$_['text_card_exp']				= 'Thẻ hết hạn';
$_['text_card_name']			= 'Tên của thẻ';
$_['text_3d_s1']				= 'Chủ thẻ không đăng ký, trách nhiệm pháp lý thay đổi';
$_['text_3d_s2']				= 'Không thể để xác minh đăng ký, không có sự thay đổi trách nhiệm pháp lý';
$_['text_3d_s3']				= 'Phản hồi không hợp lệ từ Server đăng ký, không có sự thay đổi trách nhiệm';
$_['text_3d_s4']				= 'Đã đăng ký nhưng phản hồi từ ACS là không hợp lệ (Máy chủ điều khiển truy cập), không có sự thay đổi trách nhiệm pháp lý';
$_['text_3d_s5']				= 'Xác thực thành công, trách nhiệm pháp lý thay đổi';
$_['text_3d_s6']				= 'Xác thực nỗ lực thừa nhận, trách nhiệm pháp lý thay đổi';
$_['text_3d_s7']				= 'Mật khẩu nhập không đúng, không có sự thay đổi trách nhiệm pháp lý';
$_['text_3d_s8']				= 'Xác thực không sẵn sàng, không có sự thay đổi trách nhiệm pháp lý';
$_['text_3d_s9']				= 'Không hợp lệ phản ứng từ ACS, không có sự thay đổi trách nhiệm pháp lý';
$_['text_3d_s10']				= 'RealMPI lỗi nghiêm trọng, không có sự thay đổi trách nhiệm pháp lý';
$_['text_card_visa']			= 'Visa';
$_['text_card_mc']				= 'Mastercard';
$_['text_card_amex']			= 'American Express';
$_['text_card_switch']			= 'Chuyển đổi';
$_['text_card_laser']			= 'Laser';
$_['text_card_diners']			= 'Diners';