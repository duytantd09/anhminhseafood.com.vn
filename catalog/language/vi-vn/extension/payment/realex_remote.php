<?php
// Text
$_['text_title']				= 'Thẻ tín dụng hoặc Thẻ ghi nợ';
$_['text_credit_card']			= 'Chi tiết thẻ tín dụng';
$_['text_wait']					= 'Vui lòng đợi!';
$_['text_result']				= 'Kết quả';
$_['text_message']				= 'Tin nhắn';
$_['text_cvn_result']			= 'Kết quả CVN';
$_['text_avs_postcode']			= 'AVS mã bưu chính';
$_['text_avs_address']			= 'AVS địa chỉ';
$_['text_eci']					= 'Kết quả ECI (3D an toàn)';
$_['text_tss']					= 'TSS kết quả';
$_['text_card_bank']			= 'Ngân hàng phát hành thẻ';
$_['text_card_country']			= 'Tỉnh/Thành Phố phát hành thẻ';
$_['text_card_region']			= 'Vùng phát hành thẻ';
$_['text_last_digits']			= '4 chữ số cuối';
$_['text_order_ref']			= 'Tham khảo đơn hàng';
$_['text_timestamp']			= 'Dấu thời gian';
$_['text_card_visa']			= 'Visa';
$_['text_card_mc']				= 'Mastercard';
$_['text_card_amex']			= 'American Express';
$_['text_card_switch']			= 'Chuyển đổi';
$_['text_card_laser']			= 'Laser';
$_['text_card_diners']			= 'Diners';
$_['text_auth_code']			= 'Auth code';
$_['text_3d_s1']				= 'Chủ thẻ không đăng ký, trách nhiệm pháp lý thay đổi';
$_['text_3d_s2']				= 'Không thể xác minh đăng ký, không có sự thay đổi trách nhiệm pháp lý';
$_['text_3d_s3']				= 'Phản hồi không hợp lệ từ Server đăng ký, không có sự thay đổi trách nhiệm';
$_['text_3d_s4']				= 'Đã đăng ký nhưng phản hồi từ ACS là không hợp lệ (Máy chủ điều khiển truy cập), không có sự thay đổi trách nhiệm pháp lý';
$_['text_3d_s5']				= 'Xác thực thành công, trách nhiệm pháp lý thay đổi';
$_['text_3d_s6']				= 'Xác thực nỗ lực thừa nhận, trách nhiệm pháp lý thay đổi';
$_['text_3d_s7']				= 'Không đúng mật khẩu nhập, không có sự thay đổi trách nhiệm pháp lý';
$_['text_3d_s8']				= 'Xác thực không sẵn sàng, không có sự thay đổi trách nhiệm pháp lý';
$_['text_3d_s9']				= 'Không hợp lệ phản ứng từ ACS, không có sự thay đổi trách nhiệm pháp lý';
$_['text_3d_s10']				= 'RealMPI lỗi nghiêm trọng, không có sự thay đổi trách nhiệm pháp lý';

// Entry
$_['entry_cc_type']				= 'Loại thẻ';
$_['entry_cc_number']			= 'Số thẻ';
$_['entry_cc_name']				= 'Tên chủ thẻ';
$_['entry_cc_expire_date']		= 'Ngày hết hạn thẻ';
$_['entry_cc_cvv2']				= 'Mã bảo vệ thẻ (CVV2)';
$_['entry_cc_issue']			= 'Vấn đề số thẻ';

// Help
$_['help_start_date']			= '(nếu có)';
$_['help_issue']				= '(chỉ đối với thẻ Maestro và Solo)';

// Error
$_['error_card_number']			= 'Vui lòng kiểm tra số thẻ của bạn là hợp lệ';
$_['error_card_name']			= 'Vui lòng kiểm tra tên chủ thẻ là hợp lệ';
$_['error_card_cvv']			= 'Vui lòng kiểm tra mã bảo vệ thẻ CVV2 là hợp lệ';
$_['error_3d_unable']			= 'Người bán đòi hỏi kết nối 3D an toàn nhưng không thể xác minh với ngân hàng của bạn, xin vui lòng thử lại sau';
$_['error_3d_500_response_no_payment'] = 'Một phản hồi không hợp lệ nhận được từ bộ vi xử lý thẻ, không thực hiện thanh toán được';
$_['error_3d_unsuccessful']		= 'xác thực kết nối an toàn 3D bị thất bại';