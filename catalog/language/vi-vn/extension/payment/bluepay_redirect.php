<?php
// Text
$_['text_title']				= 'Thẻ tín dụng / Thẻ ghi nợ (BluePay)';
$_['text_credit_card']			= 'Chi tiết thẻ';
$_['text_description']			= 'Sản phẩm trên %s đơn hàng số: %s';
$_['text_card_type']			= 'Loại thẻ: ';
$_['text_card_name']			= 'Tên chủ thẻ: ';
$_['text_card_digits']			= 'Chữ số cuối cùng: ';
$_['text_card_expiry']			= 'Hết hạn: ';

// Returned text
$_['text_transaction_error']	= 'Đã có một lỗi xử lý giao dịch của bạn - ';

// Entry
$_['entry_card']				= 'Thẻ mới hoặc hiện tại: ';
$_['entry_card_existing']		= 'Sẵn có';
$_['entry_card_new']			= 'Mới';
$_['entry_card_save']			= 'Nhớ chi tiết thẻ';
$_['entry_cc_owner']			= 'Chủ sở hữu thẻ';
$_['entry_cc_number']			= 'Số thẻ';
$_['entry_cc_start_date']		= 'Hiệu lực thẻ từ ngày';
$_['entry_cc_expire_date']		= 'Ngày hết hạn thẻ';
$_['entry_cc_cvv2']				= 'Mã bảo vệ thẻ (CVV2)';
$_['entry_cc_address']			= 'Địa chỉ đường phố';
$_['entry_cc_city']				= 'Thành phố';
$_['entry_cc_state']			= 'Tỉnh/thành phố';
$_['entry_cc_zipcode']			= 'Zipcode';
$_['entry_cc_phone']			= 'Điện thoại';
$_['entry_cc_email']			= 'Email';
$_['entry_cc_choice']			= 'Chọn một thẻ hiện có';