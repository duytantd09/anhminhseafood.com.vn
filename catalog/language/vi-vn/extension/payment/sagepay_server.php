<?php
// Text
$_['text_title']				= 'Thẻ tín dụng / Thẻ ghi nợ (SagePay)';
$_['text_credit_card']			= 'Chi tiết thẻ';
$_['text_description']			= 'Sản phẩm trên %s đơn hàng số: %s';
$_['text_card_type']			= 'Loại thẻ: ';
$_['text_card_name']			= 'Tên thẻ: ';
$_['text_card_digits']			= 'Chữ số cuối cùng: ';
$_['text_card_expiry']			= 'Hết hạn: ';
$_['text_trial']				= '%s mỗi %s %s cho %s khoản thanh toán sau đó ';
$_['text_recurring']			= '%s mỗi %s %s';
$_['text_length']				= ' %s khoản thanh toán';
$_['text_success']				= 'Thanh toán của bạn đã được ủy quyền.';
$_['text_decline']				= 'Thanh toán của bạn đã bị từ chối.';
$_['text_bank_error']			= 'Đã có lỗi khi xử lý yêu cầu của bạn với ngân hàng.';
$_['text_transaction_error']	= 'Đã có một lỗi khi xử lý giao dịch của bạn.';
$_['text_generic_error']		= 'Đã có lỗi khi xử lý yêu cầu của bạn.';
$_['text_hash_failed']			= 'Hàm băm kiểm tra thất bại. Vui lòng không cố gắng thanh toán đơn hàng của bạn một lần nữa và trạng thái thanh toán của đơn hàng sẽ được đặt là không rõ. Vui lòng liên hệ với người bán.';
$_['text_link']					= 'Vui lòng bấm vào <a href="%s">đây</a> để tiếp tục';

// Entry
$_['entry_card']				= 'Thẻ mới hoặc hiện tại: ';
$_['entry_card_existing']		= 'Sẵn có';
$_['entry_card_new']			= 'Mới';
$_['entry_card_save']			= 'Nhớ chi tiết thẻ để sử dụng trong tương lai';
$_['entry_cc_choice']			= 'Chọn một thẻ hiện có';