<?php
// Heading
$_['heading_title']				= 'Thanh toán Amazon';
$_['heading_address']			= 'Hãy chọn một địa chỉ giao hàng';
$_['heading_payment']			= 'Vui lòng chọn phương thức thanh toán';
$_['heading_confirm']			= 'Tóm tắt đơn hàng';

// Text
$_['text_back']					= 'Quay lại';
$_['text_cart']					= 'Giỏ hàng';
$_['text_confirm']				= 'Xác nhận';
$_['text_continue']				= 'Tiếp tục';
$_['text_cba']					= 'Thanh toán Amazon';
$_['text_enter_coupon']			= 'Nhập mã phiếu giảm giá của bạn ở đây. Nếu bạn không có mã phiếu giảm giá, hãy để trống.';
$_['text_coupon']				= 'Phiếu giảm giá';
$_['text_tax_other']			= 'Thuế / Phí xử lý khác';
$_['text_payment_failed']		= 'Thanh toán của bạn đã thất bại. Xin vui lòng liên hệ với người quản trị để được hỗ trợ hoặc sử dụng phương thức thanh toán khác.';
$_['text_success_title']		= 'Đơn hàng của bạn đã được đặt!';
$_['text_payment_success']		= 'Đơn hàng của bạn được đặt thành công. Chi tiết đơn hàng như dưới đây';

// Error
$_['error_payment_method']		= 'Xin vui lòng chọn phương thức thanh toán';
$_['error_shipping']			= 'Vui lòng chọn một phương thức vận chuyển';
$_['error_shipping_address']	= 'Vui lòng chọn một địa chỉ giao hàng';
$_['error_shipping_methods']	= 'Đã có lỗi khi lấy địa chỉ của bạn từ Amazon. Xin vui lòng liên hệ với người quản lý cửa hàng để được giúp đỡ';
$_['error_no_shipping_methods'] = 'Không có tùy chọn giao hàng đến địa chỉ đã chọn. Vui lòng chọn một địa chỉ nhận hàng khác';