<?php
// Text
$_['text_title']				= 'Thẻ tín dụng hoặc Thẻ ghi nợ (Processed securely by Perpetual Payments)';
$_['text_credit_card']			= 'Chi tiết thẻ tín dụng';
$_['text_transaction']			= 'Mã giao dịch:';
$_['text_avs']					= 'AVS/CVV:';
$_['text_avs_full_match']		= 'Phù hợp đầy đủ';
$_['text_avs_not_match']		= 'Không phù hợp';
$_['text_authorisation']		= 'Mã xác nhận:';

// Entry
$_['entry_cc_number']			= 'Số thẻ';
$_['entry_cc_start_date']		= 'Hiệu lực thẻ từ ngày';
$_['entry_cc_expire_date']		= 'Ngày hết hạn thẻ';
$_['entry_cc_cvv2']				= 'Mã bảo vệ thẻ (CVV2)';
$_['entry_cc_issue']			= 'Số thẻ lỗi';

// Help
$_['help_start_date']			= '(nếu có)';
$_['help_issue']				= '(chỉ đối với thẻ Maestro và Solo)';