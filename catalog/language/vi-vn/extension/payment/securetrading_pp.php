<?php

$_['text_title'] = 'Thẻ tín dụng / Thẻ ghi nợ';
$_['button_confirm'] = 'Xác nhận';

$_['text_postcode_check'] = 'Kiểm tra Mã bưu chính: %s';
$_['text_security_code_check'] = 'Kiểm tra  mã bảo mật CVV2: %s';
$_['text_address_check'] = 'Kiểm tra Địa chỉ: %s';
$_['text_not_given'] = 'Chưa có';
$_['text_not_checked'] = 'Chưa được kiểm tra';
$_['text_match'] = 'Phù hợp';
$_['text_not_match'] = 'Không phù hợp';
$_['text_payment_details'] = 'Chi tiết thanh toán';

$_['entry_card_type'] = 'Loại thẻ';