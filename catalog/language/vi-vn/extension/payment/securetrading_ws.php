<?php

$_['text_title'] = 'Thẻ tín dụng / Thẻ ghi nợ';
$_['text_card_details'] = 'Chi tiết thẻ';
$_['text_wait'] = 'Xử lý thanh toán của bạn';
$_['text_auth_code'] = 'Mã ủy quyền : %s';
$_['text_postcode_check'] = 'Kiểm tra Mã bưu chính: %s';
$_['text_security_code_check'] = 'Kiểm tra CVV2: %s';
$_['text_address_check'] = 'Kiểm tra Địa chỉ: %s';
$_['text_3d_secure_check'] = '3D an toàn: %s';
$_['text_not_given'] = 'Chưa có';
$_['text_not_checked'] = 'Chưa được kiểm tra';
$_['text_match'] = 'Phù hợp';
$_['text_not_match'] = 'Không phù hợp';
$_['text_authenticated'] = 'Xác thực';
$_['text_not_authenticated'] = 'Không được xác thực';
$_['text_authentication_not_completed'] = 'Đã cố gắng nhưng không hoàn thành';
$_['text_unable_to_perform'] = 'Không thể thực hiện';
$_['text_transaction_declined'] = 'Ngân hàng của bạn đã từ chối giao dịch. Vui lòng sử dụng một phương thức thanh toán khác.';
$_['text_transaction_failed'] = 'Không thể xử lý các khoản thanh toán. Xin vui lòng kiểm tra các chi tiết bạn cung cấp.';
$_['text_connection_error'] = 'Vui lòng thử lại sau hoặc sử dụng một phương thức thanh toán khác.';

$_['entry_type'] = "Loại thẻ";
$_['entry_number'] = "Số thẻ";
$_['entry_expire_date'] = "Ngày hết hạn";
$_['entry_cvv2'] = "Mã bảo vệ (CVV2)";

$_['button_confirm'] = 'Xác nhận';

$_['error_failure'] = 'Không thể hoàn tất giao dịch. Xin vui lòng thử lại sau hoặc sử dụng một phương thức thanh toán khác.';