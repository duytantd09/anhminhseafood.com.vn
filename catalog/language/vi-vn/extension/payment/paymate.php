<?php
// Text
$_['text_title']				= 'Thẻ tín dụng / Thẻ ghi nợ (Paymate)';
$_['text_unable']				= 'Không thể xác định vị trí hoặc cập nhật trạng thái đơn đặt hàng của bạn';
$_['text_declined']				= 'Thanh toán bị từ chối bởi Paymate';
$_['text_failed']				= 'Giao dịch Paymate không thành công';
$_['text_failed_message']		= '<p>Đã có lỗi khi xử lý của giao dịch Paymate của bạn.</p><p><b>Cảnh báo:</b>%s</p><p>Vui lòng xác nhận số dư tài khoản Paymate của bạn trước khi xử lý lại đơn đặt hàng này</p><p>Nếu bạn tin rằng giao dịch này đã hoàn tất thành công, hoặc hiển thị như là một khoản khấu trừ vào tài khoản Paymate của bạn, xin vui lòng <a href="%s">Liên hệ</a> với chi tiết đơn đặt hàng của bạn.</p>';
$_['text_basket']				= 'Giỏ hàng';
$_['text_checkout']				= 'Thanh toán';
$_['text_success']				= 'Thành công';