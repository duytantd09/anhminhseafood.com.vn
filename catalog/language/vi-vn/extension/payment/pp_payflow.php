<?php
// Text
$_['text_title']				= 'Thẻ tín dụng hoặc Thẻ ghi nợ (Processed securely by PayPal)';
$_['text_credit_card']			= 'Chi tiết thẻ tín dụng';
$_['text_start_date']			= '(nếu có)';
$_['text_issue']				= '(chỉ đối với thẻ Maestro và Solo)';
$_['text_wait']					= 'Vui lòng đợi!';

// Entry
$_['entry_cc_owner']			= 'Chủ sở hữu thẻ:';
$_['entry_cc_type']				= 'Loại thẻ:';
$_['entry_cc_number']			= 'Số thẻ:';
$_['entry_cc_start_date']		= 'Hiệu lực thẻ từ ngày:';
$_['entry_cc_expire_date']		= 'Ngày hết hạn thẻ:';
$_['entry_cc_cvv2']				= 'Mã bảo vệ thẻ (CVV2):';
$_['entry_cc_issue']			= 'Số thẻ lỗi:';

// Error
$_['error_required']			= 'Chú ý: Tất cả các thông tin thanh toán là bắt buộc nhập.';
$_['error_general']				= 'Chú ý: Một vấn đề đã xảy ra với giao dịch. Xin vui lòng thử lại.';
$_['error_config']				= 'Chú ý: Lỗi cấu hình mô-đun thanh toán. Hãy kiểm tra lại thông tin đăng nhập.';
$_['error_address']				= 'Chú ý: Thông tin địa chỉ thành phố, tiểu bang và mã bưu điện trong mục thanh toán không khớp. Vui lòng thử lại.';
$_['error_declined']			= 'Cảnh báo: Giao dịch này đã bị từ chối. Vui lòng thử lại.';
$_['error_invalid']				= 'Chú ý: Các thông tin thẻ tín dụng không hợp lệ. Vui lòng thử lại.';