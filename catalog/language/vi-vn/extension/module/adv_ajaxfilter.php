<?php
// Heading 
$_['heading_title']      = 'Bộ Lọc';
$_['text_tax']           = 'Thuế Ex';
$_['text_price_range']   = 'Giá';
$_['text_range']   = 'Phạm vi: ';
$_['text_manufacturers'] = 'Nhãn hiệu';
$_['text_tags'] 		 = 'Tag';
$_['text_categories']    = 'Danh mục';

$_['text_all'] 			 = 'Tất cả';
$_['clear_filter']       = 'Thiết lập lại bộ lọc';

$_['pds_model']      = 'Model:';
$_['pds_sku']        = 'SKU:';
$_['pds_brand']      = 'Nhãn hiệu:';
$_['pds_upc']        = 'UPC:';
$_['pds_location']   = 'Vị trí:';
$_['pds_stock']      = 'Sẵn có:';
$_['pds_weight']     = 'Cân nặng:';

$_['text_instock']          = 'Còn hàng';