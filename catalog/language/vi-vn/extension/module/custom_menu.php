<?php
$_['heading_title'] = 'Menus Tùy Chỉnh';

$_['text_edit']	= 'Chỉnh sửa Menus';
$_['text_success']     = 'Thành công: Bạn đã thay đổi Menus!';
$_['text_module']     = 'Các Mô-đun!';

$_['entry_name']	= 'Tên Mô-đun';
$_['entry_status']	= 'Trạng thái';
$_['entry_menu_parent'] = 'Menus Cha';
$_['entry_menu_children']	 = 'Menus Con';
$_['entry_sort']	 = 'Sắp xếp';
$_['entry_category']	 = 'Danh Mục';
$_['entry_footer']		 = 'Dưới cùng';

$_['button_menu_add'] = 'Tạo Menus mới';
