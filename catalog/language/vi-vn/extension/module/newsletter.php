<?php
$_['heading_title'] = 'Nhận bản tin';
$_['text_email'] = 'Nhập địa chỉ email của bạn';
$_['text_subscribe'] = 'Bạn đã đăng ký thành công!';
$_['text_unsubscribe'] = 'Bạn đã huỷ đăng ký thành công!';
$_['text_message'] = '<p>Bạn đã huỷ đăng ký thành công từ bản tin của chúng tôi.</p>';
$_['entry_email'] = 'Nhận ưu đãi độc quyền bạn sẽ không tìm thấy bất cứ nơi nào khác thẳng vào hộp thư đến của bạn!';
$_['button_join'] = 'Theo dõi / Hủy đăng ký';
$_['error_email'] = 'Vui lòng nhập email hợp lệ!';
$_['error_unsubscribe'] = '<p>Chúng tôi không thể bỏ đăng ký bạn từ bản tin của chúng tôi. Hãy xác minh rằng bạn có URL đúng vào.</p>';
?>