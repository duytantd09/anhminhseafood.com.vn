<?php
// Heading
$_['heading_title'] = 'Sử dụng phiếu giảm giá';

// Text
$_['text_coupon'] = 'Phiếu giảm giá (%s)';
$_['text_success']  = 'Thành công: Phiếu giảm giá của bạn đã được áp dụng!';

// Entry
$_['entry_coupon']  = 'Nhập phiếu giảm giá của bạn ở đây';

// Error
$_['error_coupon']  = 'Cảnh báo: Phiếu thưởng không hợp lệ, đã hết hạn hoặc đã đạt đến giới hạn sử dụng!';
$_['error_empty']   = 'Cảnh báo: Vui lòng nhập mã phiếu giảm giá!';
