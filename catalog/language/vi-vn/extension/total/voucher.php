<?php

// Heading
$_['heading_title'] = 'Sử dụng phiếu quà tặng ';

// Text
$_['text_voucher'] = 'Phiếu quà tặng (%s)';
$_['text_success']  = 'Thành công: Phiếu quà tặng của bạn đã được áp dụng!';

// Entry
$_['entry_voucher'] = 'Nhập phiếu quà tặng của bạn ở đây';

// Error
$_['error_voucher'] = 'Cảnh báo: Thẻ quà tặng không hợp lệ hoặc số dư đã được sử dụng hết!';
$_['error_empty']   = 'Cảnh báo: Vui lòng nhập mã phiếu quà tặng!';
