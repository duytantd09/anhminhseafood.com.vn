<?php
// Heading 
$_['heading_title']                  = 'Thanh toán';

// Text
$_['text_cart']                      = 'Giỏ hàng';
$_['text_checkout_option']           = 'Đăng nhập';
$_['text_already_have_account']      = 'Bạn đã có tài khoản? Đăng nhập';
$_['text_checkout_account']          = 'Tài khoản và Chi tiết thanh toán';
$_['text_checkout_payment_address']  = 'Chi tiết thanh toán';
$_['text_checkout_shipping_address'] = 'Thông tin giao hàng';
$_['text_checkout_shipping_method']  = 'Phương thức giao hàng';
$_['text_checkout_payment_method']   = 'Phương thức thanh toán';
$_['text_checkout_confirm']          = 'Xác nhận đơn hơn';
$_['text_new_customer']              = 'Khách hàng mới';
$_['text_returning_customer']        = 'Phản hồi khách hàng';
$_['text_checkout']                  = 'Tùy chọn thanh toán:';
$_['text_i_am_returning_customer']   = 'Bạn là một khách hàng của chúng tôi';
$_['text_register']                  = 'Đăn kí tài khoản';
$_['text_guest']                     = 'Khách thanh toán';
$_['text_register_account']          = 'Bằng cách tạo một tài khoản bạn sẽ có thể mua sắm nhanh hơn, cập nhật về tình trạng của đơn hàng , và theo dõi các đơn hàng bạn đã thực hiện trước đó.';
$_['text_forgotten']                 = 'Quên mật khẩu';
$_['text_your_details']              = 'Thông tin cá nhân của bạn';
$_['text_your_address']              = 'Địa chỉ';
$_['text_your_password']             = 'Mật khẩu';
$_['text_agree']                     = 'Tôi đã đọc và đồng ý với <a class="agree" href="%s" alt="%s"><b>%s</b></a>';
$_['text_address_new']               = 'Tôi muốn sử dụng một địa chỉ mới';
$_['text_address_existing']          = 'Tôi muốn sử dụng một địa chỉ hiện tại';
$_['text_shipping_method']           = 'Vui lòng chọn phương thức giao hàng ưa thích sử dụng đơn đặt hàng này.';
$_['text_payment_method']            = 'Vui lòng chọn phương thức thanh toán ưa thích để sử dụng trên đơn hàng này.';
$_['text_comments']                  = 'Thêm bình luận về đơn hàng của bạn';
$_['text_create_account']			 = 'Cũng tạo ra một tài khoản để sử dụng trong tương lai.';
$_['text_or']						 = '-- hoặc --';
$_['text_please_wait']				 = 'Xin vui lòng chờ...';
$_['text_coupon']        		     = 'Thành công: Phiếu giảm giá của bạn đã được áp dụng!';
$_['text_voucher']        		     = 'Thành công: Giảm giá phiếu quà tặng của bạn đã được áp dụng!';
$_['text_reward']         		     = 'Thành công: bạn giảm điểm thưởng đã được áp dụng!';
$_['text_use_coupon']      		     = 'Mã Sử dụng Phiếu giảm giá';
$_['text_use_voucher']    		     = 'Sử dụng Gift Voucher';
$_['text_use_reward']    		     = 'Sử dụng điểm thưởng';
$_['text_estimated_delivery']		 = 'Giao ước tính:';
$_['text_delivery']					 = 'Ngày giao hàng:';
$_['text_points'] 					 = 'Điểm thưởng: %s';

// Column
$_['column_name']                    = 'Sản phẩm';
$_['column_model']                   = 'Model';
$_['column_quantity']                = 'Số lượng';
$_['column_price']                   = 'Giá';
$_['column_total']                   = 'Tổng';

// Entry
$_['entry_email_address']            = 'Địa chỉ E-Mail';
$_['entry_email']                    = 'E-Mail:';
$_['entry_password']                 = 'Mật khẩu:';
$_['entry_confirm']                  = 'Xác nhận:';
$_['entry_firstname']                = 'Họ:';
$_['entry_lastname']                 = 'Tên:';
$_['entry_telephone']                = 'Điện thoại:';
$_['entry_fax']                      = 'Fax:';
$_['entry_customer_group']           = 'Nhóm khách hàng:';
$_['entry_company']                  = 'Công ty:';
$_['entry_address_1']                = 'Địa chỉ 1:';
$_['entry_address_2']                = 'Địa chỉ 2:';
$_['entry_postcode']                 = 'Mã Code:';
$_['entry_city']                     = 'Phường/Xã';
$_['entry_country']                  = 'Tỉnh/Thành Phố:';
$_['entry_zone']                     = 'Quận/Huyện::';
$_['entry_newsletter']               = 'Tôi muốn đăng ký vào các %s bản tin.';
$_['entry_shipping'] 	             = 'Địa chỉ giao hàng và thanh toán của tôi đều giống nhau.';
$_['entry_coupon']        		     = 'Tôi có một mã số phiếu giảm giá';
$_['entry_voucher']      		     = 'Tôi có một mã Voucher';
$_['entry_reward']          		 = 'Tôi muốn sử dụng điểm thưởng (Tối đa %s, có sẵn %s):';

// Error
$_['error_warning']                  = 'Có một vấn đề trong khi cố gắng để xử lý đơn đặt hàng ! Nếu vấn đề vẫn còn hãy thử chọn một phương thức thanh toán khác nhau hoặc bạn có thể liên hệ với chủ cửa hàng bởi <a href="%s">nhấn vào đây</a>.';
$_['error_login']                    = 'Cảnh báo:Địa chỉ E-Mail / hoặc mật khẩu không phù hợp.';
$_['error_attempts']                 = 'Cảnh báo: Tài khoản của bạn đã vượt quá số lượng cho phép của nỗ lực đăng nhập. Vui lòng thử lại trong 1 giờ.';
$_['error_approved']                 = 'Cảnh báo: Tài khoản của bạn đòi hỏi phải có sự chấp thuận trước khi bạn có thể đăng nhập.'; 
$_['error_exists']                   = 'Cảnh báo: E-Mail Địa chỉ đã được đăng ký!';
$_['error_firstname']                = 'Tên phải có từ 1 đến 32 ký tự!';
$_['error_lastname']                 = 'Họ phải có từ 1 đến 32 ký tự!';
$_['error_email']                    = 'E-Mail Địa chỉ không hợp lệ!';
$_['error_telephone']                = 'Điện thoại phải có từ 3 đến 32 ký tự!';
$_['error_fax']       		         = 'Fax phải có từ 3 đến 32 ký tự!';
$_['error_company']    		         = 'Công ty phải có từ 3 đến 32 ký tự!';
$_['error_password']                 = 'Mật khẩu phải từ 3 đến 20 ký tự!';
$_['error_confirm']                  = 'Mật khẩu xác nhận không khớp mật khẩu!';
$_['error_address_1']                = 'Địa chỉ 1 phải có từ 3 đến 128 ký tự!';
$_['error_address_2']                = 'Địa chỉ 2 phải có từ 3 đến 128 ký tự!';
$_['error_city']                     = 'Thành phố phải có từ 2 và 128 ký tự!';
$_['error_postcode']                 = 'Mã code phải có từ 2 đến 10 ký tự!';
$_['error_country']                  = 'Vui lòng chọn một quốc gia!';
$_['error_zone']                     = 'Vui lòng chọn một tỉnh / tp!';
$_['error_agree']                    = 'Cảnh báo: Bạn phải đồng ý với %s!';
$_['error_address']                  = 'Cảnh báo: Bạn phải chọn địa chỉ!';
$_['error_shipping']                 = 'Cảnh báo: Phương thức giao hàng yêu cầu!';
$_['error_no_shipping']              = 'Cảnh báo: Không có tùy chọn có sẵn Vận Chuyển. Vui lòng <a href="%s"> liên hệ với chúng tôi </a> để được hỗ trợ!';
$_['error_payment']                  = 'Cảnh báo: Phương thức thanh toán yêu cầu!';
$_['error_no_payment']               = 'Cảnh báo: Không có tùy chọn thanh toán có sẵn. Vui lòng <a href="%s">liên hệ với chúng tôi</a> để được hỗ trợ!';
$_['error_coupon']       		     = 'Cảnh báo: Phiếu giảm giá là không hợp lệ, hết hạn hoặc đạt nó không phải là giới hạn sử dụng!';
$_['error_voucher']      		     = 'Cảnh báo: Gift Voucher là không hợp lệ hoặc sự cân bằng đã được sử dụng hết!';
$_['error_survey']					 = 'Cảnh báo: câu hỏi khảo sát được yêu cầu để được trả lời!';
$_['error_delivery']				 = 'Cảnh báo: Ngày giao hàng được yêu cầu!';
$_['error_maximum']      		     = 'Cảnh báo: số điểm tối đa có thể được áp dụng là %s!';
$_['error_reward']        		     = 'Cảnh báo: Vui lòng nhập số tiền của các điểm thưởng để sử dụng!';	
$_['error_points']           		 = 'Cảnh báo: Bạn không có %s điểm thưởng!';
$_['error_custom_field']             = '%s cần thiết!';