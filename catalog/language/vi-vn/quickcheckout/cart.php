<?php
$_['text_cart'] = 'Giỏ hàng';
$_['text_image'] = 'Hình ảnh';
$_['text_name'] = 'Sản phẩm';
$_['text_quantity'] = 'Số lượng';
$_['text_price'] = 'Giá';
$_['text_total'] = 'Tổng cộng';
$_['text_recurring_item'] = 'Định kỳ';
$_['text_payment_recurring'] = 'Thông tin thanh toán';
$_['text_trial_description'] = '%s mỗi %d %s(s) cho %d thanh toán(s) sau đó';
$_['text_payment_description'] = '%s mỗi %d %s(s) cho %d thanh toán(s)';
$_['text_payment_cancel'] = '%s mỗi %d %s(s) cho đến khi bị hủy bỏ';
$_['text_day'] = 'ngày';
$_['text_week'] = 'tuần';
$_['text_semi_month'] = 'nửa tháng';
$_['text_month'] = 'tháng';
$_['text_year'] = 'năm';
?>