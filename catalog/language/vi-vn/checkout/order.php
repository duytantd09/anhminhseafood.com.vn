<?php
$_['heading_title'] = 'Kiểm tra đơn hàng';

$_['entry_email']   = 'Email';
$_['entry_order_id']   = '#Mã đơn hàng';

$_['button_check']  = 'Kiểm tra';

$_['error_email']              = 'Địa chỉ E-mail không có vẻ hợp lệ!';
$_['error_order_id']           = 'Vui lòng kiểm tra đơn hàng của bạn!';
$_['error_order']            = 'Đơn hàng mà bạn yêu cầu không thể tìm thấy!';