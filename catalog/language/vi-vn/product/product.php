<?php
// Text
$_['text_search']                             = 'Tìm Kiếm
';
$_['text_brand']                              = 'Thương hiệu';
$_['text_manufacturer']                       = 'Thương hiệu:';
$_['text_model']                              = 'Mã sản phẩm:';
$_['text_reward']                             = 'Điểm thưởng:';
$_['text_points']                             = 'Giá ở điểm thưởng:';
$_['text_stock']                              = 'Trạng thái:';
$_['text_instock']                            = 'Có hàng';
$_['text_tax']                                = 'Trước thuế:';
$_['text_discount']                           = ' hoặc nhiều hơn ';
$_['text_option']                             = 'Tùy chọn có sẵn';
$_['text_minimum']                            = 'Số lượng tối thiểu của sản phẩm này là %s';
$_['text_reviews']                            = '%s đánh giá';
$_['text_write']                              = 'Viết đánh giá';
$_['text_login']                              = 'Vui lòng <a href="%s">đăng nhập</a> hoặc <a href="%s">đăng ký</a> để đánh giá';
$_['text_no_reviews']                         = 'Không có đánh giá cho sản phẩm này.';
$_['text_note']                               = '<span class="text-danger">Lưu ý:</span> Không hỗ trợ HTML!';
$_['text_success']                            = 'Cảm ơn nhận xét của bạn. Nhận xét này đã được gửi cho quản trị trang web để phê duyệt.';
$_['text_related']                            = 'Sản Phẩm Cùng Loại';
$_['text_tags']                               = 'Thẻ:';
$_['text_error']                              = 'Không tìm thấy sản phẩm!';
$_['text_payment_recurring']                    = 'Hồ Sơ Thanh Toán';
$_['text_trial_description']                  = '%s mỗi %d %s cho từng %d sau đó';
$_['text_payment_description']                = '%s mỗi %d %s cho từng %d';
$_['text_payment_until_canceled_description'] = '%s mỗi %d %s cho đến khi hủy bỏ';
$_['text_day']                                = 'ngày';
$_['text_week']                               = 'tuần';
$_['text_semi_month']                         = 'nửa tháng';
$_['text_month']                              = 'tháng';
$_['text_year']                               = 'năm';
$_['text_trogiup']                              = 'Trợ giúp';
$_['text_cuahang']                               = 'Cửa hàng';

// Entry
$_['entry_qty']                               = 'Số lượng';
$_['entry_name']                              = 'Tên bạn';
$_['entry_review']                            = 'Nhận xét của bạn';
$_['entry_rating']                            = 'Đánh giá';
$_['entry_good']                              = 'Tốt';
$_['entry_bad']                               = 'Không tốt';
$_['entry_captcha']                           = 'Nhập mã xác nhận dưới đây';

// Tabs
$_['tab_description']                         = 'Mô tả';
$_['tab_description_2']          = 'Tính năng';
$_['tab_description_3']          = 'Thông số kỹ thuật';
$_['tab_description_4']          = 'Quà tặng';
$_['tab_attribute']                           = 'Đặc điểm kỹ thuật';
$_['tab_review']                              = 'Đánh giá (%s)';

// Error
$_['error_name']                              = 'Chú ý: Tên phải từ 3 đến 25 ký tự!';
$_['error_text']                              = 'Chú ý: Văn bản nhận xét phải từ 25 đến 1000 ký tự!';
$_['error_rating']                            = 'Chú ý: Vui lòng chọn một đánh giá nhận xét!';
$_['error_captcha']                           = 'Chú ý: Mã xác nhận không chính xác!';