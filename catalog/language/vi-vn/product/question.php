<?php
// Heading
$_['heading_title']         = 'FAQ';
// Button
$_['button_ask']     		= 'Hỏi về sản phẩm này
';

// Tab
$_['tab_questions']     	= 'Câu hỏi về sản phẩm';

// Text
$_['text_no_questions']     = 'Không có câu hỏi nào về sản phẩm này.';
$_['text_question_from']    = 'Câu hỏi từ';
$_['text_no_answer']        = 'Câu hỏi vẫn chưa được trả lời';
$_['text_our_answer']       = 'Câu trả lời của chúng tôi:';

// Button
$_['button_send']       	= 'Gửi';

// Form
$_['heading_ask']     		= 'Hỏi về sản phẩm này';
$_['entry_name']     	    = 'Tên';
$_['entry_email']     		= 'E-mail của bạn&nbsp;&nbsp;<i style="color:#999999">(Sẽ được bảo mật)</i>';
$_['entry_question']     	= 'Câu hỏi của bạn';
$_['entry_private']     	= 'Tôi muốn câu hỏi này được bảo mật';
$_['entry_captcha']     	= 'Nhập mã trong ô bên dưới';

// Error & Success alerts
$_['error_name']            = 'Tên bạn phải từ 2 đến 30 kí tự';
$_['error_email']           = 'Emai của bạn phải từ 2 đến 60 ký tự';
$_['error_text']            = 'Câu hỏi của bạn phải từ 10 đến 1000 ký tự';
$_['error_captcha']         = 'Mã xác minh không khớp với hình ảnh';
$_['text_success']          = 'Cảm ơn bạn! Chúng tôi sẽ trả lời sớm nhất có thể';

// Email notification to store owner
$_['text_subject']			= '%s - Câu hỏi về sản phẩm';
$_['text_waiting']			= 'Bạn có câu hỏi mới đang chờ được trả lời';
$_['text_product']			= 'Sản phẩm: %s';
$_['text_author']			= 'Người gửi: %s';
$_['text_question']			= 'Câu hỏi:';