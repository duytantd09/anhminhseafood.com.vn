<?php
// Heading
$_['heading_title']     = 'Sản phẩm mới';

// Text
$_['text_empty']        = 'Không có sản phẩm mới trong danh sách.';
$_['text_quantity']     = 'Qty:';
$_['text_manufacturer'] = 'Thương hiệu:';
$_['text_model']        = 'Mã sản phẩm:';
$_['text_points']       = 'Điểm thưởng:';
$_['text_price']        = 'Giá:';
$_['text_tax']          = 'Thuế:';
$_['text_compare']      = 'So sánh sản phẩm (%s)';
$_['text_sort']         = 'Sắp xếp:';
$_['text_default']      = 'Mặc định';
$_['text_name_asc']     = 'Tên (A - Z)';
$_['text_name_desc']    = 'Tên (Z - A)';
$_['text_price_asc']    = 'Giá (Low &gt; High)';
$_['text_price_desc']   = 'Giá (High &gt; Low)';
$_['text_rating_asc']   = 'Xếp hạng (Lowest)';
$_['text_rating_desc']  = 'Xếp hạng (Highest)';
$_['text_model_asc']    = 'Mẫu (A - Z)';
$_['text_model_desc']   = 'Mẫu (Z - A)';
$_['text_limit']        = 'Hiển thị:';