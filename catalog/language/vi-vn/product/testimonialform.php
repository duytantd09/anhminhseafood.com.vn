<?php
// Heading
$_['heading_title']  	= 'Viết lời chứng thực';
$_['sent_testimonial'] 	= 'Cảm ơn!';

// Text 
$_['text_message']   	= '<p>Cảm ơn! Lời chứng thực của bạn đã được gửi</p>';
$_['text_note']      	= '<span style="color: #FF0000;">Note:</span> HTML is not translated!';
$_['text_average']      = 'Đánh giá trung bình:';
$_['text_stars']        = '%s out of 5 Stars!';
$_['text_conditions'] 	= 'Lời chứng thực là kinh nghiệm hoặc nhận xét của bạn trên trang web.';
$_['text_add'] 			= 'Đây là những lời chứng thực mới.';
$_['button_send'] 		= 'Gửi';

// Entry Fields
$_['entry_name']     	= 'Tên:';
$_['entry_city']     	= 'Thành Phố:';
$_['entry_email']    	= 'Địa chỉ email (sẽ được bảo mật):';
$_['entry_title']	 	= 'Đối tượng:';
$_['entry_enquiry']  	= 'Lời chứng thực:';
$_['entry_captcha']  	= 'Trả lời câu hỏi dưới đây:';
$_['entry_rating']      = 'Xếp hạng:';
$_['entry_good']        = 'Tốt';
$_['entry_bad']         = 'Tệ';

// Email
$_['email_subject']  	= 'Lời chứng thực %s';

// Errors
$_['error_name']     	= 'Tên phải lớn hơn 3 và nhỏ hơn 32 ký tự';
$_['error_title']     	= 'Đối tượng phải từ 3 đến 64 ký tự';
$_['error_email']    	= 'Địa chỉ E-Mail không hợp lệ';
$_['error_enquiry']  	= 'Thông báo phải lớn hơn 1 và dưới 1000 ký tự';
$_['error_captcha']  	= 'Lỗi: Câu trả lời xác minh sai';


?>
