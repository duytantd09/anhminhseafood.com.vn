<?php
// Heading
$_['heading_title']     = 'Khuyến mãi đặc biệt';

// Text
$_['text_empty']        = 'Không có sản phẩm khuyến mãi nào trong danh sách.';
$_['text_quantity']     = 'Số lượng:';
$_['text_manufacturer'] = 'Thương hiệu:';
$_['text_model']        = 'Mã sản phẩm:';
$_['text_points']       = 'Điểm thưởng:';
$_['text_price']        = 'Giá:';
$_['text_tax']          = 'Trước thuế:';
$_['text_compare']      = 'So sánh sản phẩm (%s)';
$_['text_sort']         = 'Sắp xếp theo:';
$_['text_default']      = 'Mặc định';
$_['text_name_asc']     = 'Tên (A - Z)';
$_['text_name_desc']    = 'Tên (Z - A)';
$_['text_price_asc']    = 'Giá (thấp &gt; cao)';
$_['text_price_desc']   = 'Giá (cao &gt; thấp)';
$_['text_rating_asc']   = 'Đánh giá (Thấp nhất)';
$_['text_rating_desc']  = 'Đánh giá (Cao nhất)';
$_['text_model_asc']    = 'Mã SP (A - Z)';
$_['text_model_desc']   = 'Mã SP (Z - A)';
$_['text_limit']        = 'Hiển thị:';