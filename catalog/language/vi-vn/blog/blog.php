<?php
$_['heading_title_latest'] = 'Bạn cần biết';
$_['heading_title_category'] = 'Danh mục tin tức';
$_['text_show_all'] = 'Xem thêm';
$_['text_blog'] = 'Tin tức';
$_['text_filter_by'] = 'Bài viết được dánh dấu bởi: ';
$_['text_posted_on'] = 'Công khai:';
$_['text_posted_by'] = 'Đăng bởi:';
$_['text_read'] = 'Đã xem:';
$_['text_comments'] = 'Bình luận';
$_['text_related_blog'] = 'Bài viết liên quan';
$_['text_no_comment'] = 'Hãy là người đầu tiên bình luận bài này!';
$_['text_write_comment'] = 'Viết bình luận';
$_['text_no_results'] = 'Không có bài viết để liệt kê';
$_['text_error'] = 'Không tìm thấy trang';
$_['text_read_more'] = 'Xem thêm';
$_['text_tags'] = 'Dánh dấu:';
$_['email_notification'] = 'Bình luận mới từ: %s';
$_['text_share_this'] = 'Chia sẻ:';
$_['entry_name'] = 'Tên';
$_['entry_email'] = 'Địa chỉ Email (Sẽ không được công bố)';
$_['entry_comment'] = 'Bình luận của bạn';
$_['entry_captcha'] = 'Trả lời các câu hỏi dưới đây:';
$_['button_send'] = 'Gửi';
$_['text_success_approve'] = 'Cảm ơn bạn! Bình luận của bạn đã được đệ trình chờ phê duyệt';
$_['text_success'] = 'Cảm ơn bạn! Cảm nhận của bạn đã được gửi thành công';
$_['error_name'] = 'Tên của bạn phải có từ 2 đến 64 ký tự';
$_['error_email'] = 'Lỗi: Địa chỉ email của bạn không hợp lệ';
$_['error_comment'] = 'Lỗi: Tiêu đề bình luận phải có từ 5 đến 3000 ký tự';
$_['error_captcha'] = 'Lỗi: câu trả lời xác nhận đã sai';
?>