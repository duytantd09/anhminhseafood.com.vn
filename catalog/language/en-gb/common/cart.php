<?php
// Text
$_['text_items']     = '%s item(s) - %s';
$_['text_empty']     = 'Your shopping cart is empty!';
$_['text_cart']      = 'View Cart';
$_['text_checkout']  = 'Checkout';
$_['text_items'] = '<span class="count">%s</span> <span class="total contrast_font mobile_hide">%s</span><span class="cart-text">SHOPPING CART</span>';
$_['text_recurring'] = 'Payment Profile';