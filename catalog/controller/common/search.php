<?php
class ControllerCommonSearch extends Controller {
	public function index() {
		$this->load->language('common/search');

		$data['text_search'] = $this->language->get('text_search');
        $data['text_categories'] = $this->language->get('text_categories');

		if (isset($this->request->get['search'])) {
			$data['search'] = $this->request->get['search'];
		} else {
			$data['search'] = '';
		}
        
        if (isset($this->request->get['category_id'])) {
			$data['category_id'] = $this->request->get['category_id'];
		} else {
			$data['category_id'] = 0;
		}
        
        $this->load->model('catalog/category');
        $categories_1 = $this->model_catalog_category->getCategories(0);
		$this->load->model('tool/image');
		foreach ($categories_1 as $category_1) {
		if ($category_1['top']) {
				// First level
			$data['categories'][] = array(
				'category_id' => $category_1['category_id'],
				'column'   => $category_1['column'] ? $category_1['column'] : 1,
				'name'     => $category_1['name'],
				'href'     => $this->url->link('product/category', 'path=' . $category_1['category_id'])
			);
		}
	}

		return $this->load->view('common/search', $data);
	}
}