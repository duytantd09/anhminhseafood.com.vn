<?php
class ControllerCommonHeader extends Controller {
    public function index() {
        //check link //
        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
            $link_http = 'https://';
        } else {
            $server = $this->config->get('config_url');
            $link_http = 'http://';
            //$this->response->redirect('https://' . $_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']);
        }

        $link_see_now = $link_http . $_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI'];
        //check error url Tan
        if(isset($this->request->get['_route_'])){
            $url_array = explode('/',$this->request->get['_route_']);
            foreach($url_array as $url_1)
            {
                if(strpos($url_1,'=')==false)
                {
                    $query_language = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . (string)$url_1 . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
                    if(!isset($query_language->row['keyword']))
                    {
                        $query_language_lang = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . (string)$url_1 . "'");
                        if (isset($query_language_lang->row['language_id'])) {
                            $query_language_code = $this->db->query("SELECT * FROM " . DB_PREFIX . "language WHERE language_id = '" . $query_language_lang->row['language_id'] . "'");
                            if (isset($query_language_code->row['language_id'])) {
                                $this->config->set('config_language_id',(int)$query_language_code->row['language_id']);
                                $this->session->data['language'] = $query_language_code->row['code'];
                                $this->response->redirect($link_see_now);
                            }
                        }
                    }
                }
            }
        }
        
        $this->load->language('common/header');
        // Analytics
        $this->load->model('extension/extension');
        $data['config_appId'] = $this->config->get('config_appId');

        $data['analytics'] = array();

        $analytics = $this->model_extension_extension->getExtensions('analytics');

        foreach ($analytics as $analytic) {
            if ($this->config->get($analytic['code'] . '_status')) {
                $data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));
            }
        }

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
            $this->document->addLink(HTTP_IMAGE . $this->config->get('config_icon'), 'icon');
        }

        $data['title'] = $this->document->getTitle();
        $data['tlptech_styles'] = $this->load->controller('common/tlptech_styles');
        $data['header_login'] = $this->load->controller('common/header_login');
        $data['header_wishlist_compare'] = $this->load->controller('common/header_wishlist_compare');
        $data['tlptech_default_product_style'] = $this->config->get('tlptech_default_product_style');
        $data['tlptech_use_custom'] = $this->config->get('tlptech_use_custom');
        $data['tlptech_container_layout'] = $this->config->get('tlptech_container_layout');
        $data['tlptech_use_breadcrumb'] = $this->config->get('tlptech_use_breadcrumb');
        $data['tlptech_menu_sticky'] = $this->config->get('tlptech_menu_sticky');
        $data['tlptech_menu_border'] = $this->config->get('tlptech_menu_border');
        $data['tlptech_header_style'] = $this->config->get('tlptech_header_style');
        $data['tlptech_header_search'] = $this->config->get('tlptech_header_search');
        $data['tlptech_menu_mega_second_thumb'] = $this->config->get('tlptech_menu_mega_second_thumb');
        $data['tlptech_menu_block_width'] = $this->config->get('tlptech_menu_block_width');
        $data['tlptech_custom_menu_block'] = $this->config->get('tlptech_custom_menu_block');
        $data['tlptech_custom_menu_url1'] = $this->config->get('tlptech_custom_menu_url1');
        $data['tlptech_custom_menu_url2'] = $this->config->get('tlptech_custom_menu_url2');
        $data['tlptech_custom_menu_url3'] = $this->config->get('tlptech_custom_menu_url3');
        $data['tlptech_custom_menu_url4'] = $this->config->get('tlptech_custom_menu_url4');
        $data['tlptech_custom_menu_url5'] = $this->config->get('tlptech_custom_menu_url5');
        $tlptech_top_promo = $this->config->get('tlptech_top_promo_message');
        if (empty($tlptech_top_promo[$this->config->get('config_language_id')])) {
            $data['tlptech_top_promo_message'] = false;
        } else if (isset($tlptech_top_promo[$this->config->get('config_language_id')])) {
            $data['tlptech_top_promo_message'] = html_entity_decode($tlptech_top_promo[$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
        }
        $tlptech_menu_block_title = $this->config->get('tlptech_custom_menu_block_title');
        if (empty($tlptech_menu_block_title[$this->config->get('config_language_id')])) {
            $data['tlptech_custom_menu_block_title'] = false;
        } else if (isset($tlptech_menu_block_title[$this->config->get('config_language_id')])) {
            $data['tlptech_custom_menu_block_title'] = html_entity_decode($tlptech_menu_block_title[$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
        }
        $tlptech_menu_custom_block = $this->config->get('tlptech_menu_custom_block_content');
        if (empty($tlptech_menu_custom_block[$this->config->get('config_language_id')])) {
            $data['tlptech_menu_custom_block_content'] = false;
        } else if (isset($tlptech_menu_custom_block[$this->config->get('config_language_id')])) {
            $data['tlptech_menu_custom_block_content'] = html_entity_decode($tlptech_menu_custom_block[$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
        }
        $tlptech_menu_custom_link1 = $this->config->get('tlptech_custom_menu_title1');
        if (empty($tlptech_menu_custom_link1[$this->config->get('config_language_id')])) {
            $data['tlptech_custom_menu_title1'] = false;
        } else if (isset($tlptech_menu_custom_link1[$this->config->get('config_language_id')])) {
            $data['tlptech_custom_menu_title1'] = html_entity_decode($tlptech_menu_custom_link1[$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
        }
        $tlptech_menu_custom_link2 = $this->config->get('tlptech_custom_menu_title2');
        if (empty($tlptech_menu_custom_link2[$this->config->get('config_language_id')])) {
            $data['tlptech_custom_menu_title2'] = false;
        } else if (isset($tlptech_menu_custom_link2[$this->config->get('config_language_id')])) {
            $data['tlptech_custom_menu_title2'] = html_entity_decode($tlptech_menu_custom_link2[$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
        }
        $tlptech_menu_custom_link3 = $this->config->get('tlptech_custom_menu_title3');
        if (empty($tlptech_menu_custom_link3[$this->config->get('config_language_id')])) {
            $data['tlptech_custom_menu_title3'] = false;
        } else if (isset($tlptech_menu_custom_link3[$this->config->get('config_language_id')])) {
            $data['tlptech_custom_menu_title3'] = html_entity_decode($tlptech_menu_custom_link3[$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
        }
        $tlptech_menu_custom_link4 = $this->config->get('tlptech_custom_menu_title4');
        if (empty($tlptech_menu_custom_link4[$this->config->get('config_language_id')])) {
            $data['tlptech_custom_menu_title4'] = false;
        } else if (isset($tlptech_menu_custom_link4[$this->config->get('config_language_id')])) {
            $data['tlptech_custom_menu_title4'] = html_entity_decode($tlptech_menu_custom_link4[$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
        }
        $tlptech_menu_custom_link5 = $this->config->get('tlptech_custom_menu_title5');
        if (empty($tlptech_menu_custom_link5[$this->config->get('config_language_id')])) {
            $data['tlptech_custom_menu_title5'] = false;
        } else if (isset($tlptech_menu_custom_link5[$this->config->get('config_language_id')])) {
            $data['tlptech_custom_menu_title5'] = html_entity_decode($tlptech_menu_custom_link5[$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
        }
        $data['tlptech_show_home_icon'] = $this->config->get('tlptech_show_home_icon');
        $data['tlptech_max_width'] = $this->config->get('tlptech_max_width');
        $data['tlptech_use_responsive'] = $this->config->get('tlptech_use_responsive');
        $data['tlptech_header_cart'] = $this->config->get('tlptech_header_cart');
        // Tlptech ends


        $data['base'] = $server;
        $data['description'] = $this->document->getDescription();
        $data['keywords'] = $this->document->getKeywords();
        $data['links'] = $this->document->getLinks();
        $data['styles'] = $this->document->getStyles();
        $data['scripts'] = $this->document->getScripts();
        $data['propertys'] = $this->document->getPropertys();
        $data['lang'] = $this->language->get('code');
        $data['direction'] = $this->language->get('direction');

        $data['name'] = $this->config->get('config_name');
        if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
            $data['logo'] = HTTP_IMAGE . $this->config->get('config_logo');
        } else {
            $data['logo'] = '';
        }

        // tlptech custom code start
        $this->load->language('common/tlptech');
        $data['tlptech_text_mobile_menu'] = $this->language->get('tlptech_text_mobile_menu');
        // Tlptech ends

        $this->load->language('common/header');

        $data['text_home'] = $this->language->get('text_home');

        // Wishlist
        if ($this->customer->isLogged()) {
            $this->load->model('account/wishlist');

            $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
        } else {
            $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
        }

        $data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
        $data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

        $data['text_account'] = $this->language->get('text_account');
        $data['text_register'] = $this->language->get('text_register');
        $data['text_login'] = $this->language->get('text_login');
        $data['text_order'] = $this->language->get('text_order');
        $data['text_transaction'] = $this->language->get('text_transaction');
        $data['text_download'] = $this->language->get('text_download');
        $data['text_logout'] = $this->language->get('text_logout');
        $data['text_checkout'] = $this->language->get('text_checkout');
        $data['text_category'] = $this->language->get('text_category');
        $data['text_special'] = $this->language->get('text_special');
        $data['text_keyword_search_popular'] = $this->language->get('text_keyword_search_popular');
        $data['text_new_product'] = $this->language->get('text_new_product');
        $data['heading_title_menu'] = $this->language->get('heading_title_menu');
        $data['text_contact'] = $this->language->get('text_contact');

        $data['home'] = $this->url->link('common/home');
        $data['wishlist'] = $this->url->link('account/wishlist', '', true);
        $data['logged'] = $this->customer->isLogged();
        $data['account'] = $this->url->link('account/account', '', true);
        $data['register'] = $this->url->link('account/register', '', true);
        $data['login'] = $this->url->link('account/login', '', true);
        $data['order'] = $this->url->link('account/order', '', true);
        $data['transaction'] = $this->url->link('account/transaction', '', true);
        $data['download'] = $this->url->link('account/download', '', true);
        $data['logout'] = $this->url->link('account/logout', '', true);
        $data['shopping_cart'] = $this->url->link('checkout/cart');
        $data['checkout'] = $this->url->link('checkout/checkout', '', true);
        $data['contact'] = $this->url->link('information/contact');
        $data['special'] = $this->url->link('product/special');
        $data['ocnewproduct'] = $this->url->link('product/ocnewproduct');
        $data['telephone'] = $this->config->get('config_telephone');
        $data['text_contact'] = $this->language->get('text_contact');

        $data['fax'] = $this->config->get('config_fax');
        $data['telephone'] = $this->config->get('config_telephone');
        $data['store'] = $this->config->get('config_name');
        $data['address'] = nl2br($this->config->get('config_address'));
        $data['config_owner'] = nl2br($this->config->get('config_owner'));
        $data['email'] = $this->config->get('config_email');

        //blog
        $blog_name = $this->config->get('blogsetting_home_title');
        $data['blogs'] = array();
        $data['blogs'] = $this->cache->get('blog_categories_' . (int)$this->config->get('config_language_id'));  
        if (!$data['blogs']) {
            $this->load->model('blog/blog_category');
            $blog_categories = $this->model_blog_blog_category->getBlogCategories(0);
            $blogs = array();
            foreach ($blog_categories as $blog_category) {
                if ($blog_category['top']) {
                    $level_2_data = array();
                    $categories_2 = $this->model_blog_blog_category->getBlogCategories($blog_category['blog_category_id']);
                    if($categories_2){
                        foreach ($categories_2 as $category_2) {
                            $level_3_data = array();
                            $categories_3 = $this->model_blog_blog_category->getBlogCategories($category_2['blog_category_id']);
                            foreach ($categories_3 as $category_3) {
                                $level_3_data[] = array(
                                'category_2_id' => $category_3['blog_category_id'],
                                'name'      => $category_3['name'],
                                'href'      => $this->url->link('blog/category', 'blogpath='  . $category_3['blog_category_id'])
                                );
                            }
                            // Second level
                            $level_2_data[] = array(
                                'category_2_id' => $category_2['blog_category_id'],
                                'name'      => $category_2['name'],
                                'children' => $level_3_data,
                                'href'      => $this->url->link('blog/category', 'blogpath=' . $category_2['blog_category_id'])
                            );
                        }
                    } else {
                        $categories_2 = $this->model_blog_blog->getBlogsByBlogCategoryId($blog_category['blog_category_id']);
                        if($categories_2){
                            foreach ($categories_2 as $category_2) {
                                $level_2_data[] = array(
                                    'category_2_id' => $category_2['blog_id'],
                                    'name'      => $category_2['title'],
                                    'children'  =>'',
                                    'href'      => $this->url->link('blog/blog', 'blog_id=' . $category_2['blog_id'])
                                );
                            }
                        }
                    }
                    $data['blogs'][] = array(
                        'name' => $blog_category['name'],
                        'children' => $level_2_data,
                        'href' => $this->url->link('blog/category', 'blogpath=' . $blog_category['blog_category_id'])
                    );
                }
            }
            $this->cache->set('blog_categories_' . (int)$this->config->get('config_language_id'), $data['blogs']);
        }

        // Add current class for active categories
        if (isset($this->request->get['path'])) {
            $parts = explode('_', (string) $this->request->get['path']);
        } else {
            $parts = array();
        }

        if (isset($parts[0])) {
            $data['category_1_id'] = $parts[0];
        } else {
            $data['category_1_id'] = 0;
        }
        // Menu
        $this->load->model('tool/image');
        $data['categories'] = array();
        $data['categories'] = $this->cache->get('categories_' . (int)$this->config->get('config_language_id'));  
        if (!$data['categories']) {
            $this->load->model('catalog/category');
            $this->load->model('catalog/product');
            $dem = 0;
            $categories_1 = $this->model_catalog_category->getCategories(234);
            foreach ($categories_1 as $category_1) {$dem++;
                if ($category_1['top']) {
                    $level_2_data = array();
                    $categories_2 = $this->model_catalog_category->getCategories($category_1['category_id']);
                    foreach ($categories_2 as $category_2) {
                        $level_3_data = array();

                        // Third level
                        $categories_3 = $this->model_catalog_category->getCategories($category_2['category_id']);

                        foreach ($categories_3 as $category_3) {

                            $total = $this->model_catalog_product->getTotalProducts(array('filter_category_id' => $category_3['category_id']));

                            $level_3_data[] = array(
                                'name' => $category_3['name'] . ($this->config->get('config_product_count') ? ' (' . $total . ')' : ''),
                                'href' => $this->url->link('product/category', 'path=' . $category_1['category_id'] . '_' . $category_2['category_id'] . '_' . $category_3['category_id'])
                            );
                        }

                        // Second level
                        $categories_2 = $this->model_catalog_category->getCategories($category_2['category_id']);

                        $total = $this->model_catalog_product->getTotalProducts(array('filter_category_id' => $category_2['category_id'], 'filter_sub_category' => true));

                        if ($category_2['image_1']) {
                            $category_2_image = $this->model_tool_image->resize($category_2['image_1'], $this->config->get('tlptech_menu_mega_second_image_w'), $this->config->get('tlptech_menu_mega_second_image_h'));
                        } else {
                            $category_2_image = '';
                        }
                        $level_2_data[] = array(
                            'name' => $category_2['name'] . ($this->config->get('config_product_count') ? '<span>' . ' (' . $total . ')' . '</span>' : ''),
                            'thumb' => $category_2_image,
                            'children' => $level_3_data,
                            'href' => $this->url->link('product/category', 'path=' . $category_1['category_id'] . '_' . $category_2['category_id'])
                        );
                    }

                    if ($category_1['image_1']) {
                        $category_image = $this->model_tool_image->resize($category_1['image_1'], $this->config->get('tlptech_menu_mega_second_image_w'), $this->config->get('tlptech_menu_mega_second_image_h'));
                    } else {
                        $category_image = '';
                    }

                    $data['categories'][] = array(
                        'category_1_id' => $category_1['category_id'],
                        'column' => $category_1['column'] ? $category_1['column'] : 1,
                        'name' => $category_1['name'],
                        'thumb' => $category_image,
                        'children' => $level_2_data,
                        'href' => $this->url->link('product/category', 'path=' . $category_1['category_id'])
                    );
                }
            }
            $this->cache->set('categories_' . (int)$this->config->get('config_language_id'), $data['categories']);
        }
        // Custom categories ends

        $this->load->model('catalog/information');
        $data['categories_information'] = array();
        $data['categories_information'] = $this->cache->get('categories_information_' . (int)$this->config->get('config_language_id'));  
        if (!$data['categories_information']) {
            $informations_array = $this->model_catalog_information->getInformationCategories();
            foreach ($informations_array as $result_category) {
                //if($result_category['information_category_id'] == 1){
                    $informations = array();
                    $informations_array_1 = $this->model_catalog_information->getInformationCategories($result_category['information_category_id']);
                    foreach ($informations_array_1 as $result_category_1) {
                        $informations[] = array(
                            'information_id' => $result_category_1['information_category_id'],
                            'title' => $result_category_1['name'],
                            'href'  => $this->url->link('information/information_category', 'information_category_id=' . $result_category_1['information_category_id'])
                        );
                    }
                    
                    $data['categories_information'][] = array(
                        'information_category_id' => $result_category['information_category_id'],
                        'name' => $result_category['name'],
                        'children' => $informations,
                        'href'  => $this->url->link('information/information_category', 'information_category_id=' . $result_category['information_category_id'])
                    );
                //}
            }
            $this->cache->set('categories_information_' . (int)$this->config->get('config_language_id'), $data['categories_information']);
        }

        $data['language'] = $this->load->controller('common/language');
        $data['currency'] = $this->load->controller('common/currency');
        $data['search'] = $this->load->controller('common/search');
        $data['cart'] = $this->load->controller('common/cart');

        // For page specific css
        if (isset($this->request->get['route'])) {
            if (isset($this->request->get['product_id'])) {
                $class = '-' . $this->request->get['product_id'];
            } elseif (isset($this->request->get['path'])) {
                $class = '-' . $this->request->get['path'];
            } elseif (isset($this->request->get['manufacturer_id'])) {
                $class = '-' . $this->request->get['manufacturer_id'];
            } elseif (isset($this->request->get['information_id'])) {
                $class = '-' . $this->request->get['information_id'];
            } else {
                $class = '';
            }
            $data['class'] = str_replace('/', '-', $this->request->get['route']) . $class . ' other-page';
        } else {
            $data['class'] = 'common-home';
        }

        return $this->load->view('common/header', $data);
    }
    public function getUrlAlias($keyword) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($keyword) . "'");

        return $query->row;
    }

}
