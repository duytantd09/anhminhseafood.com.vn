<?php 
class ControllerCommonTlptechCookie extends Controller {
	public function index() {
		
		// Cookie Control
		$data['tlptech_use_cookie'] = $this->config->get('tlptech_use_cookie');
		
		$tlptech_cookie_text = $this->config->get('tlptech_cookie_text');
		if(empty($tlptech_cookie_text[$this->language->get('code')])) {
			$data['tlptech_cookie_message'] = false;
		} else if (isset($tlptech_cookie_text[$this->language->get('code')])) {
			$data['tlptech_cookie_message'] = html_entity_decode($tlptech_cookie_text[$this->language->get('code')], ENT_QUOTES, 'UTF-8');
		}
		
		$tlptech_cookie_button_readmore = $this->config->get('tlptech_cookie_button_readmore');
		if(empty($tlptech_cookie_button_readmore[$this->language->get('code')])) {
			$data['tlptech_readmore_text'] = false;
		} else if (isset($tlptech_cookie_button_readmore[$this->language->get('code')])) {
			$data['tlptech_readmore_text'] = html_entity_decode($tlptech_cookie_button_readmore[$this->language->get('code')], ENT_QUOTES, 'UTF-8');
		}
		
		$tlptech_cookie_button_accept = $this->config->get('tlptech_cookie_button_accept');
		if(empty($tlptech_cookie_button_accept[$this->language->get('code')])) {
			$data['tlptech_button_accept_text'] = false;
		} else if (isset($tlptech_cookie_button_accept[$this->language->get('code')])) {
			$data['tlptech_button_accept_text'] = html_entity_decode($tlptech_cookie_button_accept[$this->language->get('code')], ENT_QUOTES, 'UTF-8');
		}
		
		$data['tlptech_readmore_url'] = $this->config->get('tlptech_readmore_url');
		
		// Old IE check
		$data['tlptech_use_ie'] = $this->config->get('tlptech_use_ie');
		
		$tlptech_ie_update_text = $this->config->get('tlptech_ie_update_text');
		if(empty($tlptech_ie_update_text[$this->language->get('code')])) {
			$data['tlptech_ie_update_message'] = false;
		} else if (isset($tlptech_ie_update_text[$this->language->get('code')])) {
			$data['tlptech_ie_update_message'] = html_entity_decode($tlptech_ie_update_text[$this->language->get('code')], ENT_QUOTES, 'UTF-8');
		}

		$tlptech_ie_text = $this->config->get('tlptech_ie_text');
		if(empty($tlptech_ie_text[$this->language->get('code')])) {
			$data['tlptech_ie_message'] = false;
		} else if (isset($tlptech_ie_text[$this->language->get('code')])) {
			$data['tlptech_ie_message'] = html_entity_decode($tlptech_ie_text[$this->language->get('code')], ENT_QUOTES, 'UTF-8');
		}
		
		$data['tlptech_ie_url'] = $this->config->get('tlptech_ie_url');
		
		return $this->load->view('common/tlptech_cookie', $data);
	}
	public function info() {
		$this->response->setOutput($this->index());
	}
}