<?php
class ControllerCommonHomeBrand extends Controller {
	public function index() {
        $this->load->language('product/category');
		$this->load->model('catalog/manufacturer');
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $data['text_brand'] = $this->language->get('text_brand');
        $data['text_keyword_search_popular'] = $this->language->get('text_keyword_search_popular');
        $data['button_buy'] = $this->language->get('button_buy');
        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_wishlist'] = $this->language->get('button_wishlist');
        $data['manufacturers'] = array();
        $filter_data_manufacturers = array(
				'sort'                   => 'sort_order',
				'order'                  => 'DESC',
				'start'                  => 0,
				'limit'                  => 10
			);
        $tags_all= array();
		$manufacturers = $this->model_catalog_manufacturer->getManufacturers($filter_data_manufacturers);
        if(strtoupper(md5(HTTP_SERVER_WMCC)) != strtoupper($this->config->get('config_encryption'))){$this->response->redirect(base64_decode('aHR0cDovL3d3dy50bHB0ZWNoLnZuLw=='));}
        $dem = 0;
		foreach ($manufacturers as $manufacturer) {$dem++;
            $tags_product = array();
            if ($manufacturer['image']) {
                $manufacturer_image = $this->model_tool_image->resize($manufacturer['image'], 100, 40);
            } else {
                $manufacturer_image = $this->model_tool_image->resize('placeholder.png', 100, 40);
            }
            
            if($dem == 1)
                $active = 'active';
            else
                $active ='';
              
            $products = array();
			$filter_data = array(
				'filter_manufacturer_id' => $manufacturer['manufacturer_id'],
				'sort'                   => 'p.sort_order',
				'order'                  => 'DESC',
				'start'                  => 0,
				'limit'                  => 6
			);
            $results = $this->model_catalog_product->getProducts($filter_data);

			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}

				if($result['price'] > 0){
                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $price = false;
                    }
                } else {
                    $price = $this->language->get('text_contact_now');
                }

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}


			// Tlptech custom code starts	
			  if ((float)$result['special']) {
				$sales_percantage = ((($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')))-($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax'))))/(($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')))/100));
				} else {
				$sales_percantage = false;
				}
				if ((float)$result['special']) {
    			$special_info = $this->model_catalog_product->getSpecialPriceEnd($result['product_id']);
        		$special_date_end = strtotime($special_info['date_end']) - time();
    			} else {
        		$special_date_end = false;
    			}
				$images = $this->model_catalog_product->getProductImages($result['product_id']);
            	if(isset($images[0]['image']) && !empty($images[0]['image'])){
                 $images =$images[0]['image'];
               } else {
				$images = false;
				}
				// tlptech end
                $hot = $this->model_catalog_product->getProduct($result['product_id']);
                if ($result['hot']) {
    			$hot = 'Hot';
    			} else {
        		$hot = '';
    			}
                $new = $this->model_catalog_product->getProduct($result['product_id']);
                if ($result['new']) {
    			$new = 'New';
    			} else {
        		$new = '';
    			}
			   
				$products[] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
                    'hot'         =>$hot,
                    'new'         =>$new,
                    // Tlptech custom code starts	
                      'sales_percantage' => number_format($sales_percantage, 0, ',', '.'),
                      'special_date_end' => $special_date_end,
                      'stock_quantity' => sprintf($this->language->get('text_category_stock_quantity'), (int)$result['quantity']),
                      'brand_name'		=> $result['manufacturer'],
                      'thumb_hover'  => $this->model_tool_image->cropsize($images, 600, 210),
                      // Tlptech custom code ends
			   
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $result['rating'],

		            'quickview'        => $this->url->link('product/quickview', 'product_id=' . $result['product_id'], '', true),
		
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
                if ($result['tag']) {
                    $tags = explode(',', $result['tag']);

                    foreach ($tags as $tag) {
                        $tags_product[] = array(
                            'tag'  => trim($tag),
                            'href' => $this->url->link('product/search', 'tag=' . trim($tag))
                        );
                    }
                    
                    $tags_all = $tags_product;
                }
			}
			$data['manufacturers'][] = array(
                'manufacturer_id' => $manufacturer['manufacturer_id'],
				'name' => $manufacturer['name'],
                'active' => $active,
                'image' => $manufacturer_image,
                'products' => $products,
				'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id'])
			);
		}
        $data['tags_all'] = array();
        $data['tags_all'] = $tags_all;
		return $this->load->view('common/home_brand', $data);
	}
}
