<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));
        if (isset($this->request->get['product_id']) && $this->request->get['product_id']) {
			$this->response->redirect($this->url->link('product/product', 'product_id='.(int)$this->request->get['product_id']));
		} 
		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}
        $this->load->model('design/banner');
		$this->load->model('tool/image');
		
		$meta_description = $this->config->get('config_meta_description');
		$desc = str_replace('&amp;', '&', $meta_description);
		$data['url'] = $this->url->link('common/home', '', true);
		$this->document->addProperty('title', $this->config->get('config_meta_title'));
		$image_facebook = str_replace(' ', '%20',$this->model_tool_image->resize($this->config->get('config_logo'), 400, 300));
		$this->document->addProperty('image', $image_facebook);
		$temporary = explode('.', $image_facebook);
		$file_extension = end($temporary);
		$this->document->addProperty('image:type', 'image/'.$file_extension);
		$this->document->addProperty('image:width', 400);
		$this->document->addProperty('image:height', 300);
		$this->document->addProperty('url', $data['url']);
		$this->document->addProperty('site_name', $this->config->get('config_meta_title'));
		$this->document->addProperty('description', $desc);

        $data['banners'] = array();
		$results = $this->model_design_banner->getBanner(6);
		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
					'image' => 'image/' . $result['image']
				);
			}
		}

        $data['home_top_top'] = $this->load->controller('common/home_top_top');
        $data['home_top_left'] = $this->load->controller('common/home_top_left');
        $data['home_top_center'] = $this->load->controller('common/home_top_center');
        $data['home_top_right'] = $this->load->controller('common/home_top_right');
        $data['content_bottom_half'] = $this->load->controller('common/content_bottom_half');
        $data['home_function'] = $this->load->controller('common/home_function');
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('common/home', $data));
	}
}
