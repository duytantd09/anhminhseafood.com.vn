<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');
        $this->load->model('tool/image');
		$data['scripts'] = $this->document->getScripts('footer');

		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
        $data['text_copyright'] = $this->language->get('text_copyright');
        $data['text_faq'] = $this->language->get('text_faq');
        $data['text_telephone'] = $this->language->get('text_telephone');

		$this->load->model('catalog/information');

		$data['live_search'] = $this->load->controller('extension/module/d_ajax_search');
		$data['tlptech_cookie'] = $this->load->controller('common/tlptech_cookie');	
		$data['footer_modules'] = $this->load->controller('common/footer_modules');
		$tlptech_footer_block_title = $this->config->get('tlptech_footer_custom_block_title');
		if(empty($tlptech_footer_block_title[$this->config->get('config_language_id')])) {
			$data['tlptech_footer_custom_block_title'] = false;
		} else if (isset($tlptech_footer_block_title[$this->config->get('config_language_id')])) {
			$data['tlptech_footer_custom_block_title'] = nl2br($tlptech_footer_block_title[$this->config->get('config_language_id')]);
		}
		$data['tlptech_footer_payment_icon'] = $this->config->get('tlptech_footer_payment_icon');
		$data['tlptech_use_retina'] = $this->config->get('tlptech_use_retina');
		$tlptech_footer_block = $this->config->get('tlptech_footer_custom_block');
		if(empty($tlptech_footer_block[$this->config->get('config_language_id')])) {
			$data['tlptech_footer_custom_block'] = false;
		} else if (isset($tlptech_footer_block[$this->config->get('config_language_id')])) {
			$data['tlptech_footer_custom_block'] = nl2br($tlptech_footer_block[$this->config->get('config_language_id')]);
		}
		
		$data['name'] = $this->config->get('config_name');
        if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
            $data['logo'] = HTTP_IMAGE . $this->config->get('config_logo');
        } else {
            $data['logo'] = '';
        }

		$data['informations'] = array();
		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}
        $this->load->model('catalog/information');
        $data['categories_information'] = array();
        $informations_array = $this->model_catalog_information->getInformationCategories();
        foreach ($informations_array as $result_category) {
            if($result_category['information_category_id'] == 3 || $result_category['information_category_id'] == 6){
                $informations = array();
                $data_array = array(
                    'information_category_id' => $result_category['information_category_id']
                );
                foreach ($this->model_catalog_information->getInformations($data_array) as $result) {
                    $informations[] = array(
                        'information_id' => $result['information_id'],
                        'title' => $result['title'],
                        'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
                    );
                }
                
                $data['categories_information'][] = array(
                    'information_category_id' => $result_category['information_category_id'],
                    'name' => $result_category['name'],
                    'children' => $informations,
                    'href'  => $this->url->link('information/information_category', 'information_category_id=' . $result_category['information_category_id'])
                );
            }
        }

        $this->load->model('blog/blog');
        $data['posts'] = array();
        $data_blog = array(
			'start' => 0,
			'limit' => 5
		);
        foreach ($this->model_blog_blog->getLatestBlogs($data_blog) as $result) {
      		$data['posts'][] = array(
        		'title' => $result['title'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
	    		'href'  => $this->url->link('blog/blog', 'blog_id=' . $result['blog_id'])
      		);
    	}
        
        if ($this->config->get('config_image')) {
			$data['image'] = $this->model_tool_image->resize($this->config->get('config_image'), $this->config->get($this->config->get('config_theme') . '_image_location_width'), $this->config->get($this->config->get('config_theme') . '_image_location_height'));
		} else {
			$data['image'] = false;
		}

		$data['contact'] = $this->url->link('information/contact');
        $data['download'] = $this->url->link('download/download', '', true);
		$data['return'] = $this->url->link('account/return/add', '', true);
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', true);
		$data['affiliate'] = $this->url->link('affiliate/account', '', true);
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);
        $data['faq'] = $this->url->link('product/question/questionPage', '', true);
        
        $data['information_id_23'] = $this->url->link('information/information','information_id=23');
        $data['information_id_24'] = $this->url->link('information/information','information_id=24');
        
        $data['testimonial'] = $this->url->link('product/testimonial', '', true);
        $data['testimonialform'] = $this->url->link('product/testimonialform', '', true);
        
        $data['fax'] = $this->config->get('config_fax');
        $data['telephone'] = $this->config->get('config_telephone');
        $data['telephone_1'] = $this->config->get('config_telephone_1');
        
        $data['telephone_code'] = str_replace(' ', '', trim($this->config->get('config_telephone')));
        $data['telephone_1_code'] = str_replace(' ', '', trim($this->config->get('config_telephone_1')));
        $data['store'] = $this->config->get('config_name');
		$data['address'] = nl2br($this->config->get('config_address'));
        $data['config_owner'] = nl2br($this->config->get('config_owner'));
        $data['email'] = $this->config->get('config_email');
        $data['config_comment'] = nl2br($this->config->get('config_comment'));
        $data['open'] = nl2br($this->config->get('config_open'));

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));
        $this->load->model('extension/module');
		$setting_info = $this->model_extension_module->getModule(35);
		$data['module_newsletter'] = $this->load->controller('extension/module/newsletter', $setting_info);
        
        $setting_info = $this->model_extension_module->getModule(34);
		$setting_info['footer'] = true;
		$data['socicals'] = $this->load->controller('extension/module/tlptech_socials',$setting_info);
		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}
        
        // For page specific css
        if (isset($this->request->get['route'])) {
            if (isset($this->request->get['product_id'])) {
                $class = '-' . $this->request->get['product_id'];
            } elseif (isset($this->request->get['path'])) {
                $class = '-' . $this->request->get['path'];
            } elseif (isset($this->request->get['manufacturer_id'])) {
                $class = '-' . $this->request->get['manufacturer_id'];
            } elseif (isset($this->request->get['information_id'])) {
                $class = '-' . $this->request->get['information_id'];
            } else {
                $class = '';
            }

            $data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
        } else {
            $data['class'] = 'common-home';
        }

		return $this->load->view('common/footer', $data);
	}

	public function banner($banner_id, $width, $height, $image_type = 0) {
		$this->load->model('tool/image');
		$this->load->model('design/banner');
		$data_banners = array();
		$results = $this->model_design_banner->getBanner($banner_id);
		foreach ($results as $result) {
			if ($result['image']) {
        		if($image_type)
                $result_image = $this->model_tool_image->cropsize($result['image'], $width, $height);
            	else
 				$result_image = $this->model_tool_image->resize($result['image'], $width, $height);
            } else {
                $result_image = $this->model_tool_image->resize('placeholder.png', $width, $height);
            }
			$data_banners[] = array(
				'title' => $result['title'],
				'link'  => $result['link'],
				'short' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 200) . "..",
				'description' => nl2br($result['description']),
				'image' => $result_image,
			);
		}
        return $data_banners;
	}
}
