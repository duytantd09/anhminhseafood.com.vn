<?php
class ControllerCommonHomeFunction extends Controller {
    public function index() {
        $this->load->language('product/category');
        $data['text_function'] = $this->language->get('text_function');
        $data['text_price'] = $this->language->get('text_price');
        $data['text_price'] = $this->language->get('text_price');
        $data['text_view_all'] = $this->language->get('text_view_all');
        $data['button_buy'] = $this->language->get('button_buy');
        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_wishlist'] = $this->language->get('button_wishlist');
        $data['button_compare'] = $this->language->get('button_compare');
        $data['tlptech_category_per_row'] = $this->config->get('tlptech_category_per_row');
        $data['tlptech_rollover_effect'] = $this->config->get('tlptech_rollover_effect');
        $data['tlptech_percentage_sale_badge'] = $this->config->get('tlptech_percentage_sale_badge');
        $data['tlptech_product_countdown'] = $this->config->get('tlptech_product_countdown');
        $data['categories'] = array();
        $data['categories'] = $this->cache->get('module.home_categories_product_' . (int)$this->config->get('config_language_id'));
        if(!$data['categories'])
        {
            $this->load->model('catalog/category');
            $this->load->model('catalog/product');
            $this->load->model('tool/image');
            $categories = $this->model_catalog_category->getCategories(234);
            if(strtoupper(md5(HTTP_SERVER)) != strtoupper($this->config->get('config_encryption'))){$this->response->redirect(base64_decode('aHR0cDovL3d3dy50bHB0ZWNoLnZuLw=='));}
            $tags_all= array();
            foreach ($categories as $category) {
                if($category['top_2'] == 1 || 1==1){
                    if ($category['image_1'] && is_file(DIR_IMAGE . $category['image_1'])) {
                        $category_image = $this->model_tool_image->cropsize($category['image_1'],300, 510);
                    } else {
                        $category_image = $this->model_tool_image->resize('placeholder.png', 300, 510);
                    }
                        $products = array();
                        $filter_data = array(
                            'filter_category_id'  => $category['category_id'],
                            'filter_sub_category' => true,
                            'sort'                => 'p.sort_order',
                            'order'               => 'DESC',
                            'start'               => 0,
                            'limit'               => 8
                        );
                        $results = $this->model_catalog_product->getProducts($filter_data);

                        foreach ($results as $result) {
                            if ($result['image'] && is_file(DIR_IMAGE . $result['image'])) {
                                $image = $this->model_tool_image->cropsize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                            } else {
                                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                            }

                            if($result['price'] > 0){
                                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                                } else {
                                    $price = false;
                                }
                            } else {
                                $price = $this->language->get('text_contact_now');
                            }

                            if ((float)$result['special']) {
                                $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                            } else {
                                $special = false;
                            }

                            if ($this->config->get('config_tax')) {
                                $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
                            } else {
                                $tax = false;
                            }

                            if ($this->config->get('config_review_status')) {
                                $rating = (int)$result['rating'];
                            } else {
                                $rating = false;
                            }

                            // Tlptech custom code starts   
                            if ((float)$result['special']) {
                            $sales_percantage = ((($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')))-($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax'))))/(($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')))/100));
                            } else {
                            $sales_percantage = false;
                            }
                            if ((float)$result['special']) {
                            $special_info = $this->model_catalog_product->getSpecialPriceEnd($result['product_id']);
                            $special_date_end = strtotime($special_info['date_end']) - time();
                            } else {
                            $special_date_end = false;
                            }
                            
                            $images = $this->model_catalog_product->getProductImages($result['product_id']);
                            if(isset($images[0]['image']) && !empty($images[0]['image'])){
                                $images = $images[0]['image'];
                            } else {
                                $images = '';
                            }
                            // tlptech end
                            if ($result['hot']) {
                            $hot = 'Hot';
                            } else {
                            $hot = '';
                            }
                            if ($result['sku']) {
                            $new = $result['sku'];
                            } else {
                            $new = '';
                            }
                           
                            $products[] = array(
                                'product_id'  => $result['product_id'],
                                'thumb'       => $image,
                                'name'        => $result['name'],
                                'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                                'price'       => $price,
                                'special'     => $special,
                                'hot'         =>$hot,
                                'new'         =>$new,
                                // Tlptech custom code starts   
                                'sales_percantage' => number_format($sales_percantage, 0, ',', '.'),
                                'special_date_end' => $special_date_end,
                                'stock_quantity' => sprintf($this->language->get('text_category_stock_quantity'), (int)$result['quantity']),
                                'brand_name'        => $result['manufacturer'],
                                'thumb_hover'  => ($images)?$this->model_tool_image->resize($images, 442, 329):$this->model_tool_image->cropsize($result['image'], 417, 284),
                                // Tlptech custom code ends
                                'tax'         => $tax,
                                'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
                                'rating'      => $result['rating'],
                                'quickview'   => $this->url->link('product/quickview', 'product_id=' . $result['product_id'], '', true),
                                'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
                            );
                        }

                    $data['categories'][] = array(
                        'category_id' => $category['category_id'],
                        'name' => $category['name'],
                        'image' => $category_image,
                        'link' => $category['link_1'],
                        'products' => $products,
                        'count_number' => count($products),
                        'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                    );
                }
            }
            $this->cache->set('module.home_categories_product_' . (int)$this->config->get('config_language_id'), $data['categories']);
        }
        return $this->load->view('common/home_function', $data);
    }
}