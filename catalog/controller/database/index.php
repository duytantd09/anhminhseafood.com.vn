<?php
class ControllerDatabaseIndex extends Controller {
	public function index() {
		//$this->attribute();
		//$this->option();
		//$this->orders();
		//$this->category();
		//$this->product();
		//$this->manufacturer();
		//$this->seo();
		$this->blog();
		//$this->customer();
		//$col = $this->getColumnNames('tgq_customer',DB_PREFIX_OLD . 'customer','aaa');
		//echo $col['Field'][0];
		//echo "<br>";
		// if($col == 1){
		// } else{
		// 	foreach ($col as $kadasdf) {
		// 		echo $kadasdf['Field']; echo "<br>";
		// 		echo $kadasdf['Type']; echo "<br>";
		// 	}
		// }
		//var_dump(encode($col));
		//$this->currency();
		//$this->information();
		//$this->location();
		//$this->updateProductImage();
		//$this->status();
		//$this->language();
		
		//$this->updateDatabaseNew();
	}

	public function updateDatabaseNew() {
		$this->copyDatabase('sale', DB_PREFIX_OLD . 'sale');
		$this->copyDatabase('sale_description', DB_PREFIX_OLD . 'sale_description');
		$this->copyDatabase('product_sale', DB_PREFIX_OLD . 'product_sale');
	}

	public function updateProductImage() {
		$query = $this->db->query("SELECT image,product_id FROM " . DB_PREFIX . "product");
		foreach ($query->rows as $result) {
			if(strpos($result['image'], ' ') > 0){
			    $image_o = 1;
			    $stack = explode('/',$result['image']);
				$fruit = array_pop($stack);
				$link_ok = $this->url->keywordImage(implode('/',$stack));
				if (!is_dir(DIR_IMAGE . $link_ok)) {
				    mkdir(DIR_IMAGE . $link_ok, 0777, true);
				}

				$link_image_ok = $link_ok . '/' . $this->url->keywordImage($fruit);
                
                $content = file_get_contents(str_replace(' ', '%20','http://www.bbtel.com.vn/image/' . $result['image']));
                $fp = fopen(DIR_IMAGE . $link_image_ok, "w");
                fwrite($fp, $content);
                fclose($fp);

				//file_put_contents(DIR_IMAGE . $link_image_ok,file_get_contents('http://image.iservice.vn/' . $result['image']));
                //copy(DIR_IMAGE . $result['image'], DIR_IMAGE . $link_image_ok);
				if (is_file(DIR_IMAGE . $link_image_ok)) {
				 	$this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($link_image_ok) . "' WHERE product_id = '" . (int)$result['product_id'] . "'");
				}
			}
		}
	}

	public function manufacturer() {
		$this->go('manufacturer', DB_PREFIX_OLD . 'manufacturer');
	}

	public function status() {
		$this->go('stock_status', DB_PREFIX_OLD . 'stock_status');
	}

	public function location() {
		$this->go('location', DB_PREFIX_OLD . 'location');
	}

	public function information() {
		$this->go('information', DB_PREFIX_OLD . 'information');
		$this->go('information_description', DB_PREFIX_OLD . 'information_description');
		$this->go('information_to_layout', DB_PREFIX_OLD . 'information_to_layout');
		$this->go('information_to_store', DB_PREFIX_OLD . 'information_to_store');
	}

	public function currency() {
		$this->go('currency', DB_PREFIX_OLD . 'currency');
	}

	public function customer() {
		$this->go('customer', DB_PREFIX_OLD . 'customer');
		$this->go('address', DB_PREFIX_OLD . 'address');
	}

	public function blog() {
		/*
		$data_rep = array(
			'simple_blog_category_id' => 'blog_category_id'
		);

		$blog_category = $this->go('blog_category', DB_PREFIX_OLD . 'simple_blog_category', $data_rep);
		$add_table = 'blog_category_to_store';
		$this->db->query("DELETE FROM `" . DB_PREFIX . $add_table . "`");
		foreach ($blog_category as $result) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . $add_table . "` SET blog_category_id = '" . (int)$result['simple_blog_category_id'] . "'");
		}
		//=====
		$data_rep_1 = array(
			'simple_blog_category_id' => 'blog_category_id'
		);
		$this->go('blog_category_description', DB_PREFIX_OLD . 'simple_blog_category_description', $data_rep_1);

		*/
		//=====
		$data_rep_2 = array(
			'simple_blog_article_id' => 'blog_id'
		);
		$blog_description = $this->go('blog',DB_PREFIX_OLD . 'simple_blog_article', $data_rep_2);
		$this->db->query("UPDATE " . DB_PREFIX . "blog SET image = featured_image");

		$data_rep_3 = array(
			'article_title' => 'title',
			'simple_blog_article_id' => 'blog_id'
		);
		$this->go('blog_description',DB_PREFIX_OLD . 'simple_blog_article_description', $data_rep_3);
		$this->db->query("UPDATE " . DB_PREFIX . "blog_description SET short_description = description");

		$blog_description_1 = $this->db->query("SELECT * FROM " . DB_PREFIX_OLD . "simple_blog_article_description_additional");
		foreach ($blog_description_1->rows as $result) {
			$this->db->query("UPDATE " . DB_PREFIX . "blog_description SET description = '" . $this->db->escape($result['additional_description']) . "' WHERE blog_id = '" . (int)$result['simple_blog_article_id'] . "' AND language_id = '" . $result['language_id'] . "'");
		}

		$data_rep_3 = array(
			'simple_blog_category_id' => 'blog_category_id',
			'simple_blog_article_id' => 'blog_id'
		);
		$this->go('blog_to_category',DB_PREFIX_OLD . 'simple_blog_article_to_category', $data_rep_3);

		$add_table = 'blog_to_store';
		$this->db->query("DELETE FROM `" . DB_PREFIX . $add_table . "`");
		foreach ($blog_description as $result) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . $add_table . "` SET blog_id = '" . (int)$result['simple_blog_article_id'] . "'");
		}
	}

	public function seo() {
		//=====
		$this->go('url_alias',DB_PREFIX_OLD . 'url_alias');
	}

	public function language() {
		//=====
		$this->go('language',DB_PREFIX_OLD . 'language');
	}


	public function orders() {
		//=====
		$this->go('order',DB_PREFIX_OLD . 'order');

		//=====
		$this->go('order_history',DB_PREFIX_OLD . 'order_history');

		//=====
		$this->go('order_option',DB_PREFIX_OLD . 'order_option');

		//=====
		$this->go('order_product',DB_PREFIX_OLD . 'order_product');

		//=====
		$this->go('order_status',DB_PREFIX_OLD . 'order_status');

		//=====
		$this->go('order_total',DB_PREFIX_OLD . 'order_total');
	}

	public function attribute() {
		//=====
		$this->go('attribute',DB_PREFIX_OLD . 'attribute');
		//=====
		$this->go('attribute_description',DB_PREFIX_OLD . 'attribute_description');
		//=====
		$this->go('attribute_group',DB_PREFIX_OLD . 'attribute_group');
		//=====
		$this->go('attribute_group_description',DB_PREFIX_OLD . 'attribute_group_description');
	}

	public function option() {
		//=====
		$this->go('option',DB_PREFIX_OLD . 'option');
		//=====
		$this->go('option_description',DB_PREFIX_OLD . 'option_description');
		//=====
		$this->go('option_value',DB_PREFIX_OLD . 'option_value');
		//=====
		$this->go('option_value_description',DB_PREFIX_OLD . 'option_value_description');
	}

	public function product() {
		//=====
		$this->go('product',DB_PREFIX_OLD . 'product');

		//=====
		$this->go('product_description',DB_PREFIX_OLD . 'product_description');

		//=====
		$this->go('product_image',DB_PREFIX_OLD . 'product_image');

		//=====
		$this->go('product_filter',DB_PREFIX_OLD . 'product_filter');

		//=====
		$this->go('product_attribute',DB_PREFIX_OLD . 'product_attribute');

		//=====
		$this->go('product_option',DB_PREFIX_OLD . 'product_option');
		//=====
		//$this->go('product_option_path',DB_PREFIX_OLD . 'product_option_path');
		//=====
		$this->go('product_option_value',DB_PREFIX_OLD . 'product_option_value');

		//=====
		$this->go('product_to_category',DB_PREFIX_OLD . 'product_to_category');

		//=====
		$this->go('product_to_layout',DB_PREFIX_OLD . 'product_to_layout');

		//=====
		$this->go('product_to_store',DB_PREFIX_OLD . 'product_to_store');
	}

	public function category() {
		//=====
		$this->go('category',DB_PREFIX_OLD . 'category');

		//=====
		$this->go('category_description',DB_PREFIX_OLD . 'category_description');

		//=====
		$this->go('category_filter',DB_PREFIX_OLD . 'category_filter');

		//=====
		$this->go('category_path',DB_PREFIX_OLD . 'category_path');

		//=====
		$this->go('category_to_layout',DB_PREFIX_OLD . 'category_to_layout');

		//=====
		$this->go('category_to_store',DB_PREFIX_OLD . 'category_to_store');
	}

	public function go($table_root, $table_old, $data_rep = array()) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . $table_root . "`");
		$query = $this->db->query("SELECT * FROM `" . $table_old . "`");
		foreach ($query->rows as $result) {
			$sql = "INSERT INTO `" . DB_PREFIX . $table_root . "` SET ";
			$quote = ", ";
			$i = 1;
			$data = $result;
			$n = count($data);
			foreach ($data as $key => $value) {
				if(count($data_rep) > 0){
					foreach ($data_rep as $key_rep => $value_rep) {
						if($key == $key_rep){
							$key = $value_rep;
						}
					}
				}
				if ($i==$n) $quote = "";
				$this->getColumnNames(DB_PREFIX . $table_root,$table_old,$key);
				$sql .= "`" . $key . "` = '" . $this->db->escape(html_entity_decode($value, ENT_QUOTES, 'UTF-8')) . "' " . $quote;
				$i++;
			}
			$this->db->query($sql);
		}
		return $query->rows;
	}

	public function getColumnNames($table_root, $table_old, $column = ''){
	    $query = $this->db->query("SHOW COLUMNS FROM $table_root LIKE '".$column."'");
	    if(!$query->num_rows){
	    	$query_old = $this->db->query("SHOW COLUMNS FROM $table_old LIKE '".$column."'");
	    	if($query_old->num_rows){
	    		foreach ($query_old->rows as $kadasdf) {
	    			$sql ="ALTER TABLE $table_root ADD " . $kadasdf['Field'] . " " . $kadasdf['Type'] . " NOT NULL";
	    			//var_dump($sql);exit;
			    	$this->db->query($sql);
			    }
		    }
	    }
	    //return $query->rows;
	}

	public function copyDatabase($table_root, $table_old){
		// $sql = array(
		// "DROP TABLE IF EXISTS `" . DB_PREFIX . $table_root . "`;",
		// "CREATE TABLE `" . DB_PREFIX . $table_root . "` SELECT * FROM `" . $table_old . "`");

		// Create an array of MySQL queries to run
		$sql = array("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . $table_root . "` SELECT * FROM `" . $table_old . "`");

		// Run the MySQL queries
		if (sizeof($sql) > 0) {
			foreach ($sql as $query) {
				if (!$this->db->query($query)) {
				die('A MySQL error has occurred!<br><br>' . $con->error);
				}
			}
		}
	}

}