<?php

class ControllerExtensionPaymentAlepay extends Controller {

    public function index() {
        
        $data['button_confirm'] = $this->language->get('button_confirm');

        $data['action'] = $this->url->link('extension/payment/alepay/confirm');
        
        $data['payment_methods'] = array();
        if ($this->config->get('alepay_method_normal')) {
            $data['payment_methods']['1'] = $this->config->get('alepay_method_normal_label');
        }
        if ($this->config->get('alepay_method_installment')) {
            $data['payment_methods']['2'] = $this->config->get('alepay_method_installment_label');
        }
        if ($this->config->get('alepay_method_token') && $this->customer->getId()) { // khach hang phai login moi co the thanh toan token
            $data['payment_methods']['3'] = $this->config->get('alepay_method_token_label');
        }
        
        return $this->load->view('extension/payment/alepay.tpl', $data);
    }
	public function confirm2() {
			$this->load->model('checkout/order');
			$status = "2";
			//$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('nganluong_order_status_id'));
			
			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $status);
			//echo $this->session->data['order_id'];// die;
			//$this->load->controller('checkout/confirm');
			//echo $this->session->data['order_id'];
		}
	public function confirm() {
        if ($this->session->data['payment_method']['code'] == 'alepay') {
            // Begin process once customer
            // click button confirm-order
            if (isset($this->request->post['alepay_method'])) {
                $log = new Log('alepay.log');

                // Goi API alepay
                $this->load->model('checkout/order');

                // Get order info
                $order_id = $this->session->data['order_id'];
                $order_info = $this->model_checkout_order->getOrder($order_id);
                
                // add order history
                $this->model_checkout_order->addOrderHistory($order_id, 2);

                $config = array(
                    'public_key' => $this->config->get('alepay_public_key'),
                    'encrypt_key' => $this->config->get('alepay_encrypt_key'),
                    'checksum_key' => $this->config->get('alepay_checksum_key'),
                    'language' => $this->config->get('alepay_language'),
                    'currency' => $this->config->get('alepay_currency'),
                    'popup' => $this->config->get('alepay_popup'), // enable popup
                    'method_normal' => $this->config->get('alepay_method_normal'),
                    'method_normal_label' => $this->config->get('alepay_method_normal_label'),
                    'method_installment' => $this->config->get('alepay_method_installment'),
                    'method_installment_label' => $this->config->get('alepay_method_installment_label'),
                    'method_token' => $this->config->get('alepay_method_token'),
                    'method_token_label' => $this->config->get('alepay_method_token_label'),
                    'env' => $this->config->get('alepay_test') == "sandbox" ? 'test' : 'live',
                    'alepay_method' => $this->request->post['alepay_method']
                );
                
                $this->load->model('extension/payment/alepay');
                list($alepay, $result) = $this->model_extension_payment_alepay->pagePay($order_info, $config);
                //print_r($result);exit;

                // Alepay tra ve ket qua
                if ($result->errorCode === '000') { // success
                    $res = json_decode($alepay->alepayUtils->decryptData($result->data, $alepay->publicKey));

                    if ($res->checkoutUrl) {
                        $log->write('Alepay tra ve checkoutUrl: '.$res->checkoutUrl);
                    } else {
                        $log->write('Thanh cong nhung Alepay ko tra ve checkoutUrl');
                    }

                    die(json_encode(array(
                        'errorCode' => $result->errorCode,
                        'errorDescription' => '',
                        'checkoutUrl' => !empty($res->checkoutUrl) ? $res->checkoutUrl : '',
                        'popup' => $config['popup'],
                        'redirectUrl' => $this->url->link('checkout/success')
                    )));
                } else { // failure

                    $log->write('Alepay tra ve loi: '.$result->errorDescription);

                    die(json_encode(array(
                        'errorCode' => $result->errorCode,
                        'errorDescription' => $result->errorDescription,
                        'checkoutUrl' => '',
                        'popup' => $config['popup'],
                        'redirectUrl' => $this->url->link('checkout/failure')
                    )));
                }
            }
            // End process
            
        }
    }
	public function callback() {
		$this->load->language('checkout/success');
		$this->load->model('extension/payment/alepay');
		$data = $this->request->get['data'];
        
        $config = array(
            'public_key' => $this->config->get('alepay_public_key'),
            'encrypt_key' => $this->config->get('alepay_encrypt_key'),
            'checksum_key' => $this->config->get('alepay_checksum_key'),
            'language' => $this->config->get('alepay_language'),
            'currency' => $this->config->get('alepay_currency'),
            'popup' => $this->config->get('alepay_popup'), // enable popup
            'method_normal' => $this->config->get('alepay_method_normal'),
            'method_normal_label' => $this->config->get('alepay_method_normal_label'),
            'method_installment' => $this->config->get('alepay_method_installment'),
            'method_installment_label' => $this->config->get('alepay_method_installment_label'),
            'method_token' => $this->config->get('alepay_method_token'),
            'method_token_label' => $this->config->get('alepay_method_token_label'),
            'env' => $this->config->get('alepay_test') == "sandbox" ? 'test' : 'live',
			'status' => $this->config->get('alepay_order_status_id'),
        );
		//echo $this->config->get('alepay_order_status_id');
		$this->model_extension_payment_alepay->updateOrder($data, $config);
		$this->cart->clear();
		$this->load->controller('checkout/success');
	}
    
}
