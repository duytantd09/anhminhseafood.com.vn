<?php
class ControllerExtensionModuleLatest extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/latest');
		if (isset($setting['module_description'][$this->config->get('config_language_id')])) {
			$data['heading_title'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');
			$data['description'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');
			$data['link'] = $setting['module_description'][$this->config->get('config_language_id')]['link'];
		} else {
			if(isset($setting['name']) && $setting['name'])
			$data['heading_title'] = $setting['name'];
			else
			$data['heading_title'] = $this->language->get('heading_title');
			$data['description'] = '';
			$data['link'] = '';
		}

		$data['text_tax'] = $this->language->get('text_tax');
		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['tlptech_category_per_row'] = $this->config->get('tlptech_category_per_row');
		$data['tlptech_rollover_effect'] = $this->config->get('tlptech_rollover_effect');
		$data['tlptech_percentage_sale_badge'] = $this->config->get('tlptech_percentage_sale_badge');
		$data['tlptech_product_countdown'] = $this->config->get('tlptech_product_countdown');
		$tlptech_quicklook = $this->config->get('tlptech_text_ql');
		if(empty($tlptech_quicklook[$this->config->get('config_language_id')])) {
			$data['tlptech_text_ql'] = false;
		} else if (isset($tlptech_quicklook[$this->config->get('config_language_id')])) {
			$data['tlptech_text_ql'] = html_entity_decode($tlptech_quicklook[$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
		}
        $data['tlptech_brand'] = $this->config->get('tlptech_brand');

		$data['products'] = array();
		//$data['products'] = $this->cache->get('module.latest_' . (int)$this->config->get('config_language_id'));  
        if (!$data['products']) {
        	$this->load->model('catalog/product');
			$this->load->model('tool/image');
			$filter_data = array(
				'sort'  => 'p.sort_order',
				'order' => 'ASC',
				'start' => 0,
				'limit' => $setting['limit']
			);

			$results = $this->model_catalog_product->getProducts($filter_data);
			if ($results) {
				foreach ($results as $result) {
					if ($result['image'] && is_file(DIR_IMAGE . $result['image'])) {
						$image = $this->model_tool_image->cropsize($result['image'], $setting['width'], $setting['height']);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}

					if($result['price'] > 0){
						if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
							$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						} else {
							$price = false;
						}
					} else {
	                    $price = $this->language->get('text_contact_now');
	                }

					if ((float)$result['special']) {
						$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$special = false;
					}

	                if ((float)$result['special']) {
						$sales_percantage = ((($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')))-($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax'))))/(($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')))/100));
					} else {
						$sales_percantage = false;
					}
					
					if ((float)$result['special']) {
						$special_info = $this->model_catalog_product->getSpecialPriceEnd($result['product_id']);
	    				$special_date_end = strtotime($special_info['date_end']) - time();
					} else {
	    				$special_date_end = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $result['rating'];
					} else {
						$rating = false;
					}

					if ($result['hot']) {
	    			$hot = 'Hot';
	    			} else {
	        		$hot = '';
	    			}
	                if ($result['new']) {
	    			$new = 'New';
	    			} else {
	        		$new = '';
	    			}

	    			$attribute_groups = array();
	    			//$attribute_groups = $this->model_catalog_product->getProductAttributesChoose($result['product_id'], 126);
	    			$attribute_groups = $this->model_catalog_product->getProductAttributes($result['product_id']);

					$data['products'][] = array(
						'product_id'  => $result['product_id'],
						'thumb'       => $image,
						'name'        => $result['name'],
						'minimum'     => $result['minimum'],
						'description' => strip_tags(html_entity_decode($result['description_2'], ENT_QUOTES, 'UTF-8')),
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $rating,
						'attribute_groups' => $attribute_groups,
						'hot'         =>$hot,
	                    'new'         =>$new,
						'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
					);
				}
			}
			$this->cache->set('module.latest_' . (int)$this->config->get('config_language_id'), $data['products']);
		}

		return $this->load->view('extension/module/latest', $data);
	}
}
