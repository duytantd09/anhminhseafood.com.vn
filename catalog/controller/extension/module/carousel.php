<?php
class ControllerExtensionModuleCarousel extends Controller {
	public function index($setting) {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		//$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
		//$this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');

		$data['banners'] = array();

		// $results = $this->model_design_banner->getBanner($setting['banner_id']);

		// foreach ($results as $result) {
		// 	if (is_file(DIR_IMAGE . $result['image'])) {
		// 		$data['banners'][] = array(
		// 			'title' => $result['title'],
		// 			'link'  => $result['link'],
		// 			'description'  => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
		// 			'image' => $this->model_tool_image->cropsize($result['image'], $setting['width'], $setting['height'])
		// 		);
		// 	}
		// }

		$this->load->model('catalog/information');
		$information_category_id = 1;
		$information_info = $this->model_catalog_information->getInformationCategory($information_category_id);

		if ($information_info) {
			$data['information_c_title'] = $information_info['name'];
			$data['information_c_description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');
			$data['information_c_link'] = $this->url->link('information/information_category', 'information_category_id=' . (int)$information_category_id);
		}

		$informations = array();
	    $data_array = array(
	        'information_category_id' => 9
	    );
	    foreach ($this->model_catalog_information->getInformations($data_array) as $result) {
	    	if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $this->url->link('information/information', 'information_id=' . $result['information_id']),
					'description'  => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
					'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
				);
			}
	        // $informations[] = array(
	        //     'information_id' => $result['information_id'],
	        //     'title' => $result['title'],
	        //     'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
	        // );
	    }

		$data['module'] = $module++;

		return $this->load->view('extension/module/carousel', $data);
	}
}