<?php
class ControllerModuleWidget extends Controller {
	public function index($setting)
	{
		$data['display'] = $setting['show'];
		$data['title']	= $setting['name'];
		$data['widgets'] = array();
		$this->load->model('tool/image');

		$widgets = $setting['widget'];
		$sort =  array();
		if($widgets){
			foreach ($widgets as $value) {
				$sort[] = $value['sort'];
			}

			array_multisort($sort,SORT_ASC,$widgets);
			foreach ($widgets as $widget) {
				if($setting['show'] == 'box')
				{
					$widget['thumb'] = HTTPS_SERVER .'image/'. $widget['image'];
					$widget['image'] = $this->model_tool_image->resize($widget['image'], 316,170);
					
				} elseif($setting['show'] == 'tab')
					$widget['image'] = $this->model_tool_image->resize($widget['image'], 360,270);
				if(isset($widget[$this->config->get('config_language_id')]))
					$widget['desciption'] = $widget[$this->config->get('config_language_id')]['content'];
				else
					$widget['desciption'] = $widget[1]['content'];

				if(isset($widget[$this->config->get('config_language_id')]))
					$widget['title'] = $widget[$this->config->get('config_language_id')]['title'];
				else
					$widget['title'] = $widget[1]['title'];
				$data['widgets'][] = $widget; 
			}
		}

		if (isset($setting['id'])) {
			$data['id'] = $setting['id'];
		} else {
			$data['id'] = 0;
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/widget.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/widget.tpl', $data);
		} else {
			return $this->load->view('default/template/module/widget.tpl', $data);
		}
	}
}