<?php
class ControllerExtensionModuleCustomMenu extends Controller
{
	public function index($setting = array()) {
		static $module = 0;
		$this->load->model('design/banner');
		$this->load->model('tool/image');
		$data['menus'] = array();
		$data['menus'] = array();
        $data['menus'] = $this->cache->get('custom_menu_' . (int)$this->config->get('config_language_id'));
        if(!$data['menus'])
        {
			$data['modules'] = array();
			if($setting)
			{
				if(isset($setting['menus']) && $setting['status'] == 1)
				{
					foreach ($setting['menus'] as $menus) {
						$menu_children = array();
						if (isset($menus['menus-category'])) {
							foreach ($menus['menus-category'] as $category) {
								$category_info = $this->model_catalog_category->getCategory($category);
	                            if(isset($category_info['category_id'])){
	                                if ($category_info['image']) {
	                                    $image = $this->model_tool_image->resize($category_info['image'],160,160);
	                                }else{
	                                    $image = $this->model_tool_image->resize('no_image.png',160,160);
	                                }

	                                $menu_children[] = array(
	                                    'category_id' => $category_info['category_id'],
	                                    'name'		  => $category_info['name'],
	                                    'href'		  => $this->url->link('product/category', $category_info['category_id']),
	                                    'thumb'		  => $image
	                                );
	                            }
							}
						}
						
						$link_category = '';
						if($menus['href']){
							$link_category = $this->url->link('product/category', 'path='.$setting['parent'] .'_'.$menus['href']);
						}

						$data['menus'][] = array(
							'name' => $menus[$this->config->get('config_language_id')]['name'],
							'href' => $link_category,
							'childrens' => $menu_children, 
						);
					}
				}
			} else {
				//load all modlue
				$this->load->model("extension/module");
				$this->load->model("catalog/information");
				$this->load->model("catalog/category");
				$modules = $this->model_extension_module->getModulesByCode("custom_menu");
				foreach ($modules as $module) {
					$module = json_decode($module['setting'],true);
					if (isset($module['footer']) && $module['footer'] == 1) {
						$menu = array();
						$name = '';
						foreach ($module['menus'] as $menus) {
							if($menus['href'])
							{
								$link = explode("_", $menus['href']);
								if($link){
									if ($link[0] == "information") {
										$href = $this->url->link("information/information","information_id=".$link[1]);
										$info = $this->model_catalog_information->getInformation($link[1]);
										if($info)
											$name = $menus[$this->config->get("config_language_id")]['name'];
										else
											$name = $menus[$this->config->get("config_language_id")]['name'];
									} else if ($link[0] == "category") {
										$href = $this->url->link("product/category","category_id=".$link[1]);
										$cat = $this->model_catalog_category->getCategory($link[1]);
										$name = $menus[$this->config->get("config_language_id")]['name'];
									} else if ($link[0] == "blog") {
										$href = $this->url->link("blog/blog","blog_id=" . $path[1]);
										$cat = $this->model_pavblog_blog->getInfo($link[1]);
										$name = $menus[$this->config->get("config_language_id")]['name'];
									} elseif ($link[0] == "categoryblog") {
										$href = $this->url->link('blog/category', "blogpath=". $link[1]);
										$name = $menus[$this->config->get("config_language_id")]['name'];
									}

								}
							} else {
								$href = "";
								$name = $menus[$this->config->get("config_language_id")]['name'];
							}

							$menu[] = array(
								'name' => $name,
								'href' => $href,
							);
						}

						$module['menus'] = $menu;
						if (!empty($module['mask'][$this->config->get("config_language_id")])) {
							$module['name'] = $module['mask'][$this->config->get("config_language_id")];
						} 

						$data['modules'][] = $module;
					}
				}
			}
			$this->cache->set('custom_menu_' . (int)$this->config->get('config_language_id'), $data['menus']);
		}
		return $this->load->view('extension/module/custom_menu', $data);
	}
}
?>