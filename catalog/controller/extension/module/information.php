<?php
class ControllerExtensionModuleInformation extends Controller {
	public function index() {
		$this->load->language('extension/module/information');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_faq'] = $this->language->get('text_faq');
        
        if (isset($this->request->get['information_category_id'])) {
			$data['information_category_id'] = (int)$this->request->get['information_category_id'];
		} else {
			$data['information_category_id'] = 0;
		}

		if (isset($this->request->get['information_id'])) {
			$data['information_id'] = $this->request->get['information_id'];
		} else {
			$data['information_id'] = 0;
		}

		$this->load->model('catalog/information');

		$data['categories'] = array();
        $informations_array = $this->model_catalog_information->getInformationCategories();
        foreach ($informations_array as $result_category) {
            $informations = array();
            $data_array = array(
                'information_category_id' => $result_category['information_category_id']
            );
            foreach ($this->model_catalog_information->getInformations($data_array) as $result) {
                $informations[] = array(
                    'information_id' => $result['information_id'],
                    'title' => $result['title'],
                    'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
                );
            }
            
            $data['categories'][] = array(
                'information_category_id' => $result_category['information_category_id'],
                'name' => $result_category['name'],
                'children' => $informations,
                'href'  => $this->url->link('information/information_category', 'information_category_id=' . $result_category['information_category_id'])
            );
        }

		$data['contact'] = $this->url->link('information/contact');
		$data['faq'] = $this->url->link('product/question/questionPage');

		return $this->load->view('extension/module/information', $data);
	}
}