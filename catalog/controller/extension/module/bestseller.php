<?php
class ControllerExtensionModuleBestSeller extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/showintabs');
		$this->load->language('extension/module/bestseller');

		if (isset($setting['module_description'][$this->config->get('config_language_id')])) {
			$data['heading_title'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');
			$data['description'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');
			$data['link'] = $setting['module_description'][$this->config->get('config_language_id')]['link'];
		} else {
			if(isset($setting['name']) && $setting['name'])
			$data['heading_title'] = $setting['name'];
			else
			$data['heading_title'] = $this->language->get('heading_title');
			$data['description'] = '';
			$data['link'] = '';
		}

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
        $data['button_wishlist'] = $this->language->get('button_wishlist');
        $data['button_compare'] = $this->language->get('button_compare');
        $data['tlptech_category_per_row'] = $this->config->get('tlptech_category_per_row');
        $data['tlptech_rollover_effect'] = $this->config->get('tlptech_rollover_effect');
        $data['tlptech_percentage_sale_badge'] = $this->config->get('tlptech_percentage_sale_badge');
        $data['tlptech_product_countdown'] = $this->config->get('tlptech_product_countdown');
        $tlptech_quicklook = $this->config->get('tlptech_text_ql');
        
        if(empty($tlptech_quicklook[$this->config->get('config_language_id')])) {
            $data['tlptech_text_ql'] = false;
        } else if (isset($tlptech_quicklook[$this->config->get('config_language_id')])) {
            $data['tlptech_text_ql'] = html_entity_decode($tlptech_quicklook[$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
        }
        $data['tlptech_brand'] = $this->config->get('tlptech_brand');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		$results = $this->model_catalog_product->getBestSellerProducts($setting['limit']);

		if ($results) {
			foreach ($results as $result) {
					if ($result['image']) {
						$image = $this->model_tool_image->cropsize($result['image'], $setting['width'], $setting['height']);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}
					
					$images = $this->model_catalog_product->getProductImages($result['product_id']);
					if(isset($images[0]['image']) && !empty($images[0]['image'])){
					$images =$images[0]['image'];
				   	} else {
					$images = false;
					}
					
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						
					} else {
						$price = false;
					}
							
					if ((float)$result['special']) {
						$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$special = false;
					}
					
					if ((float)$result['special']) {
					$sales_percantage = ((($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')))-($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax'))))/(($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')))/100));
					} else {
					$sales_percantage = false;
					}
					
					if ((float)$result['special']) {
    				$special_info = $this->model_catalog_product->getSpecialPriceEnd($result['product_id']);
        			$special_date_end = strtotime($special_info['date_end']) - time();
    				} else {
        			$special_date_end = false;
    				}
					
					if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}	
					
					if ($this->config->get('config_review_status')) {
						$rating = $result['rating'];
					} else {
						$rating = false;
					}
                    if ($result['hot']) {
        			$hot = 'Hot';
        			} else {
            		$hot = '';
        			}
                    if ($result['new']) {
        			$new = 'New';
        			} else {
            		$new = '';
        			}
									
					$data['products'][] = array(
						'product_id' => $result['product_id'],
						'thumb'   	 => $image,
						'name'    	 => $result['name'],
						'minimum'    => $result['minimum'],
						'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
						'price'   	 => $price,
						'special' 	 => $special,
                        'hot'         =>$hot,
                        'new'         =>$new,
						'tax'         => $tax,
						'rating'     => $rating,
						'reviews'    => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
						'href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
						'quickview'  => $this->url->link('product/quickview', 'product_id=' . $result['product_id'], '', true),
						'sales_percantage' => number_format($sales_percantage, 0, ',', '.'),
						'special_date_end' => $special_date_end,
						'brand_name'		=> $result['manufacturer'],
						'thumb_hover'  => $this->model_tool_image->resize($images, $setting['width'], $setting['height'])
					);
				}
		}

		//=========information==========//
		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}
		$product_info = $this->model_catalog_product->getProduct($product_id);
        if (isset($product_info['name'])) {
			$data['name_project'] = $product_info['name'];
			if ($product_info['image'] && is_file(DIR_IMAGE . $product_info['image'])) {
				$data['thumb'] = $this->model_tool_image->cropsize($product_info['image'], 600,400);
			} else {
				$data['thumb'] = '';
			}
			$data['upc']       = $product_info['upc'];
            $data['location']  = $product_info['location'];
            $data['ean']       = $product_info['ean'];
            $data['jan']       = $product_info['jan'];
            $data['isbn']      = $product_info['isbn'];
            $data['mpn']       = $product_info['mpn'];
            $data['quantity']  = $product_info['quantity'];
            if($product_info['price'] == 0){
                $data['price'] = 'Liên hệ';
            } elseif (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$data['price'] = false;
			}
		} else {
			$data['name_project'] = '';
		}

		$data['config_email'] = $this->config->get('config_email');
		$data['config_telephone'] = $this->config->get('config_telephone');
		$data['config_telephone_1'] = $this->config->get('config_telephone_1');
		return $this->load->view('extension/module/bestseller', $data);
	}
}
