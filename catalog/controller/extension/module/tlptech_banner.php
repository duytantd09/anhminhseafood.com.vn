<?php
class ControllerExtensionModuleTlptechBanner extends Controller {
	public function index($setting) {
		$data['text_read_more'] = $this->language->get('text_read_more');
		if(empty($setting['module_title'][$this->config->get('config_language_id')])) {
			$data['module_title'] = false;
		} else if (isset($setting['module_title'][$this->config->get('config_language_id')])) {
			$data['module_title'] = html_entity_decode($setting['module_title'][$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
		}
		
		$data['columns'] = $setting['columns'];
        if(isset($setting['width']) && (int)$setting['width'] > 20){
            $width = (int)$setting['width'];
        } else {
            $width = 200;
        }

        if(isset($setting['height']) && (int)$setting['height'] > 20){
            $height = $setting['height'];
        } else {
            $height = 200;
        }

		if (isset($setting['sections'])) {        
            $data['sections'] = array();

            $section_row = 0;
            
            foreach($setting['sections'] as $section) {
                $this->load->model('tool/image');

                if (isset($section['block'][$this->config->get('config_language_id')])){
                    $block = html_entity_decode($section['block'][$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
                } else {
                    $block = false;
                }
                
                if (isset($section['title'][$this->config->get('config_language_id')])){
                    $title = html_entity_decode($section['title'][$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
                } else {
                    $title = false;
                }
                
                if (isset($section['link'][$this->config->get('config_language_id')])){
                    $link = $section['link'][$this->config->get('config_language_id')];
                } else {
                    $link = false;
                }
				
				if (isset($section['thumb_image']) && is_file(DIR_IMAGE . $section['thumb_image'])){
				    $image = $this->model_tool_image->cropsize($section['thumb_image'], $width, $height);
				} else {
				    $image = false;
				}

                $section_row++;

                $data['sections'][] = array(
                    'index'   => $section_row,
                    'title'   => $title,
                    'description'   => $block,
                    'link'   => $link,
					'image' => $image
                );
            }
		//exit;
			return $this->load->view('extension/module/tlptech_banner', $data);
		}
	}
}