<?php
class ControllerExtensionModuleTlptechCategory extends Controller {
	public function index() {
		$this->load->language('extension/module/category');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$data['heading_title'] = $this->language->get('heading_title');
		$parts = array();

		if (isset($this->request->get['path'])) {
			$parts_check = explode('_', (string)$this->request->get['path']);
			if(count($parts_check) > 1){
				$this->response->redirect($this->url->link('product/category', 'path='.array_pop($parts_check), true));
			}
			$parts = $this->model_catalog_category->getCategoriesPath1($this->request->get['path']);
		} else {
			$parts = array();
		}

		if (isset($parts) && $parts) {
			$parts_array = explode('_', $parts);
		} else {
			$parts_array = array();
		}

		if (isset($parts_array[0])) {
			$data['category_id'] = $parts_array[0];
		} else {
			$data['category_id'] = 223;
		}

		if (isset($parts_array[1])) {
			$data['child_id'] = $parts_array[1];
		} else {
			$data['child_id'] = 0;
		}

		if (isset($parts_array[2])) {
			$data['child_child_id'] = $parts_array[2];
		} else {
			$data['child_child_id'] = 0;
		}

		if (isset($parts_array[3])) {
			$data['child_child_child_id'] = $parts_array[3];
		} else {
			$data['child_child_child_id'] = 0;
		}

		$data['categories'] = array();
		$data['categories'] = $this->cache->get('module.tlptech_category_' . (int)$this->config->get('config_language_id'));  
	    if (!$data['categories']) {
			$categories = $this->model_catalog_category->getCategories(0);
			$this->load->model('tool/image');
			foreach ($categories as $category_1) {
				$level_2_data = array();
				$categories_2 = $this->model_catalog_category->getCategories($category_1['category_id']);
				
				foreach ($categories_2 as $category_2) {
					$level_3_data = array();

					// Third level
					$categories_3 = $this->model_catalog_category->getCategories($category_2['category_id']);

					foreach ($categories_3 as $category_3) {
						
						$total = $this->model_catalog_product->getTotalProducts(array('filter_category_id'  => $category_3['category_id']));
						if($category_3['link_direct'])
	                        $href = $category_3['link_direct'];
	                    else
	                        $href = $this->url->link('product/category', 'path=' . $category_3['category_id']);
						$level_3_data[] = array(
						'category_3_id' => $category_3['category_id'],
						'name' => $category_3['name'] .($this->config->get('config_product_count') ?  ' (' . $total . ')' : ''),
						'href' => $href
						);
					 } 
					
					// Second level
					$categories_2 = $this->model_catalog_category->getCategories(0);
					
					$total = $this->model_catalog_product->getTotalProducts(array('filter_category_id'  => $category_2['category_id'], 'filter_sub_category' => true));
					if($category_2['link_direct'])
	                    $href = $category_2['link_direct'];
	                else
	                    $href = $this->url->link('product/category', 'path=' . $category_2['category_id']);
					$level_2_data[] = array(
						'category_2_id' => $category_2['category_id'],
						'name'    	=> $category_2['name']  .($this->config->get('config_product_count') ?  '<span>' . ' (' . $total . ')' . '</span>' : ''),
						'children'	=> $level_3_data,
						'href'    	=> $href
					);					
				}
				
					// First level
	        	$total = $this->model_catalog_product->getTotalProducts(array('filter_category_id'  => $category_1['category_id'], 'filter_sub_category' => true));

	        	if($category_1['link_direct'])
	                $href = $category_1['link_direct'];
	            else
	                $href = $this->url->link('product/category', 'path=' . $category_1['category_id']);

				$data['categories'][] = array(
					'category_1_id' => $category_1['category_id'],
					'name'     => $category_1['name'] .($this->config->get('config_product_count') ?  '<span>' . ' (' . $total . ')' . '</span>' : ''),				
					'children' => $level_2_data,
					'href'     => $href
				);
			}
			$this->cache->set('module.tlptech_category_' . (int)$this->config->get('config_language_id'), $data['categories']);
		}
		return $this->load->view('extension/module/tlptech_category', $data);
	}
}