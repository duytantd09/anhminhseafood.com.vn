<?php
class ControllerExtensionModuleBlogLatest extends Controller {
	public function index($setting) {
		
		$this->document->addStyle('catalog/view/theme/tlptech/stylesheet/blog.css');
		
		static $module = 0;
		
		$this->load->language('blog/blog');
		$this->load->model('blog/blog');
		$this->load->model('tool/image');
		
		$data_new = array(
            'blog_category_id' => 0,
			'start' => 0,
			'limit' => 12
		);
		
		$data['characters'] = $setting['characters'];
		$data['carousel'] = $setting['carousel'];
		$data['columns'] = $setting['columns'];
		$data['thumb'] = $setting['thumb'];
		//blog
        $blog_name = $this->config->get('blogsetting_home_title');
		$blogs = array();
        $data['blog_categories']= array();
        //$data['blog_categories'] = $this->cache->get('module.blog_category_' . (int)$this->config->get('config_language_id'));  
        if (!$data['blog_categories']) {
        	$this->load->model('blog/blog_category');
			$blog_categories = $this->model_blog_blog_category->getBlogCategories(2);
			$dem = 0;
			foreach ($blog_categories as $blog_category) {$dem++;
				if($dem == 1)
					$active = 'active';
				else
					$active = '';
				$data['blog_categories'][] = array(
					'blog_category_id' => $blog_category['blog_category_id'],
					'name' => $blog_category['name'],
					'active' => $active,
	                'children' => $blogs,
					'href' => $this->url->link('blog/category','blogpath='. $blog_category['blog_category_id'])
				);
			}
			$this->cache->set('module.blog_category_' . (int)$this->config->get('config_language_id'), $data['blog_categories']);
		}
		$data['posts'] = array();
		$data['posts'] = $this->cache->get('module.blog_latest_' . (int)$this->config->get('config_language_id'));  
        if (!$data['posts']) {
			foreach ($this->model_blog_blog->getLatestBlogs($data_new) as $result) {
	      		$data['posts'][] = array(
	        		'title' => $result['title'],
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					
					'author' => $result['author'],
					'comment_total' => $this->model_blog_blog->getTotalCommentsByBlogId($result['blog_id']),
					'date_added_full' => $result['date_added'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['short_description'], ENT_QUOTES, 'UTF-8')), 0, $setting['characters']) . "...",
					'count_read' => $result['count_read'],
					'image_big' => $this->model_tool_image->cropsize($result['image'], 800, 600),
					'image' => $this->model_tool_image->cropsize($result['image'], $setting['width'], $setting['height']),
		    		'href'  => $this->url->link('blog/blog', 'blog_id=' . $result['blog_id'])
	      		);
	    	}
	    	$this->cache->set('module.blog_latest_' . (int)$this->config->get('config_language_id'), $data['posts']);
	    }
		
		$data['blog_show_all'] = $this->url->link('blog/category','blogpath='. 2);
		
		$data['text_show_all'] = $this->language->get('text_show_all');
		$data['text_posted_on'] = $this->language->get('text_posted_on');
		$data['text_posted_by'] = $this->language->get('text_posted_by');
		$data['text_read'] = $this->language->get('text_read');
		$data['text_comments'] = $this->language->get('text_comments');
		$data['text_not_found'] = $this->language->get('text_not_found');
		$data['heading_title_latest'] = $this->language->get('heading_title_latest');
		$data['text_read_more'] = $this->language->get('text_read_more');
		
		$data['date_added_status'] = $this->config->get('blogsetting_date_added');
		$data['comments_count_status'] = $this->config->get('blogsetting_comments_count');
		$data['page_view_status'] = $this->config->get('blogsetting_page_view');
		$data['author_status'] = $this->config->get('blogsetting_author');
		
		$data['module'] = $module++;

		return $this->load->view('extension/module/blog_latest', $data);
		
	}
}