<?php
class ControllerExtensionModuleHTML extends Controller {
	public function index($setting) {
		if (isset($setting['module_description'][$this->config->get('config_language_id')])) {
			$data['heading_title'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');
			$data['html'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');

			$this->load->model('design/banner');
			$this->load->model('tool/image');
	        $data['banners'] = array();
			$results = $this->model_design_banner->getBanner(11);
			foreach ($results as $result) {
				if ($result['image'] && is_file(DIR_IMAGE . $result['image'])) {
					$image = $this->model_tool_image->resize($result['image'], 150, 150);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', 150, 150);
				}
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
					'image' => $image
				);
			}

			return $this->load->view('extension/module/html', $data);
		}
	}
}