<?php
class ControllerExtensionModuleFeatured extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/featured');

		if (isset($setting['module_description'][$this->config->get('config_language_id')])) {
			$data['heading_title'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');
			$data['description'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');
			$data['link'] = $setting['module_description'][$this->config->get('config_language_id')]['link'];
		} else {
			if(isset($setting['name']) && $setting['name'])
			$data['heading_title'] = $setting['name'];
			else
			$data['heading_title'] = $this->language->get('heading_title');
			$data['description'] = '';
			$data['link'] = '';
		}

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
        $data['button_buy'] = $this->language->get('button_buy');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['tlptech_category_per_row'] = $this->config->get('tlptech_category_per_row');
		$data['tlptech_rollover_effect'] = $this->config->get('tlptech_rollover_effect');
		$data['tlptech_percentage_sale_badge'] = $this->config->get('tlptech_percentage_sale_badge');
		$data['tlptech_product_countdown'] = $this->config->get('tlptech_product_countdown');
		
		$tlptech_quicklook = $this->config->get('tlptech_text_ql');

		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		$data['products'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 5;
		}
        $tlptech_quicklook = $this->config->get('tlptech_text_ql');
		
		if(empty($tlptech_quicklook[$this->config->get('config_language_id')])) {
			$data['tlptech_text_ql'] = false;
		} else if (isset($tlptech_quicklook[$this->config->get('config_language_id')])) {
			$data['tlptech_text_ql'] = html_entity_decode($tlptech_quicklook[$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
		}
        $data['tlptech_brand'] = $this->config->get('tlptech_brand');
        //$data['products'] = $this->cache->get('module.featured_' . (int)$this->config->get('config_language_id'));  
        if (!$data['products']) {
			if (!empty($setting['product'])) {
				$products = array_slice($setting['product'], 0, (int)$setting['limit']);

				foreach ($products as $product_id) {
					$product_info = $this->model_catalog_product->getProduct($product_id);

					if ($product_info && is_file(DIR_IMAGE . $product_info['image'])) {
						if ($product_info['image']) {
							$image = $this->model_tool_image->cropsize($product_info['image'], $setting['width'], $setting['height']);
						} else {
							$image = $this->model_tool_image->cropsize('placeholder.png', $setting['width'], $setting['height']);
						}
	                    
	                    $images = $this->model_catalog_product->getProductImages($product_info['product_id']);
						if(isset($images[0]['image']) && !empty($images[0]['image'])){
						$images =$images[0]['image'];
					   	} else {
						$images = false;
						}

						
	                    if($product_info['price'] > 0){
	                        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
	                            $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
	                        } else {
	                            $price = false;
	                        }
	                    } else {
	                        $price = $this->language->get('text_contact_now');
	                    }

	                    
	                    if ((float)$product_info['special']) {
						$sales_percantage = ((($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')))-($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax'))))/(($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')))/100));
						} else {
						$sales_percantage = false;
						}
						
						if ((float)$product_info['special']) {
	    				$special_info = $this->model_catalog_product->getSpecialPriceEnd($product_info['product_id']);
	        			$special_date_end = strtotime($special_info['date_end']) - time();
	    				} else {
	        			$special_date_end = false;
	    				}

						if ((float)$product_info['special']) {
							$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						} else {
							$special = false;
						}

						if ($this->config->get('config_tax')) {
							$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
						} else {
							$tax = false;
						}

						if ($this->config->get('config_review_status')) {
							$rating = $product_info['rating'];
						} else {
							$rating = false;
						}
	                    
	                    if ($product_info['hot']) {
	        			$hot = 'Hot';
	        			} else {
	            		$hot = '';
	        			}
	                    if ($product_info['new']) {
	        			$new = 'New';
	        			} else {
	            		$new = '';
	        			}

						$data['products'][] = array(
							'product_id'  => $product_info['product_id'],
							'thumb'       => $image,
							'name'        => $product_info['name'],
							'minimum'     => $product_info['minimum'],
							'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
							'price'       => $price,
							'special'     => $special,
							'tax'         => $tax,
							'rating'      => $rating,
	                        'hot'         =>$hot,
	                        'new'         =>$new,
	                        'brand_name'		=> $product_info['manufacturer'],
	                        'special_date_end' => $special_date_end,
	                        'sales_percantage' => number_format($sales_percantage, 0, ',', '.'),
	                        'quickview'        => $this->url->link('product/quickview', 'product_id=' . $product_info['product_id'], '', true),
	                        'thumb_hover'  => $this->model_tool_image->cropsize($images, $setting['width'], $setting['height']),
							'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
						);
					}
				}
			}
			$this->cache->set('module.featured_' . (int)$this->config->get('config_language_id'), $data['products']);
		}
		if ($data['products']) {
			return $this->load->view('extension/module/featured', $data);
		}
	}
}