<?php
class ControllerExtensionModuleTlptechTagcloud extends Controller {
	public function index() {
		
		$this->load->model('localisation/language');
		$this->load->model('extension/module/tlptech_tagcloud');
		
		$title = $this->config->get('tlptech_tagcloud_title');
		if(empty($title[$this->config->get('config_language_id')])) {
			$data['module_title'] = false;
		} else if (isset($title[$this->config->get('config_language_id')])) {
			$data['module_title'] = html_entity_decode($title[$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
		}
	
		$data['limit'] = $this->config->get('tlptech_tagcloud_limit');	
		
		$data['tagcloud'] = $this->model_extension_module_tlptech_tagcloud->getRandomTags(array(
			'limit'   => (int)$data['limit']
		));
			
       return $this->load->view('extension/module/tlptech_tagcloud', $data);
		
	}
}