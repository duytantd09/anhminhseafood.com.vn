<?php
class ControllerExtensionModuleBlogCategory extends Controller {
	public function index() {
		
		$this->load->language('blog/blog');
		
		$this->load->model('blog/blog_category');

		$this->load->model('blog/blog');
		
		$data['heading_title_category'] = $this->language->get('heading_title_category');


		if (isset($this->request->get['blogpath'])) {
			$parts = explode('_', (string)$this->request->get['blogpath']);
		} else {
			$parts = array();
		}
		
		if (isset($parts[0])) {
			$data['category_1_id'] = $parts[0];
		} else {
			$data['category_1_id'] = 0;
		}
		
		if (isset($parts[1])) {
			$data['category_2_id'] = $parts[1];
		} else {
			$data['category_2_id'] = 0;
		}
		
		$categories_1 = $this->model_blog_blog_category->getBlogCategories(0);
		
		$data['categories_1'] = array();
		
		foreach ($categories_1 as $category_1) {
		
			$level_2_data = array();
			
			$categories_2 = $this->model_blog_blog_category->getBlogCategories($category_1['blog_category_id']);
			
			foreach ($categories_2 as $category_2) {
				$level_3_data = array();

		// Second level
		$categories_2 = $this->model_blog_blog_category->getBlogCategories(0);
						
		$level_2_data[] = array(
			'category_2_id' => $category_2['blog_category_id'],
			'name'    	=> $category_2['name'],
			'children'	=> $level_3_data,
			'href'    	=> $this->url->link('blog/category', 'blogpath=' . $category_1['blog_category_id'] . '_' . $category_2['blog_category_id'])
			);					
		}
			
			// First level

			
			$data['categories'][] = array(
				'category_1_id' => $category_1['blog_category_id'],
				'name'     => $category_1['name'],				
				'children' => $level_2_data,
				'href'     => $this->url->link('blog/category', 'blogpath=' . $category_1['blog_category_id'])
			);
		}


		//blog view most
        $data['posts'] = array();
		
		$data_posts = array(
            'blog_category_id' => $data['category_1_id'],
			'start' => 0,
			'limit' => 5
		);
        foreach ($this->model_blog_blog->getViewBlogs($data_posts) as $result) {
            $data['posts'][] = array(
                'title' => $result['title'],
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),

                'author' => $result['author'],
                'comment_total' => $this->model_blog_blog->getTotalCommentsByBlogId($result['blog_id']),
                'date_added_full' => $result['date_added'],

                'description' => substr(strip_tags(html_entity_decode($result['short_description'], ENT_QUOTES, 'UTF-8')), 0, 200) . "...",
                'count_read' => $result['count_read'],
                'image'   		=> $this->model_tool_image->cropsize($result['image'], 200, 150),
                'href'  => $this->url->link('blog/blog', 'blog_id=' . $result['blog_id'])
            );
        }
		
		return $this->load->view('extension/module/blog_category', $data);
	}
}