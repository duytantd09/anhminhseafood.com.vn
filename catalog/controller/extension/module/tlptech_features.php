<?php 
class ControllerExtensionModuleTlptechFeatures extends Controller {
	public function index() {
		
		
		// Custom CSS
		$data['custom_css'] = $this->config->get('tlptech_use_custom_css');
		$data['custom_css_content'] = html_entity_decode(($this->config->get('tlptech_custom_css_content')), ENT_QUOTES, 'UTF-8');
		
		// Custom Javascript
		$data['custom_javascript'] = $this->config->get('tlptech_use_custom_javascript');
		$data['custom_javascript_content'] = html_entity_decode(($this->config->get('tlptech_custom_javascript_content')), ENT_QUOTES, 'UTF-8');
		
		// Custom style
		$data['custom_style'] = $this->config->get('tlptech_use_custom');
		
		$data['tlptech_custom_bg_icon'] = $this->config->get('tlptech_custom_bg_icon');
		$data['tlptech_body_image'] = $this->config->get('tlptech_body_image');
		$data['tlptech_body_background'] = $this->config->get('tlptech_body_background');
		$data['tlptech_top_border_background'] = $this->config->get('tlptech_top_border_background');
		$data['tlptech_top_border_border'] = $this->config->get('tlptech_top_border_border');
		$data['tlptech_top_border_text'] = $this->config->get('tlptech_top_border_text');
		$data['tlptech_top_border_link'] = $this->config->get('tlptech_top_border_link');
		$data['tlptech_top_border_link_hover'] = $this->config->get('tlptech_top_border_link_hover');
		$data['tlptech_shortcut_separator'] = $this->config->get('tlptech_shortcut_separator');
		$data['tlptech_menu_background'] = $this->config->get('tlptech_menu_background');
		$data['tlptech_menu_link_color'] = $this->config->get('tlptech_menu_link_color');
		$data['tlptech_menu_link_color_hover'] = $this->config->get('tlptech_menu_link_color_hover');
		$data['tlptech_menu_link_background_hover'] = $this->config->get('tlptech_menu_link_background_hover');
		$data['tlptech_primary_color'] = $this->config->get('tlptech_primary_color');
		$data['tlptech_secondary_color'] = $this->config->get('tlptech_secondary_color');
		$data['tlptech_offer_color'] = $this->config->get('tlptech_offer_color');
		$data['tlptech_link_hover_color'] = $this->config->get('tlptech_link_hover_color');
		$data['tlptech_salebadge_background'] = $this->config->get('tlptech_salebadge_background');
		$data['tlptech_price_color'] = $this->config->get('tlptech_price_color');
		$data['tlptech_icons_background_hover'] = $this->config->get('tlptech_icons_background_hover');
		$data['tlptech_button_background'] = $this->config->get('tlptech_button_background');
		$data['tlptech_button_color'] = $this->config->get('tlptech_button_color');
		$data['tlptech_button_border'] = $this->config->get('tlptech_button_border');
		$data['tlptech_button_hover_background'] = $this->config->get('tlptech_button_hover_background');
		$data['tlptech_button_hover_color'] = $this->config->get('tlptech_button_hover_color');
		$data['tlptech_button_hover_border'] = $this->config->get('tlptech_button_hover_border');
		$data['tlptech_button2_background'] = $this->config->get('tlptech_button2_background');
		$data['tlptech_button2_color'] = $this->config->get('tlptech_button2_color');
		$data['tlptech_button2_border'] = $this->config->get('tlptech_button2_border');
		$data['tlptech_button2_hover_background'] = $this->config->get('tlptech_button2_hover_background');
		$data['tlptech_button2_hover_color'] = $this->config->get('tlptech_button2_hover_color');
		$data['tlptech_button2_hover_border'] = $this->config->get('tlptech_button2_hover_border');
		
		
		// Custom fonts
		$data['custom_style_font'] = $this->config->get('tlptech_use_custom_font');
			
		if ($data['custom_style_font']) {
			// http://fonts.googleapis.com/css?family=Exo:300,400,700' rel='stylesheet' type='text/css
			$this->document->addStyle('//fonts.googleapis.com/css?' . $this->config->get('config_template'));
		} else {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/carousel.css');
		}
		
		    $this->children = array(
        'common/header'
    ); 
		
		return $this->load->view('common/tlptech_features', $data);
		
	}
	public function info() {
		$this->response->setOutput($this->index());
	}
}