<?php
class ControllerStartupSeoUrl extends Controller {
	public function index() {
		// Add rewrite to url class
		if ($this->config->get('config_seo_url')) {
			$this->url->addRewrite($this);
		}		
		// Decode URL
		if (isset($this->request->get['_route_'])) {
			$language_id = (int)$this->config->get('config_language_id');
			if($language_id == 2)
			$language_code = '';
			else
			$language_code = '/'. $this->session->data['language'];
			//var_dump($this->request->get['_route_']);
			$parts = explode('/', $this->request->get['_route_']);
			$route = "";
			foreach ($parts as $part) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($part) . "' AND store_id = 3 AND language_id = '" . (int)$language_id . "'");
				if (!$query->num_rows)
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($part) . "' AND store_id = 0 AND language_id = '" . (int)$language_id . "'");
				if ($query->num_rows) {
					
					$url = explode('=', $query->row['query']);
					//print_r($url);
					if ($url[0] == 'product_id') {
						$this->request->get['product_id'] = $url[1];
					}
					
					
					if ($url[0] == 'category_id') {
						if (!isset($this->request->get['path'])) {
							$this->request->get['path'] = $url[1];
						} else {
							$this->request->get['path'] .= '_' . $url[1];
						}
					}	
					
					if ($url[0] == 'manufacturer_id') {
						$this->request->get['manufacturer_id'] = $url[1];
					}
					
					if ($url[0] == 'news_id') {
						$this->request->get['news_id'] = $url[1];
					}
					
					if ($url[0] == 'menu_id') {
						$this->request->get['menu_id'] = $url[1];
					}
					
					if ($url[0] == 'information_id') {
						$this->request->get['information_id'] = $url[1];
					}

					if ($url[0] == 'account_information_id') {
						$this->request->get['account_information_id'] = $url[1];
					}

					if ($url[0] == 'landingspage_id') {
						$this->request->get['landingspage_id'] = $url[1];
					}
					else{
                      $route = $url[0];
                    }
					
				} else {
					$this->request->get['route'] = 'error/not_found';	
				}
			}
			
			if (isset($this->request->get['product_id'])) {
				$this->request->get['route'] = 'product/product';
			} elseif (isset($this->request->get['path'])) {
				$this->request->get['route'] = 'product/category';
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$this->request->get['route'] = 'product/manufacturer/info';
			} elseif (isset($this->request->get['information_id'])) {
				$this->request->get['route'] = 'information/information';
			} elseif (isset($this->request->get['account_information_id'])) {
				$this->request->get['route'] = 'account/information';
			} elseif (isset($this->request->get['landingspage_id'])) {
				$this->request->get['route'] = 'information/landingspage';
			} elseif (isset($this->request->get['news_id'])) {
				$this->request->get['route'] = 'information/news';
			} elseif (isset($this->request->get['menu_id'])) {
				$this->request->get['route'] = 'menu/why';
			} else 
			{
                $this->request->get['route'] = $route;
            }
			
            if (isset($this->request->get['route'])) {
				return new Action($this->request->get['route']);
			}
		}
	}
	
	public function rewrite($link) {
		
		//$this->load->model('localisation/language');
		if(isset($this->session->data['language']))
		{
		//$results = $this->model_localisation_language->getLanguageID($this->session->data['language']);
		//$language_id = (int)$this->config->get('config_language_id');
			$language_id = (int)$this->config->get('config_language_id');
			if($language_id == 2)
			$language_code = '';
			else
			$language_code = '/'. $this->session->data['language'];
		}
		else{
			$language_id = 4;
			$language_code = 'en-gb';
		}
		
		$url_info = parse_url(str_replace('&amp;', '&', $link));
	
		$url = ''; 
		
		$data = array();
		
		if(isset($url_info['query']))
		parse_str($url_info['query'], $data);
		
		foreach ($data as $key => $value) {
			if (isset($data['route'])) {
				if (($data['route'] == 'product/product' && $key == 'product_id') || ($data['route'] == 'information/information' && $key == 'information_id') || ($data['route'] == 'account/information' && $key == 'account_information_id') || ($data['route'] == 'information/landingspage' && $key == 'landingspage_id')) {
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "' AND language_id = '" . (int)$language_id . "'");
				
					if ($query->num_rows) {
						$url .= '/' . $query->row['keyword'];
						
						unset($data[$key]);
					}					
				} elseif ($key == 'path') {
					$categories = explode('_', $value);
					
					foreach ($categories as $category) {
						$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'category_id=" . (int)$category . "' AND language_id = '" . (int)$language_id . "'");
				
						if ($query->num_rows) {
							$url .= '/' . $query->row['keyword'];
							unset($data[$key]);
						}							
					}
					
					
				}elseif ($key == 'route' && ($value == 'information/news' || $value == 'menu/why')) {
					//var_dump($data);
					//echo $language_id;	
					if(!isset($data['news_id'])){	
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($value) . "' AND language_id = '" . (int)$language_id . "' AND store_id = 3");
					 if (!$query->num_rows)	
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` ='" . (string)$value. "' AND language_id = '" . (int)$language_id . "' AND store_id = 0");
					if ($query->num_rows) {
						$url .= '/' . $query->row['keyword'];
						unset($data[$key]);
					}
					}
					
					/*if(isset($data['category_news_id']))
					{
						$query_1= $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'category_news_id=" . (int)$data['category_news_id'] . "' AND language_id = '" . (int)$language_id . "'");
						if ($query_1->num_rows) {
							$url .= '/' . $query_1->row['keyword'];
							unset($data['category_news_id']);
						}
					}*/
					if(isset($data['menu_id']))
					{
						$query_1= $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'menu_id=" . (int)$data['menu_id'] . "' AND language_id = '" . (int)$language_id . "'");
						if ($query_1->num_rows) {
							$url .= '/' . $query_1->row['keyword'];
							unset($data['menu_id']);
						}
					}
					
					if(isset($data['news_id']))
					{
						$query_1= $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'news_id=" . (int)$data['news_id'] . "' AND language_id = '" . (int)$language_id . "'");
						if ($query_1->num_rows) {
							$url .= '/' . $query_1->row['keyword'];
							unset($data['news_id']);
						}
					}
					
				} elseif ($key == 'route') {
                   $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($value) . "' AND language_id = '" . (int)$language_id . "' AND store_id = 3");
                   if (!$query->num_rows)
                   $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($value) . "' AND language_id = '" . (int)$language_id . "' AND store_id = 0");
                
                   if ($query->num_rows) {
                      $url .= '/' . $query->row['keyword'];
                      
                      unset($data[$key]);
                   }            
                }
			}
		}
	
		if ($url) {
			unset($data['route']);
		
			$query = '';
		
			if ($data) {
				foreach ($data as $key => $value) {
					$query .= '&' . $key . '=' . $value;
				}
				
				if ($query) {
					$query = '?' . trim($query, '&');
				}
			}
			if(isset($this->session->data['language']) && $this->session->data['language'] && $this->session->data['language'] != 'vi-vn')
			{
				//echo $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') ;
				//if($url_info['path'])
				if($url == '/'.$this->session->data['language'])
				return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '').'/' . $this->session->data['language'];
				else
			    return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . '/' . $this->session->data['language'] . str_replace('/index.php', '', $url_info['path']) . $url . $query;
			}
			else
			return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $url . $query;
		} else {
			return $link;
		}
	}	
}
