<?php
class ControllerStartupSeoUrl extends Controller {
	public function index() {
		// Add rewrite to url class
		if ($this->config->get('config_seo_url')) {
			$this->url->addRewrite($this);
		}

		
		$uri = new Uri();
		$query_string = $uri->getQuery();
 
		$route = str_replace($this->getFullUrl(), '', rawurldecode($uri->toString()));
        $route = str_replace('?'.$query_string, '', $route);

       

        $seo_url = str_replace('index.php', '', $route);
        $seo_url = ltrim($seo_url, '/');

         // Don't parse if home page
        if (empty($route)) {
        	$this->response->redirect($this->getFullUrl() . $this->session->data['language_code'][$this->session->data['language']]);
        }
        
        // Add language code to URL
        $is_lang_home = false;
        if (!$this->config->get('config_seo_lang_code')) {
            if ($seo_url == $this->session->data['language_code'][$this->session->data['language']]) {
                $is_lang_home = true;
            } else {
            	if ($seo_url) {
            		$_seo_url = explode('/', $seo_url);
	            	if ($_seo_url[0] != $this->session->data['language_code'][$this->session->data['language']]) {
	            		foreach ($this->session->data['language_code'] as $key => $value) {            			
		            		if ($value == $_seo_url[0]) {
		            			$this->session->data['language'] = $key;
		            		}
		            	}

		            	$url_data = $this->request->get;
				       if (isset($url_data['_route_'])) {
				       		 $link = $this->getFullUrl() . $url_data['_route_'];
				       } else {
				       		$link = $this->getFullUrl() . $_seo_url[0] .'/';
				       }
				     
				     	$this->response->redirect($link);
	            	}          
            	}           	  	
            }

            $seo_url = ltrim($seo_url, $this->session->data['language_code'][$this->session->data['language']]);
            $seo_url = ltrim($seo_url, '/');
        }

        
      	

         $parts = explode('/', $seo_url);
        // remove any empty arrays from trailing
        if (utf8_strlen(end($parts)) == 0) {
            array_pop($parts);
        }

        $seo = new Seo($this->registry);

         foreach ($parts as $part) {
            $query = $seo->getAliasQuery($part);

            if (!empty($query)) {
                $url = explode('=', $query);
                switch ($url[0]) {
                    case 'product_id':
                        $this->request->get['product_id'] = $url[1];

                        if (!$this->config->get('config_seo_category')) {
                            $categories = array();

                            $category_id = $seo->getCategoryIdBySortOrder($url[1]);

                            if (!is_null($category_id)) {
                                $categories = $seo->getParentCategoriesIds($category_id);

                                $categories[] = $category_id;
                            }

                            if (!empty($categories)) {
                                $this->request->get['path'] = implode('_', $categories);
                            }
                        }
                        break;
                    case 'category_id':
                        if ($this->config->get('config_seo_category') == 'last') {
                            $categories = $seo->getParentCategoriesIds($url[1]);

                            $categories[] = $url[1];

                            if (!empty($categories)) {
                                $this->request->get['path'] = implode('_', $categories);
                            }
                        } else {
                            if (!isset($this->request->get['path'])) {
                                $this->request->get['path'] = $url[1];
                            } else {
                                $this->request->get['path'] .= '_' . $url[1];
                            }
                        }
                        break;
                    case 'manufacturer_id':
                        $this->request->get['manufacturer_id'] = $url[1];
                        break;
                    case 'information_id':
                        $this->request->get['information_id'] = $url[1];
                        break;
                    case 'information_category_id':
                        $this->request->get['information_category_id'] = $url[1];
                        break;
                    case 'blog_id':
                        $this->request->get['blog_id'] = $url[1];
                        if (!$this->config->get('config_seo_category')) {
                            $categories = array();

                            $category_id = $seo->getBlogCategoryIdBySortOrder($url[1]);

                            if (!is_null($category_id)) {
                                $categories = $seo->getParentBlogCategoriesIds($category_id);

                                $categories[] = $category_id;
                            }

                            if (!empty($categories)) {
                                $this->request->get['blogpath'] = implode('_', $categories);
                            }
                        }
                        break;
                    case 'blog_category_id':
                        if (!isset($this->request->get['blogpath'])) {
							$this->request->get['blogpath'] = $url[1];
						} else {
							$this->request->get['blogpath'] .= '_' . $url[1];
						}
                        break;
                    default:
                        $this->request->get['route'] = $query;
                        break;
                }
            } elseif ($is_lang_home) {
                $this->request->get['route'] = 'common/home';

                break;
            } elseif (in_array($seo_url, $this->getSeoRouteList())) {
                $this->request->get['route'] = $seo_url;

                break;
            } else {
                $this->request->get['route'] = 'error/not_found';

                break;
            }
        }

         if (!isset($this->request->get['route'])) {
            if (isset($this->request->get['product_id'])) {
                $this->request->get['route'] = 'product/product';
            } elseif (isset($this->request->get['blog_id'])) {
				$this->request->get['route'] = 'blog/blog';
			} elseif (isset($this->request->get['blogpath'])) {
				$this->request->get['route'] = 'blog/category';
            } elseif (isset($this->request->get['path'])) {
                $this->request->get['route'] = 'product/category';
            } elseif (isset($this->request->get['manufacturer_id'])) {
                $this->request->get['route'] = 'product/manufacturer/info';
            } elseif (isset($this->request->get['information_id'])) {
                $this->request->get['route'] = 'information/information';
            } elseif (isset($this->request->get['information_category_id'])) {
                $this->request->get['route'] = 'information/information_category';
            }
        }

        unset($this->request->get['_route_']); // For B/C purpose

	}

	 public function getSeoRouteList()
    {
        static $route = array();

        if (empty($route)) {
            $route[] = 'account/account';
            $route[] = 'account/address';
            $route[] = 'account/credit';
            $route[] = 'account/download';
            $route[] = 'account/forgotten';
            $route[] = 'account/login';
            $route[] = 'account/newsletter';
            $route[] = 'account/order';
            $route[] = 'account/recurring';
            $route[] = 'account/register';
            $route[] = 'account/return';
            $route[] = 'account/reward';
            $route[] = 'account/voucher';
            $route[] = 'account/wishlist';
            $route[] = 'affiliate/account';
            $route[] = 'affiliate/forgotten';
            $route[] = 'affiliate/login';
            $route[] = 'affiliate/register';
            $route[] = 'checkout/cart';
            $route[] = 'checkout/checkout';
            $route[] = 'common/home';
            $route[] = 'information/contact';
            $route[] = 'information/information';
            $route[] = 'information/information_category';
            $route[] = 'information/sitemap';
            $route[] = 'product/category';
            $route[] = 'product/manufacturer/info';
            $route[] = 'product/product';
            $route[] = 'product/special';
            $route[] = 'product/ocnewproduct';
            $route[] = 'product/testimonialform';
            $route[] = 'product/questionPage';
            $route[] = 'blog/home';
        }

        return $route;
    }

	public function rewrite_old($link) {
		$url_info = parse_url(str_replace('&amp;', '&', $link));

		$url = '';

		$data = array();

		parse_str($url_info['query'], $data);

		foreach ($data as $key => $value) {
			if (isset($data['route'])) {
				if (($data['route'] == 'product/product' && $key == 'product_id') || (($data['route'] == 'product/manufacturer/info' || $data['route'] == 'product/product') && $key == 'manufacturer_id') || 
					($data['route'] == 'information/information' && $key == 'information_id') || ($data['route'] == 'information/information_category' && $key == 'information_category_id') || ($data['route'] == 'blog/blog' && $key == 'blog_id'))
				{
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "'");

					if ($query->num_rows && $query->row['keyword']) {
						$url .= '/' . $query->row['keyword'];

						unset($data[$key]);
					}

				} elseif ($key == 'blogpath') {
					$blog_categories = explode('_', $value);
					foreach ($blog_categories as $category) {
						$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'blog_category_id=" . (int)$category . "'");
						if ($query->num_rows) {
							$url .= '/' . $query->row['keyword'];
						} else {
							$url = '';
							break;
						}}
						unset($data[$key]);
					} elseif ($key == 'path') {
						$categories = explode('_', $value);

						foreach ($categories as $category) {
							$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'category_id=" . (int)$category . "'");

							if ($query->num_rows && $query->row['keyword']) {
								$url .= '/' . $query->row['keyword'];
							} else {
								$url = '';

								break;
							}
						}

						unset($data[$key]);
					}
				}
			}

			if ($url) {
				unset($data['route']);

				$query = '';

				if ($data) {
					foreach ($data as $key => $value) {
						$query .= '&' . rawurlencode((string)$key) . '=' . rawurlencode((is_array($value) ? http_build_query($value) : (string)$value));
					}

					if ($query) {
						$query = '?' . str_replace('&', '&amp;', trim($query, '&'));
					}
				}

				return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $url . $query;
			} else {
				return $link;
			}
		}


		public function getFullUrl($path_only = false, $host_only = false) {
        $url = '';

        if ($host_only == false) {
            if (strpos(php_sapi_name(), 'cgi') !== false && !ini_get('cgi.fix_pathinfo') && !empty($_SERVER['REQUEST_URI'])) {
                $script_name = $_SERVER['PHP_SELF'];
            }
            else {
                $script_name = $_SERVER['SCRIPT_NAME'];
            }

            $url = rtrim(dirname($script_name), '/.\\');
        }

        if ($path_only == false) {
            $port = 'http://';
            if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
                $port = 'https://';
            }

            $url = $port . $_SERVER['HTTP_HOST'] . $url;
        }

        if (substr($url, -1) != '/') {
            $url .= '/';
        }

        return $url;
    }


    public function rewrite($link)
    {
        $url = '';
        $is_home = false;

        // common/currency, $data['redirect']
        $link = str_replace('amp;amp;', 'amp;', $link);

        $uri = new Uri($link);

        if ($uri->getVar('route')) {
            $seo = new Seo($this->registry);
            switch ($uri->getVar('route')) {
                case 'common/home':
                    $is_home = true;
                    break;
                case 'product/product':
                    if ($this->config->get('config_seo_category')) {
                        if ($uri->getVar('path') and ($this->config->get('config_seo_category') == 'last')) {
                            $categories = explode('_', $uri->getVar('path'));

                            $categories = array(end($categories));
                        } else {
                            $categories = array();

                            $category_id = $seo->getCategoryIdBySortOrder($uri->getVar('product_id'));

                            if (!is_null($category_id)) {
                                $categories = $seo->getParentCategoriesIds($category_id);

                                $categories[] = $category_id;

                                if ($this->config->get('config_seo_category') == 'last') {
                                    $categories = array(end($categories));
                                }
                            }
                        }

                        foreach ($categories as $category) {
                            $alias = $seo->getAlias($category, 'category');

                            if ($alias) {
                                $url .= '/' . $alias;
                            }
                        }

                        $uri->delVar('path');
                    }

                    if ($uri->getVar('product_id')) {
                        $alias = $seo->getAlias($uri->getVar('product_id'), 'product');

                        if ($alias) {
                            $url .= '/' . $alias;
                        }

                        $uri->delVar('product_id');
                        $uri->delVar('manufacturer_id');
                        $uri->delVar('path');
                        $uri->delVar('search');
                    }
                    break;
                case 'product/category':
                    if ($uri->getVar('path')) {
                        $categories = explode('_', $uri->getVar('path'));

                        foreach ($categories as $category) {
                            $alias = $seo->getAlias($category, 'category');

                            if ($alias) {
                                $url .= '/' . $alias;
                            }
                        }

                        $uri->delVar('path');
                    }
                    break;
                case 'information/information':
                    if ($uri->getVar('information_id')) {
                        $alias = $seo->getAlias($uri->getVar('information_id'), 'information');

                        if ($alias) {
                            $url .= '/' . $alias;
                        }

                        $uri->delVar('information_id');
                    }
                    break;
                case 'information/information_category':
                    if ($uri->getVar('information_category_id')) {
                        $alias = $seo->getAlias($uri->getVar('information_category_id'), 'information_category');

                        if ($alias) {
                            $url .= '/' . $alias;
                        }

                        $uri->delVar('information_category_id');
                    }
                    break;
                case 'product/manufacturer/info':
                    if ($uri->getVar('manufacturer_id')) {
                        $alias = $seo->getAlias($uri->getVar('manufacturer_id'), 'manufacturer');

                        if ($alias) {
                            $url .= '/' . $alias;
                        }

                        $uri->delVar('manufacturer_id');
                    }
                    break;
                 case 'blog/category':
                    if ($uri->getVar('blogpath')) {
                        $alias = $seo->getAlias($uri->getVar('blogpath'), 'blog_category');
                        if ($alias) {
                            $url .= '/' . $alias;
                        }

                        $uri->delVar('blogpath');
                    }
                    break;
 				case 'blog/blog':
 					if ($uri->getVar('blogpath')) {
                            $categories = explode('_', $uri->getVar('blogpath'));

                            $categories = array(end($categories));
                        } else {
                            $categories = array();

                            $category_id = $seo->getBlogCategoryIdBySortOrder($uri->getVar('blog_id'));

                            if (!is_null($category_id)) {
                                $categories = $seo->getParentCategoriesIds($category_id);

                                $categories[] = $category_id;

                                if ($this->config->get('config_seo_category') == 'last') {
                                    $categories = array(end($categories));
                                }
                            }
                        }

                    foreach ($categories as $category) {
                        $alias = $seo->getAlias($category, 'blog_category');

                        if ($alias) {
                        	$url .= '/' . $alias;
                    	}
                    }

                    $uri->delVar('blogpath');

                    if ($uri->getVar('blog_id')) {
                        $alias = $seo->getAlias($uri->getVar('blog_id'), 'blog');

                        if ($alias) {
                            $url .= '/' . $alias;
                        }

                        $uri->delVar('blog_id');
                    }
                    break;
                default:
                    if (!$this->seoDisabled($uri->getVar('route'))) {
                        $url = '/' . $uri->getVar('route');
                    }

                    break;
            }

            $uri->delVar('route');
        }

        if ($url || $is_home) {
            // Add language code to URL
            //var_dump($this->session->data['language']);
            if($this->session->data['language'] == 'vi-vn'){
                if (!$this->config->get('config_seo_lang_code')) {
                    $url = '/'.$url;
                }
            } else {
                if (!$this->config->get('config_seo_lang_code')) {
                    $url = '/'.$this->session->data['language_code'][$this->session->data['language']].$url;
                }
            }
            if (!$this->config->get('config_seo_lang_code')) {
                    $url = '/'.$this->session->data['language_code'][$this->session->data['language']].$url;
                }
            $uri->delVar('lang');

            // Append the suffix if enabled
            if ($this->config->get('config_seo_suffix') && !$is_home) {
                $url .= '.html';
            }

            $path = $uri->getPath();

            // if ($this->config->get('config_seo_rewrite') || ($is_home && !$this->config->get('config_seo_lang_code'))) {
                $path = str_replace('index.php/', '', $path);
                $path = str_replace('index.php', '', $path);
            // }

            $path .= $url;

            $uri->setPath($path);

            return $uri->toString();
        } else {
            return $link;
        }
    }

    public function seoDisabled($route = '')
    {
        $status = false;

        if (!in_array($route, $this->getSeoRouteList())) {
            $status = true;
        }

	    return $status;
    }
}

	
