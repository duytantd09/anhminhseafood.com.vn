<?php
class ControllerInformationStore extends Controller {
	private $error = array();

	public function index() {

		$this->load->language('information/store');

		$this->document->setTitle($this->language->get('heading_title'));
		$data['heading_title'] = $this->language->get('heading_title');
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('information/store')
		);

		$this->load->model('catalog/local');
		$this->load->model('localisation/location');
		$this->load->model('tool/image');

		$locals = $this->model_catalog_local->getLocals();
		$data['locals'] = array();
		foreach ($locals as $local) {
			$childrens = array();
			foreach ($this->model_catalog_local->getLocals($local['local_id']) as $children) {
				$locations = $this->model_catalog_local->getLocalByLocaltions($children['local_id']);
				$zone = array();
				foreach ($locations as $location) {
					$location_info = $this->model_localisation_location->getLocation($location);
					if ($location_info) {
						if ($location_info['image']) {
							$image = $this->model_tool_image->cropsize($location_info['image'], 400, 300,'w');
							//$image = $this->model_tool_image->cropsize($location_info['image'], $this->config->get($this->config->get('config_theme') . '_image_location_width'), $this->config->get($this->config->get('config_theme') . '_image_location_height'),'w');
						} else {
							$image = false;
						}

						$zone[] = array(
							'location_id' => $location_info['location_id'],
							'name'        => $location_info['name'],
							'address'     => nl2br($location_info['address']),
							'geocode'     => $location_info['geocode'],
							'telephone'   => $location_info['telephone'],
							'fax'         => $location_info['fax'],
							'image'       => $image,
							'open'        => nl2br($location_info['open']),
							'comment'     => $location_info['comment'],
							'href'		  => $this->url->link('information/store/info', 'location_id='. $location_info['location_id'])
						);
					}
				}

				$childrens[] = array(
					'id' => $children['local_id'],
					'name' => $children['name'],
					'locations' => $zone
				);
			}

			$data['locals'][] = array(
				'id' => $local['local_id'],
				'name' => $local['name'],
				'childrens' => $childrens
			);
		}


		$data['locations'] = array();
		foreach((array)$this->config->get('config_location') as $location_id) {
			$location_info = $this->model_localisation_location->getLocation($location_id);

			if ($location_info) {
				if ($location_info['image']) {
					$image = $this->model_tool_image->cropsize($location_info['image'], $this->config->get($this->config->get('config_theme') . '_image_location_width'), $this->config->get($this->config->get('config_theme') . '_image_location_height'));
				} else {
					$image = false;
				}

				$data['locations'][] = array(
					'location_id' => $location_info['location_id'],
					'name'        => $location_info['name'],
					'address'     => nl2br($location_info['address']),
					'geocode'     => $location_info['geocode'],
					'telephone'   => $location_info['telephone'],
					'fax'         => $location_info['fax'],
					'image'       => $image,
					'open'        => nl2br($location_info['open']),
					'comment'     => $location_info['comment']
				);
			}
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('information/store', $data));
	}

	public function info()
	{
		if (isset($this->request->get['location_id'])) {
			$this->load->language('information/store');
			

			$this->load->model('localisation/location');
			$this->load->model('tool/image');
			
			$location_info = $this->model_localisation_location->getLocation($this->request->get['location_id']);
			

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('information/store')
			);

			$data['breadcrumbs'][] = array(
				'text' => $location_info['name'],
				'href' => $this->url->link('information/store/info', 'location_id='. $location_info['location_id'])
			);

			$this->document->setTitle($location_info['name']);
			$data['heading_title'] = $this->language->get('heading_title');

			$this->load->language('information/contact');
			$data['text_location'] = $this->language->get('text_location');
			$data['text_store'] = $this->language->get('text_store');
			$data['text_contact'] = $this->language->get('text_contact');
			$data['text_address'] = $this->language->get('text_address');
			$data['text_telephone'] = $this->language->get('text_telephone');
			$data['text_fax'] = $this->language->get('text_fax');
			$data['text_open'] = $this->language->get('text_open');
			$data['text_comment'] = $this->language->get('text_comment');

			if ($location_info) {
				if ($location_info['image']) {
					$data['image'] = $this->model_tool_image->cropsize($location_info['image'], 700, 400, 'w');
				} else {
					$data['image'] = '';
				}

				$data['store'] = $location_info['name'];
				$data['address'] = nl2br($location_info['address']);
				$data['geocode'] = $location_info['geocode'];
				$data['telephone'] = $location_info['telephone'];
				$data['fax'] = $location_info['fax'];
				$data['open'] = nl2br($location_info['open']);
				$data['comment'] = $location_info['comment'];

				$data['description'] = $this->model_localisation_location->getLocationDescription($this->request->get['location_id']);
			}

		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('information/store_info', $data));
	}
}
