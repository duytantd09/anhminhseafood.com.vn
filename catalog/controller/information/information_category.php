<?php
class ControllerInformationInformationCategory extends Controller {
	public function index() {
		$this->load->language('information/information');
        $this->load->model('tool/image');
		$this->load->model('catalog/information');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
        
        if (isset($this->request->get['information_category_id'])) {
			$information_category_id = (int)$this->request->get['information_category_id'];
		} else {
			$information_category_id = 0;
		}

		if (isset($this->request->get['information_id'])) {
			$information_id = (int)$this->request->get['information_id'];
		} else {
			$information_id = 0;
		}
        
		$information_info = $this->model_catalog_information->getInformationCategory($information_category_id);

		if ($information_info) {
			//=========start check url=========//
			if(strstr($_SERVER['REQUEST_URI'],'information_category_id=' . (int)$this->request->get['information_category_id']) != false)
			{
				if($this->customer->insertKeyword('information_category_id', $information_category_id, $information_info['name']))
				$this->response->redirect($this->url->link('information/information_category', 'information_category_id=' . (int)$information_category_id),301);
			}
			//=========end check url=========//
			$this->document->setTitle($information_info['meta_title']);
			$this->document->setDescription($information_info['meta_description']);
			$this->document->setKeywords($information_info['meta_keyword']);

			$data['breadcrumbs'][] = array(
				'text' => $information_info['name'],
				'href' => $this->url->link('information/information', 'information_id=' .  $information_id)
			);

			$data['heading_title'] = $information_info['name'];

			$data['button_continue'] = $this->language->get('button_continue');

			$data['description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');
            $data['categories'] = array();
            $data_array = array(
                'information_category_id' => $information_info['information_category_id']
            );
            foreach ($this->model_catalog_information->getInformations($data_array) as $result) {
                if ($result['image'] && is_file(DIR_IMAGE . $result['image'])) {
					$image = $this->model_tool_image->resize($result['image'], 200, 150);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', 200, 150);
				}
                $data['categories'][] = array(
                    'information_id' => $result['information_id'],
                    'title' => $result['title'],
                    'image' => $image,
                    'description' => substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')),0,700),
                    'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
                );
            }
            
			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('information/information_category', $data));
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('information/information', 'information_id=' . $information_id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

	public function agree() {
		$this->load->model('catalog/information');

		if (isset($this->request->get['information_id'])) {
			$information_id = (int)$this->request->get['information_id'];
		} else {
			$information_id = 0;
		}

		$output = '';

		$information_info = $this->model_catalog_information->getInformation($information_id);

		if ($information_info) {
			$output .= html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8') . "\n";
		}

		$this->response->setOutput($output);
	}
}