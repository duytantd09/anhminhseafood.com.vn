

function getURLVar(key) {
    var value = [];
    var query = String(document.location).split('?');
    if (query[1]) {
        var part = query[1].split('&');
        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');
            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }
        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

// change url function
function changeUrl(a){
    var hash = a.attr('href');
    var l = location;
    var do_main = l.host+l.pathname
    if(do_main[do_main.length-1] == "/"){
        do_main = do_main.slice(0, -1);
    }
    location.href = (l.protocol+'//'+do_main+'/'+hash);
}


$(document).ready(function() {

    $('.text-danger').each(function() {
        var element = $(this).parent().parent();
        if (element.hasClass('form-group')) {
            element.addClass('has-error');
        }
    });
    $('#currency .currency-select').on('click', function(e) {
        e.preventDefault();
        $('#currency input[name=\'code\']').attr('value', $(this).attr('name'));
        $('#currency').submit();
    });
    $('#language a').on('click', function(e) {
        e.preventDefault();
        $('#language input[name=\'code\']').attr('value', $(this).attr('href'));
        $('#language').submit();
    });
    $('.button-search').bind('click', function() {
        url = $('base').attr('href') + 'index.php?route=product/search';
        var value = $('#search input[name=\'search\']').val();
        var value_category_id = document.getElementById("search_category_id").value;
        if (value || value_category_id) {
            url += '&search=' + encodeURIComponent(value) + '&category_id=' + encodeURIComponent(value_category_id);
        }
        location = url;
    });
    $('#search input[name=\'search\']').on('keydown', function(e) {
        if (e.keyCode == 13) {
            $('#search input[name=\'search\']').parent().find('.button-search').trigger('click');
        }
    });
    $("#menu >ul >li").has("li").addClass("withsubs");
    $("#menu > ul > li > div > .wrapper > ul > li").has("li").addClass("hasthird");
    $('#menu .menu_drop_down').each(function() {
        var menu = $('#menu').offset();
        var dropdown = $(this).parent().offset();
        var dropdown_wrapper = $(this).offset();
        var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());
        if (i > 0) {
            $(this).css('margin-left', '-' + i + 'px');
        }
        var r = (menu.left - dropdown_wrapper.left);
        if (r > 0) {
            $(this).css('margin-right', '-' + r + 'px');
        }
    });
    /*$('.mobile_menu_trigger').click(function() {
        $('.mobile_menu_wrapper').slideToggle(500)
    });*/
    $('.mobile_menu_trigger').click(function() {
        $(this).find(".nav-icon").toggleClass("active");
        $('.mobile_menu_wrapper').toggleClass("active");
    });
    $(".mobile_menu_wrapper").click(function(){ 
        $('.mobile_menu_trigger').find(".nav-icon").toggleClass("active");
        $(this).toggleClass("active"); 
    }).children().click(function(e) {
        return false; 
    });

    $('.mobile_menu li').bind().click(function(e) {
        $(this).toggleClass("open").find('>ul').stop(true, true).slideToggle(500).end().siblings().find('>ul').slideUp().parent().removeClass("open");
        e.stopPropagation();
    });
    $('.mobile_menu li a').click(function(e) {
        e.stopPropagation();
    });

    $('.breadcrumb').appendTo($('.breadcrumb'));
    $('.login_input').focus(function() {
        $('.login_drop_heading').stop(true, true).addClass('active');
    });
    $('.login_input').focusout(function() {
        $('.login_drop_heading').stop(true, true).removeClass('active');
    });
    $('.search_input').focus(function() {
        $('#search').stop(true, true).addClass('active');
    });
    $('.search_input').focusout(function() {
        $('#search').stop(true, true).removeClass('active');
        $('#ajax_search_results').hide(200);
    });
    $('a.external').on('click', function(e) {
        e.preventDefault();
        window.open($(this).attr('href'));
    });

    $(".product-thumb").hover(function() {
        $(this).find(".image-hover").stop(true).fadeTo(600, 1);
    }, function() {
        $(this).find(".image-hover").stop(true).fadeTo(300, 0);
    });
    $('.product-list .item, .product-grid .item, .item').mousemove(function(e) {
        $(this).find('.offer_popup').stop(true, true).fadeIn();
        $(this).find('.offer_popup').offset({
            top: e.pageY + 50,
            left: e.pageX + 50
        });
    }).mouseleave(function() {
        $('.offer_popup').fadeOut();
    });


    // module category (product-category, blog-category, infomation-category)
    $('.box-category .box-content li').bind().click(function(e) {
       $(this).toggleClass("open").find('>ul').stop(true, true).slideToggle(500)
       .end().siblings().find('>ul').slideUp().parent().removeClass("open");
       e.stopPropagation();
   });
    $('.box-category .box-content li a').click(function(e) {
       e.stopPropagation();
   });

    // xu ly khi scroll top
    $(window).scroll(function () {
        var scrollTop = $(this).scrollTop();
        //make fixed top menu
        if (scrollTop > 50) {
            //$(".header").addClass("fix-header");
            $('#back-to-top').fadeIn();
        } else {
           // $(".header").removeClass("fix-header");
            $('#back-to-top').fadeOut();
        }
        
    });

    $('#back-to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $(".footer_modules").has(".box").addClass("contrast");
    $('[data-toggle=\'tooltip\']').tooltip({
        container: 'body'
    });
    $(document).ajaxStop(function() {
        $('[data-toggle=\'tooltip\']').tooltip({
            container: 'body'
        });
    });
});
var cart = {
    'add': function(product_id, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();
                $('#cart > button').button('reset');
                if (json['redirect']) {
                    location = json['redirect'];
                }
                if (json['success']) {
                    $('body').addClass('is-popup');
                    $.colorbox({
                        html: '<div class="cart_notification"><div class="product clearfix"><img src="' + json['image'] + '"/><span>' + json['success'] + '</span></div><div class="bottom text-center"><a class="btn btn-secondary" href="' + json['link_cart'] + '">' + json['text_cart'] + '</a> ' + '<a class="btn btn-primary" href="' + json['link_checkout'] + '">' + json['text_checkout'] + '</a></div></div>',
                        className: "notification",
                        initialHeight: 50,
                        initialWidth: 50,
                        width: "90%",
                        maxWidth: 400,
                        height: "90%",
                        maxHeight: 200
                    });
                    $('#cart').load('index.php?route=common/cart/info #cart > *');
                    $('body').removeClass('is-popup');
                }
            }
        });
    },
    'buy': function(product_id, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();
                $('#cart > button').button('reset');
                location = 'index.php?route=quickcheckout/checkout';
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'update': function(key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            success: function(json) {
                $('#cart > button').button('reset');
                $('#cart-total').html(json['total']);
                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart').load('index.php?route=common/cart/info #cart > *');
                }
            }
        });
    },
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            success: function(json) {
                $('#cart > button').button('reset');
                $('#cart-total').html(json['total']);
                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart').load('index.php?route=common/cart/info #cart > *');
                }
            }
        });
    }
}
var voucher = {
    'add': function() {},
    'remove': function(key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                $('#cart-total').html(json['total']);
                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart').load('index.php?route=common/cart/info #cart > *');
                }
            }
        });
    }
}
var wishlist = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert').remove();
                if (json['success']) {
                    $.colorbox({
                        html: '<div class="cart_notification"><div class="product"><img src="' + json['image'] + '"/><span>' + json['success'] + '</span></div><div class="bottom"><a class="btn btn-primary" href="' + json['link_wishlist'] + '">' + json['text_wishlist'] + '</a></div></div>',
                        className: "notification",
                        initialHeight: 50,
                        initialWidth: 50,
                        width: "90%",
                        maxWidth: 400,
                        height: "90%",
                        maxHeight: 200
                    });
                    $('.shortcut.wishlist').load('index.php?route=common/header_wishlist_compare/info #header_wishlist');
                }
                if (json['info']) {
                    $.colorbox({
                        html: '<div class="cart_notification"><div class="product"><img src="' + json['image'] + '"/><span>' + json['info'] + '</span></div><div class="bottom"><a class="btn btn-primary" href="' + json['link_wishlist'] + '">' + json['text_wishlist'] + '</a></div></div>',
                        className: "notification",
                        initialHeight: 50,
                        initialWidth: 50,
                        width: "90%",
                        maxWidth: 400,
                        height: "90%",
                        maxHeight: 200
                    });
                    $('.shortcut.wishlist').load('index.php?route=common/header_wishlist_compare/info #header_wishlist');
                }
            }
        });
    },
    'remove': function() {}
}
var compare = {
    'add': function(product_id) {
        $.ajax({
            url: 'index.php?route=product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert').remove();
                if (json['success']) {
                    $.colorbox({
                        html: '<div class="cart_notification"><div class="product"><img src="' + json['image'] + '"/><span>' + json['success'] + '</span></div><div class="bottom"><a class="btn btn-primary" href="' + json['link_compare'] + '">' + json['text_compare'] + '</a></div></div>',
                        className: "notification",
                        initialHeight: 50,
                        initialWidth: 50,
                        width: "90%",
                        maxWidth: 400,
                        height: "90%",
                        maxHeight: 200
                    });
                    $('#compare-total').html(json['total']);
                    $('#header_compare').html(json['compare_total']);
                }
            }
        });
    },
    'remove': function() {}
}
$(document).delegate('.agree', 'click', function(e) {
    e.preventDefault();
    $('#modal-agree').remove();
    var element = this;
    $.ajax({
        url: $(element).attr('href'),
        type: 'get',
        dataType: 'html',
        success: function(data) {
            html = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <h5 class="modal-title">' + $(element).text() + '</h5>';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div';
            html += '  </div>';
            html += '</div>';
            $('body').append(html);
            $('#modal-agree').modal('show');
        }
    });
});
(function($) {
    $.fn.autocomplete = function(option) {
        return this.each(function() {
            this.timer = null;
            this.items = new Array();
            $.extend(this, option);
            $(this).attr('autocomplete', 'off');
            $(this).on('focus', function() {
                this.request();
            });
            $(this).on('blur', function() {
                setTimeout(function(object) {
                    object.hide();
                }, 200, this);
            });
            $(this).on('keydown', function(event) {
                switch (event.keyCode) {
                    case 27:
                    this.hide();
                    break;
                    default:
                    this.request();
                    break;
                }
            });
            this.click = function(event) {
                event.preventDefault();
                value = $(event.target).parent().attr('data-value');
                if (value && this.items[value]) {
                    this.select(this.items[value]);
                }
            }
            this.show = function() {
                var pos = $(this).position();
                $(this).siblings('ul.dropdown-menu').css({
                    top: pos.top + $(this).outerHeight(),
                    left: pos.left
                });
                $(this).siblings('ul.dropdown-menu').show();
            }
            this.hide = function() {
                $(this).siblings('ul.dropdown-menu').hide();
            }
            this.request = function() {
                clearTimeout(this.timer);
                this.timer = setTimeout(function(object) {
                    object.source($(object).val(), $.proxy(object.response, object));
                }, 200, this);
            }
            this.response = function(json) {
                html = '';
                if (json.length) {
                    for (i = 0; i < json.length; i++) {
                        this.items[json[i]['value']] = json[i];
                    }
                    for (i = 0; i < json.length; i++) {
                        if (!json[i]['category']) {
                            html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
                        }
                    }
                    var category = new Array();
                    for (i = 0; i < json.length; i++) {
                        if (json[i]['category']) {
                            if (!category[json[i]['category']]) {
                                category[json[i]['category']] = new Array();
                                category[json[i]['category']]['name'] = json[i]['category'];
                                category[json[i]['category']]['item'] = new Array();
                            }
                            category[json[i]['category']]['item'].push(json[i]);
                        }
                    }
                    for (i in category) {
                        html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';
                        for (j = 0; j < category[i]['item'].length; j++) {
                            html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                        }
                    }
                }
                if (html) {
                    this.show();
                } else {
                    this.hide();
                }
                $(this).siblings('ul.dropdown-menu').html(html);
            }
            $(this).after('<ul class="dropdown-menu"></ul>');
            $(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));
        });
    }
})(window.jQuery);