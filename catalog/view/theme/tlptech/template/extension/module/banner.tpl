<div id="banner<?php echo $module; ?>" class="owl-carousel owl-theme">
  <?php foreach ($banners as $banner) { ?>
  <div class="item border">
    <?php if ($banner['link']) { ?>
    <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
    <?php } ?>
  </div>
  <?php } ?>
</div>
<script type="text/javascript">
  $('#banner<?php echo $module; ?>').owlCarousel({
    animateOut: 'fadeOut',
    loop:true,
    autoplay: true,
    margin:5,
    items:1,
    nav: false,
    dots: false
  });
</script>
