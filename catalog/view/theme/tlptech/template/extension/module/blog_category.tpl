<div class="blog-category-col box-category mb-3">
  <div class="box-title"><?php echo $heading_title_category; ?></div>
  <div class="box-content ">
    <?php if (!empty($categories)) { ?>
    <ul class=" list-unstyled px-0 my-0">
      <?php foreach ($categories as $category_1) { ?>
      <?php if ($category_1['category_1_id'] == $category_1_id) { ?>
      <li class="open active"><a href="<?php echo $category_1['href']; ?>" ><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> <?php echo $category_1['name']; ?></a>
        <?php } else { ?>
        <li><a href="<?php echo $category_1['href']; ?>" ><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> <?php echo $category_1['name']; ?></a> 
          <?php } ?>
          <?php if ($category_1['children']) { ?>
          <div class="sign"><span class="plus"><i class="fa fa-angle-right" aria-hidden="true"></i></span><span class="minus"><i class="fa fa-angle-down" aria-hidden="true"></i></span></div>
          <ul class="list-unstyled px-0 my-0">
            <?php foreach ($category_1['children'] as $category_2) { ?>
            <li><a href="<?php echo $category_2['href']; ?>">- <?php echo $category_2['name']; ?></a></li>
            <?php } ?>
          </ul>
          <?php } ?>
        </li>
        <?php } ?>
      </ul>
      <?php } ?>
    </div>
  </div>

<?php if(!empty($posts)){ ?>
<div class="col-best-sale">
  <div class="title">Bài viết xem nhiều</div>
  <?php foreach ($posts as $blog) {?> 
  <div class="article">
    <div class="row">
      <div class="col-4 pr-0">
        <a href="<?php echo $blog['href']; ?>" title="<?php echo $blog['title']; ?>">
        <img src="<?php echo $blog['image']; ?>" atl="<?php echo $blog['title']; ?>">
      </a>
      </div>
      <div class="col-8">
        <a href="<?php echo $blog['href']; ?>" title="<?php echo $blog['title']; ?>"><div class="name"><?php echo $blog['title']; ?></div></a>
        <div class="des">
        <?php echo $blog['description']; ?>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<?php } ?>