<div class="banner-walls mb-3">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <?php if ($image1) { ?>
                <a href="#">
                    <div class="zoom-image-container banner-walls-image-div">
                        <img class="zoom-image" alt="" src="<?php echo $image1; ?>" />
                        <div class="wrap-info">
                            <h5 class="mb-0"><?php echo $html1; ?></h5>
                        </div>
                    </div>
                </a>
                <?php } else { ?>
                <div class="">
                    <?php echo $html1; ?>
                </div>
                <?php } ?>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <?php if ($image2) { ?>
                        <a href="#">
                            <div class="zoom-image-container banner-walls-image-div">
                                <img class="zoom-image" alt="" src="<?php echo $image2; ?>" />
                                <div class="wrap-info">
                                    <h5 class="mb-0"><?php echo $html2; ?></h5>
                                </div>
                            </div>
                        </a>
                        <?php } else { ?>
                        <div class="">
                            <?php echo $html2; ?>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="col-sm-6">
                        <?php if ($image3) { ?>
                        <a href="#">
                            <div class="zoom-image-container banner-walls-image-div">
                                <img class="zoom-image" alt="" src="<?php echo $image3; ?>" />
                                <div class="wrap-info">
                                    <h5 class="mb-0">Sữa tươi</h5>
                                </div>
                            </div>
                        </a>
                        <?php } else { ?>
                        <div class="">
                            <?php echo $html3; ?>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="col-sm-6">
                        <?php if ($image3) { ?>
                        <a href="#">
                            <div class="zoom-image-container banner-walls-image-div">
                                <img class="zoom-image" alt="" src="<?php echo $image3; ?>" />
                                <div class="wrap-info">
                                    <h5 class="mb-0">Rau</h5>
                                </div>
                            </div>
                        </a>
                        <?php } else { ?>
                        <div class="">
                            <?php echo $html3; ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="row">
                    <div class="col-12">
                        <?php if ($image4) { ?>
                        <a href="#">
                            <div class="zoom-image-container banner-walls-image-div">
                                <img class="zoom-image" alt="" src="<?php echo $image4; ?>" />
                                <div class="wrap-info">
                                    <h5 class="mb-0"><?php echo $html4; ?></h5>
                                </div>
                            </div>
                        </a>
                        <?php } else { ?>
                        <div class="">
                            <?php echo $html3; ?>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="col-12">
                        <?php if ($image5) { ?>
                        <a href="#">
                            <div class="zoom-image-container banner-walls-image-div">
                                <img class="zoom-image" alt="" src="<?php echo $image5; ?>" />
                                <div class="wrap-info">
                                    <h5 class="mb-0"><?php echo $html5; ?></h5>
                                </div>
                            </div>
                        </a>
                        <?php } else { ?>
                        <div class="">
                            <?php echo $html5; ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>