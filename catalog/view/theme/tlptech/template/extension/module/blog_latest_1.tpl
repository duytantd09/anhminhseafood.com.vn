<br />
<div class="wt_blog_latest">
    <div class="wrapper_content">
        <div class="block-title"><h2><?php echo $heading_title_latest; ?></h2></div>
        <div class="box-content latest">
            <div class="row">
                <?php if(!empty($posts)){ ?>
                    <?php $dem = 0; foreach ($posts as $blog) { $dem++; ?> 
                    <?php if ($dem == 1){ ?>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="blog-item bolg_1">
                            <a href="<?php echo $blog['href']; ?>" title="<?php echo $blog['title']; ?>">
                                <div class="wt_content_bloglatest">
                                <?php if($blog['image']){ ?>
                                <div class="hover"><img src="<?php echo $blog['image']; ?>" atl="<?php echo $blog['title']; ?>" /></div>
                                <?php } ?>
                                <h2><?php echo $blog['title']; ?></h2>
                                <div class="description"><?php echo $blog['description']; ?></div>
                                </div>
                            </a>
                        </div>    
                    </div>
                    <?php } ?>
                    <?php } ?>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="blog-item box_bolg">
                            <?php $dem = 0; foreach ($posts as $blog) { $dem++; ?> 
                            <?php if ($dem > 1){ ?>
                            <div class="row">
                                <div class="col-sm-4 hover">
                                    <a href="<?php echo $blog['href']; ?>" title="<?php echo $blog['title']; ?>">
                                        <?php if($blog['image'] && $thumb){ ?>
                                        <img src="<?php echo $blog['image']; ?>" atl="<?php echo $blog['title']; ?>" />
                                        <?php } ?>
                                    </a>
                                </div>
                                <div class="col-sm-8">
                                    <h2><a href="<?php echo $blog['href']; ?>" title="<?php echo $blog['title']; ?>"><?php echo $blog['title']; ?></a></h2>
                                    <div class="description"><?php echo $blog['description']; ?></div>
                                </div>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div> 
                    </div>
                 <?php } ?>
            </div>
        </div>
    </div>
</div>