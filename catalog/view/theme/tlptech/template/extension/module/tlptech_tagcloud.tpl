<div class="box tag-col-left">
<?php if ($module_title){ ?>
<div class="block-title "><h2><i class="fa fa-tag" aria-hidden="true"></i><?php echo $module_title; ?></h2></div>
<?php } ?>
<div class="box-content tags contrast_font"> 
<?php if($tagcloud) { ?>
<div class="icons_wrapper">
<?php echo $tagcloud; ?>
</div>
<?php } ?>
</div>
</div>
