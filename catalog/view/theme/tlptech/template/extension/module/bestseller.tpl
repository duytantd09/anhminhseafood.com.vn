<div class="col-contact-form"  id="hform-dangkytuvan">
   <div class="title"><i class="fa fa-headphones" aria-hidden="true"></i> Liên hệ với chúng tôi</div>
   <p>NV kinh doanh: <b><?php echo $config_telephone;?></b> </p>
   <p>NV kỹ thuật: <b><?php echo $config_telephone_1;?></b> </p>
   <p>Email: <b><?php echo $config_email;?></b> </p>
   <div class="title-form">Đăng ký tư vấn</div>
   <div class="form-group">
     <input type="text" name="name_project" type="text" value="<?php echo $name_project;?>" placeholder="Tên dự sp" class="form-control">
   </div>
   <div class="form-group">
     <input type="text" name="enquiry" type="text" placeholder="Nội dung" class="form-control">
   </div>
   <div class="form-group">
     <input type="text" name="name" placeholder="Họ tên" class="form-control">
   </div>
   <div class="form-group">
     <input type="text" name="email" placeholder="Email" class="form-control">
   </div>
   <div class="form-group">
     <input type="text" name="telephone" placeholder="Điện thoại" class="form-control">
   </div>
   <div class="">
     <button type="button"  name="button_newsletter" id="button_newsletter" class="btn btn-danger">Đăng ký</button>
   </div>
</div>
<script type="text/javascript">
$('#hform-dangkytuvan #button_newsletter').on('click', function() {
 $.ajax({
  url: 'index.php?route=information/contact/validateNewsletter',
  type: 'post',
  data: $('#hform-dangkytuvan input[type=\'text\']'),
  dataType: 'json',
        beforeSend: function() {
            $('#hform-dangkytuvan button[name=\'button_newsletter\']').button('loading');
        },
        complete: function() {
            $('#hform-dangkytuvan button[name=\'button_newsletter\']').button('reset');
        },
  success: function(json) {
    $('.alert, .text-danger').remove();
   if (json['error']) {
        if (json['error']['name_project']) {
            $('#hform-dangkytuvan input[name=\'name_project\']').after('<div class="text-danger example">' + json['error']['name_project'] + '</div>');
        }
        if (json['error']['telephone']) {
            $('#hform-dangkytuvan input[name=\'telephone\']').after('<div class="text-danger example">' + json['error']['telephone'] + '</div>');
        }

        if (json['error']['email']) {
            $('#hform-dangkytuvan input[name=\'email\']').after('<div class="text-danger example">' + json['error']['email'] + '</div>');
        }
   } else {
        $('#hform-dangkytuvan input[name=\'name_project\']').val('');
        $('#hform-dangkytuvan input[name=\'name\']').val('');
        $('#hform-dangkytuvan input[name=\'email\']').val('');
        $('#hform-dangkytuvan input[name=\'address\']').val('');
        $('#hform-dangkytuvan input[name=\'telephone\']').val('');
        $('#hform-dangkytuvan input[name=\'enquiry\']').val('');
        $('#hform-dangkytuvan .title-form').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i>' + json['success'] + '<button type="button" class="close" data-dismiss="alert">×</button></div>');
   }
  }
 }); 
}); 
</script>
<div class="col-best-sale">
  <div class="title"><?php echo $heading_title;?></div>
  <div class="module-description text-center"><?php echo $description;?></div>
  <?php foreach ($products as $product) { ?>
  <div class="product">
    <a href="<?php echo $product['href']; ?>">
      <div class="name"><?php echo $product['name']; ?></div>
      <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
    </a>
  </div>
  <?php } ?>
</div>