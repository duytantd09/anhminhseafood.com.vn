<div class="recently-product">
  <div class="container">
    <div class="module-title text-center">
      <h3 class="title"><?php echo $heading_title; ?></h3>
    </div>

    <div class="owl-carousel owl-theme recently-carousel">
      <?php foreach ($products as $product) { ?>
      <div class="item-grid">
        <div class="product-thumb">
          <div class="thumb-image">
            <?php if ($product['special'] && $tlptech_percentage_sale_badge == 'enabled') { ?>
            <div class="sale">
              <span class="sale-text">sale</span>
              <span class="sale-percent"><?php echo $product['sales_percantage']; ?>%</span>
            </div>
            <?php } ?>
            <?php if ($product['new']!='') { ?>
            <div class="tag-new"><?php echo $product['new']; ?></div>
            <?php } ?>
            <?php if ($product['hot']!='') { ?>
            <div class="tag-hot"><?php echo $product['hot']; ?></div>
            <?php } ?>
            <div class="image">
              <?php if ($product['thumb']) { ?>
              <a href="<?php echo $product['href']; ?>">
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
              </a>
              <?php } ?>
            </div>

            <div class="actions-link" hidden>                
              <button class="btn-wishlist button" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="icon icon-Heart"></i></button>

              <button class="btn-compare button" type="button" data-toggle="tooltip" title="" onclick="compare.add('<?php echo $product['product_id']; ?>');" data-original-title="<?php echo $button_compare; ?>">
                <i class="icon icon-MusicMixer"></i>
              </button>

              <button class="btn-quickview button" type="button" data-toggle="tooltip" title="" onclick="ocquickview.ajaxView('')" data-original-title="Quick View">
                <i class="icon icon-Eye"></i>
              </button> 

            </div>
          </div>

          <div class="caption text-center">
            <div class="rating" >
              <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($product['rating'] < $i) { ?>
              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
              <?php } ?>
              <?php } ?>
            </div>
            <h5 class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h5>
            <?php if ($product['price']) { ?>
            <p class="price" >
              <?php if (!$product['special']) { ?>
              <?php echo $product['price']; ?>
              <?php } else { ?>
              <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
              <?php } ?>
              <?php if ($product['tax']) { ?>
              <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
              <?php } ?>
            </p>
            <?php } ?>
            <div class="btn-group thumb-bottom text-center" role="group">
              <button class="btn-cart btn btn-secondary " type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');">
                <span class="d-xl-none d-lg-none d-md-none"><i class="icon icon-ShoppingCart"></i></span>
                <span class="d-none d-md-block"><?php echo $button_cart; ?></span>
              </button>

              <button type="button" class="btn btn-primary" onclick="cart.buy('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"> <?php echo $button_buy; ?> </button>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>


    </div>
  </div>
</div>
<script type="text/javascript">
  $('.recently-carousel').owlCarousel({
    loop:true,
    margin:15,
    items:4,
    nav: true,
    dots: false,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    responsive:{
      0:{
        items:2
      },
      600:{
        items:4
      }
    }
  });
</script>  