<div class="info-home mt-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-12">
                <img src="https://www.webmaucaocap.com/onecrm//image/cache/catalog/tin-tuc/crm-cr-400x300.jpg" alt="info">
            </div>
            <div class="col-sm-6 col-12 text-center align-self-center">
                <div class="module-title text-center">
                    <h3 class="title">Infomation</h3>
              </div> 
              <p> Designed for compliance driven organisations, SuiteASSURED delivers the freedoms, quality and innovation of open source AND the security, warranties and indemnities of proprietary software. It’s the total care package for SuiteCRM! </p>
              <a href="#" class="btn btn-primary">More...</a>
          </div>
      </div>
  </div>
</div>

<div class="wrap-static-policy">
    <div class="module-title text-center">
      <h3 class="title">Join the over 4 million people using SuiteCRM!</h3>
  </div>     
  <div class="container">  
    <div class="row">
        <div class="col-xl-1 col-lg-1 d-none d-lg-block"></div>
        <div class="col-xl-10 col-lg-10 col-md-12">
            <div class="row">
                <?php foreach($sections as $section){ ?>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6">
                    <div class="item border text-center mb-3 rounded p-3">
                        <div class="image text-center mb-2">
                            <img src="<?php echo $section['image']; ?>" alt="<?php echo $section['description']; ?>" class="rounded-circle" style="width: auto">
                        </div>
                        <div class="content">
                            <h3 class="h1"><?php echo $section['title']; ?></h3>
                            <p><?php echo $section['description']; ?></p>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>  
</div>                                  
</div>