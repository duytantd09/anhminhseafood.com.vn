<div class="information-category-col box-category mb-3">
  <div class="box-title"><?php echo $heading_title; ?></div>
  <div class="box-content sidebar">
    <ul class="list-unstyled px-0 my-0">
      <?php foreach ($categories as $category) { ?>
      <?php if ($category['information_category_id'] == $information_category_id) { ?>
      <li class="open active"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
       <?php } else {?>
       <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
         <?php }?>
         <?php if ($category['children']) { ?>
         <div class="sign"><span class="plus"><i class="fa fa-angle-right" aria-hidden="true"></i></span><span class="minus"><i class="fa fa-angle-down" aria-hidden="true"></i></span></div>
         <ul class="list-unstyled px-0 my-0">
          <?php foreach ($category['children'] as $child) { ?>
          <?php if ($child['information_id'] == $information_id) { ?>
          <li class=" active"><a href="<?php echo $child['href']; ?>" ><?php echo $child['title']; ?></a></li>
          <?php } else { ?>
          <li><a href="<?php echo $child['href']; ?>"><?php echo $child['title']; ?></a></li>
          <?php } ?>
          <?php } ?>
        </ul>
        <?php } ?>
      </li>
      <?php } ?>
      <li>
        <a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a>
      </li>
    </ul>

  </div>
</div>