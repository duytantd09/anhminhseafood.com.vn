 <div class="wt_banner_home">
  <?php if ($banners) { ?>
  <div class="slider">
   <ul class="bxslider">
     <?php foreach ($banners as $banner) { ?>
     <li>
       <div class="wt_bxslider clearfix">
        <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
        <div class="slider-info">
          <div class="container text-center">
            <h2><?php echo $banner['title']; ?></h2>
            <p class="des"><?php echo $banner['description']; ?></p>  
            <a href="#" class="btn btn-secondary">Xem thêm</a>              
          </div>
        </div>
      </div>
    </li>

    <?php }?>
  </ul>

</div>

<script type="text/javascript">
  $('.bxslider').bxSlider({
    mode: 'fade',
    captions: true,                      
    minSlides: 1,
    maxSlides: 1,
    moveSlides: 1,
    slideMargin: 15,
    pager: false,
    controls:true,
    nextText: '<i class="fa fa-angle-right"></i>',
    prevText: '<i class="fa fa-angle-left"></i>'
  });
</script>
<?php }?>
</div>

