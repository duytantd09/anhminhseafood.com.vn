
<div class="container">
  <div class="wt_banner_home">
    <?php if ($banners) { ?>
    <div class="slider">
      <div class="owl-carousel owl-theme slider-carousel">
       <?php foreach ($banners as $i=>$banner) { ?>
       <a href="<?php echo $banner['link']; ?>">
         <div class="item">
          <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
          <div class="slider-info d-none ">
            <div class="container text-center text-white">
              <div class="row">
                <div class="d-none d-md-block col-md-2"></div>
                <div class="col-12 col-sm-12 col-md-8">
                  <h2 class="text-uppercase mb-4 h3"><?php echo $banner['title']; ?></h2>
                  <?php if($banner['description']){ ?>
                  <div class="des-div">
                    <p class="des mb3" id="des-<?php echo $i; ?>"><?php echo $banner['description']; ?></p> 
                  </div>
                  <?php }?>
                </div>
              </div>            
            </div>
          </div>
        </div>
      </a>
      <?php }?>
    </div>
  </div>
  <script type="text/javascript">
    var slide = $('.slider-carousel');
    slide.owlCarousel({
      animateOut: 'fadeOut',
      autoplayTimeout: 4000,
      smartSpeed: 2000,
      loop:true,
      items:1,
      lazyLoad: true,
      autoplay: true
    });

  </script>
  <?php }?>
</div>
</div>





