<div class="main-special-bg">    
    <div class="container">    
    <!--    <div class="deals_heading heading welcome"><?php echo $heading_title; ?></div> -->
        <div class="deals_wrapper">
            <?php foreach ($products as $product) { ?> 
            <div class="deals">
                <div class="deals_content">
                    <?php if ($product['thumb']) { ?>
                    <div class="image" style="width:<?php echo $images_width; ?>px;"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
                    <?php } ?>
                    <div class="info">
                        <div class="name contrast_font"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                        <?php if ($product['rating']) { ?>
                        <span class="rating r<?php echo $product['rating']; ?>">
                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                        <?php } ?>
                        <div class="description">
                        <?php echo $product['description']; ?>
                        </div>
                        <?php if ($product['price']) { ?>
                        <div class="extended_offer">
                        <?php if ($product['special']) { ?>
                        <div class="price-new"><?php echo $text_special_price; ?><span class="amount contrast_font"><?php echo $product['special']; ?></span></div>
                        <div class="price-old"><?php echo $text_old_price; ?><span class="amount contrast_font"><?php echo $product['price']; ?></span></div>
                        <div class="price-save"><?php echo $text_you_save; ?><span class="amount contrast_font"><?php echo $product['yousave']; ?></span> </div>
                        <?php } else { ?>
                        <div class="price-regular"><span class="amount price"><?php echo $product['price']; ?></span></div>
                        <?php } ?>
                        </div>        
                        <?php if (($product['special_date_end'] > 0) && ($tlptech_product_countdown)) { ?>1
                        <div class="contrast_font"><div class="offer <?php echo $product['product_id']; ?>"></div></div> 
                        <script type="text/javascript">
                        $('.offer.<?php echo $product['product_id']; ?>').countdown({
                        until: <?php echo $product['special_date_end']; ?>, 
                        layout: '<span class="main_font"><?php echo $text_expire; ?></span> <i>{dn}</i> {dl}&nbsp; <i>{hn}</i>  {hl}&nbsp; <i>{mn}</i>  {ml}&nbsp; <i>{sn}</i> {sl}'});
                        </script>
                        
                        <?php if ($tlptech_product_hurry) { ?>
                        <div class="hurry">
                            <span class="items_left contrast_color"><?php echo $product['stock_quantity']; ?></span>
                            <span class="items_sold"><?php echo $product['items_sold']; ?></span>
                        </div>
                        <?php } ?>
                        
                        <?php } ?>
                        <?php } ?> <!-- if price ends -->
                        <div class="cart">
                            <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="button contrast"><i class="fa fa-shopping-cart"></i> <?php echo $button_cart; ?></button>
                            <div class="icons_wrapper">
                                <a class="sq_icon" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" data-tooltip="<?php echo $button_wishlist; ?>"><i class="fa fa-heart"></i></a>
                                <a class="sq_icon compare" onclick="compare.add('<?php echo $product['product_id']; ?>');" data-tooltip="<?php echo $button_compare; ?>"><i class="fa fa-arrow-right"></i><i class="fa fa-arrow-left"></i></a>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
            <?php } ?>

        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
$('.deals_wrapper').owlCarousel({
singleItem : true,
mouseDrag:false,
autoPlay:<?php echo $autoplay; ?>,
stopOnHover:true,
autoHeight:true,
transitionStyle:"backSlide"
});
});
</script> 




<div class="description-static-policy">     
    <div class="container">  
        <div class="static-policy">                               
            <div class="row">
                <div class="col-md-3 col-xs-6">
                    <div class="col-img">
                        <i class="fa fa-comment"></i>
                    </div>
                    <div class="text-content1">
                        <h2>Đặt Online</h2>
                        <p>Giao hàng tận nhà</p>
                    </div>
                </div>
                <div class="col-md-3 col-xs-6">
                    <div class="col-img">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="text-content1">
                        <h2>Gửi Hàng</h2>
                        <p>Toàn quốc thanh toán COD</p>
                    </div>
                </div>
                <div class="col-md-3 col-xs-6">
                    <div class="col-img">
                        <i class="fa fa-tags"></i>
                    </div>
                    <div class="text-content1">
                        <h2>Giá Tốt Nhất</h2>
                        <p>Mọi thời điểm</p>
                    </div>
                </div>
                <div class="col-md-3 col-xs-6">
                    <div class="col-img">
                        <i class="fa fa-thumbs-o-up"></i>
                    </div>
                    <div class="text-content1">
                        <h2>100% Sản Phẩm</h2>
                        <p>Hàng chính hãng</p>
                    </div>
                </div>
            </div> 
        </div>  
    </div>                                  
</div>
<div class="trending-items">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 product_module">
                <div class="module-title">
                    <h2>
                        Mục xu hướng
                    </h2>
                    <div class="module-description">
                        Chúng tôi cung cấp lựa chọn tốt nhất của nước ép hữu cơ &amp; và lành mạnh, thực phẩm chế biến với giá bạn sẽ yêu thích!
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 tt-product">
                <div class="product-trending">
                    <?php foreach ($products as $product) { ?>
                    <div class="product-layout col-lg-4 col-md-4 col-sm-6 col-xs-12">
                      <div class="product-thumb transition">
                        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive1" /></a></div>
                          <!-- hover vào hiện ra -->
                          <div class="actions-link">                
                            <button class="btn-cart" type="button" data-toggle="tooltip" title="" onclick="cart.add('48');" data-original-title="Add to Cart">
                              <i class="fa fa-shopping-basket"></i><span>Add to Cart</span>
                            </button>
                            <button class="btn-compare" type="button" data-toggle="tooltip" title="" onclick="compare.add('48');" data-original-title="Add to Compare">
                              <i class="fa fa-compress"></i><span>Add to Compare</span>
                            </button>
                            <button class="btn-quickview" type="button" data-toggle="tooltip" title="" onclick="ocquickview.ajaxView('')" data-original-title="Quick View">
                              <i class="fa fa-eye"></i><span>Quick View</span>
                            </button>
                          </div>

                          <div class="caption">
                            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                         <!--   <p><?php echo $product['description']; ?></p> -->
                            <?php //if ($product['rating']) { ?>
                            <div class="rating">
                              <?php for ($i = 1; $i <= 5; $i++) { ?>
                              <?php if ($product['rating'] < $i) { ?>
                              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                              <?php } else { ?>
                              <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                              <?php } ?>
                              <?php } ?>
                            </div>
                            <?php //} ?>
                            <?php if ($product['price']) { ?>
                            <p class="price">
                              <?php if (!$product['special']) { ?>
                              <?php echo $product['price']; ?>
                              <?php } else { ?>
                              <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                              <?php } ?>
                              <?php if ($product['tax']) { ?>
                              <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                              <?php } ?>
                            </p>
                            <?php } ?>
                          </div>
                          <div class="button-group">
                            <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                          </div>
                        </div>
                    </div>
                  <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="cmsblock">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 banner-cmsblock1">
                <div class="col-img">
                    <a href="#"><img alt="banner 2" src="http://www.demo1004.sacmauweb.com/image/catalog/cmsblock/banner2-1.jpg"></a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 banner-cmsblock2">
                <div class="col-img">
                    <a href="#"><img alt="banner 2" src="http://www.demo1004.sacmauweb.com/image/catalog/cmsblock/banner2-2.jpg"></a>
                </div>
                <div class="col-img">
                    <a href="#"><img alt="banner 2" src="http://www.demo1004.sacmauweb.com/image/catalog/cmsblock/banner2-3.jpg"></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="trending-items">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 product_module">
                <div class="module-title">
                    <h2>
                        Người bán tốt nhất
                    </h2>
                    <div class="module-description">
                        Duyệt qua bộ sưu tập các sản phẩm thú vị &amp; và bán chạy nhất của chúng tôi. Bạn sẽ tìm thấy những gì bạn đang tìm kiếm.
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 tt-product">
                <div class="product-trending">
                    <?php foreach ($products as $product) { ?>
                    <div class="product-layout col-lg-4 col-md-4 col-sm-6 col-xs-12">
                      <div class="product-thumb transition">
                        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive1" /></a></div>
                          <!-- hover vào hiện ra -->
                          <div class="actions-link">                
                            <button class="btn-cart" type="button" data-toggle="tooltip" title="" onclick="cart.add('48');" data-original-title="Add to Cart">
                              <i class="fa fa-shopping-basket"></i><span>Add to Cart</span>
                            </button>
                            <button class="btn-compare" type="button" data-toggle="tooltip" title="" onclick="compare.add('48');" data-original-title="Add to Compare">
                              <i class="fa fa-compress"></i><span>Add to Compare</span>
                            </button>
                            <button class="btn-quickview" type="button" data-toggle="tooltip" title="" onclick="ocquickview.ajaxView('')" data-original-title="Quick View">
                              <i class="fa fa-eye"></i><span>Quick View</span>
                            </button>
                          </div>

                          <div class="caption">
                            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                         <!--   <p><?php echo $product['description']; ?></p> -->
                            <?php //if ($product['rating']) { ?>
                            <div class="rating">
                              <?php for ($i = 1; $i <= 5; $i++) { ?>
                              <?php if ($product['rating'] < $i) { ?>
                              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                              <?php } else { ?>
                              <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                              <?php } ?>
                              <?php } ?>
                            </div>
                            <?php //} ?>
                            <?php if ($product['price']) { ?>
                            <p class="price">
                              <?php if (!$product['special']) { ?>
                              <?php echo $product['price']; ?>
                              <?php } else { ?>
                              <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                              <?php } ?>
                              <?php if ($product['tax']) { ?>
                              <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                              <?php } ?>
                            </p>
                            <?php } ?>
                          </div>
                          <div class="button-group">
                            <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                          </div>
                        </div>
                    </div>
                  <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>