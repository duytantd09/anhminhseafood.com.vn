<div class="deals">
  <div class="container">
    <div class="module-title text-center">
      <a href="<?php echo $link;?>"><h3 class="title"><?php echo $heading_title;?></h3></a>
    </div>
    <div class="module-description text-center"><?php echo $description;?></div>
    <div class="owl-carousel owl-theme deals-carousel">
      <?php foreach ($products as $product) { ?>
      <div class="item-carousel">
        <div class="row">
          <div class="col-lg-2 d-none d-lg-block"></div>
          <div class="col-lg-4 col-md-6 col-12 text-center">
            <div class="product-deal">
              <a href="<?php echo $product['href']; ?>"><h4 class="name"><?php echo $product['name']; ?></h4></a>
              <p class="product-des text-justify"><?php echo $product['description']; ?></p>
              <div class="label-product">Sale - 20% off</div>
              <?php if ($product['price']) { ?>
              <div class="extended_offer">
                <?php if ($product['special']) { ?>
                <div class="price-new"><?php echo $text_special_price; ?><span class="amount contrast_font"><?php echo $product['special']; ?></span></div>
                <div class="price-old"><?php echo $text_old_price; ?><span class="amount contrast_font"><?php echo $product['price']; ?></span></div>
                <div class="price-save"><?php echo $text_you_save; ?><span class="amount contrast_font"><?php echo $product['yousave']; ?></span> </div>
                <?php } else { ?>
                <div class="price-regular"><span class="amount price"><?php echo $product['price']; ?></span></div>
                <?php } ?>
              </div>        
              <?php if (($product['special_date_end'] > 0) && ($tlptech_product_countdown)) { ?>
              <div class="contrast_font"><div class="offer <?php echo $product['product_id']; ?>"></div></div> 
              <script type="text/javascript">
                $('.offer.<?php echo $product['product_id']; ?>').countdown({
                  until: <?php echo $product['special_date_end']; ?>, 
                  layout: '<span class="main_font"><?php //echo $text_expire; ?></span> <i>{dn}</i> {dl}&nbsp; <i>{hn}</i>  {hl}&nbsp; <i>{mn}</i>  {ml}&nbsp; <i>{sn}</i> {sl}'});
                </script>
                
                <?php if ($tlptech_product_hurry) { ?>
                <div class="hurry">
                  <span class="items_left contrast_color"><?php echo $product['stock_quantity']; ?></span>
                  <span class="items_sold"><?php echo $product['items_sold']; ?></span>
                </div>
                <?php } ?>
                
                <?php } ?>
                <?php } ?> <!-- if price ends -->
              </div>
              <div class="product-intro">
                <a href="<?php echo $product['href']; ?>" ><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo $button_cart; ?></a>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 text-center">
              <div class="product-inner-img">
                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $('.deals-carousel').owlCarousel({
      nav:true,
      center: true,
      items:1,
      loop:true,
      margin:40,
      dots: false,
      navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
      responsive:{
        600:{
          items:1
        }
      }
    });
  </script>