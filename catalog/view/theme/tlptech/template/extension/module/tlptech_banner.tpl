<?php if($columns == 'column1'){?>
<!-- 3 lý do chon Bestcrm -->
<div class="tlp-banner1 bg-light">
  <div class="container">
    <div class="module-title text-center">
      <?php if ($module_title){ ?>
      <h3 class="title"><?php echo $module_title; ?></h3>
      <?php } ?>
    </div>
    <div class="row align-items-center">
      <div class="col-xl-4 col-lg-4 col-md- col-sm-12 col-12 ">
        <img src="<?php echo $sections[0]['image']; ?>" alt="<?php echo $sections['0']['title']; ?>" />
      </div>
      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 ">
        <?php foreach($sections as $section){ ?>
        <div class="reason">
          <h3><?php echo $section['title']; ?></h3>
          <p><?php echo $section['description']; ?></p>
        </div>
        <?php }?>
      </div>
    </div>
  </div>
</div>
<?php } elseif($columns == 'column2'){?>
<div class="customer-comments" style="background: linear-gradient(rgba(0,0,0,0.5), rgba(0,0,0,0.5)),url('https://www.webmaucaocap.com/onecrm//image/cache/catalog/banner/top_cta_large-cr-1700x500.jpg')">
  <div class="module-title text-center mt-0">
    <?php if ($module_title){ ?>
    <h3 class="title text-white"><?php echo $module_title; ?></h3>
    <?php } ?>
  </div>
  <div class="container-fluid">
    <div class="owl-carousel owl-theme customer-comments-carousel">
      <?php foreach($sections as $section){ ?>
      <div class="item-customer-comments-carousel">
        <div class="slide-content clearfix"> 
          <img src="<?php echo $section['image']; ?>" alt="<?php echo $section['title']; ?>" /> 
          <div class="home-testim text-center"> 
            <div class="home-testim-name"><?php echo $section['title']; ?></div>                         
            <div class="home-testim-text"><?php echo $section['description']; ?></div> 
          </div> 
        </div>
      </div>
      <?php }?>
    </div>
  </div>
</div>
<script type="text/javascript">
 $('.customer-comments-carousel').owlCarousel({
  nav:true,
  //center: true,
  items:1,
  loop:true,
  margin:40,
  dots: false,
  navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
  responsive:{
    600:{
      items:1
    }
  }
});
</script>
<?php } else {?>
<!-- BestCrm làm gì? -->
<div class="wt_fourcol">
  <div class="container">
    <div class="module-title text-center">
      <?php if ($module_title){ ?>
      <h3 class="title"><?php echo $module_title; ?></h3>
      <?php } ?>
    </div>
    <div class="row">
      <?php foreach($sections as $section){ ?>
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
        <div class="col-wapper rounded text-center">
          <div class="image ">
            <a href="<?php echo $section['link']; ?>" title="<?php echo $section['title']; ?>">
              <img src="<?php echo $section['image']; ?>" alt="<?php echo $section['title']; ?>" />
            </a>
          </div>
          <div class="h3-bg">
            <h3><?php echo $section['title']; ?></h3>
            <p><?php echo $section['description']; ?></p>
          </div>
          <a href="<?php echo $section['link']; ?>" title="<?php echo $section['title']; ?>" class="wt_fourcol_a">Xem thêm...</a>
        </div>
      </div>
      <?php }?>
    </div>
  </div>
</div>
<?php }?>