<?php if($menus){ ?>
<div class="menu_vertical">
    <ul>
    <?php foreach ($menus as $menu) { ?>
        <li>
            <?php if ($menu['href']) { ?>
            <span>
                <a href="<?php echo $menu['href']; ?>"><?php echo $menu['name']; ?></a>
            </span> 
            <?php } else { ?>
            <span>
                <a><?php echo $menu['name']; ?></a>
            </span> 
            <span class="glyphicon glyphicon-chevron-right"></span>                 
            <?php } ?>
            <?php if($menu['childrens']) { ?>
            <ul class="vertical_child">
            <?php foreach ($menu['childrens'] as $children) { ?>
                <li><a id="<?php echo $children['category_id']; ?>" href="<?php echo $children['href']; ?>" title="<?php echo $children['name']; ?>"><span><img src="<?php echo $children['thumb']; ?>" alt="<?php echo $children['name']; ?>" /></span><?php echo $children['name']; ?></a></li>
            <?php } ?>
            </ul>
            <?php } ?>
        </li>
        <?php } ?>
    </ul>
</div>
<?php } elseif ($modules) { ?>
    <?php foreach ($modules as $module) { ?>
    <div class="col-sm-4">
        <div class="footer_h5_mobie recruitment_active"><h5><?php echo $module['name']; ?></h5></div>
        <div class="footer_h5_mobie_content">
             <ul class="list-unstyled">
             <?php foreach ($module['menus'] as $menu) { ?>
                <li><a href="<?php echo $menu['href']; ?>"><?php echo $menu['name']; ?></a></li>
             <?php } ?> 
             </ul>
         </div>
    </div>   
    <?php } ?>
<?php } ?>