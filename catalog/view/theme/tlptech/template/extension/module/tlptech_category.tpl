<div class="product-category box-category mb-3">
<div class="box-content">
  <ul class="list-unstyled px-0 my-0">
   <?php $dem=0; foreach ($categories as $category_1) { $dem++; ?>
   <?php if ($category_1['category_1_id'] == $category_id) { ?>
   <li class="open active"><a href="<?php echo $category_1['href']; ?>" ><span><?php echo $dem;?></span><?php echo $category_1['name']; ?></a>
    <?php } else { ?>
    <li><a href="<?php echo $category_1['href']; ?>" ><span><?php echo $dem;?></span><?php echo $category_1['name']; ?></a> 
      <?php } ?>
      <?php if ($category_1['children']) { ?>
      <div class="sign"><span class="plus"><i class="fa fa-angle-down" aria-hidden="true"></i></span><span class="minus"><i class="fa fa-angle-up" aria-hidden="true"></i></span></div>
      <ul class="list-unstyled px-0 my-0">
        <?php foreach ($category_1['children'] as $category_2) { ?>
        <?php if ($category_2['category_2_id'] == $child_id) { ?>
        <li class="open active"><a href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?></a>
          <?php } else { ?>
          <li><a href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?></a>
            <?php } ?>
            <?php if ($category_2['children']) { ?>
            <div class="sign"><span class="plus">+</span><span class="minus">-</span></div>
            <ul class="list-unstyled px-0 my-0">
              <?php foreach ($category_2['children'] as $category_3) { ?>
              
              <?php if ($category_3['category_3_id'] == $child_child_id) { ?>
              <li class="active"><a href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a></li>
              <?php } else { ?>
              <li><a href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a></li>
              <?php } ?>
              <?php } ?>
            </ul>
            <?php } ?>
          </li>
          <?php } ?>
        </ul>
        <?php } ?>
      </li>
      <?php } ?>
    </ul>
  </div>
</div>
