<!-- carousel phiên bản cũ --> 
<div class="evaluation-strategy">
    <div class="container">
        <div class="home_carousel_wt">
            <div class="wrapper_content">
                <div class="">
                    <div class="home_carousel">
                        <div class="grid_holder_carousel">
                            <div class="bannercarousel <?php echo $module; ?>">
                                <?php foreach ($banners as $banner) { ?>
                                <div class="carousel_item">
                                    <?php if ($banner['link']) { ?>
                                    <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" title="<?php echo $banner['title']; ?>" /></a>
                                    <?php } else { ?>
                                    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" title="<?php echo $banner['title']; ?>" />
                                    <?php } ?>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript"><!--
    $(document).ready(function () {
        $('.bannercarousel.<?php echo $module; ?>').owlCarousel({
            /*itemsCustom: <?php echo $columns; ?>,*/
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            pagination: false,
            navigation: true,
            slideSpeed: 1000,
            autoHeight: true,
            navigationText: [
            "<div class='slide_arrow_prev'><i class='fa fa-angle-left'></i></div>",
            "<div class='slide_arrow_next'><i class='fa fa-angle-right'></i></div>"
            ]
        });
    });
    //--></script>

</div>