<div class="col-contact-info">
	<div class="title"><?php if($heading_title) { ?><?php echo $heading_title; ?><?php } ?></div>
	<div class="info">
	  <?php if($html) { ?>
	  <div class="row m-0 py-2 align-items-center">
	  <?php echo $html; ?>
	  </div>
	  <?php } ?>
	  <?php foreach ($banners as $banner) { ?>
	  <div class="row m-0 py-2 align-items-center">
	    <div class="col-3 px-2">
	      <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
	    </div>
	    <div class="col-9 pr-2 pl-0">
	      <div class="position">
	        <?php echo $banner['title']; ?><br>
	        <?php echo $banner['description']; ?><br>
	      </div>
	    </div>
	  </div>
	  <?php } ?>
	</div>
</div>