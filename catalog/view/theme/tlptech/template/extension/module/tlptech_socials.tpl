<ul class="share list-unstyled list-inline px-0">
<?php foreach($sections as $section){ ?>
<li class="list-inline-item"><a href="<?php echo $section['link']; ?>" title=""  <?php if ($section['tooltip']){ ?> data-tooltip="<?php echo $section['tooltip']; ?>" <?php } ?>><i class="fa <?php echo $section['social_type']; ?>" aria-hidden="true"></i></a></li>
<?php }?>
</ul>