<div class="">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class=" box products <?php echo $columns; ?> box_category box-homepage home-blocks">
                    <?php if (count($tabs) <= 1) { ?>
                    <div class="clearfix"> 
                        <div class="home-title">
                            <?php foreach ($tabs as $keyTab => $tab) { ?>
                            <h2><?php echo $tab['title']; ?></h2>
                        </div>
                        <?php } ?>
                        <?php } else { ?>
                        <div class="c_slider_wrapper <?php echo $module; ?>">
                            <div class=" box-heading product-tabs">
                                <ul id="tabs-<?php echo $module; ?>" class="nav nav-tabs tab_border carousel_tab_slider" data-tabs="tabs">
                                    <?php foreach ($tabs as $keyTab => $tab) { ?>
                                    <?php if($keyTab == 0) { ?>
                                    <li class="active text-center">
                                        <a href="#tab<?php echo $module; ?><?php echo $keyTab; ?>" data-toggle="tab">
                                            <?php if($tab['thumb']){ ?>
                                            <img src="<?php echo $tab['thumb']; ?>" alt="<?php echo $tab['title']; ?>" style="width:50px;" />
                                            <?php } ?>
                                            <div><?php echo $tab['title']; ?></div>
                                        </a>
                                    </li>
                                    <?php } else { ?>
                                    <li class="text-center">
                                        <a href="#tab<?php echo $module; ?><?php echo $keyTab; ?>" data-toggle="tab">
                                            <?php if($tab['thumb']){ ?>
                                            <img src="<?php echo $tab['thumb']; ?>" alt="<?php echo $tab['title']; ?>" style="width:50px;" />
                                            <?php } ?>
                                            <div><?php echo $tab['title']; ?></div>
                                        </a>
                                    </li>
                                    <?php } ?>
                                    <?php } ?>
                                </ul>
                            </div>
                            <?php } ?>
                            <div class="tab-content">
                                <?php foreach ($tabs as $key => $tab) { ?>
                                <div class="tab-pane <?php echo (empty($key) ? 'active' : ''); ?>" id="tab<?php echo $module; ?><?php echo $key; ?>">
                                    <div class="grid_holder">
                                        <div class="product-grid <?php echo $module; ?>"" <!--<?php if ($carousel) { ?>carousel<?php } ?>-->>
                                            <div class="row">
                                                <?php foreach ($tab['products'] as $product) { ?>
                                                <div class="item contrast_font product-layout">
                                                    <div class="row_item"> 
                                                        <div class="image">
                                                            <?php if ($product['special'] && $tlptech_percentage_sale_badge == 'enabled') { ?>
                                                            <div class="sale-off">-<?php echo $product['sales_percantage']; ?>%</div>
                                                            <?php } ?>
                                                            <?php if ($product['thumb_hover'] && $tlptech_rollover_effect == 'enabled') { ?>
                                                            <div class="image_hover"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb_hover']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
                                                            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
                                                            <?php } elseif ($product['thumb']) { ?>
                                                            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
                                                            <?php } ?>

                                                            <?php if ($tlptech_text_ql) { ?>
                                                            <div class="main_quicklook">
                                                                <a href="<?php echo $product['quickview']; ?>" rel="nofollow" class="button quickview"><i class="fa fa-eye"></i> <?php echo $tlptech_text_ql; ?></a>
                                                            </div>
                                                            <?php } ?>
                                                            <?php if ($product['new']!='') { ?>
                                                            <div class="new"><?php echo $product['new']; ?></div>
                                                            <?php } ?>
                                                            <?php if ($product['hot']!='') { ?>
                                                            <div class="hot"><?php echo $product['hot']; ?></div>
                                                            <?php } ?>

                                                        </div><!-- image ends -->
                                                        <div class="information_wrapper">
                                                            <div class="left">
                                                                <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                                                                <?php if ($product['brand_name'] && $tlptech_brand) { ?>
                                                                <span class="brand main_font"><?php echo $product['brand_name']; ?></span>
                                                                <?php } ?>
                                                                <?php if ($product['rating'] || 1==1) { ?>
                                                                <div class="rating">
                                                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                                    <?php if ($product['rating'] < $i) { ?>
                                                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                                    <?php } else { ?>
                                                                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                                    <?php } ?>
                                                                    <?php } ?>
                                                                </div>
                                                                <?php } ?>
                                                                <button class="btn favorite-btn" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" data-tooltip="<?php echo $button_wishlist; ?>"><i class="fa fa-heart" aria-hidden="true"></i></button>
                                                            </div>
                                                            <div class="description main_font"><?php echo $product['description']; ?></div>
                                                            <?php if ($product['price']) { ?>
                                                            <div class="price">
                                                                <?php if (!$product['special']) { ?>
                                                                <?php echo $product['price']; ?>
                                                                <?php } else { ?>
                                                                <span class="price-new"><?php echo $product['special']; ?></span><span class="price-old"><?php echo $product['price']; ?></span> 
                                                                <?php } ?>
                                                                <?php if ($product['tax']) { ?>
                                                                <br />
                                                                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                                                <?php } ?>
                                                            </div>
                                                            <?php } ?>
                                                            <div class="group-btn">
                                                                <button class="btn buy-btn" onclick="cart.buy('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" data-tooltip="<?php echo $button_buy; ?>"><?php echo $button_buy; ?></button>
                                                                <button class="btn add-to-cart" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" data-tooltip="<?php echo $button_cart; ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i></button>
                                                            </div>
                                                            <?php if (($product['special']) && ($product['special_date_end'] > 0) && ($tlptech_product_countdown)) { ?>
                                                            <div class="offer_popup">
                                                                <div class="offer_background"></div>
                                                                <div class="offer_content">
                                                                    <div class="countdown <?php echo $product['product_id']; ?>"></div>
                                                                    <?php if ($tlptech_product_hurry) { ?>
                                                                    <span class="hurry main_font"><?php echo $product['stock_quantity']; ?></span>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <script type="text/javascript">
                                                                $('.<?php echo $product['product_id']; ?>').countdown({
                                                                    until: < ?php echo $product['special_date_end']; ? > ,
                                                                    layout: '<span class="main_font"><?php echo $text_category_expire; ?></span><br /><i>{dn}</i> {dl}&nbsp; <i>{hn}</i>  {hl}&nbsp; <i>{mn}</i>  {ml}&nbsp; <i>{sn}</i> {sl}'});</script>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>    
            </div>

            <?php if ($carousel) { ?>
            <script type="text/javascript"><!--
            $(document).ready(function() {
                var grid5 = 5;
                var grid4 = 4;
                var grid3 = 3;
                var grid1 = 1;
                $('.carousel_tab_slider').owlCarousel({
                    itemsCustom: [ [0, 1], [320, 2], [550, 3], [1024, 5], [1025, 6]],
                    pagination: false,
                    navigation:true,
                    slideSpeed:500,
                    scrollPerPage:false,
                    autoPlay : false,
                    navigationText: [
                    "<div class='slide_arrow_prev'><i class='fa fa-angle-left'></i></div>",
                    "<div class='slide_arrow_next'><i class='fa fa-angle-right'></i></div>"
                    ]
                });
                $('.c_slider_wrapper.<?php echo $module; ?> .nav-tabs li a[index=1]').parent().addClass('active');
                $('.c_slider_wrapper.<?php echo $module; ?> .tab-content .tab-pane[index=1]').addClass('active');
                $('.c_slider_wrapper.<?php echo $module; ?> .nav-tabs li a').on('click', function () {
                    $(this).parent().parent().parent().parent().find('li').removeClass('active')
                    $(this).parent().addClass('active')
                    $('.c_slider_wrapper.<?php echo $module; ?> .tab-content .tab-pane').removeClass('active');
                    $('.c_slider_wrapper.<?php echo $module; ?> .tab-content ' + $(this).attr('href')).addClass('active');
                // return false;
            });
            });
            //--></script>
            <script type="text/javascript"><!--
            $(document).ready(function() {
                var grid5 = 5;
                var grid4 = 4;
                var grid3 = 3;
                var grid1 = 1;
                $('.product-grid.<?php echo $module; ?>').owlCarousel({
                    itemsCustom: [ [0, 1], [320, 2], [550, 3], [768, 3], [1024, 4], [1025, < ?php echo $columns; ? > ]],
                    pagination: false,
                    navigation:true,
                    slideSpeed:500,
                    scrollPerPage:false,
                    autoPlay : false,
                    navigationText: [
                    "<div class='slide_arrow_prev'><i class='fa fa-angle-left'></i></div>",
                    "<div class='slide_arrow_next'><i class='fa fa-angle-right'></i></div>"
                    ]
                });
            });
            //--></script>
            <?php } ?>