<div class="wt_blog_latest">
    <div class="wrapper_content">
    	<div class="box_title"><h2><?php echo $heading_title_latest; ?></h2></div>
        <div class="box-content latest">
        <?php if(!empty($posts)){ ?>
    	   <ul class="clearfix bxslider_bloglatest carousel-<?php echo $carousel; ?> column-<?php echo $columns; ?>">
              <?php foreach ($posts as $blog) { ?>  
              <li class="slide">
                <a href="<?php echo $blog['href']; ?>" title="<?php echo $blog['title']; ?>">
                    <div class="wt_content_bloglatest">
                    <?php if($blog['image'] && $thumb){ ?>
                    <img src="<?php echo $blog['image']; ?>" atl="<?php echo $blog['title']; ?>" />
                    <?php } ?>
                    <div class="name_blog"><?php echo $blog['title']; ?></div>
                    </div>
                </a>
              </li>
              <?php } ?>
            </ul>
    	<?php } ?>
        </div>
    </div>
</div>


<?php if($carousel) { ?>
<script type="text/javascript">
$(document).ready(function(){
    $('.bxslider_bloglatest.carousel-<?php echo $carousel; ?>').bxSlider({
    	  minSlides:2 ,
          maxSlides: <?php echo $columns; ?>,
          slideWidth: 320,
          slideMargin: 20,
          nextText: '<i class="fa fa-angle-right"></i>',
		  prevText: '<i class="fa fa-angle-left"></i>'
    });
});
</script>
<?php } ?>




<div class="featured-news">
    <div class="container">
        <div class="">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 xs-cloud">
                    <div class="home-title">
                        VIDEO
                        <span class="text-center"></span>
                    </div>
                    <div class="slider_content">
                        <div class="slider5">
                            <div class="slide">
                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/KLFQ8BZ8UWo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>
                            <div class="slide">
                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/1xcmhOuUnU4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>
                            <div class="slide">
                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/DXPFjn9Sr1I" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>                            
                        </div>
                    </div>
        <script type="text/javascript">
        $(document).ready(function(){
          $('.slider5').bxSlider({
            mode: 'fade',
            nextText: '<i class="fa fa-chevron-right"></i>',
            prevText: '<i class="fa fa-chevron-left"></i>'
          });
        });
        </script>
                <!--    <div class="iframe_height image col-sm-12"> 
                        <div class="wrap-video"><iframe width="560" height="315" src="https://www.youtube.com/embed/KLFQ8BZ8UWo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div> 
                    </div> -->
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="home-title">
                        TIN TỨC NỔI BẬT
                        <span class="text-center"></span>
                    </div>
                    <?php if(!empty($posts)){ ?>
                    <?php foreach ($posts as $blog) {?>
                    <div class="box-post"> 
                        <div class="row"> 
                            <div class="clearfix"> 
                                <div class="col-sm-8 col-xs-8"> 
                                    <div class="block-analytics"> 
                                        <!-- <span class="rating r0"> 
                                            <i class="fa fa-star green-blue"></i>
                                            <i class="fa fa-star green-blue"></i>
                                            <i class="fa fa-star green-blue"></i>
                                            <i class="fa fa-star green-blue"></i>
                                            <i class="fa fa-star"></i>
                                        </span> -->
                                        <div class="clock-date"><i class="fa fa-calendar"></i><?php echo $blog['date_added']; ?></div>
                                    </div>                                     
                                    <div class="item-name pull-left"> 
                                        <span>
                                            <a href="<?php echo $blog['href']; ?>" title="<?php echo $blog['title']; ?>"><?php echo $blog['title']; ?></a>
                                        </span> 
                                        <div class="item-intro pull-left"><?php echo $blog['description']; ?></div> 
                                    </div> 
                                </div> 
                                <div class="col-sm-4 col-xs-4">
                                    <a href="<?php echo $blog['href']; ?>" title="<?php echo $blog['title']; ?>">
                                        <div class="hover">
                                            <img src="<?php echo $blog['image']; ?>" atl="<?php echo $blog['title']; ?>" />
                                        </div>
                                    </a>
                                </div>                                 
                            </div> 
                        </div> 
                    </div>
                    <?php }} ?>                                       
                </div>
            </div>
        </div>
    </div>
</div>