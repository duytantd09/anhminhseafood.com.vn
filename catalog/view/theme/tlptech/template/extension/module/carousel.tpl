<div class="pt-5" style="background-color: #f9f9fd">
    <div class="container">
        <div class="module-title text-center">
            <h3 class="title"><?php echo $information_c_title;?></h3>
        </div> 
        <div class="module-description text-center">
            <?php echo $information_c_description;?>
            <a href="<?php echo $information_c_link; ?>">Xem thêm</a>
        </div>
    </div>
</div>
<div class="module-carousel">
    <div class="container">
        <div id="module-carousel" class="owl-carousel owl-theme">
            <?php foreach ($banners as $banner) { ?>
            <div class="item">
                <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" title="<?php echo $banner['title']; ?>" /></a>
            </div><?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#module-carousel').owlCarousel({
        auto: true,
        loop:true,
        margin:10,
        dots: false,
        nav: true,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        responsive:{
            0:{
                items:2
            },
            600:{
                items:5
            },
            1000:{
                items:7
            }
        }
    })
</script>

