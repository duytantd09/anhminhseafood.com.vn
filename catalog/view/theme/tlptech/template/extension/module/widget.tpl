<?php if( $display == 'box'){ ?>
<!-- wt_slider_bottom -->
<?php if($id == 49){ ?>
<div class="container">
    <div class="row">
    <?php if($widgets){?>
		<?php foreach ($widgets as $widget) { ?>
		<div class="col-sm-3 col-xs-6 top_footer_wt">
	        <div class="clearfix">
                <div class="img"><img src="<?php echo $widget['thumb']; ?>" alt="<?php echo $widget['title']; ?>" /></div>
                <div class="p">
                	<div><b><?php echo $widget['title']; ?></b></div>
                	<div><a href="<?php echo $widget['link']; ?>" title="<?php echo $widget['title']; ?>"><?php echo $widget['desciption']; ?></a></div>
               </div>
            </div>
        </div>
		<?php } ?>
	<?php } ?>
    </div>
</div>
<?php } else { ?>
<section class="wt_slider_bottom">		
	<div class="row">
		<?php if($widgets){?>
		<?php foreach ($widgets as $widget) { ?>			
			<!-- start -->
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 xs_25">	
			     <div class="product-thumb transition">	
                    <div class="slider_bottom_content md-shadow-z-1">					
                        <a href="<?php echo $widget['link']; ?>" title="<?php echo $widget['title']; ?>"><img src="<?php echo $widget['image']; ?>" alt="<?php echo $widget['title']; ?>" /></a>					
                    </div>
                </div>
			</div>
			<!-- end -->
		<?php } ?>
		<?php } ?>			
	</div>
</section><!-- wt_slider_bottom -->
<?php } ?>
<?php } elseif ($display == 'tab') { ?>
<?php if($widgets){ $tab_active = 0;  ?>
<div role="tabpanel">
	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
	<?php foreach ($widgets as $widget) { ?>
		<li role="presentation" <?php if($tab_active == 0){ echo 'class="active"';} ?>>
			<a aria-expanded="true" href="#<?php echo $widget['tab']; ?>" aria-controls="home" role="tab" data-toggle="tab" class="caption-subject theme-font-color bold uppercase"><?php echo $widget['title']; ?></a>
		</li>
	<?php $tab_active++; } ?>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<?php $tab_active = 0;  ?>
		<?php foreach ($widgets as $widget) { ?>
		<div role="tabpanel" class="tab-pane <?php if($tab_active == 0){ echo 'active';} ?>" id="<?php echo $widget['tab']; ?>">	
			<div class="news_aboutus_content">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">		
						<img src="<?php echo $widget['image']; ?>" alt="<?php echo $widget['title']; ?>">
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">										
						<p><?php echo html_entity_decode($widget['desciption']); ?></p>
						<div>
							<a class="btn btn-default" href="<?php echo $widget['link']; ?>">Đăng ký</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php $tab_active++; } ?>
	</div>
</div>
<?php } ?>
<?php }  elseif ($display == 'terminal') { ?>

<div class="wt_customer_reviews">
	<h2><?php echo $title; ?></h2>
	<?php foreach ($widgets as $widget) { ?>
	<div class="customer_reviews_content md-shadow-z-1">
		<h3><?php echo $widget['title']; ?></h3>
		<?php echo html_entity_decode($widget['desciption']); ?>
	</div>
	<?php } ?>
<!-- end -->				
</div>

<?php } ?>