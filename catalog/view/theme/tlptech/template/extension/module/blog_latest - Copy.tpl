
<div class="">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="wt_blog_latest">
                    <div class="wrapper_content">
                        <div class="home-blocks">
                            <div class="home-title">
                                <h2><?php echo $heading_title_latest; ?></h2>
                            </div>
                            
                            <div class="box-content latest">
                                <div class="row">
                                    <?php if(!empty($posts)){ ?>
                                    <?php foreach ($posts as $blog) { ?>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col">
                                        <div class="blog-item box_bolg">
                                            <div class="">
                                                <div class=" hover">
                                                    <a href="<?php echo $blog['href']; ?>" title="<?php echo $blog['title']; ?>">
                                                        <?php if($blog['image'] && $thumb){ ?>
                                                        <img src="<?php echo $blog['image']; ?>" atl="<?php echo $blog['title']; ?>" />
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                                <div class="">
                                                    <h2><a href="<?php echo $blog['href']; ?>" title="<?php echo $blog['title']; ?>"><?php echo $blog['title']; ?></a></h2>
                                                    <div class="description"><?php echo $blog['description']; ?></div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="blocks-title">
                                <ul>
                                    <?php foreach($blogs as $blog_root) { ?>
                                    <li><a href="<?php echo $blog_root['href']; ?>" title="<?php echo $blog_root['name']; ?>"><?php echo $blog_root['name']; ?></a></li>
                                    <?php } ?>
                                </ul>
                                <a href="<?php echo $blog_show_all;?>" title="<?php echo $heading_title_latest; ?>" class="view-all-blog"> Xem tất cả <span><i class="fa fa-play-circle" aria-hidden="true"></i></span></a>
                                <div style="clear: both;"></div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>