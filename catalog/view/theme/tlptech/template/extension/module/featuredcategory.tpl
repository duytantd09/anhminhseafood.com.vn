<div class="featured-category">
  <div class="container">
    <div class="module-title text-center">
      <h3 class="title"><?php echo $name; ?></h3>
    </div>
    <div class="row">
      <?php foreach ($categories as $category) { ?>
      <div class="col-lg-6 col-md-6 col-sm-12 col-12 fc-col">

        <div class="category_box clearfix mb-3">
          <div class="name-box">
            <div class="info p-2 text-white">
              <a href="<?php echo $category['href']; ?>"><h3 class="name mb-3"><?php echo $category['name']; ?></h3></a>
              <div class="short-des d-none d-md-block"><?php echo $category['description']; ?></div>
            </div>
          </div> 
          <div class="image zoom-image-container">
            <a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" class="zoom-image" /></a>
          </div> 
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</div>