<div class="blog-latest-v2">
    <div class="container">
        <div class="module-title text-center">
            <h3 class="title"><?php echo $heading_title_latest; ?></h3>
        </div>
        <div class="owl-carousel owl-theme latest-blog">
            <?php if(!empty($posts)){ ?>
            <?php foreach ($posts as $blog) { ?> 
            <div class="item-blog mb-3 border rounded">
                <div class="img-item-blog">
                    <a href="<?php echo $blog['href']; ?>" title="<?php echo $blog['title']; ?>">
                        <img src="<?php echo $blog['image']; ?>" atl="<?php echo $blog['title']; ?>" class="rounded-top"/>
                    </a>
                </div>                          
                <div class="content-item-blog p-3"> 
                    <a href="<?php echo $blog['href']; ?>" title="<?php echo $blog['title']; ?>">
                        <h5 class="name"><?php echo $blog['title']; ?></h5>
                    </a>
                    <div class="clock-date text-muted"><small><i class="fa fa-calendar"></i>  <?php echo $blog['date_added']; ?></small></div> 
                    <div class="item-intro"><?php echo $blog['description']; ?></div> 
                </div> 
            </div>
            <?php } ?>
            <?php } ?>
        </div>
    </div>        
</div>

<script type="text/javascript">
  $('.latest-blog').owlCarousel({
    autoplay: true,
    loop:true,
    margin:15,
    items:4,
    nav: true,
    dots: false,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    responsive:{
      0:{
        items:2
      },
      600:{
        items:4
      }
    }
  });
</script>  