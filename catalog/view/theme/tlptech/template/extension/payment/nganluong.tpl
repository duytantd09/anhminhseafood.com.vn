<div class="buttons">
  <div class="pull-right"><a id="button-confirm" class="button"><span><?php echo $button_confirm; ?></span></a></div>
</div>
<script type="text/javascript"><!--
$('#button-confirm').on('click', function() {
	//if (!checkTerms()) { return false; }
	$.ajax({ 
		type: 'GET',
		url: 'index.php?route=extension/payment/nganluong/confirm',
		cache: false,
		beforeSend: function() {
			$('#button-confirm').button('loading');
		},
		complete: function() {
			$('#button-confirm').button('reset');
		},
		success: function() {
			location = '<?php echo $continue; ?>';
		}		
	});
});
//--></script> 