<?php echo $header; ?>
<?php echo $content_top; ?>
<div class="bg-light breadcrumb-w">
  <div class="container">
    <nav aria-label="breadcrumb">
     <ol class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li class="breadcrumb-item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
  </ol>
</nav>
</div>
</div>
<div class="container">
    <div class="blog-detail">
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-lg-9 col-md-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>">
            <div class="blog_post">
                <?php if($main_thumb && $blogsetting_post_thumb){ ?>
                <div class="main_thumb hover">
                    <img src="<?php echo $blogsetting_post_thumb; ?>" class="zoom_image" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" />
                </div>
                <?php } ?>
                <div class="blog_summary">
                    <h1><?php echo $heading_title; ?></h1>
                    <div class="right">
                        <?php if($date_added_status){ ?>
                        <p class="post-date"><span><i class="fa fa-clock-o" aria-hidden="true"></i></span> <?php echo $date_added;?></p>
                        <?php } ?>
                        <?php echo $description; ?>
                    </div>
                </div>

                <?php if ($posts_images) { ?>
                <div class="blog_list">
                    <?php 
                    $dem = 0;
                    foreach ($posts_images as $posts_image) { $dem++;
                    if($dem%2 == 0){?>
                    <div class="blog-item py-5">
                        <div class="row">
                            <div class="image col-xl-6 col-lg-6 col-md-6 col-sm-16 col-12">
                                <img src="<?php echo $posts_image['image'];?>" class="rounded shadow" alt="<?php echo $heading_title; ?>" />
                            </div>
                            <div class="blog_summary col-xl-6 col-lg-6 col-md-6 col-sm-16 col-12">
                                <h3 class="blog_title h2"><?php echo $posts_image['title']; ?></h3>
                                <div class="blog_description"><p><?php echo $posts_image['description'];?></p>
                                </div>
                            </div> <!-- summary ends -->
                        </div>
                    </div> <!-- item ends -->
                    <?php } else { ?>
                    <div class="blog-item py-5">
                        <div class="row">
                            <div class="image col-xl-6 col-lg-6 col-md-6 col-sm-16 col-12">
                                <img src="<?php echo $posts_image['image'];?>" class="rounded shadow" alt="<?php echo $heading_title; ?>" />
                            </div>
                            <div class="blog_summary col-xl-6 col-lg-6 col-md-6 col-sm-16 col-12">
                                <h3 class="blog_title h2"><?php echo $posts_image['title']; ?></h3>
                                <div class="blog_description"><p><?php echo $posts_image['description'];?></p>
                                </div>
                            </div> <!-- summary ends -->
                        </div>
                    </div> <!-- item ends -->
                    <?php } ?>
                    <?php } ?>
                </div>
                <?php } ?>


                <!-- AddThis Button BEGIN -->
                <div class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
                <!-- AddThis Button END -->
                <?php if ($tags) { ?>
                <div class="tags">
                    <span class="contrast_font"><?php echo $text_tags; ?></span>
                    <?php for ($i = 0; $i < count($tags); $i++) { ?>
                    <?php if ($i < (count($tags) - 1)) { ?>
                    <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>, 
                    <?php } else { ?>
                    <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
                    <?php } ?>
                    <?php } ?>
                </div>
                <?php } ?>


                <!-- Comment Area start -->
                <?php if($allow_comment && 1==2){ ?>
                <div class="box-heading"><?php echo $text_comments; ?></div>
                <form id="comment_form">
                    <div id="comment"></div>
                    <div class="box-heading"><?php echo $text_write_comment; ?></div>
                    <div class="row">
                        <div class="form-group col-sm-6 required">
                            <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                            <input type="text" name="name" value="" id="input-name" class="form-control" />
                        </div>
                        <div class="form-group col-sm-6 required">
                            <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                            <input type="text" name="email" value="" id="input-email" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-12 required">
                            <label class="control-label" for="input-review"><?php echo $entry_comment; ?></label>
                            <textarea name="comment" rows="5" id="input-comment" class="form-control"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group required">
                                <label class="control-label" for="input-captcha_comment"><?php echo $entry_captcha; ?></label>
                                <div class="input-group mb-3 ">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-light"><img src="index.php?route=blog/blog/captcha" alt="" id="captcha_comment" /></span>
                                    </div>
                                    <input type="text" class="form-control" name="captcha_comment" value="" id="input-captcha_comment" >
                                </div>
                            </div>

                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-12 text-right">
                            <button type="button" id="button-comment" class="btn btn-primary"><?php echo $button_send; ?></button>
                        </div>
                    </div>
                </form>
                <?php } ?>
            </div>

            <?php if ($related_blogs) { ?>
            <div class="">
                <div class="module-title text-center">
                  <h3 class="title"><?php echo $text_related_blog; ?></h3>
              </div>
              <?php foreach ($related_blogs as $blog) { ?>
              <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <a href="<?php echo $blog['href']; ?>" title="<?php echo $blog['title']; ?>">
                        <?php if($blog['image']){ ?>
                        <img src="<?php echo $blog['image']; ?>" atl="<?php echo $blog['title']; ?>" class="rounded border"/>
                        <?php } ?>
                    </a>
                </div>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                    <a href="<?php echo $blog['href']; ?>" title="<?php echo $blog['title']; ?>"><h5><?php echo $blog['title']; ?></h5></a>
                    <p class="post-date text-muted"><small><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $blog['date_added_full'];?></small></p>
                    <div class="blog_description"><?php echo $blog['short_description']; ?></div>
                </div>
            </div>
            <br>
            <?php } ?>
        </div>
        <?php } ?>
    </div> 
    <?php echo $column_right; ?>
    </div>
</div>

<?php echo $content_bottom; ?>
</div>
</div>
<script type="text/javascript"><!--

    $('#comment').delegate('.pagination a', 'click', function (e) {
        e.preventDefault();
        $("html,body").animate({scrollTop:(($("#comment").offset().top)-50)}, 500);
        $('#comment').fadeOut(50);
        $('#comment').load(this.href);
        $('#comment').fadeIn(500);
    });

    $('#comment').load('index.php?route=blog/blog/comment&blog_id=<?php echo $blog_id; ?>');
    //--></script>

    <script type="text/javascript"><!--

        $('#button-comment').on('click', function () {
            $.ajax({
                url: 'index.php?route=blog/blog/write&blog_id=<?php echo $blog_id; ?>',
                type: 'post',
                dataType: 'json',
                data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&email=' + encodeURIComponent($('input[name=\'email\']').val()) + '&comment=' + encodeURIComponent($('textarea[name=\'comment\']').val()) + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
                data: $("#comment_form").serialize(),
                complete: function () {
                    $('#button-comment').button('reset');
                    $('input[name=\'captcha_comment\']').val('');
                    $('#captcha_comment').attr('src', 'index.php?route=blog/blog/captcha#' + new Date().getTime());
                },
                success: function (json) {
                    $('.alert-success, .alert-danger').remove();

                    if (json['error']) {
                        $('#comment').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                    }

                    if (json['success']) {
                        $('#comment').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                        $('input[name=\'name\']').val('');
                        $('input[name=\'email\']').val('');
                        $('textarea[name=\'comment\']').val('');
                        $('input[name=\'captcha\']').val('');
                    }
                }
            });
        });
    </script>
    <?php echo $footer; ?> 