<?php echo $header; ?>
<?php echo $content_top; ?>
<div class="bg-light breadcrumb-w">
  <div class="container">
    <nav aria-label="breadcrumb">
     <ol class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li class="breadcrumb-item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ol>
  </nav>
</div>
</div>
<div class="container">
    <div class="row">
        <?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-lg-9 col-md-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>">

            <div class="blog-top-article">
                <div class="row">
                    <div class="col-md-9 col-sm-12 col-12">
                        <h1 class="blog-h1"><?php echo $heading_title; ?></h1>
                        <div class="blog-item">
                            <div class="image mb-3">
                                <a class="d-block" href="http://www.demo2.webmaucaocap.com/tdcvietnam.com.vn/cham-soc-khach-hang"><img src="http://www.demo2.webmaucaocap.com/tdcvietnam.com.vn/image/cache/catalog/blog/blog3-cr-600x400.jpg" alt="Chăm sóc khách hàng" title="Chăm sóc khách hàng"></a>
                            </div>
                            <div class="blog_summary">
                                <!-- AddThis Button BEGIN -->
                                <div class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
                                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
                                <!-- AddThis Button END -->
                                <a href="http://www.demo2.webmaucaocap.com/tdcvietnam.com.vn/cham-soc-khach-hang"><h3 class="blog_title h4">Chăm sóc khách hàng</h3></a>
                                <div class="blog_description">  Cung cấp các Module hỗ trợ chăm sóc khách hàng. Giám sát lịch sử giao dịch khách hàng </div>
                            </div> <!-- summary ends -->
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 col-12 border-left">
                        <div class="blog_summary">
                            <a href="http://www.demo2.webmaucaocap.com/tdcvietnam.com.vn/cham-soc-khach-hang"><h3 class="blog_title h6">Chăm sóc khách hàng</h3></a>
                            <div class="blog_description">  Cung cấp các Module hỗ trợ chăm sóc khách hàng. Giám sát lịch sử giao dịch khách hàng </div>
                            <hr>
                        </div>
                        <div class="blog_summary">
                            <a href="http://www.demo2.webmaucaocap.com/tdcvietnam.com.vn/cham-soc-khach-hang"><h3 class="blog_title h6">Chăm sóc khách hàng</h3></a>
                            <div class="blog_description">  Cung cấp các Module hỗ trợ chăm sóc khách hàng. Giám sát lịch sử giao dịch khách hàng </div>
                            <hr>
                        </div>
                        <div class="blog_summary">
                            <a href="http://www.demo2.webmaucaocap.com/tdcvietnam.com.vn/cham-soc-khach-hang"><h3 class="blog_title h6">Chăm sóc khách hàng</h3></a>
                            <div class="blog_description">  Cung cấp các Module hỗ trợ chăm sóc khách hàng. Giám sát lịch sử giao dịch khách hàng </div>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
            <hr>

            <div class="blog_list">
                <?php if($blogs){ ?>
                <div class=" grid<?php echo $list_columns; ?>">
                    <?php foreach ($blogs as $blog) { ?>
                    <div class="blog-item py-3">
                        <div class="row">
                            <div class="image col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                <?php if($blog['image']){ ?>
                                <a class="d-block" href="<?php echo $blog['href']; ?>"><img src="<?php echo $blog['image']; ?>" alt="<?php echo $blog['title']; ?>" title="<?php echo $blog['title']; ?>"/></a>
                                <?php } ?>
                            </div>
                            <div class="blog_summary col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                <a href="<?php echo $blog['href']; ?>"><h3 class="blog_title h4"><?php echo $blog['title']; ?></h3></a>
                                <?php if($date_added_status){ ?>
                                <p class="post-date text-secondary"><span><i class="fa fa-calendar" aria-hidden="true"></i></span> <?php echo $blog['date_added_full'];?></p>
                                <?php } ?>
                                <div class="blog_description"><?php echo $blog['short_description']; ?></div>
                                <a href="<?php echo $blog['href']; ?>">Xem chi tiết >></a>
                            </div> <!-- summary ends -->
                        </div>
                    </div> <!-- item ends -->
                    <hr>
                    <?php } ?>

                </div>
                <div class="row">
                    <div class="col-sm-6 text-center"><?php echo $results; ?></div>
                    <div class="col-sm-6"><?php echo $pagination; ?></div>
                </div>
                <?php }else{ ?>
                <?php echo $text_no_results; ?>
                <?php } ?>
            </div>

            <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
    </div>
    <div class="" hidden>
        <div class="module-title text-center">
            <h3 class="title">Tin xem nhiều</h3>
        </div>
        <div class="most-viewed-list-article row">
            <?php if($posts){ ?>
            <?php foreach ($posts as $blog) { ?>
            <div class="article col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6">
                <div class="article-img mb-3">
                    <a href="<?php echo $blog['href']; ?>" title="<?php echo $blog['title']; ?>">
                        <?php if($blog['image']){ ?>
                        <img src="<?php echo $blog['image']; ?>" atl="<?php echo $blog['title']; ?>" />
                        <?php } ?>
                    </a>
                </div>
                <div class="article-info">                         
                    <a href="<?php echo $blog['href']; ?>" title="<?php echo $blog['title']; ?>"><h5><?php echo $blog['title']; ?></h5></a>

                    <p class="post-date"><span><i class="fa fa-calendar" aria-hidden="true"></i></span> <?php echo $blog['date_added_full'];?></p>   
                </div>
            </div>
            <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>
<?php echo $footer; ?> 