<?php echo $header; ?>
<?php echo $content_top; ?>
<div class="bg-light breadcrumb-w">
  <div class="container">
    <nav aria-label="breadcrumb">
     <ol class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li class="breadcrumb-item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ol>
  </nav>
</div>
</div>
<div class="container">
    <div class="blog-categ-ds">
        <div class="row">
            <?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-lg-9 col-md-9'; ?>
            <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>">
                <h1><?php echo $heading_title; ?></h1>
                <?php if($thumb){ ?>
                <img src="<?php echo $thumb; ?>" atl="<?php echo $thumb; ?>" />
                <?php } ?>
                <?php echo $blog_category_description;?>
                <div class="blog_list">
                    <?php if($blogs){ ?>
                    <div class=" grid<?php echo $list_columns; ?>">
                        <?php foreach ($blogs as $blog) { ?>
                        <div class="blog-item py-3">
                            <div class="row">
                                <div class="image col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                    <?php if($blog['image']){ ?>
                                    <a class="d-block" href="<?php echo $blog['href']; ?>"><img src="<?php echo $blog['image']; ?>" alt="<?php echo $blog['title']; ?>" title="<?php echo $blog['title']; ?>"/></a>
                                    <?php } ?>
                                </div>
                                <div class="blog_summary col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                                    <a href="<?php echo $blog['href']; ?>"><h3 class="blog_title h5"><?php echo $blog['title']; ?></h3></a>
                                    <?php if($date_added_status){ ?>
                                    <p class="post-date text-secondary"><span><i class="fa fa-calendar" aria-hidden="true"></i></span> <?php echo $blog['date_added_full'];?></p>
                                    <?php } ?>
                                    <div class="blog_description"><?php echo $blog['short_description']; ?></div>
                                    <a href="<?php echo $blog['href']; ?>">Xem chi tiết >></a>
                                </div> <!-- summary ends -->
                            </div>
                        </div> <!-- item ends -->
                        <hr>
                        <?php } ?>

                    </div>
                    <div class="row">
                        <div class="col-sm-6"><?php echo $results; ?></div>
                        <div class="col-sm-6"><?php echo $pagination; ?></div>
                    </div>
                    <?php }else{ ?>
                    <?php echo $text_no_results; ?>
                    <?php } ?>
                </div>

                <?php echo $content_bottom; ?>
            </div>
            <?php echo $column_right; ?>
        </div>
    </div>
</div>
<?php echo $footer; ?> 