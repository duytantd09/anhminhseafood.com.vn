<?php echo $header; ?>
<?php echo $content_top; ?>
<div class="bg-light breadcrumb-w">
  <div class="container">
    <nav aria-label="breadcrumb">
     <ol class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li class="breadcrumb-item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ol>
  </nav>
</div>
</div>
<div class="container ">
    <?php if ($attention) { ?>
    <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="row">
        <?php echo $column_left; ?>

        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-lg-9 col-md-9 col-sm-8'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>">
            <h1 class="text-center"><?php echo $heading_title; ?>
                <?php if ($weight) { ?>
                &nbsp;(<?php echo $weight; ?>)
                <?php } ?>
            </h1>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="basket">
                <div class="cart-info-wrapper">
                    <div class="cart-info table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="c-image" scope="col"><?php echo $column_image; ?></th>
                                    <th class="c-name" scope="col"><?php echo $column_name; ?></th>
                                    <th class="c-model" scope="col"><?php echo $column_model; ?></th>
                                    <th class="c-price" scope="col"><?php echo $column_price; ?></th>
                                    <th class="c-quantity" scope="col"><?php echo $column_quantity; ?></th>
                                    <th class="c-total" scope="col"><?php echo $column_total; ?></th>
                                    <th class="c-remove" scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($products as $product) { ?>
                                <tr>
                                    <td class="image" title="<?php echo $column_image; ?>">
                                        <?php if ($product['thumb']) { ?>
                                        <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
                                        <?php } ?>
                                    </td>
                                    <td class="name" title="<?php echo $column_name; ?>">
                                        <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                        <?php if (!$product['stock']) { ?>
                                        <span class="text-danger">***</span>
                                        <?php } ?>
                                        <?php if ($product['option']) { ?>
                                        <?php foreach ($product['option'] as $option) { ?>
                                        <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                        <?php } ?>
                                        <?php } ?>
                                        <?php if ($product['reward']) { ?>
                                        <small><?php echo $product['reward']; ?></small>
                                        <?php } ?>
                                        <?php if ($product['recurring']) { ?>
                                        <small><?php echo $text_recurring_item; ?>: <?php echo $product['recurring']; ?></small>
                                        <?php } ?>
                                        <div >
                                            <small><?php echo $column_price; ?>: <?php echo $product['price']; ?></small>
                                            <small><?php echo $column_total; ?>: <?php echo $product['total']; ?></small>
                                            <a class="remove_link" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>"><small>[<?php echo $button_remove; ?>]</small></a>
                                        </div>
                                    </td>
                                    <td class="model" title="<?php echo $column_model; ?>"><?php echo $product['model']; ?></td>
                                    <td class="unit_price" title="<?php echo $column_price; ?>"><?php echo $product['price']; ?></td>
                                    <td class="quantity" title="<?php echo $column_quantity; ?>">
                                        <div class="input-group mb-3">
                                            <input type="text" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" class="form-control"/>
                                            <div class="input-group-append">
                                                <button class="btn btn-default" type="button" onclick="$('#basket').submit();" data-tooltip="<?php echo $button_update; ?>"><i class="fa fa-refresh"></i></button>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="price total" title="<?php echo $column_total; ?>"><?php echo $product['total']; ?></td>
                                    <td class="remove" title="Remove">
                                        <button class="btn btn-outline-default" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" data-tooltip="<?php echo $button_remove; ?>"><i class="fa fa-times"></i></button>
                                    </td>
                                </tr>
                                <?php } ?>
                                <?php foreach ($vouchers as $vouchers) { ?>
                                <tr>
                                    <td class="image"></td>
                                    <td class="name"><?php echo $vouchers['description']; ?>
                                        <a class="remove_link up_to_mobile" onclick="voucher.remove('<?php echo $vouchers['key']; ?>');" title="<?php echo $button_remove; ?>"><small>[<?php echo $button_remove; ?>]</small></a>
                                    </td>
                                    <td class="model"></td>
                                    <td class="unit_price"><?php echo $vouchers['amount']; ?></td>
                                    <td class="quantity"><input type="text" name="" value="1" size="1" disabled="disabled" /></td>
                                    <td class="price total"><?php echo $vouchers['amount']; ?></td>
                                    <td class="remove">
                                        <a onclick="voucher.remove('<?php echo $vouchers['key']; ?>');" data-tooltip="<?php echo $button_remove; ?>" class="sq_icon"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                            <tr>
                                <td colspan="7">
                                    <a href="<?php echo $continue; ?>" class="btn btn-secondary"><?php echo $button_shopping; ?></a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </form>

            <div class="row">
                <div class="col-sm-8">
                    <?php if ($modules) { ?>
                    <div class="row">
                        <?php foreach ($modules as $module) { ?>
                        <?php echo $module; ?>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </div>

                <div class="col-sm-4">
                    <div class="cart-total">
                        <table id="total" class="table table-bordered">
                            <?php foreach ($totals as $total) { ?>
                            <tr>
                                <td ><?php echo $total['title']; ?></td>
                                <td ><?php echo $total['text']; ?></td>
                            </tr>
                            <?php } ?>
                        </table>
                        <div class="cart-total-bottom">
                            <a href="<?php echo $checkout; ?>" class="btn btn-primary btn-block"><?php echo $button_checkout; ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $content_bottom; ?> 
        </div>
        <?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?> 