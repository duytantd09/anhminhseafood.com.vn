<?php if ($questions) { ?>
<?php foreach ($questions as $question) { ?>
<div class="group-question bg-light p-2 rounded mb-3">
	<p><b class="contrast_font"><?php echo $text_question_from; ?>: <?php echo $question['author']; ?></b> - <small class="text-muted"><?php echo $question['date_added']; ?></small></p>
	<p >- <?php echo $question['text']; ?></p>
	<div class="bg-white p-2">
		<?php if ($question['answer']) { ?>
		<p ><b class="contrast_font"><?php echo $text_our_answer; ?></b></p>
		<p><?php echo $question['answer']; ?></p>
		<?php } else { ?>
		<p><i><?php echo $text_no_answer; ?></i></p>
		<?php } ?>
	</div>
</div>
<?php } ?>
<div class="text-right"><?php echo $pagination; ?></div>
<?php } else { ?>
<p><?php echo $text_no_questions; ?></p>
<?php } ?>