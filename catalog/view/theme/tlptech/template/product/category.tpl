<?php echo $header; ?>
<?php echo $content_top; ?>
<?php echo $content_bottom; ?>
<div class="bg-light breadcrumb-w">
  <div class="container">
    <nav aria-label="breadcrumb">
     <ol class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li class="breadcrumb-item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ol>
  </nav>
</div>
</div>

<div class="container "> 
  <div class="row">
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>

    <div id="content" class="<?php echo $class; ?> <?php echo $tlptech_grid_category; ?>">

      <div class="heading-title"><h1><?php echo $heading_title; ?></h1></div>
      <?php if ($description) { ?>
      <p><?php echo $description; ?></p>
      <?php } ?>

      <?php if ($products) { ?>
      <div class="product-filter">
        <div class="display"> 
          <a id="grid_view_icon"><i class="fa fa-th"></i></a><a id="list_view_icon"><i class="fa fa-list"></i></a>
        </div>
        <div class="limit form-group form-inline">
          <label><?php echo $text_limit; ?></label>
          <select id="input-limit" class="form-control form-control-sm" onchange="location = this.value;">
            <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        <div class="sort form-group form-inline">
          <label><?php echo $text_sort; ?></label>
          <select id="input-sort" class="form-control form-control-sm" onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>

        </div>
        <div class="compare-link "><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></div>
      </div>
      <div id="main" class=" product-<?php echo $tlptech_default_view; ?> ">
        <div id="product-1">
         <div class="row grid3">
          <?php foreach ($products as $product) { ?>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item-grid product-layout">
            <div class="product-thumb">
              <div class="thumb-image">
                <?php if ($product['special'] && $tlptech_percentage_sale_badge == 'enabled') { ?>
                <div class="sale">
                  <span class="sale-text">sale</span>
                  <span class="sale-percent"><?php echo $product['sales_percantage']; ?>%</span>
                </div>
                <?php } ?>
                <?php if ($product['new']!='') { ?>
                <div class="tag-new"><?php echo $product['new']; ?></div>
                <?php } ?>
                <?php if ($product['hot']!='') { ?>
                <div class="tag-hot"><?php echo $product['hot']; ?></div>
                <?php } ?>
                <div class="image">
                  <?php if ($product['thumb']) { ?>
                  <a href="<?php echo $product['href']; ?>">
                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
                  </a>
                  <?php } ?>
                </div>
          </div>

          <div class="caption text-center">
            <div class="rating" >
              <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($product['rating'] < $i) { ?>
              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
              <?php } ?>
              <?php } ?>
            </div>
            <a href="<?php echo $product['href']; ?>"><h5 class="name"><?php echo $product['name']; ?></h5></a>

            <?php if ($product['price']) { ?>
            <p class="price">
              <?php if (!$product['special']) { ?>
              <?php echo $product['price']; ?>
              <?php } else { ?>
              <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
              <?php } ?>
              <?php if ($product['tax']) { ?>
              <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
              <?php } ?>
            </p>
            <?php } ?>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
    <div class="row">
      <div class="col-sm-7"><?php echo $results; ?></div>
      <div class="col-sm-5"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
</div>
<?php echo $column_right; ?>
<?php } ?>
</div></div></div>
</div>
<?php echo $footer; ?>