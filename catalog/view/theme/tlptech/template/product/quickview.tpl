<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8" />
  <script src="catalog/view/theme/tlptech/js/jquery-2.1.1.min.js" type="text/javascript"></script>

  <script  src="catalog/view/theme/tlptech/js/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>

  <link href="catalog/view/theme/tlptech/js/bootstrap/css/bootstrap.min.css" rel="stylesheet" />

  <script  type="text/javascript" src="catalog/view/theme/tlptech/js/owlcarousel2/js/owl.carousel.min.js"></script>

  <link href="catalog/view/theme/tlptech/js/owlcarousel2/css/owl.carousel.min.css" rel="stylesheet" type="text/css" />

  <link href="catalog/view/theme/tlptech/js/owlcarousel2/css/owl.theme.default.min.css" rel="stylesheet" type="text/css" />

  <link href="catalog/view/theme/tlptech/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/tlptech/js/cloud-zoom/css/cloud-zoom.css" />
  <script  src="catalog/view/theme/tlptech/js/cloud-zoom/js/cloud-zoom.1.0.2.min.js" type="text/javascript"></script>

  <script  type="text/javascript" src="catalog/view/theme/tlptech/js/tlptech_common.min.js"></script>

  <script type="text/javascript" src="catalog/view/javascript/jquery/datetimepicker/moment.js"></script>
  <script type="text/javascript" src="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js"></script>

  <?php echo $tlptech_styles; ?>
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/tlptech/stylesheet/style.css" />
</head>
<body style="background-color:#fff">
  <div >
   <div class="row product-info quickview quickview-htl">
     <div id="notification"></div>
     <?php if ($thumb || $images) { ?>
     <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
      <?php if ($thumb) { ?>
      <div class="image">
        <a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="cloud-zoom"  rel="position:'inside', showTitle: false" id="zoom1">
          <img itemprop="image" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" class="rounded"/>
        </a>
        <?php if ($tlptech_percentage_sale_badge == 'enabled') { ?>
        <?php if (!$special) { ?>
        <?php } else { ?>
        <div class="sale">
          <div class="sale-nt">sale</div>
          <div class="sale-so-ft"><?php echo $sales_percantage_main; ?>%</div>
        </div>
        <?php } ?>
        <?php } ?>
      </div>
      
      <?php } ?>
      

      <?php if ($images) { ?>

      <div class="image-additional" >      
        <ul class="image_carousel list-unstyled owl-carousel owl-theme"><!-- Additional images -->
          <?php foreach ($images as $image) { ?>
          <li class="rounded">
            <a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="cloud-zoom-gallery " rel="useZoom: 'zoom1', smallImage: '<?php echo $image['thumb']; ?>'">
              <img  src="<?php echo $image['small']; ?>" title="<?php echo $heading_title; ?>"  alt="<?php echo $heading_title; ?>" class="rounded"/>
            </a>
          </li>
          <?php } ?>
        </ul>
      </div>
      <?php } ?>

      <!-- AddThis Button START -->
      <div class="addthis_toolbox addthis_default_style addthis_32x32_style" addthis:url="<?php echo $share_url; ?>">
        <a class="addthis_button_preferred_1"></a>
        <a class="addthis_button_preferred_2"></a>
        <a class="addthis_button_preferred_3"></a>
        <a class="addthis_button_preferred_4"></a>
        <a class="addthis_button_compact"></a>
        <a class="addthis_counter addthis_bubble_style"></a>
      </div>
      <!-- AddThis Button END -->

    </div>
    <?php } ?>
    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">   
      <h1><?php echo $heading_title; ?></h1>

      <?php if ($review_status) { ?>
      <div class="review">
       <span class="rating r<?php echo $rating; ?>">
         <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
       </span>
       <a class="to_review" target="_top" href="<?php echo $product_href; ?>"><?php echo $reviews; ?></a>
       <a class="to_review" target="_top" href="<?php echo $product_href; ?>"><?php echo $text_write; ?></a>
     </div>



     <?php } ?>

     <div class="description">
      <?php if ($manufacturer) { ?>
      <span class="contrast_font"><?php echo $text_manufacturer; ?></span> <a href="<?php echo $manufacturers; ?>" target="_top"><?php echo $manufacturer; ?></a><br />
      <?php } ?>    

      <span class="contrast_font"><?php echo $text_model; ?></span> <?php echo $model; ?><br />

      <?php if ($reward) { ?>
      <span class="contrast_font"><?php echo $text_reward; ?></span> <?php echo $reward; ?><br />
      <?php } ?>

      <span class="contrast_font" itemprop="availability" content="<?php if ($data_qty > 0) {echo "in_stock"; } else {echo "out_of_stock"; } ?>"><?php echo $text_stock; ?></span> <?php echo $stock; ?>

    </div> <!-- .description ends -->

    <?php if ($price) { ?> 
    <?php if (!$special) { ?>
    <div class="price product-page-price font-weight-bold">
      <span itemprop="price"><?php echo $price; ?></span>
    </div>
    <?php } else { ?>
    <?php if (!$tlptech_product_yousave) { ?>
    <div class="price product-page-price font-weight-bold">
      <span class="price-old"><?php echo $price; ?></span> 
      <span class="price-new" itemprop="price"><?php echo $special; ?></span>
    </div>
    <?php } ?>
    <?php } ?>

    <meta itemprop="currency" content="<?php echo $currency_code; ?>" />
    <span itemprop="offerDetails" itemscope itemtype="http://data-vocabulary.org/Offer"><!-- Rich snippets start -->
      <?php if (($special) && ($tlptech_product_yousave)) { ?>

      <div class="extended_offer row my-3">
        <div class="price-new offer col-lg-4 col-md-6 col-sm-4 col-6 border">
          <label><?php echo $text_special_price; ?></label>
          <p class="amount font-weight-bold" itemprop="price"><?php echo $special; ?></p>
        </div>
        <div class="price-old offer col-lg-4 col-md-6 col-sm-4 col-6 border">
          <label><?php echo $text_old_price; ?></label>
          <p class="amount font-weight-bold"><del><?php echo $price; ?></del></p>
        </div>
        <div class="price-save offer col-lg-4 col-md-12 col-sm-4 col-12 border">
          <label><?php echo $text_you_save; ?></label>
          <p class="amount font-weight-bold"><?php echo $yousave; ?></p>
        </div>
      </div>
    </span>
    <?php } ?>
    <?php } ?>

    <div id="product">
                        <?php if ($recurrings) { ?>
                        <hr>
                        <h3><?php echo $text_payment_recurring ?></h3>
                        <div class="form-group required">
                            <select name="recurring_id" class="form-control">
                                <option value=""><?php echo $text_select; ?></option>
                                <?php foreach ($recurrings as $recurring) { ?>
                                <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                                <?php } ?>
                            </select>
                            <div class="help-block" id="recurring-description"></div>
                        </div>
                        <?php } ?>

                        <?php if ($options) { ?>
                        <div class="options contrast_font">
                            <h2 hidden><?php echo $text_option; ?></h2>

                            <?php foreach ($options as $option) { ?>
                            <?php if ($option['type'] == 'select') { ?>
                            <div class="form-group row <?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="col-lg-3 col-md-4 col-sm-5 col-12" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <div class="col-lg-9 col-md-8 col-sm-7 col-12">
                                    <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                                        <option value=""><?php echo $text_select; ?></option>
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                        <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                            <?php if ($option_value['price']) { ?>
                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                            <?php } ?>
                                        </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if ($option['type'] == 'radio') { ?>
                            <div class="clearfix form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="col-form-label"><?php echo $option['name']; ?></label>

                                <?php if($tlptech_image_options == 'thumbs'){ ?>
                                <div id="input-option<?php echo $option['product_option_id']; ?>" class="clean-option-image">
                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <?php if ($option_value['image']) { ?>
                                    <div class="single-option main_font" <?php if ($option_value['price']) { ?>data-tooltip="<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>"<?php } ?>>
                                        <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" /><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name']; ?>" /></label>
                                    </div>
                                    <?php } ?>
                                    <?php } ?>
                                </div>
                                <?php } else { ?>
                                <div id="input-option<?php echo $option['product_option_id']; ?>" class="radio-image">
                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio"  name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />

                                            <?php if ($option_value['image']) { ?>
                                            <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                                            <?php } ?>  

                                            <div hidden>
                                                <?php echo $option_value['name']; ?>
                                                <?php if ($option_value['price']) { ?>
                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                <?php } ?>
                                            </div>
                                        </label>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                            </div>
                            <?php } ?>

                            <?php if ($option['type'] == 'checkbox') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="form-check-label"><?php echo $option['name']; ?></label>
                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <div class="checkbox custom-control custom-checkbox">
                                        <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" class="custom-control-input" id="<?php echo $option_value['product_option_value_id']; ?>"/>
                                        <label class="custom-control-label" for="<?php echo $option_value['product_option_value_id']; ?>">
                                            <?php if ($option_value['image']) { ?>
                                            <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                                            <?php } ?>
                                            <?php echo $option_value['name']; ?>
                                            <?php if ($option_value['price']) { ?>
                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                        </label>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php } ?>


                            <?php if ($option['type'] == 'text') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="form-check-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                            </div>
                            <?php } ?>

                            <?php if ($option['type'] == 'textarea') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="form-check-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'file') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="form-check-label"><?php echo $option['name']; ?></label><br />
                                <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="button"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'date') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'datetime') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <div class="input-group datetime">
                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'time') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <div class="input-group time">
                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div> <!-- .options ends -->
                        <?php } ?> 

                        <div class="cart" >
                            <div class="row">
                                <div class="col-lg-2 col-md-4 col-sm-5 col-4">
                                    <label class="control-label"><?php echo $text_quantity; ?></label>
                                </div>
                                <div class="col-lg-10 col-md-8 col-sm-7 col-8">
                                    <div class="input-group mb-3 quantity-input-group">
                                        <div class="input-group-prepend">
                                            <button class="btn btn-default quantity_button plus" type="button">+</button>
                                        </div>
                                        <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="quantity form-control text-center" />
                                        <div class="input-group-append">
                                            <button class="btn btn-default quantity_button minus" type="button">-</button>
                                        </div>
                                    </div>
                                    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                                </div>
                            </div>
                            <div class="product-page-group-btn row">
                                <div class="col-md-6 col-sm-12 col-12">
                                    <button type="submit" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-lg btn-secondary w-100 mb-3"><?php echo $button_cart; ?></button>
                                </div>
                                <div class="col-md-6 col-sm-12 col-12">
                                    <button type="submit" id="btn-mua-ngay" data-loading-text="<?php echo $text_loading; ?>"class="btn btn-lg btn-primary w-100 mb-3"> <?php echo $button_buy; ?></button>
                                </div>


                            </div>
                        </div> <!-- Cart ends -->

                        <?php if ($minimum > 1 && 1==2) { ?>
                        <div class="minimum"><?php echo $text_minimum; ?></div>
                        <?php } ?>
                        <?php if ($price) { ?>
                        <?php if ($points && 1==3) { ?>
                        <div class="reward"><?php echo $text_points; ?> <?php echo $points; ?></div>
                        <?php } ?>
                        <?php if ($discounts) { ?>
                        <div class="discount">
                            <?php foreach ($discounts as $discount) { ?>
                            <span><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></span>
                            <?php } ?>
                        </div>
                        <?php } ?>
                        <?php } ?>
                    </div>

      <div class="share">

        <?php if ($price) { ?>
        <?php if ($tax) { ?>
        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $tax; ?></span><br />
        <?php } ?>
        <?php } ?>
      </div> <!-- .share ends -->

    </div> <!-- product-info-right END -->

  </div> <!-- product-info END -->


  <script type="text/javascript">
    jQuery(function($) {
      //Product thumbnails
      $(".cloud-zoom-gallery").last().removeClass("cboxElement");
      $(".cloud-zoom-gallery").click(function() {
        $("#zoom-btn").attr('href', $(this).attr('href'));
        $("#zoom-btn").attr('title', $(this).attr('title'));
        $(".cloud-zoom-gallery").each(function() {
          $(this).addClass("cboxElement");
        });
        $(this).removeClass("cboxElement");
      });
    });
  </script>

  <script type="text/javascript">
    $(document).ready(function() {
      var owlAdditionals = $('.image_carousel');
      owlAdditionals.owlCarousel({
        items : 5,
        dots: false,
        nav: true,
        margin: 10,
        responsive:{
          0:{
            items:3
          },
          600:{
            items:4
          },
          1000:{
            items:6
          }
        },
        navText: [
        "<i class='fa fa-angle-left'></i>",
        "<i class='fa fa-angle-right'></i>"
        ]
      });
    });
  </script> 
  <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js"></script>
  <script type="text/javascript">
    $('.quantity_button.plus').on('click', function(){
      var oldVal = $('input.quantity').val();
      var newVal = (parseInt($('input.quantity').val(),10) +1);
      $('input.quantity').val(newVal);
    });

    $('.quantity_button.minus').on('click', function(){
      var oldVal = $('input.quantity').val();
      if (oldVal > 1)
      {
        var newVal = (parseInt($('input.quantity').val(),10) -1);
      }
      else
      {
        newVal = 1;
      }
      $('input.quantity').val(newVal);
    });
  </script>
  <?php if ($special_date_end > 0) { ?>
  <script type="text/javascript" src="catalog/view/theme/tlptech/js/countdown/jquery.countdown.min.js"></script>
  <script type="text/javascript">
    $('.offer').countdown({
      until: <?php echo $special_date_end ?>, 
      layout: '{desc}<i>{dn}</i> {dl} <i>{hn}</i> {hl} <i>{mn}</i> {ml} <i>{sn}</i> {sl}',
      description: '<span class="main_font"><?php echo $text_expire ?></span>&nbsp;'
    });
  </script>
  <?php } ?>


  <!-- Default scrips below -->    
  <script type="text/javascript"><!--
    $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
      $.ajax({
        url: 'index.php?route=product/product/getRecurringDescription',
        type: 'post',
        data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
        dataType: 'json',
        beforeSend: function() {
          $('#recurring-description').html('');
        },
        success: function(json) {
          $('.alert, .text-danger').remove();

          if (json['success']) {
            $('#recurring-description').html(json['success']);
          }
        }
      });
    });
    //--></script> 
    <script type="text/javascript"><!--
      $('#button-cart').on('click', function() {
        $.ajax({
          url: 'index.php?route=checkout/cart/add',
          type: 'post',
          data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
          dataType: 'json',
          beforeSend: function() {
            $('#button-cart').button('loading');
          },
          complete: function() {
            $('#button-cart').button('reset');
          },
          success: function(json) {
            $('.alert, .text-danger').remove();
            $('.form-group').removeClass('has-error');

            if (json['error']) {
              if (json['error']['option']) {
                for (i in json['error']['option']) {
                  var element = $('#input-option' + i.replace('_', '-'));

                  if (element.parent().hasClass('input-group')) {
                    element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                  } else {
                    element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                  }
                }
              }

              if (json['error']['recurring']) {
                $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
              }

        // Highlight any found errors
        $('.text-danger').parent().addClass('has-error');
      }
      
      if (json['success']) {
        $('#notification').html('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        
        $('#cart-total').html(json['total']);
        
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        $('.alert a').attr('target','_top');
        $('#cart').load('index.php?route=common/cart/info #cart > *'); //Added
      }
    }
  });
      });
      //--></script> 
      <script type="text/javascript"><!--
        $('.date').datetimepicker({
          pickTime: false
        });

        $('.datetime').datetimepicker({
          pickDate: true,
          pickTime: true
        });

        $('.time').datetimepicker({
          pickDate: false
        });

        $('button[id^=\'button-upload\']').on('click', function() {
          var node = this;

          $('#form-upload').remove();

          $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

          $('#form-upload input[name=\'file\']').trigger('click');

          timer = setInterval(function() {
            if ($('#form-upload input[name=\'file\']').val() != '') {
              clearInterval(timer);

              $.ajax({
                url: 'index.php?route=tool/upload',
                type: 'post',
                dataType: 'json',
                data: new FormData($('#form-upload')[0]),
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                  $(node).button('loading');
                },
                complete: function() {
                  $(node).button('reset');
                },
                success: function(json) {
                  $('.text-danger').remove();

                  if (json['error']) {
                    $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                  }

                  if (json['success']) {
                    alert(json['success']);

                    $(node).parent().find('input').attr('value', json['code']);
                  }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
              });
            }
          }, 500);
        });



        var ql_wishlist = {
          'add': function(product_id) {
            $.ajax({
              url: 'index.php?route=account/wishlist/add',
              type: 'post',
              data: 'product_id=' + product_id,
              dataType: 'json',
              success: function(json) {
                $('.alert').remove();

                if (json['success']) {
                  $('#notification').html('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                if (json['info']) {
                  $('#notification').html('<div class="alert alert-info"><i class="fa fa-info-circle"></i> ' + json['info'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                $('.success').fadeIn('slow');
                $('.alert a').attr('target','_top');
                $('#wishlist-total').html(json['total']);
                $('#header_wishlist').html(json['wishlist_total']);

                $('html, body').animate({ scrollTop: 0 }, 'slow');
              }
            });
          },
          'remove': function() {

          }
        }

        var ql_compare = {
          'add': function(product_id) {
            $.ajax({
              url: 'index.php?route=product/compare/add',
              type: 'post',
              data: 'product_id=' + product_id,
              dataType: 'json',
              success: function(json) {
                $('.alert').remove();

                if (json['success']) {
                  $('#notification').html('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                  $('.success').fadeIn('slow');
                  $('.alert a').attr('target','_top');
                  $('#compare-total').html(json['total']);
                  $('#header_compare').html(json['compare_total']);
                  $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
              }
            });
          },
          'remove': function() {

          }
        }

        //--></script> 


      </body></html>