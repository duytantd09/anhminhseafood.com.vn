<?php echo $header; ?><?php echo $content_top; ?>
<div class="bg-light breadcrumb-w">
  <div class="container">
    <nav aria-label="breadcrumb">
     <ol class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li class="breadcrumb-item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ol>
  </nav>
</div>
</div>

<div class="product_ss">
  <div class="container">
    <div class="row"><?php echo $column_left; ?>
      <?php if ($column_left && $column_right) { ?>
      <?php $class = 'col-sm-6'; ?>
      <?php } elseif ($column_left || $column_right) { ?>
      <?php $class = 'col-sm-9'; ?>
      <?php } else { ?>
      <?php $class = 'col-sm-12'; ?>
      <?php } ?>
      <div id="content" class="<?php echo $class; ?>">
        <?php if ($products) { ?>
        <br />
        <div class="row grid4">
          <?php foreach ($products as $product) { ?>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item-grid product-layout">
            <div class="product-thumb">
              <div class="thumb-image">
                <?php if ($product['special'] && $tlptech_percentage_sale_badge == 'enabled') { ?>
                <div class="sale">
                  <span class="sale-text">sale</span>
                  <span class="sale-percent"><?php echo $product['sales_percantage']; ?>%</span>
                </div>
                <?php } ?>
                <?php if ($product['new']!='') { ?>
                <div class="tag-new"><?php echo $product['new']; ?></div>
                <?php } ?>
                <?php if ($product['hot']!='') { ?>
                <div class="tag-hot"><?php echo $product['hot']; ?></div>
                <?php } ?>
                <div class="image">
                  <?php if ($product['thumb']) { ?>
                  <a href="<?php echo $product['href']; ?>">
                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
                  </a>
                  <?php } ?>
                </div>

                <div class="actions-link" hidden>                
                  <button class="btn-wishlist button" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="icon icon-Heart"></i></button>

                  <button class="btn-compare button" type="button" data-toggle="tooltip" title="" onclick="compare.add('<?php echo $product['product_id']; ?>');" data-original-title="<?php echo $button_compare; ?>">
                    <i class="icon icon-MusicMixer"></i>
                  </button>

                  <button class="btn-quickview button" type="button" data-toggle="tooltip" title="" onclick="ocquickview.ajaxView('')" data-original-title="Quick View">
                    <i class="icon icon-Eye"></i>
                  </button> 

                </div>
              </div>

              <div class="caption text-center">
                <div class="rating" >
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } ?>
                  <?php } ?>
                </div>
                <a href="<?php echo $product['href']; ?>"><h5 class="name"><?php echo $product['name']; ?></h5></a>

                <?php if ($product['price']) { ?>
                <p class="price">
                  <?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } else { ?>
                  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                  <?php } ?>
                  <?php if ($product['tax']) { ?>
                  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                  <?php } ?>
                </p>
                <?php } ?>
                <div class="btn-group thumb-bottom text-center" role="group">
                  <button class="btn-cart btn btn-secondary " type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');">
                    <span class="d-xl-none d-lg-none d-md-none"><i class="icon icon-ShoppingCart"></i></span>
                    <span class="d-none d-md-block"><?php echo $button_cart; ?></span>
                  </button>

                  <button type="button" class="btn btn-primary" onclick="cart.buy('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"> <?php echo $button_buy; ?> </button>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
        <?php } else { ?>
        <p><?php echo $text_empty; ?></p>
        <div class="buttons">
          <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
        </div>
        <?php } ?><br/>
        <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
      </div>
    </div>
    <?php echo $footer; ?>