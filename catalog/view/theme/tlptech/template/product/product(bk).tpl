<?php echo $header; ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/tlptech/js/cloud-zoom/css/cloud-zoom.css" />
<script  src="catalog/view/theme/tlptech/js/cloud-zoom/js/cloud-zoom.1.0.2.min.js" type="text/javascript"></script>
<?php echo $content_top; ?>
<div class="container">
    <nav aria-label="breadcrumb">
       <ol class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="breadcrumb-item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ol>
</nav>
</div>
<div class="container product-details">
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-lg-9 col-md-9 col-sm-8'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?> product">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="product-info clearfix">
                        <?php if ($thumb || $images) { ?>
                        <div class="left">

                            <?php if ($thumb) { ?>
                            <div class="image rounded">
                              <?php if ($tlptech_product_zoom) { ?>
                              <a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="cloud-zoom"  rel="position:'inside', showTitle: false" id="zoom1">
                                <img itemprop="image" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" class="rounded"/>
                            </a>
                            <!-- zoom link-->
                            <a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" id="zoom-btn" class="colorbox hidden" rel="colorbox"><i class="fa fa-search-plus"></i></a>
                            <?php } else { ?>
                            <a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="colorbox"><img itemprop="image" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
                            <?php } ?>

                            <?php if ($tlptech_percentage_sale_badge == 'enabled') { ?>
                            <?php if (!$special) { ?>
                            <?php } else { ?>
                            <div class="sale">
                              <div class="sale-nt">sale</div>
                              <div class="sale-so-ft"><?php echo $sales_percantage_main; ?>%</div>
                          </div>
                          <?php } ?>
                          <?php } ?>
                          <?php if ($product_new!='') { ?>
                          <div class="tag-new"><?php echo $product_new; ?></div>
                          <?php } ?>

                          <?php if ($product_hot!='') { ?>
                          <div class="tag-hot"><?php echo $product_hot; ?></div>
                          <?php } ?>
                      </div>

                      <?php } ?>

                      <?php if ($images) { ?>

                      <div class="image-additional" >      
                        <ul class="image_carousel list-unstyled owl-carousel owl-theme">
                            <!-- Additional images -->
                            <?php foreach ($images as $image) { ?>
                            <li class="rounded">
                                <?php if ($tlptech_product_zoom) { ?>
                                <a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="cloud-zoom-gallery " rel="useZoom: 'zoom1', smallImage: '<?php echo $image['thumb']; ?>'">
                                    <img  src="<?php echo $image['small']; ?>" title="<?php echo $heading_title; ?>"  alt="<?php echo $heading_title; ?>" class="rounded"/>
                                </a>
                                <?php } else { ?>
                                <a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="colorbox" rel="useZoom: 'zoom1', smallImage: '<?php echo $image['thumb']; ?>'">
                                    <img  src="<?php echo $image['small']; ?>" title="<?php echo $heading_title; ?>"  alt="<?php echo $heading_title; ?>" class="rounded"/>
                                </a>
                                <?php } ?>
                            </li>
                            <?php } ?>

                            <!-- Show even the main image among the additional if  -->
                            <?php if ($tlptech_product_zoom) { ?>
                            <li class="rounded">
                                <a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="cloud-zoom-gallery " rel="useZoom: 'zoom1', smallImage: '<?php echo $thumb; ?>'">
                                    <img class="zoom-tiny-image" src="<?php echo $small; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" class="rounded" />
                                </a>
                            </li>
                            <?php } ?>
                        </ul>

                    </div>
                    <?php } else { ?>
                    <?php if ($tlptech_product_zoom) { ?>
                    <div class="image-additional" >      
                        <ul class="image_carousel">
                            <li><a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="cloud-zoom-gallery colorbox" rel="useZoom: 'zoom1', smallImage: '<?php echo $thumb; ?>'"><img src="<?php echo $small; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" width="<?php echo $additional_width; ?>" height="<?php echo $additional_height; ?>"/></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php }?>

                    <?php if($tlptech_product_share == 'image'){ ?>
                    <!-- AddThis Button START -->
                    <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                        <a class="addthis_button_preferred_1"></a>
                        <a class="addthis_button_preferred_2"></a>
                        <a class="addthis_button_preferred_3"></a>
                        <a class="addthis_button_preferred_4"></a>
                        <a class="addthis_button_compact"></a>
                        <a class="addthis_counter addthis_bubble_style"></a>
                    </div>
                    <!-- AddThis Button END -->
                    <?php } ?>
                </div>
                <?php } ?>
                <div class="right">

                    <h1 itemprop="name"><?php echo $heading_title; ?></h1>
                    <!-- AddThis Button BEGIN -->
                    <div class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
                    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
                    <!-- AddThis Button END -->
                    <?php if ($review_status) { ?>
                    <div class="review">
                        <div class="rating text-left">
                            <?php for ($i = 1; $i <= 5; $i++) { ?>
                            <?php if ($rating < $i) { ?>
                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                            <?php } else { ?>
                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                            <?php } ?>
                            <?php } ?>
                        </div>
                        <a class="to_review" onclick="$('a[href=\'#tab-review\']').trigger('click');"><small><?php echo $reviews; ?></small></a>
                        <a class="to_review" onclick="$('a[href=\'#tab-review\']').trigger('click');"><small><?php echo $text_write; ?></small></a>
                    </div>

                    <?php if ($count_reviews > 0) { ?><!-- Rich snippet start -->
                    <div itemprop="review" itemscope itemtype="http://data-vocabulary.org/Review-aggregate"> 
                        <span itemprop="rating" content="<?php echo $rating; ?>"></span><span itemprop="count" content="<?php echo $reviews; ?>"></span>
                    </div> 
                    <?php } ?><!-- Rich snippet end -->

                    <?php } ?>

                    <?php if ($price) { ?> 
                    <?php if (!$special) { ?>

                    <div class="price product-page-price font-weight-bold">
                        <span itemprop="price"><?php echo $price; ?></span>
                    </div>
                    <?php } else { ?>
                    <?php if (!$tlptech_product_yousave) { ?>
                    <div class="price product-page-price font-weight-bold">
                        <span class="price-old"><?php echo $price; ?></span> 
                        <span class="price-new" itemprop="price"><?php echo $special; ?></span>
                    </div>
                    <?php } ?>
                    <?php } ?>

                    <?php } ?>

                    <?php if ($price) { ?>
                    <meta itemprop="currency" content="<?php echo $currency_code; ?>" />
                    <span itemprop="offerDetails" itemscope itemtype="http://data-vocabulary.org/Offer"><!-- Rich snippets start -->
                        <?php if (($special) && ($tlptech_product_yousave)) { ?>

                        <div class="extended_offer row border-top border-bottom my-3">
                            <div class="price-new offer col-4 border-left">
                                <label><?php echo $text_special_price; ?></label>
                                <p class="amount font-weight-bold" itemprop="price"><?php echo $special; ?></p>
                            </div>
                            <div class="price-old offer col-4 border-left">
                                <label><?php echo $text_old_price; ?></label>
                                <p class="amount font-weight-bold"><del><?php echo $price; ?></del></p>
                            </div>
                            <div class="price-save offer col-4 border-left border-right">
                                <label><?php echo $text_you_save; ?></label>
                                <p class="amount font-weight-bold"><?php echo $yousave; ?></p>
                            </div>
                        </div>

                        <?php } ?>
                        <?php } ?>

                        <?php if (($special_date_end > 0) && ($tlptech_product_countdown)) { ?>
                        <div class="contrast_font"><div class="offer"></div></div> 

                        <?php if ($tlptech_product_hurry) { ?>
                        <div class="hurry">
                            <span class="items_left contrast_color"><?php echo $text_stock_quantity; ?></span>
                            <span class="items_sold"><?php echo $text_items_sold; ?></span>
                        </div>
                        <?php } ?>
                        <?php } ?>

                        <div class="description mb-3">
                            <div><?php echo $description_4; ?></div>
                            <?php if ($manufacturer) { ?>
                            <label><?php echo $text_manufacturer; ?></label> 
                            <span><a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></span>
                            <br />
                            <?php } ?>    

                            <label><?php echo $text_model; ?></label>
                            <span><?php echo $model; ?></span>
                            <br />

                            <?php if ($reward) { ?>
                            <label><?php echo $text_reward; ?></label>
                            <span><?php echo $reward; ?></span>
                            <br />
                            <?php } ?>

                            <span class="contrast_font" itemprop="availability" content="<?php if ($data_qty > 0) {echo "in_stock"; } else {echo "out_of_stock"; } ?>"><?php echo $text_stock; ?></span> <span class="stock_status <?php if ($data_qty > 0) {echo "in_stock"; } ?>"><?php echo $stock; ?></span>

                        </div> <!-- .description ends -->


                        <div id="product">

                            <?php if ($recurrings) { ?>
                            <hr>
                            <h3><?php echo $text_payment_recurring ?></h3>
                            <div class="form-group required">
                                <select name="recurring_id" class="form-control">
                                    <option value=""><?php echo $text_select; ?></option>
                                    <?php foreach ($recurrings as $recurring) { ?>
                                    <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block" id="recurring-description"></div>
                            </div>
                            <?php } ?>

                            <?php if ($options) { ?>
                            <div class="options contrast_font">
                                <h2 class="hidden"><?php echo $text_option; ?></h2>
                                <?php foreach ($options as $option) { ?>

                                <?php if ($option['type'] == 'select') { ?>
                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                    <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                                        <option value=""><?php echo $text_select; ?></option>
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                        <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                            <?php if ($option_value['price']) { ?>
                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                            <?php } ?>
                                        </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <?php } ?>

                                <?php if ($option['type'] == 'radio') { ?>
                                <div class="clearfix form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                    <label class="control-label"><?php echo $option['name']; ?></label>

                                    <?php if($tlptech_image_options == 'thumbs'){ ?>

                                    <div id="input-option<?php echo $option['product_option_id']; ?>" class="clean-option-image">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                        <?php if ($option_value['image']) { ?>
                                        <div class="single-option main_font" <?php if ($option_value['price']) { ?>data-tooltip="<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>"<?php } ?>>
                                           <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" /><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name']; ?>" /></label>
                                       </div>
                                       <?php } ?>
                                       <?php } ?>
                                   </div>

                                   <?php } else { ?>
                                   <div id="input-option<?php echo $option['product_option_id']; ?>" class="clearfix">
                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <div class="radio product-page">
                                        <label>
                                            <input class="radio_input" type="radio" style="margin-top:20px;" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                            <?php if ($option_value['image']) { ?>
                                            <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                                            <?php } ?>  

                                            <div class="hidden">
                                                <?php echo $option_value['name']; ?>
                                                <?php if ($option_value['price']) { ?>
                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                <?php } ?>

                                            </div>
                                        </label>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php } ?>

                            </div>
                            <?php } ?>


                            <?php if ($option['type'] == 'checkbox') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"><?php echo $option['name']; ?></label>
                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                            <?php if ($option_value['image']) { ?>
                                            <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                                            <?php } ?>
                                            <?php echo $option_value['name']; ?>
                                            <?php if ($option_value['price']) { ?>
                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                            <?php } ?>
                                        </label>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php } ?>


                            <?php if ($option['type'] == 'text') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                            </div>
                            <?php } ?>

                            <?php if ($option['type'] == 'textarea') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'file') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"><?php echo $option['name']; ?></label><br />
                                <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="button"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'date') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                    </span></div>
                                </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'datetime') { ?>
                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                    <div class="input-group datetime">
                                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                        </span></div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'time') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group time">
                                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                            </span></div>
                                        </div>
                                        <?php } ?>
                                        <?php } ?>
                                    </div> <!-- .options ends -->
                                    <?php } ?> 

                                    <div class="cart">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-5 col-sm-5 col-4">
                                                <label class="control-label"><?php echo $text_quantity; ?></label>
                                            </div>
                                            <div class="col-lg-5 col-md-7 col-sm-7 col-8">
                                                <div class="input-group mb-3">
                                                  <div class="input-group-prepend">
                                                    <button class="btn btn-secondary quantity_button plus" type="button">+</button>
                                                </div>
                                                <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="quantity form-control text-center" />
                                                <div class="input-group-append">
                                                    <button class="btn btn-secondary quantity_button minus" type="button">-</button>
                                                </div>
                                            </div>
                                            <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                                        </div>
                                    </div>

                                </div> <!-- Cart ends -->

                                <div class="product-page-group-btn">
                                    <button type="submit" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg"><?php echo $button_cart; ?></button>
                                    <button class="btn btn-outline-primary btn-lg wishlist rounded-circle" onclick="wishlist.add('<?php echo $product_id; ?>');" data-tooltip="<?php echo $button_wishlist; ?>"><i class="icon icon-Heart"></i></button>

                                    <button class="btn btn-outline-primary btn-lg compare rounded-circle" onclick="compare.add('<?php echo $product_id; ?>');" data-tooltip="<?php echo $button_compare; ?>"><i class="icon icon-MusicMixer"></i></button>
                                </div>

                                <?php if ($minimum > 1 && 1==2) { ?>
                                <div class="minimum"><?php echo $text_minimum; ?></div>
                                <?php } ?>
                                <?php if ($price) { ?>
                                <?php if ($points && 1==3) { ?>
                                <div class="reward"><?php echo $text_points; ?> <?php echo $points; ?></div>
                                <?php } ?>
                                <?php if ($discounts) { ?>
                                <div class="discount">
                                    <?php foreach ($discounts as $discount) { ?>
                                    <span><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></span>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                                <?php } ?>
                            </div>
                            <br>
                            <?php if($description_4){ ?>
                            <div class="price product-page-price-tl"> </div>
                            <div class="short-description-tl">
                                <div class="nd-description-tl">
                                    <?php echo $description_4; ?>
                                </div>
                            </div>
                            <?php } ?>
                        </div> <!-- product-info-right END -->

                    </div> <!-- product-info END -->
                </div>
            </div>
            <div class="tab-product-page">
             <ul class="nav nav-tabs">

                <li class=" nav-item">
                    <a class="nav-link active" href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a>
                </li>
                <?php if ($question_status) { ?>
                <li id="product-question" class=" nav-item"><a class="nav-link" href="#tab-questions" data-toggle="tab"><?php echo $tab_questions; ?> (<?php echo $questions_total; ?>)</a></li>
                <?php } ?>

                <?php if ($attribute_groups) { ?>
                <li class=" nav-item"><a class="nav-link" href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                <?php }?>


                <?php if ($review_status) { ?>
                <li class=" nav-item"><a class="nav-link" href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
                <?php } ?>


                <?php if ($product_tabs_5) { ?>
                <?php foreach($product_tabs_5 as $product_tab_5) { ?>
                <li class=" nav-item"><a class="nav-link" href="#tab-product-tab<?php echo $product_tab_5['tab_id'];?>" data-toggle="tab"><?php echo $product_tab_5['name']; ?></a></li>
                <?php } ?>
                <?php } ?>


            </ul>
            <div class="tab-content">
                <?php if ($question_status) { ?>
                <div class="tab-pane" id="tab-questions" role="tabpanel">
                    <?php echo $product_questions; ?>
                </div>
                <?php } ?>
                <div class="tab-pane active" id="tab-description" role="tabpanel"><?php echo $description; ?>
                    <?php if ($tags) { ?>
                    <div class="tags"><?php echo $text_tags;?>
                        <?php for ($i = 0; $i < count($tags); $i++) { ?>
                        <?php if ($i < (count($tags) - 1)) { ?>
                        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
                        <?php } else { ?>
                        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
                        <?php } ?>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </div>

                <?php if ($attribute_groups) { ?>
                <div class="tab-pane" id="tab-specification" role="tabpanel">
                    <table class="attribute">
                        <?php foreach ($attribute_groups as $attribute_group) { ?>
                        <thead>
                            <tr>
                                <td colspan="2"><?php echo $attribute_group['name']; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                            <tr>
                                <td><?php echo $attribute['name']; ?></td>
                                <td><?php echo $attribute['text']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        <?php } ?>
                    </table>
                </div>
                <?php } ?>

                <?php if ($review_status) { ?>
                <div class="tab-pane" id="tab-review" role="tabpanel">
                    <form id="form-review" >
                        <div id="review"></div>
                        <div class="write_review contrast_font">
                            <h2 id="review-title"><?php echo $text_write; ?></h2>
                            <?php if ($review_guest) { ?>

                            <div class="form-group required">
                                <label class="font-weight-bold" for="input-name"><?php echo $entry_name; ?></label>
                                <input type="text" name="name" value="" id="input-name" class="form-control" />
                            </div>

                            <div class="form-group required">
                                <label class="font-weight-bold" for="input-review"><?php echo $entry_review; ?></label>
                                <textarea name="text" id="input-review" rows="8" style="width: 100%;"></textarea>
                                <small><?php echo $text_note; ?></small>
                            </div>

                            <div class="form-group required">

                                <label class="font-weight-bold"><?php echo $entry_rating; ?></label>
                                &nbsp;&nbsp;&nbsp; <span class="main_font"><?php echo $entry_bad; ?></span>&nbsp;
                                <input type="radio" name="rating" value="1" />
                                &nbsp;
                                <input type="radio" name="rating" value="2" />
                                &nbsp;
                                <input type="radio" name="rating" value="3" />
                                &nbsp;
                                <input type="radio" name="rating" value="4" />
                                &nbsp;
                                <input type="radio" name="rating" value="5" />
                                &nbsp;<span class="main_font"><?php echo $entry_good; ?></span>
                            </div>

                            <?php if ((float)VERSION >= 2.1) { ?>
                            <div class="vertical-captcha"><?php echo $captcha; ?></div>
                            <div class="col-sm-12 text-right"><a id="button-review" class="btn btn-default"><?php echo $button_continue; ?></a></div>
                            <div class="clearfix"></div>
                            <?php } else { ?>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <?php if ($site_key) { ?>
                                    <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                                    <?php } ?>
                                </div>
                                <div class="col-sm-6"><a id="button-review" class="button pull-right"><?php echo $button_continue; ?></a></div>
                            </div>
                            <?php } ?>

                        </div>

                        <?php } else { ?>
                        <div class="alert alert-info main_font"><?php echo $text_login; ?></div>
                    </form>
                </div>
                <?php } ?>
            </div>
            <hr/>
            <div>
                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.10&appId=1647422078623194";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
                <div class="fb-comments" data-href="<?php echo HTTPS_SERVER; ?>index.php?product_id=<?php echo $product_id; ?>#moderation" data-width="100%" data-numposts="5"></div>
            </div>
        </div>
    </div>

</div>



</div>

<?php } ?>


<?php if ($product_tabs_5) { ?>
<?php foreach($product_tabs_5 as $product_tab_5) { ?>
<div class="tab-pane" id="tab-product-tab<?php echo $product_tab_5['tab_id'];?>">
    <?php echo $product_tab_5['text']; ?>
</div>
<?php } ?>
<?php } ?>
</div>
<div class="container">
    <?php if ($products) { ?>
    <div class="module-title text-center">
        <h3 class="title">  <?php echo $text_related; ?> </h3>
    </div>
    <div class="row grid5">
        <?php foreach ($products as $product) { ?>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 item-grid">
            <div class="product-thumb">
                <div class="thumb-image">
                    <?php if ($product['special'] && $tlptech_percentage_sale_badge == 'enabled') { ?>
                    <div class="sale">
                      <span class="sale-text">sale</span>
                      <span class="sale-percent"><?php echo $product['sales_percantage']; ?>%</span>
                  </div>
                  <?php } ?>
                  <?php if ($product['new']!='') { ?>
                  <div class="tag-new"><?php echo $product['new']; ?></div>
                  <?php } ?>
                  <?php if ($product['hot']!='') { ?>
                  <div class="tag-hot"><?php echo $product['hot']; ?></div>
                  <?php } ?>
                  <div class="image">
                      <?php if ($product['thumb_hover'] && $tlptech_rollover_effect == 'enabled') { ?>
                      <div class="image-hover">
                        <a href="<?php echo $product['href']; ?>">
                          <img src="<?php echo $product['thumb_hover']; ?>" alt="<?php echo $product['name']; ?>" />
                      </a>
                  </div>
                  <a href="<?php echo $product['href']; ?>">
                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
                </a>
                <?php } elseif ($product['thumb']) { ?>
                <a href="<?php echo $product['href']; ?>">
                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
                </a>
                <?php } ?>
            </div>

            <div class="actions-link">                
              <button class="btn-wishlist button" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="icon icon-Heart"></i></button>

              <button class="btn-compare button" type="button" data-toggle="tooltip" title="" onclick="compare.add('<?php echo $product['product_id']; ?>');" data-original-title="<?php echo $button_compare; ?>">
                <i class="icon icon-MusicMixer"></i>
            </button>

            <button class="btn-quickview button" type="button" data-toggle="tooltip" title="" onclick="ocquickview.ajaxView('')" data-original-title="Quick View">
                <i class="icon icon-Eye"></i>
            </button> 

        </div>
    </div>
    <div class="caption text-center">
        <div class="rating">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($product['rating'] < $i) { ?>
          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } else { ?>
          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } ?>
          <?php } ?>
      </div>
      <h5 class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h5>

      <?php if ($product['price']) { ?>
      <p class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
      </p>
      <?php } ?>
      <div class="thumb-bottom text-center">
          <button class="btn-cart btn btn-primary " type="button" data-toggle="tooltip" title="" onclick="cart.add('<?php echo $product['product_id']; ?>');" data-original-title="<?php echo $button_cart; ?>">
            Add to card
        </button>
    </div>
</div>
</div>
</div>
<?php } ?>
</div>
<?php } ?>
</div>
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content" id="popup-report">
        <span class="close">&times;</span>
        <div style="clear: both"></div>
        <h2><?php echo $text_contact_wholesale_price;?></h2>
        <div class="row">
            <div class="form-group col-sm-12 required">
                <label class="control-label" for="input-name">Tên bạn</label>
                <input type="text" name="name" value="" id="input-name" class="form-control">
            </div>

            <div class="form-group col-sm-12 required">
                <label class="control-label" for="input-email">Số điện thoại</label>
                <input type="text" name="telephone" value="" id="input-number" class="form-control">
            </div>

            <div class="form-group col-sm-12 required">
                <label class="control-label" for="input-email">Địa chỉ email</label>
                <input type="text" name="email" value="" id="input-email" class="form-control">
            </div>
            <div class="form-group col-sm-12 required">
                <label class="control-label" for="input-enquiry">Yêu cầu</label>
                <textarea name="enquiry" rows="5" id="input-enquiry" class="form-control"></textarea>
            </div>
            <div class="vertical-captcha">
            </div>

            <div class="col-sm-12 text-right">
                <input class="btn btn-primary" type="button" id="button-send" name="button_send_report" value="Gửi">
            </div>
        </div>
    </div>

</div>
<?php echo $content_bottom; ?></div>
<?php echo $column_right; ?></div>

<script type="text/javascript">
    var $radioButtons = $(".radio_input");
    $radioButtons.click(function() {
        $radioButtons.each(function() {
            $(this).parent().toggleClass('checked_img', this.checked);
        });
    });
</script>


<script type="text/javascript"><!--
    $('#popup-report #button-send').on('click', function() {
        $.ajax({
            url: 'index.php?route=information/contact/validateWhosesale&product_id=<?php echo $product_id;?>',
            type: 'post',
            data: $('#popup-report textarea[name=\'enquiry\'], #popup-report input[type=\'text\']'),
            dataType: 'json',
            beforeSend: function() {
                $('#popup-report button[name=\'button_send_report\']').button('loading');
            },
            complete: function() {
                $('#popup-report button[name=\'button_send_report\']').button('reset');
            },
            success: function(json) {
                $('.alert, .text-danger, .error').remove();
                if (json['error']) {
                    if (json['error']['telephone']) {
                        $('#popup-report input[name=\'telephone\']').after('<div class="text-danger example">' + json['error']['telephone'] + '</div>');
                    }
                    if (json['error']['name']) {
                        $('#popup-report input[name=\'name\']').after('<div class="text-danger example">' + json['error']['name'] + '</div>');
                    }
                    if (json['error']['email']) {
                        $('#popup-report input[name=\'email\']').after('<div class="text-danger example">' + json['error']['email'] + '</div>');
                    }
                    if (json['error']['enquiry']) {
                        $('#popup-report textarea[name=\'enquiry\']').after('<div class="error">' + json['error']['enquiry'] + '</div>');
                    }
                } else {
                    $('#popup-report input[name=\'name\']').val('');
                    $('#popup-report input[name=\'email\']').val('');
                    $('#popup-report input[name=\'telephone\']').val('');
                    $('#popup-report textarea[name=\'enquiry\']').val('');
                    $('#popup-report h2').after('<div class="alert alert-success">' + json['success'] + '</div>');
                }
            }
        });
    });
    //--></script>
    <script type="text/javascript"><!--
        var modal = document.getElementById('myModal');
        var btn = document.getElementById("lienhemuahang");
        var span = document.getElementsByClassName("close")[0];
        btn.onclick = function() {
            modal.style.display = "block";
        }

        span.onclick = function() {
            modal.style.display = "none";
        }

        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
        //--></script>
        <script type="text/javascript"><!--
            $(document).ready(function() {
                $('.colorbox').colorbox({
                    overlayClose: true,
                    maxWidth:'95%',
                    rel:'gallery',
                    opacity: 0.5
                });
            });
            //--></script>
            <script type="text/javascript">
                jQuery(function($) {
            //Product thumbnails
            $(".cloud-zoom-gallery").last().removeClass("cboxElement");
            $(".cloud-zoom-gallery").click(function() {
                $("#zoom-btn").attr('href', $(this).attr('href'));
                $("#zoom-btn").attr('title', $(this).attr('title'));
                $(".cloud-zoom-gallery").each(function() {
                    $(this).addClass("cboxElement");
                });
                $(this).removeClass("cboxElement");
            });
        });</script>
        <script type="text/javascript">
            $(document).ready(function() {
                var owlAdditionals = $('.image_carousel');
                owlAdditionals.owlCarousel({
                    items : 6,
                    dots: false,
                    nav: true,
                    margin: 10,
                    responsive:{
                        0:{
                          items:2
                      },
                      600:{
                          items:4
                      },
                      1000:{
                        items:6
                    }
                },
                navText: [
                "<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"
                ]
            });
        });</script> 
        <script type="text/javascript">
            $('.quantity_button.plus').on('click', function(){
                var oldVal = $('input.quantity').val();
                var newVal = (parseInt($('input.quantity').val(), 10) + 1);
                $('input.quantity').val(newVal);
            });
            $('.quantity_button.minus').on('click', function(){
                var oldVal = $('input.quantity').val();
                if (oldVal > 1)
                {
                    var newVal = (parseInt($('input.quantity').val(), 10) - 1);
                }
                else
                {
                    newVal = 1;
                }
                $('input.quantity').val(newVal);
            });</script>
            <?php if ($special_date_end > 0) { ?>

            <script type="text/javascript">
                $('.offer').countdown({
                    until: <?php echo $special_date_end ?> ,
                    layout: '{desc}<i>{dn}</i> {dl} <i>{hn}</i> {hl} <i>{mn}</i> {ml} <i>{sn}</i> {sl}',
                    description: '<span class="main_font"><?php echo $text_expire ?></span>&nbsp;'
                });</script>
                <?php } ?>
                <script type="text/javascript">
                    $(".to_review").click(function() {
                        $('html, body').animate({
                            scrollTop: $("#tab-review").offset().top
                        }, 1000);
                    });</script>

                    <!-- Default scrips below -->    
                    <script type="text/javascript"><!--
                        $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
                            $.ajax({
                                url: 'index.php?route=product/product/getRecurringDescription',
                                type: 'post',
                                data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
                                dataType: 'json',
                                beforeSend: function() {
                                    $('#recurring-description').html('');
                                },
                                success: function(json) {
                                    $('.alert, .text-danger').remove();
                                    if (json['success']) {
                                        $('#recurring-description').html(json['success']);
                                    }
                                }
                            });
                        });
                        //--></script> 
                        <script type="text/javascript"><!--
                            $('#button-cart').on('click', function() {
                                $.ajax({
                                    url: 'index.php?route=checkout/cart/add',
                                    type: 'post',
                                    data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
                                    dataType: 'json',
                                    beforeSend: function() {
                                        $('#button-cart').button('loading');
                                    },
                                    complete: function() {
                                        $('#button-cart').button('reset');
                                    },
                                    success: function(json) {
                                        $('.alert, .text-danger').remove();
                                        $('.form-group').removeClass('has-error');
                                        if (json['error']) {
                                            if (json['error']['option']) {
                                                for (i in json['error']['option']) {
                                                    var element = $('#input-option' + i.replace('_', '-'));
                                                    if (element.parent().hasClass('input-group')) {
                                                        element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                                    } else {
                                                        element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                                    }
                                                }
                                            }

                                            if (json['error']['recurring']) {
                                                $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                                            }

            // Highlight any found errors
            $('.text-danger').parent().addClass('has-error');
        }

        if (json['success']) {
            $('body').addClass('is-popup');
            $.colorbox({
                html:'<div class="cart_notification"><div class="product clearfix"><img src="' + json['image'] + '"/><span>' + json['success'] + '</span></div><div class="bottom text-center"><a class="btn btn-primary" href="' + json['link_cart'] + '">' + json['text_cart'] + '</a> ' + '<a class="btn btn-secondary" href="' + json['link_checkout'] + '">' + json['text_checkout'] + '</a></div></div>',
                className: "notification",
                initialHeight:50,
                initialWidth:50,
                width:"90%",
                maxWidth:400,
                height:"90%",
                maxHeight:200
            });
            $('#cart-total').html(json['total']);
                    $('#cart').load('index.php?route=common/cart/info #cart > *'); //Added
                    $('body').removeClass('is-popup');
                }
            }
        });
                            });
                            //--></script> 
                            <script type="text/javascript"><!--
                                $('#button-buy,#btn-mua-ngay').on('click', function() {
                                    $.ajax({
                                        url: 'index.php?route=checkout/cart/add',
                                        type: 'post',
                                        data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
                                        dataType: 'json',
                                        beforeSend: function() {
                                            $('#button-buy,#btn-mua-ngay').button('loading');
                                        },
                                        complete: function() {
                                            $('#button-buy,#btn-mua-ngay').button('reset');
                                        },
                                        success: function(json) {
                                            $('.alert, .text-danger').remove();
                                            $('.form-group').removeClass('has-error');
                                            if (json['error']) {
                                                if (json['error']['option']) {
                                                    for (i in json['error']['option']) {
                                                        var element = $('#input-option' + i.replace('_', '-'));
                                                        if (element.parent().hasClass('input-group')) {
                                                            element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                                        } else {
                                                            element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                                        }
                                                    }
                                                }

                                                if (json['error']['recurring']) {
                                                    $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                                                }

            // Highlight any found errors
            $('.text-danger').parent().addClass('has-error');
        }

        if (json['success']) {
            location = 'index.php?route=quickcheckout/checkout';
        }
    }
});
                                });
                                //--></script> 
                                <script type="text/javascript"><!--
                                    $('.date').datetimepicker({
                                        pickTime: false
                                    });
                                    $('.datetime').datetimepicker({
                                        pickDate: true,
                                        pickTime: true
                                    });
                                    $('.time').datetimepicker({
                                        pickDate: false
                                    });
                                    $('button[id^=\'button-upload\']').on('click', function() {
                                        var node = this;
                                        $('#form-upload').remove();
                                        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');
                                        $('#form-upload input[name=\'file\']').trigger('click');
                                        if (typeof timer != 'undefined') {
                                            clearInterval(timer);
                                        }

                                        timer = setInterval(function() {
                                            if ($('#form-upload input[name=\'file\']').val() != '') {
                                                clearInterval(timer);
                                                $.ajax({
                                                    url: 'index.php?route=tool/upload',
                                                    type: 'post',
                                                    dataType: 'json',
                                                    data: new FormData($('#form-upload')[0]),
                                                    cache: false,
                                                    contentType: false,
                                                    processData: false,
                                                    beforeSend: function() {
                                                        $(node).button('loading');
                                                    },
                                                    complete: function() {
                                                        $(node).button('reset');
                                                    },
                                                    success: function(json) {
                                                        $('.text-danger').remove();
                                                        if (json['error']) {
                                                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                                                        }

                                                        if (json['success']) {
                                                            alert(json['success']);
                                                            $(node).parent().find('input').attr('value', json['code']);
                                                        }
                                                    },
                                                    error: function(xhr, ajaxOptions, thrownError) {
                                                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                                    }
                                                });
                                            }
                                        }, 500);
                                    });
                                    //--></script> 
                                    <script type="text/javascript"><!--
                                        $('#review').delegate('.pagination a', 'click', function(e) {
                                            e.preventDefault();
                                            $('#review').fadeOut('slow');
                                            $('#review').load(this.href);
                                            $('#review').fadeIn('slow');
                                        });
                                        $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');
                                        $('#button-review').on('click', function() {
                                            $.ajax({
                                                url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
                                                type: 'post',
                                                dataType: 'json',
                                                data: $("#form-review").serialize(),
                                                beforeSend: function() {
                                                    $('#button-review').button('loading');
                                                },
                                                complete: function() {
                                                    $('#button-review').button('reset');
                                                },
                                                success: function(json) {
                                                    $('.alert-success, .alert-danger').remove();
                                                    if (json['error']) {
                                                        $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                                                    }

                                                    if (json['success']) {
                                                        $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
                                                        $('input[name=\'name\']').val('');
                                                        $('textarea[name=\'text\']').val('');
                                                        $('input[name=\'rating\']:checked').prop('checked', false);
                                                    }
                                                }
                                            });
                                        });
                                        //--></script>
                                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js"></script>
                                    </div>
                                    <?php echo $footer; ?>