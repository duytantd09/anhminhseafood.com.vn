<?php echo $header; ?>
<div class="bbtel-spnb">
    <div class="container">
      <div class="home-titlel"><h1><?php echo $heading_title; ?></h1></div>
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
    </div>
  </div>
<div class="container ocbestseller-htl">
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <?php if ($products) { ?>
            <p><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></p>
            <div class="row">
                <div class="col-md-4">
                    <div class="btn-group hidden-xs">
                        <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
                        <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
                    </div>
                </div>
                <div class="col-md-2 text-right">
                    <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
                </div>
                <div class="col-md-3 text-right">
                    <select id="input-sort" class="form-control" onchange="location = this.value;">
                        <?php foreach ($sorts as $sorts2) { ?>
                            <?php if ($sorts2['value'] == $sort . '-' . $order) { ?>
                                <option value="<?php echo $sorts2['href']; ?>" selected="selected"><?php echo $sorts2['text']; ?></option>
                            <?php } else { ?>
                                <option value="<?php echo $sorts2['href']; ?>"><?php echo $sorts2['text']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-1 text-right">
                    <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
                </div>
                <div class="col-md-2 text-right">
                    <select id="input-limit" class="form-control" onchange="location = this.value;">
                        <?php foreach ($limits as $limits2) { ?>
                        <?php if ($limits2['value'] == $limit) { ?>
                        <option value="<?php echo $limits2['href']; ?>" selected="selected"><?php echo $limits2['text']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $limits2['href']; ?>"><?php echo $limits2['text']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <br />
            <div class="row products-category product-grid">
            <?php foreach ($products as $product) { ?>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 product-col1 width-200 item">
                    <div class="row_item product-thumb transition"> 
                        <div class="image">
                          <?php if ($product['special'] && $tlptech_percentage_sale_badge == 'enabled') { ?>
                          <div class="sale">
                              <div class="sale-nt">sale</div>
                              <div class="sale-so-ft"><?php echo $product['sales_percantage']; ?>%</div>
                          </div>
                          <?php } ?>
                          <?php if ($product['thumb_hover'] && $tlptech_rollover_effect == 'enabled') { ?>
                          <div class="image_hover"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb_hover']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
                          <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
                          <?php } elseif ($product['thumb']) { ?>
                          <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
                          <?php } ?>

                          <?php if ($tlptech_text_ql) { ?>
                          <div class="main_quicklook">
                            <a href="<?php echo $product['quickview']; ?>" rel="nofollow" class="button quickview"><i class="fa fa-eye"></i> <?php echo $tlptech_text_ql; ?></a>
                          </div>
                          <?php } ?>
                          <div class="all-new-hot clearfix">
                            <?php if ($product['new']!='') { ?>
                            <div class="new"><?php echo $product['new']; ?></div>
                            <?php } ?>
                            <?php if ($product['hot']!='') { ?>
                            <div class="hot"><?php echo $product['hot']; ?></div>
                            <?php } ?>
                          </div>

                        </div><!-- image ends -->
                        <!-- hover vào hiện ra -->
                        <div class="actions-link">                
                            <button class="btn-cart transiton-product" type="button" data-toggle="tooltip" title="" onclick="cart.add('<?php echo $product['product_id']; ?>');" data-original-title="<?php echo $button_cart; ?>">
                            <i class="fa fa-shopping-basket"></i><span><?php echo $button_cart; ?></span>
                            </button>
                            <button class="btn-compare transiton-product" type="button" data-toggle="tooltip" title="" onclick="compare.add('<?php echo $product['product_id']; ?>');" data-original-title="<?php echo $button_compare; ?>">
                            <i class="fa fa-compress"></i><span><?php echo $button_compare; ?></span>
                            </button>
                            <a href="<?php echo $product['quickview']; ?>" rel="nofollow" class="sq_icon qlook quickview transiton-product" data-tooltip="<?php echo $tlptech_text_ql; ?>" title><i class="fa fa-eye"></i></a>
                        </div>
                        <div class="caption">
                        <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                        <!--   <p><?php echo $product['description']; ?></p> -->
                        <?php //if ($product['rating']) { ?>
                        <div class="rating">
                          <?php for ($i = 1; $i <= 5; $i++) { ?>
                          <?php if ($product['rating'] < $i) { ?>
                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                          <?php } else { ?>
                          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                          <?php } ?>
                          <?php } ?>
                        </div>
                        <?php //} ?>
                        <?php if ($product['price']) { ?>
                        <p class="price">
                          <?php if (!$product['special']) { ?>
                          <?php echo $product['price']; ?>
                          <?php } else { ?>
                          <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                          <?php } ?>
                          <?php if ($product['tax']) { ?>
                          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                          <?php } ?>
                        </p>
                        <?php } ?>
                      </div>
                      <div class="button-group">
                        <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                      </div>
                    </div>
                </div>
                <?php } ?>
            </div> <!-- products-category -->
            <div class="row">
                <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                <div class="col-sm-6 text-right"><?php echo $results; ?></div>
            </div>
            <?php } else { ?>
            <p><?php echo $text_empty; ?></p>
            <div class="buttons">
                <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
            </div>
            <?php } ?>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
