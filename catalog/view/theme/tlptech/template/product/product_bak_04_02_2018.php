=======
<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?> <span><i class="fa fa-play-circle" aria-hidden="true"></i></span></a></li>
        <?php } ?>
    </ul>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-lg-9 col-md-9 col-sm-8'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?> product">
            <?php echo $content_top; ?>

            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    <div class="product-info clearfix">
                        <?php if ($thumb || $images) { ?>
                        <div class="left">
                            <?php if ($thumb) { ?>
                            <div class="image">

                                <?php if ($tlptech_product_zoom) { ?>
                                <a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="cloud-zoom" style="cursor:move" rel="position:'inside', showTitle: false" id='zoom1'><img itemprop="image" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
                                <!-- zoom link-->
                                <a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" id="zoom-btn" class="colorbox" rel="colorbox"><i class="fa fa-search-plus"></i></a>
                                <?php } else { ?>
                                <a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="colorbox"><img itemprop="image" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
                                <?php } ?>

                                <?php if ($tlptech_percentage_sale_badge == 'enabled') { ?>
                                <?php if (!$special) { ?>
                                <?php } else { ?>
                                <div class="sale-off product-page">-<?php echo $sales_percantage_main; ?>%</div>
                                <?php } ?>
                                <?php } ?>

                                <?php if ($product_new!='') { ?>
                                <div class="new"><?php echo $product_new; ?></div>
                                <?php } ?>

                                <?php if ($product_hot!='') { ?>
                                <div class="hot"><?php echo $product_hot; ?></div>
                                <?php } ?>

                            </div>

                            <?php } ?>



                            <?php if ($images) { ?>

                            <div class="image-additional" >      
                                <ul class="image_carousel">
                                    <!-- Additional images -->
                                    <?php foreach ($images as $image) { ?>
                                    <li>
                                        <?php if ($tlptech_product_zoom) { ?>
                                        <a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="cloud-zoom-gallery colorbox" rel="useZoom: 'zoom1', smallImage: '<?php echo $image['thumb']; ?>'">
                                            <?php } else { ?>
                                            <a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="colorbox" rel="useZoom: 'zoom1', smallImage: '<?php echo $image['thumb']; ?>'">
                                                <?php } ?>
                                                <img src="<?php echo $image['small']; ?>" title="<?php echo $heading_title; ?>" width="<?php echo $additional_width; ?>" height="<?php echo $additional_height; ?>" alt="<?php echo $heading_title; ?>" /></a>
                                            </li>
                                            <?php } ?>

                                            <!-- Show even the main image among the additional if  -->
                                            <?php if ($tlptech_product_zoom) { ?>
                                            <li><a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="cloud-zoom-gallery colorbox" rel="useZoom: 'zoom1', smallImage: '<?php echo $thumb; ?>'"><img src="<?php echo $small; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" width="<?php echo $additional_width; ?>" height="<?php echo $additional_height; ?>"/></a></li>
                                            <?php } ?>
                                        </ul>

                            </div>
                                <?php } else { ?>
                                <?php if ($tlptech_product_zoom) { ?>
                                <div class="image-additional" >      
                                    <ul class="image_carousel">
                                        <li><a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="cloud-zoom-gallery colorbox" rel="useZoom: 'zoom1', smallImage: '<?php echo $thumb; ?>'"><img src="<?php echo $small; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" width="<?php echo $additional_width; ?>" height="<?php echo $additional_height; ?>"/></a></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <?php }?>


                                <?php if($tlptech_product_share == 'image'){ ?>
                                <!-- AddThis Button START -->
                                <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                                    <a class="addthis_button_preferred_1"></a>
                                    <a class="addthis_button_preferred_2"></a>
                                    <a class="addthis_button_preferred_3"></a>
                                    <a class="addthis_button_preferred_4"></a>
                                    <a class="addthis_button_compact"></a>
                                    <a class="addthis_counter addthis_bubble_style"></a>
                                </div>
                                <!-- AddThis Button END -->
                                <?php } ?>
                        </div>
                        <?php } ?>
                        <div class="right">
                            <h1 itemprop="name"><?php echo $heading_title; ?></h1>
                            <?php if ($review_status) { ?>
                            <div class="review">
                                <div class="rating">
                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                    <?php if ($rating < $i) { ?>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <?php } else { ?>
                                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <?php } ?>
                                    <?php } ?>
                                </div>
                                <!-- AddThis Button BEGIN -->
                                <div class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a>
                                </div>
                                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
                                <!-- AddThis Button END -->

                                <a class="to_review" onclick="$('a[href=\'#tab-review\']').trigger('click');"><?php echo $reviews; ?></a>
                                <a class="to_review" onclick="$('a[href=\'#tab-review\']').trigger('click');"><?php echo $text_write; ?></a>
                            </div>
                            <?php if ($count_reviews > 0) { ?><!-- Rich snippet start -->
                            <div itemprop="review" itemscope itemtype="http://data-vocabulary.org/Review-aggregate"> 
                                <span itemprop="rating" content="<?php echo $rating; ?>"></span><span itemprop="count" content="<?php echo $reviews; ?>"></span>
                            </div> 
                            <?php } ?><!-- Rich snippet end -->

                            <?php } ?>


                            <?php if ($price) { ?> 
                            <div class="price product-page-price">

                                <?php if (!$special) { ?>
                                <span itemprop="price"><?php echo $price; ?></span>
                                <?php } else { ?>
                                <?php if (!$tlptech_product_yousave) { ?>
                                <span class="price-old"><?php echo $price; ?></span> <span class="price-new" itemprop="price"><?php echo $special; ?></span>
                                <?php } ?>
                                <?php } ?>

                            </div> <!-- rich snippet ends -->
                            <?php } ?>                    


                            <?php if ($price) { ?>
                            <meta itemprop="currency" content="<?php echo $currency_code; ?>" />
                                    <span itemprop="offerDetails" itemscope itemtype="http://data-vocabulary.org/Offer"><!-- Rich snippets start -->
                                        <?php if (($special) && ($tlptech_product_yousave)) { ?>

                                        <div class="extended_offer">

                                            <div class="price-new"><?php echo $text_special_price; ?><span class="amount contrast_font" itemprop="price"><?php echo $special; ?></span></div>
                                            <div class="price-old"><?php echo $text_old_price; ?><span class="amount contrast_font"><?php echo $price; ?></span></div>
                                            <!-- <div class="price-save"><?php echo $text_you_save; ?><span class="amount contrast_font"><?php echo $yousave; ?></span> </div>-->
                                        </div>

                                        <?php } ?>
                                        <?php } ?>



                                        <div class="description">
                                            <div><?php echo $description_4; ?></div>
                                            <?php if ($manufacturer) { ?>
                                            <span class="contrast_font"><?php echo $text_manufacturer; ?></span> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a><br />
                                            <?php } ?>    

                                            <span class="contrast_font"><?php echo $text_model; ?></span> <?php echo $model; ?><br />

                                            <?php if ($reward) { ?>
                                            <span class="contrast_font"><?php echo $text_reward; ?></span> <?php echo $reward; ?><br />
                                            <?php } ?>

                                            <span class="contrast_font" itemprop="availability" content="<?php if ($data_qty > 0) {echo "in_stock"; } else {echo "out_of_stock"; } ?>"><?php echo $text_stock; ?></span> <span class="stock_status <?php if ($data_qty > 0) {echo "in_stock"; } ?>"><?php echo $stock; ?></span>

                                        </div> <!-- .description ends -->

                                        <div class="icons_wrapper">
                                            <a class="sq_icon" onclick="wishlist.add('<?php echo $product_id; ?>');" data-tooltip="<?php echo $button_wishlist; ?>"><i class="fa fa-heart"></i></a>
                                            <a class="sq_icon compare" onclick="compare.add('<?php echo $product_id; ?>');" data-tooltip="<?php echo $button_compare; ?>"><i class="fa fa-arrow-right"></i><i class="fa fa-arrow-left main_compare"></i></a>
                                <!--
                                        <?php if ($question_status) { ?>
                                        <a id="button_ask" onclick="$('a[href=\'#tab-questions\']').trigger('click');" class="sq_icon hidden" data-tooltip="<?php echo $button_ask; ?>"><i class="fa fa-question"></i>&nbsp;</a>
                                        <?php } ?>-->

                                        </div>                                        
                                        <div id="product">

                                            <?php if ($recurrings) { ?>
                                            <hr>
                                            <h3><?php echo $text_payment_recurring ?></h3>
                                            <div class="form-group required">
                                                <select name="recurring_id" class="form-control">
                                                    <option value=""><?php echo $text_select; ?></option>
                                                    <?php foreach ($recurrings as $recurring) { ?>
                                                    <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                                                    <?php } ?>
                                                </select>
                                                <div class="help-block" id="recurring-description"></div>
                                            </div>
                                            <?php } ?>

                                            <?php if ($options) { ?>
                                            <div class="options contrast_font">
                                                <h2 class="hidden"><?php echo $text_option; ?></h2>
                                                <?php foreach ($options as $option) { ?>

                                                <?php if ($option['type'] == 'radio') { ?>
                                                <div class="select-color clearfix show_error">
                                                    <label class="control-label"><strong>Chọn màu sắc</strong></label>
                                                    <div class="box-color " id="input-option<?php echo $option['product_option_id']; ?>">
                                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                          <label>
                                                            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" data-image="<?php echo $option_value['product_image']; ?>" data-price="<?php echo $option_value['price']; ?>"><?php echo $option_value['name']; ?></label>
                                                        </div>
                                                        <?php } ?>                                      
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php if ($option['type'] == 'select') { ?>
                                                <div class="form-group row select-package show_error">
                                                  <label class="control-label col-xs-3" for="input-option988"><?php echo $option['name']; ?></label>
                                                  <div class="col-xs-9">
                                                      <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <option value="<?php echo $option_value['product_option_value_id']; ?>" data-price="<?php echo $option_value['price']; ?>"><?php echo $option_value['name']; ?>
                                                        </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <?php } ?>


                                            <?php if ($option['type'] == 'checkbox') { ?>
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"><?php echo $option['name']; ?></label>
                                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                            <?php if ($option_value['image']) { ?>
                                                            <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                                                            <?php } ?>
                                                            <?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <?php } ?>


                                            <?php if ($option['type'] == 'text') { ?>
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                            </div>
                                            <?php } ?>

                                            <?php if ($option['type'] == 'textarea') { ?>
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                                            </div>
                                            <?php } ?>
                                            <?php if ($option['type'] == 'file') { ?>
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"><?php echo $option['name']; ?></label><br />
                                                <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="button"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                                <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                                            </div>
                                            <?php } ?>
                                            <?php if ($option['type'] == 'date') { ?>
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <div class="input-group date">
                                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                    </span></div>
                                                </div>
                                                <?php } ?>
                                                <?php if ($option['type'] == 'datetime') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                    <div class="input-group datetime">
                                                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                        </span></div>
                                                    </div>
                                                    <?php } ?>
                                                    <?php if ($option['type'] == 'time') { ?>
                                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                        <div class="input-group time">
                                                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                            </span></div>
                                                        </div>
                                                        <?php } ?>
                                                        <?php } ?>
                                            </div> <!-- .options ends -->
                                            <?php } ?> 

                                            <div class="cart">
                                                <span><?php echo $text_quantity; ?> </span>
                                                <a class="quantity_button minus icon">-</a>
                                                <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="quantity" />
                                                <a class="quantity_button plus icon">+</a>
                                                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />

                                                <div class="product-page group-btn">
                                                    <button type="submit" id="btn-mua-ngay" data-loading-text="<?php echo $text_loading; ?>" class="button contrast"> <?php echo $button_buy; ?> <br><span> Giao hàng tận nơi hoặc nhận ở shop</span></button>
                                                    <button type="submit" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="button contrast"><?php echo $button_cart; ?><br><span> Giao hàng tận nơi hoặc nhận ở shop</span></button>
                                                </div>
                                                <div class="product-page lienhemuahang"> <a href="#myModal" class="btn-tragop" title="" id="lienhemuahang" data-toggle="modal">Trả góp. Chỉ trả trước từ 1,179,000đ<br> <span>Liên hệ (028) 3922 5527 hoặc (028) 3922 5527 để được tư vấn</span></a> </div>


                                            </div> <!-- Cart ends -->
                            <?php if ($minimum > 1 && 1==2) { ?>
                            <div class="minimum"><?php echo $text_minimum; ?></div>
                            <?php } ?>
                            <?php if ($price) { ?>
                            <?php if ($points && 1==3) { ?>
                            <div class="reward"><?php echo $text_points; ?> <?php echo $points; ?></div>
                            <?php } ?>
                            <?php if ($discounts) { ?>
                            <div class="discount">
                                <?php foreach ($discounts as $discount) { ?>
                                <span><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></span>
                                <?php } ?>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>

                        </div> <!-- product-info-right END -->
                    </div> <!-- product-info END -->

                    <ul class="nav nav-tabs product-page">

                        <li class="active">
                            <a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a>
                        </li>
                        <?php if ($attribute_groups) { ?>
                        <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                        <?php }?>
                        
                        <?php if ($question_status) { ?>
                        <li id="product-question"><a href="#tab-questions" data-toggle="tab"><?php echo $tab_questions; ?> (<?php echo $questions_total; ?>)</a></li>
                        <?php } ?>
                        <?php if ($review_status) { ?>
                        <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
                        <?php } ?>


                        <?php if ($product_tabs_5) { ?>
                        <?php foreach($product_tabs_5 as $product_tab_5) { ?>
                        <li><a href="#tab-product-tab<?php echo $product_tab_5['tab_id'];?>" data-toggle="tab"><?php echo $product_tab_5['name']; ?></a></li>
                        <?php } ?>
                        <?php } ?>
                    </ul>
                    <div class="tab-content">
                        <?php if ($question_status) { ?>
                        <div class="tab-pane" id="tab-questions">
                            <?php echo $product_questions; ?>
                        </div>
                        <?php } ?>
                        <div class="tab-pane active" id="tab-description"><?php echo $description; ?>
                            <?php if ($tags) { ?>
                            <div class="tags"><?php echo $text_tags;?>
                                <?php for ($i = 0; $i < count($tags); $i++) { ?>
                                <?php if ($i < (count($tags) - 1)) { ?>
                                <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
                                <?php } else { ?>
                                <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
                                <?php } ?>
                                <?php } ?>
                            </div>
                            <?php } ?>
                        </div>

                        <?php if ($attribute_groups) { ?>
                        <div class="tab-pane" id="tab-specification">
                            <table class="attribute">
                                <?php foreach ($attribute_groups as $attribute_group) { ?>
                                <thead>
                                    <tr>
                                        <td colspan="2"><?php echo $attribute_group['name']; ?></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                    <tr>
                                        <td><?php echo $attribute['name']; ?></td>
                                        <td><?php echo $attribute['text']; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                <?php } ?>
                            </table>
                        </div>
                        <?php } ?>

                        <?php if ($review_status) { ?>
                        <div class="tab-pane" id="tab-review">
                            <form id="form-review" class="form-horizontal">
                                <div id="review"></div>
                                <div class="write_review contrast_font">
                                    <h2 id="review-title"><?php echo $text_write; ?></h2>


                                    <?php if ($review_guest) { ?>

                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                                            <input type="text" name="name" value="" id="input-name" class="form-control" />
                                        </div>
                                    </div>

                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                                            <textarea name="text" id="input-review" rows="8" style="width: 100%;"></textarea>
                                            <small><?php echo $text_note; ?></small>
                                        </div>
                                    </div>

                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label"><?php echo $entry_rating; ?></label>
                                            &nbsp;&nbsp;&nbsp; <span class="main_font"><?php echo $entry_bad; ?></span>&nbsp;
                                            <input type="radio" name="rating" value="1" />
                                            &nbsp;
                                            <input type="radio" name="rating" value="2" />
                                            &nbsp;
                                            <input type="radio" name="rating" value="3" />
                                            &nbsp;
                                            <input type="radio" name="rating" value="4" />
                                            &nbsp;
                                            <input type="radio" name="rating" value="5" />
                                            &nbsp;<span class="main_font"><?php echo $entry_good; ?></span>
                                        </div>
                                    </div>

                                    <?php if ((float)VERSION >= 2.1) { ?>
                                    <div class="vertical-captcha"><?php echo $captcha; ?></div>
                                    <div class="col-sm-12 text-right"><a id="button-review" class="button"><?php echo $button_continue; ?></a></div>
                                    <div class="clearfix"></div>
                                    <?php } else { ?>
                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <?php if ($site_key) { ?>
                                            <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-6"><a id="button-review" class="button pull-right"><?php echo $button_continue; ?></a></div>
                                    </div>
                                    <?php } ?>

                                </div>

                                <?php } else { ?>
                                <div class="alert alert-info main_font"><?php echo $text_login; ?></div>
                            </form>
                        </div>
                        <?php } ?>
                        </div>
                        <hr/>
                        <div>
                            <div id="fb-root"></div>
                            <script>(function(d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s); js.id = id;
                                js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.10&appId=1647422078623194";
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                            <div class="fb-comments" data-href="<?php echo HTTPS_SERVER; ?>index.php?product_id=<?php echo $product_id; ?>#moderation" data-width="100%" data-numposts="5"></div>
                        </div>
                        <div class="div-produc hidden">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                              <li class="nav-item active">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Máy tính xách tay tương tự </a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Máy tính xách tay cùng hãng</a>
                              </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                              <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                <div class="tab-content"> 
                                    <div class="tab-pane active" id="tab00"> 
                                        <div class="grid_holder"> 
                                            <div class="product-grid 0">
                                                <div class="row">
                                                    <?php foreach ($products as $product) { ?>
                                                    <div class="item contrast_font product-layout">
                                                        <div class="row_item"> 
                                                            <div class="image">
                                                                <?php if ($product['special'] && $tlptech_percentage_sale_badge == 'enabled') { ?>
                                                                <div class="sale-off">-<?php echo $product['sales_percantage']; ?>%</div>
                                                                <?php } ?>
                                                                <?php if ($product['thumb_hover'] && $tlptech_rollover_effect == 'enabled') { ?>
                                                                <div class="image_hover"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb_hover']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
                                                                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
                                                                <?php } elseif ($product['thumb']) { ?>
                                                                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
                                                                <?php } ?>

                                                                <?php if ($tlptech_text_ql) { ?>
                                                                <div class="main_quicklook">
                                                                    <a href="<?php echo $product['quickview']; ?>" rel="nofollow" class="button quickview"><i class="fa fa-eye"></i> <?php echo $tlptech_text_ql; ?></a>
                                                                </div>
                                                                <?php } ?>
                                                                <?php if ($product['new']!='') { ?>
                                                                <div class="new"><?php echo $product['new']; ?></div>
                                                                <?php } ?>
                                                                <?php if ($product['hot']!='') { ?>
                                                                <div class="hot"><?php echo $product['hot']; ?></div>
                                                                <?php } ?>

                                                            </div><!-- image ends -->
                                                            <div class="information_wrapper">
                                                                <div class="left">
                                                                    <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                                                                    <?php if ($product['brand_name'] && $tlptech_brand) { ?>
                                                                    <span class="brand main_font"><?php echo $product['brand_name']; ?></span>
                                                                    <?php } ?>
                                                                    <?php if ($product['rating'] || 1==1) { ?>
                                                                    <?php } ?>
                                                                    
                                                                </div>
                                                                <div class="description main_font"><?php echo $product['description']; ?></div>
                                                                <?php if ($product['price']) { ?>
                                                                <div class="price">
                                                                    <?php if (!$product['special']) { ?>
                                                                    <?php echo $product['price']; ?>
                                                                    <?php } else { ?>
                                                                    <span class="price-new"><?php echo $product['special']; ?></span><span class="price-old"><?php echo $product['price']; ?></span> 
                                                                    <?php } ?>
                                                                    <?php if ($product['tax']) { ?>
                                                                    <br />
                                                                    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                                                    <?php } ?>
                                                                </div>
                                                                <?php } ?>
                                                                <div class="group-btn">

                                                                    <button class="btn buy-btn" onclick="cart.buy('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" data-tooltip="<?php echo $button_buy; ?>"><?php echo $button_buy; ?></button>
                                                                    <button class="btn add-to-cart" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" data-tooltip="<?php echo $button_cart; ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i></button>
                                                                </div>
                                                                <?php if (($product['special']) && ($product['special_date_end'] > 0) && ($tlptech_product_countdown)) { ?>
                                                                <div class="offer_popup">
                                                                    <div class="offer_background"></div>
                                                                    <div class="offer_content">
                                                                        <div class="countdown <?php echo $product['product_id']; ?>"></div>
                                                                        <?php if ($tlptech_product_hurry) { ?>
                                                                        <span class="hurry main_font"><?php echo $product['stock_quantity']; ?></span>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                                <script type="text/javascript">
                                                                    $('.<?php echo $product['product_id']; ?>').countdown({
                                                                        until: < ?php echo $product['special_date_end']; ? > ,
                                                                        layout: '<span class="main_font"><?php echo $text_category_expire; ?></span><br /><i>{dn}</i> {dl}&nbsp; <i>{hn}</i>  {hl}&nbsp; <i>{mn}</i>  {ml}&nbsp; <i>{sn}</i> {sl}'});</script>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                              </div>
                              <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">...</div>
                            </div>
                        </div>
                    </div>
                </div>
      
                <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
                    <div class="product-page lienhemuahang">
                        <a href="" title="" id="lienhemuahang" data-toggle="modal" target="#myModal"><p><!--<?php echo $text_contact_wholesale_price;?>-->GIÁ SĨ TỐT NHẤT</p><br><span>Liên hệ chúng tôi qua Hotline</a>
                    </div>
                    <?php if($description_2){ ?>
                    <div class="box_commitment">
                        <div class="panel panel-default">
                          <div class="panel-heading">
                              Khuyến mãi
                          </div>
                          <div class="panel-body">
                            <?php echo $description_2; ?>
                            </div>
                        </div>
                    </div>
                     <?php }?>
                    <div class=" box_commitment">
                        <div class="panel panel-default">
                          <div class="panel-heading">Cam kết bán hàng</div>
                          <div class="panel-body">
                            <ul><li><i class="fa fa-check-circle"></i>Hàng chất lượng. Nguồn gốc rõ ràng</li><li><i class="fa fa-check-circle"></i>Giao hàng ngay (nội thành TPHCM)</li><li><i class="fa fa-check-circle"></i>Giao trong 2 đến 3 ngày (toàn quốc)</li><li><i class="fa fa-check-circle"></i>Gọi lại cho quý khách trong 5 phút</li><li><i class="fa fa-check-circle"></i>Xem hàng tại kho công ty<br></li></ul>  </div>
                        </div>
                    </div>
                    <?php if ($products) { ?>
                    <div class="grid_holder_0">
                        <div class="box products">
                            <div class="box-heading_before"><div class="box_title"><h2 class="related-title"><?php echo $text_related; ?></h2></div></div>
                            <div class="<?php echo $tlptech_grid_related; ?>">
                                <div class="grid_holder">
                                    <div class="product-grid carousel related">
                                        <?php foreach ($products as $product) { ?>
                                        <div class=" product-col">
                                            <div class="product-group">
                                                <div class="row">
                                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                        <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a>
                                                    </div>
                                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                                        <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><h3 class="product-name"><?php echo $product['name']; ?></h3></a>
                                                        <?php if ($product['price']) { ?>
                                                        <div class="price">
                                                            <?php if (!$product['special']) { ?>
                                                            <?php echo $product['price']; ?>
                                                            <?php } else { ?>
                                                            <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                                                            <?php } ?>
                                                            <?php if ($product['tax']) { ?>
                                                            <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                                            <?php } ?>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>


    </div>

    <?php } ?>


<?php if ($product_tabs_5) { ?>
<?php foreach($product_tabs_5 as $product_tab_5) { ?>
<div class="tab-pane" id="tab-product-tab<?php echo $product_tab_5['tab_id'];?>">
    <?php echo $product_tab_5['text']; ?>
</div>
<?php } ?>
<?php } ?>

</div>

    <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content" id="popup-report">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h2><?php echo $text_contact_wholesale_price;?></h2>
                      </div>
                <p class="trantrong">Quý khách hàng hãy dành 01 phút để hoàn tất thông tin dưới đây. Tất cả thông tin được cung cấp sẽ giúp chúng tôi phục vụ quý khách hàng tốt hơn. Thông tin trong bảng tính trả góp mang tính tham khảo.</p>
                <div class="clearfix"> 
                    <form action="product_submit" method="get" accept-charset="utf-8">

                    </form>
                    <form method="post" id="form-bank">
                        <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                        <input type="hidden" name="price-bank" value="<?php echo $root_price; ?>">
                        <input type="hidden" name="product_price" value="<?php echo $price; ?>">
                        <div class="form-group col-sm-4 required">
                            <label class="control-label" for="input-name">Ngân hàng</label>
                            <select name="nganhang" class="form-control">                         
                                <option value="0">----Chọn----</option>
                                <?php foreach ($banks as $bank): ?>
                                    <option value="<?php echo $bank['credit_bank_id'];?>"><?php echo $bank['title'];?></option>
                                <?php endforeach ?>
                            </select>
                            <!-- <label for="exampleInputFile"><?php echo $root_price; ?></label> -->
                        </div>
                        <div class="form-group col-sm-3 required">
                            <label class="control-label" for="input-name">Trả trước</label>
                            <select name="tratruoc" class="form-control">
                                <option value="0">----Chọn----</option>
                                <option value="10">10%</option>
                                <option value="20">20%</option>
                                <option value="30">30%</option>
                                <option value="40">40%</option>
                                <option value="50">50%</option>
                                <option value="60">60%</option>
                                <option value="70">70%</option>
                            </select>
                            
                        </div>

                        <div class="form-group col-sm-5 required">
                            <label class="control-label" for="input-name">Thời gian</label>
                            <select name="thoigian" class="form-control">
                                <option value="0">----Chọn----</option>
                                <option value="3">3 tháng</option>
                                <option value="6">6 tháng</option>
                                <option value="9">9 tháng</option>
                                <option value="12">12 tháng</option>
                            </select>
                            
                        </div>
                        
                        <div class="form-group col-sm-7 required">
                            <label class="price_vn"></label>
                            <input type="hidden" name="price_tratruoc" value="">
                        </div>    
                        <div class="form-group col-sm-5 required">
                            <label class="trahangthang"></label> 
                            <input type="hidden" name="price_trahangthang" value="">
                        </div>

                        <div class="form-group col-sm-12 required">
                            <label class="control-label" for="input-name">Tên bạn</label>
                            <input type="text" name="name" value="" id="input-name" class="form-control">
                        </div>

                        <div class="form-group col-sm-12 required">
                            <label class="control-label" for="input-email">Số điện thoại</label>
                            <input type="text" name="telephone" value="" id="input-number" class="form-control">
                        </div>

                        <div class="form-group col-sm-12 required">
                            <label class="control-label" for="input-email">Địa chỉ email</label>
                            <input type="text" name="email" value="" id="input-email" class="form-control">
                        </div>
                        <div class="form-group col-sm-12 required">
                            <label class="control-label" for="input-enquiry">Yêu cầu</label>
                            <textarea name="enquiry" rows="5" id="input-enquiry" class="form-control"></textarea>
                        </div>
                        <div class="vertical-captcha">
                        </div>

                        <div class="col-sm-12 text-right">
                            <input class="btn btn-primary" type="submit" value="Gửi">
                        </div>
                    </form>
                </div>
            </div>
            </div>

        </div><!--./mymodal-->

</div>
<?php echo $content_bottom; ?></div>
<?php echo $column_right; ?></div>
<script type="text/javascript">
    $(document).on('change', 'select[name=\'tratruoc\']', function(e) {
        e.preventDefault();
        $this = $(this);

        $.ajax({
            url: 'index.php?route=product/product/getTratruoc',
            type: 'POST',
            dataType: 'json',
            data: {
                tratruoc: $this.val(),
                price_bank: $('input[name=\'price-bank\']').val()
            },
            success: function(json) {
                $('.price_vn').text(json['price_vn']);
                $('.price_vn').attr('data-price', json['success']);
                $('input[name=\'price_tratruoc\']').val(json['success']);
                $('select[name=\'thoigian\']').trigger('change');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    $(document).on('change', 'select[name=\'thoigian\']', function(e) {
        e.preventDefault();
        $this = $(this);
        $.ajax({
            url: 'index.php?route=product/product/getTrahangthang',
            type: 'POST',
            dataType: 'json',
            data: {
                thoigian: $this.val(),
                tienconlai: $('.price_vn').attr('data-price'),
                nganhang: $('select[name=\'nganhang\'] option:selected').val()
            },
            success: function(json) {
                $('.trahangthang').text(json['trahangthang']);
                $('.trahangthang').attr('data-price', json['success']);
                $('input[name=\'price_trahangthang\']').val(json['success']);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    $(document).ready(function() {
        $(document).on('submit', '#form-bank', function(e) {
            e.preventDefault();
            $this = $(this);
            $.ajax({
                url: 'index.php?route=product/product/addTragop',
                type: 'POST',
                dataType: 'json',
                data: $('#form-bank').serialize(),
                success: function(json) {
                    $this.find('.error').remove();
                    if (json['success']) {
                        alert('Yêu cầu trả góp thành công, Chúng tôi sẽ liên hệ cho bạn sớm nhất có thể.');
                        $('#myModal').modal('hide');
                    }
                    if (json['error']) {
                        $.each(json['error'], function(index, val) {
                            $this.find('[name=\'' + index + '\']').closest('.form-group').append('<label class="error">' + val + '</label>');
                        });
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    });
</script>
<script type="text/javascript">
//cloud-zoom-wrap
$(document).on('click', '.radio label', function(e) {
    e.preventDefault();
    $(this).closest('.box-color').find('.active').removeClass('active');
    $(this).addClass('active');
    $(this).closest('.box-color').find('input').prop('checked', false);
    $(this).closest('.radio').find('input').prop('checked', true);
    img = $(this).closest('.radio').find('input').attr('data-image');
    $('.image').find('a').attr('href', img);
    $('.image').find('img').attr('src', img);
    changeprice();
    // change price
    /*var price = $(this).attr('data-price').split(symbolright);
    price = price.shift().split(",");
    var pr = '';
    for (var n = 0; n < price.length; n++) {
        pr += price[n];
    };
    price = pr;*/
    
});
function changeprice(){
    //SELECT EACH PRICE
    select_price = 0;
    $.each($(".select-package"), function(index, val) {
        // console.log($(this).find("select option:selected").attr('data-price'));
        // alert();
        var price_choice = $(this).find("select option:selected").attr('data-price');
        if (price_choice!="") {
            price_choice = price_choice.toString().replace(/\đ/g, "");
            price_choice = price_choice.toString().replace(/\,/g, "");
            price_choice = price_choice.toString().replace(/\+/g, "");
            price_choice = price_choice.toString().replace("$", "");

        }else{
            price_choice = 0;
        }

        select_price += parseInt(price_choice);
        
    });

    
    // Color Price
    color_price = 0;
    if ($(".box-color").find('label').hasClass('active')) {
        var color_price = $(".box-color .active input").attr('data-price');
        if (color_price!="") {
            color_price = color_price.toString().replace(/\đ/g, "");
            color_price = color_price.toString().replace(/\,/g, "");
            color_price = color_price.toString().replace(/\+/g, "");
            color_price = color_price.toString().replace("$", "");
        }else{
            color_price = 0;
        }
    }
    color_price = parseInt(color_price);
    
    // ROOT PRICE
    var price_product = $('input[name=\'product_price\']').val();
    price_product = price_product.toString().replace(/\đ/g, "");
    // console.log(price_product+'1');
    price_product = price_product.toString().replace(/\,/g, "");
    price_product = price_product.toString().replace("$", "");
    // console.log(price_product+'--2');
    price_product = parseInt(price_product);
    // console.log(price_product+'--3');

    price_change = price_product+color_price+select_price;
   /* console.log(select_price);
    console.log(price_product);
    console.log(color_price);*/
    price_change = price_change.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+'đ';
    $('.product-page-price span').html('<span>Giá Bán:&nbsp;&nbsp;&nbsp;</span> '+price_change);
}
$(document).on('change', '.select-package select', function(e) {
    changeprice();
});
$('.select-package select').trigger('change');
$(document).on('click', '.image_carousel a', function(e) {
    e.preventDefault();
    $('.image').find('a').attr('href', $(this).attr('href'));
    $('.image').find('img').attr('src', $(this).attr('href'));
});


</script>


<script type="text/javascript">
    var $radioButtons = $(".radio_input");
    $radioButtons.click(function() {
        $radioButtons.each(function() {
            $(this).parent().toggleClass('checked_img', this.checked);
        });
    });
</script>


<script type="text/javascript"><!--
$('#popup-report #button-send').on('click', function() {
    $.ajax({
        url: 'index.php?route=information/contact/validateWhosesale&product_id=<?php echo $product_id;?>',
        type: 'post',
        data: $('#popup-report textarea[name=\'enquiry\'], #popup-report input[type=\'text\']'),
        dataType: 'json',
        beforeSend: function() {
            $('#popup-report button[name=\'button_send_report\']').button('loading');
        },
        complete: function() {
            $('#popup-report button[name=\'button_send_report\']').button('reset');
        },
        success: function(json) {
            $('.alert, .text-danger, .error').remove();
            if (json['error']) {
                if (json['error']['telephone']) {
                    $('#popup-report input[name=\'telephone\']').after('<div class="text-danger example">' + json['error']['telephone'] + '</div>');
                }
                if (json['error']['name']) {
                    $('#popup-report input[name=\'name\']').after('<div class="text-danger example">' + json['error']['name'] + '</div>');
                }
                if (json['error']['email']) {
                    $('#popup-report input[name=\'email\']').after('<div class="text-danger example">' + json['error']['email'] + '</div>');
                }
                if (json['error']['enquiry']) {
                    $('#popup-report textarea[name=\'enquiry\']').after('<div class="error">' + json['error']['enquiry'] + '</div>');
                }
            } else {
                $('#popup-report input[name=\'name\']').val('');
                $('#popup-report input[name=\'email\']').val('');
                $('#popup-report input[name=\'telephone\']').val('');
                $('#popup-report textarea[name=\'enquiry\']').val('');
                $('#popup-report h2').after('<div class="alert alert-success">' + json['success'] + '</div>');
            }
        }
    });
});
//--></script>
<script type="text/javascript"><!--
var modal = document.getElementById('myModal');
var btn = document.getElementById("lienhemuahang");
var span = document.getElementsByClassName("close")[0];
btn.onclick = function() {
    modal.style.display = "block";
}

span.onclick = function() {
    modal.style.display = "none";
}

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
//--></script>
<script type="text/javascript" src="catalog/view/theme/tlptech/js/cloud-zoom.1.0.2.min.js"></script>

<script type="text/javascript"><!--
$(document).ready(function() {
    $('.colorbox').colorbox({
        overlayClose: true,
        maxWidth:'95%',
        rel:'gallery',
        opacity: 0.5
    });
});
//--></script>
<script type="text/javascript">
    jQuery(function($) {
            //Product thumbnails
            $(".cloud-zoom-gallery").last().removeClass("cboxElement");
            $(".cloud-zoom-gallery").click(function() {
                $("#zoom-btn").attr('href', $(this).attr('href'));
                $("#zoom-btn").attr('title', $(this).attr('title'));
                $(".cloud-zoom-gallery").each(function() {
                    $(this).addClass("cboxElement");
                });
                $(this).removeClass("cboxElement");
            });
        });</script>
        <script type="text/javascript">
            /*$(document).ready(function() {
                var owlAdditionals = $('.image_carousel');
                var wrapperWidth = $(".image-additional").width();
                // var itemWidth = ( < ?php echo $additional_width; ? > + 10);
                var itemcalc = Math.round(wrapperWidth / itemWidth);
                owlAdditionals.owlCarousel({
                    // items : itemcalc,
                    mouseDrag: true,
                    responsive:false,
                    pagination: false,
                    navigation:true,
                    slideSpeed:200,
                    navigationText: [
                    "<div class='slide_arrow_prev add_img'><i class='fa fa-angle-left'></i></div>",
                    "<div class='slide_arrow_next add_img'><i class='fa fa-angle-right'></i></div>"
                    ]
                });
            });*/
        </script>
            <script type="text/javascript">
                /*$(document).ready(function() {
                    var grid5 = 5;
                    var grid4 = 4;
                    var grid3 = 3;
                    var owlRelated = $('.product-grid.related.carousel');
                    owlRelated.owlCarousel({
                        itemsCustom: [ [0, 1], [350, 1], [550, 2], [768, 3], [1024, 4], [1025, <?php echo $tlptech_grid_related; ?> ]],
                        pagination: false,
                        navigation:true,
                        slideSpeed:500,
                        scrollPerPage:false,
                        navigationText: [
                        "<div class='slide_arrow_prev'><i class='fa fa-angle-left'></i></div>",
                        "<div class='slide_arrow_next'><i class='fa fa-angle-right'></i></div>"]
                    });
                });*/
            </script>
                <script type="text/javascript">
                    $('.quantity_button.plus').on('click', function(){
                        var oldVal = $('input.quantity').val();
                        var newVal = (parseInt($('input.quantity').val(), 10) + 1);
                        $('input.quantity').val(newVal);
                    });
                    $('.quantity_button.minus').on('click', function(){
                        var oldVal = $('input.quantity').val();
                        if (oldVal > 1)
                        {
                            var newVal = (parseInt($('input.quantity').val(), 10) - 1);
                        }
                        else
                        {
                            newVal = 1;
                        }
                        $('input.quantity').val(newVal);
                    });</script>
                    <?php if ($special_date_end > 0) { ?>

                    <script type="text/javascript">
                        $('.offer').countdown({
                            until: < ?php echo $special_date_end ? > ,
                            layout: '{desc}<i>{dn}</i> {dl} <i>{hn}</i> {hl} <i>{mn}</i> {ml} <i>{sn}</i> {sl}',
                            description: '<span class="main_font"><?php echo $text_expire ?></span>&nbsp;'
                        });</script>
                        <?php } ?>
                        <script type="text/javascript">
                            $(".to_review").click(function() {
                                $('html, body').animate({
                                    scrollTop: $("#tab-review").offset().top
                                }, 1000);
                            });</script>

                            <!-- Default scrips below -->    
                            <script type="text/javascript"><!--
                            $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
                                $.ajax({
                                    url: 'index.php?route=product/product/getRecurringDescription',
                                    type: 'post',
                                    data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
                                    dataType: 'json',
                                    beforeSend: function() {
                                        $('#recurring-description').html('');
                                    },
                                    success: function(json) {
                                        $('.alert, .text-danger').remove();
                                        if (json['success']) {
                                            $('#recurring-description').html(json['success']);
                                        }
                                    }
                                });
                            });
                            //--></script> 
                            <script type="text/javascript"><!--
                            $('#button-cart').on('click', function() {
                                $.ajax({
                                    url: 'index.php?route=checkout/cart/add',
                                    type: 'post',
                                    data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
                                    dataType: 'json',
                                    beforeSend: function() {
                                        $('#button-cart').button('loading');
                                    },
                                    complete: function() {
                                        $('#button-cart').button('reset');
                                    },
                                    success: function(json) {
                                        return;
                                        $('.alert, .text-danger').remove();
                                        $('.form-group').removeClass('has-error');
                                        if (json['error']) {
                                            if (json['error']['option']) {
                                                for (i in json['error']['option']) {
                                                    var element = $('#input-option' + i.replace('_', '-'));
                                                    if (element.parent().hasClass('input-group')) {
                                                        element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                                    } else {
                                                        element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                                    }
                                                }
                                            }

                                            if (json['error']['recurring']) {
                                                $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                                            }

            // Highlight any found errors
            $('.text-danger').parent().addClass('has-error');
        }

        if (json['success']) {
            $('body').addClass('is-popup');
            $.colorbox({
                html:'<div class="cart_notification"><div class="product"><img src="' + json['image'] + '"/><span>' + json['success'] + '</span></div><div class="bottom"><a class="btn btn-default" href="' + json['link_cart'] + '">' + json['text_cart'] + '</a> ' + '<a class="btn btn-primary" href="' + json['link_checkout'] + '">' + json['text_checkout'] + '</a></div></div>',
                className: "notification",
                initialHeight:50,
                initialWidth:50,
                width:"90%",
                maxWidth:400,
                height:"90%",
                maxHeight:200
            });
            $('#cart-total').html(json['total']);
                    $('#cart').load('index.php?route=common/cart/info #cart > *'); //Added
                    $('body').removeClass('is-popup');
                }
            }
        });
                            });
                            //--></script> 
                            <script type="text/javascript"><!--
                            $('#button-buy,#btn-mua-ngay').on('click', function() {
                                $.ajax({
                                    url: 'index.php?route=checkout/cart/add',
                                    type: 'post',
                                    data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
                                    dataType: 'json',
                                    beforeSend: function() {
                                        $('#button-buy,#btn-mua-ngay').button('loading');
                                    },
                                    complete: function() {
                                        $('#button-buy,#btn-mua-ngay').button('reset');
                                    },
                                    success: function(json) {
                                        $('.alert, .text-danger').remove();
                                        $('.form-group').removeClass('has-error');
                                        if (json['error']) {
                                            if (json['error']['option']) {
                                                for (i in json['error']['option']) {
                                                    var element = $('#input-option' + i.replace('_', '-'));
                                                    if (element.parent().hasClass('input-group')) {
                                                        element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                                    } else {
                                                        element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                                    }
                                                }
                                            }

                                            if (json['error']['recurring']) {
                                                $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                                            }

            // Highlight any found errors
            $('.text-danger').parent().addClass('has-error');
        }

        if (json['success']) {
            location = 'index.php?route=quickcheckout/checkout';
        }
    }
});
                            });
                            //--></script> 
                            <script type="text/javascript"><!--
                            $('.date').datetimepicker({
                                pickTime: false
                            });
                            $('.datetime').datetimepicker({
                                pickDate: true,
                                pickTime: true
                            });
                            $('.time').datetimepicker({
                                pickDate: false
                            });
                            $('button[id^=\'button-upload\']').on('click', function() {
                                var node = this;
                                $('#form-upload').remove();
                                $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');
                                $('#form-upload input[name=\'file\']').trigger('click');
                                if (typeof timer != 'undefined') {
                                    clearInterval(timer);
                                }

                                timer = setInterval(function() {
                                    if ($('#form-upload input[name=\'file\']').val() != '') {
                                        clearInterval(timer);
                                        $.ajax({
                                            url: 'index.php?route=tool/upload',
                                            type: 'post',
                                            dataType: 'json',
                                            data: new FormData($('#form-upload')[0]),
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            beforeSend: function() {
                                                $(node).button('loading');
                                            },
                                            complete: function() {
                                                $(node).button('reset');
                                            },
                                            success: function(json) {
                                                $('.text-danger').remove();
                                                if (json['error']) {
                                                    $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                                                }

                                                if (json['success']) {
                                                    alert(json['success']);
                                                    $(node).parent().find('input').attr('value', json['code']);
                                                }
                                            },
                                            error: function(xhr, ajaxOptions, thrownError) {
                                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                            }
                                        });
                                    }
                                }, 500);
                            });
                            //--></script> 
                            <script type="text/javascript"><!--
                            $('#review').delegate('.pagination a', 'click', function(e) {
                                e.preventDefault();
                                $('#review').fadeOut('slow');
                                $('#review').load(this.href);
                                $('#review').fadeIn('slow');
                            });
                            $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');
                            $('#button-review').on('click', function() {
                                $.ajax({
                                    url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
                                    type: 'post',
                                    dataType: 'json',
                                    data: $("#form-review").serialize(),
                                    beforeSend: function() {
                                        $('#button-review').button('loading');
                                    },
                                    complete: function() {
                                        $('#button-review').button('reset');
                                    },
                                    success: function(json) {
                                        $('.alert-success, .alert-danger').remove();
                                        if (json['error']) {
                                            $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                                        }

                                        if (json['success']) {
                                            $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
                                            $('input[name=\'name\']').val('');
                                            $('textarea[name=\'text\']').val('');
                                            $('input[name=\'rating\']:checked').prop('checked', false);
                                        }
                                    }
                                });
                            });
                            //--></script>




                            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js"></script>
                        </div>
>>>>>>> 9e28eed4ea04578e595e6dc8c92e67856256b86b
                        <?php echo $footer; ?>