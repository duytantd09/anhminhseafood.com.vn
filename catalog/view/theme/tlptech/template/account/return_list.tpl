<?php echo $header; ?>
<?php echo $content_top; ?>
<div class="bg-light breadcrumb-w">
  <div class="container">
    <nav aria-label="breadcrumb">
     <ol class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li class="breadcrumb-item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ol>
  </nav>
</div>
</div>
<div class="container my-account">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-lg-9 col-md-9 col-sm-8'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1><?php echo $heading_title; ?></h1>
      <?php if ($returns) { ?>
      <?php foreach ($returns as $return) { ?>
      <div class="table-responsive">
      <table class="compare-info table table-bordered">
    <tbody>
       <tr>
       <td class="contrast_font history"><?php echo $column_return_id; ?></td>
       <td class="contrast_font">#<?php echo $return['return_id']; ?></td>
      </tr>
      <tr>
       <td class="contrast_font history"><?php echo $column_status; ?></td>
       <td class="contrast_font"><?php echo $return['status']; ?></td>
      </tr>
      <tr>
       <td class="contrast_font history"><?php echo $column_date_added; ?></td>
       <td class="contrast_font"><?php echo $return['date_added']; ?></td>
      </tr>
      <tr>
       <td class="contrast_font history"><?php echo $column_order_id; ?></td>
       <td class="contrast_font"><?php echo $return['order_id']; ?></td>
      </tr>
      <tr>
       <td class="contrast_font history"><?php echo $column_customer; ?></td>
       <td class="contrast_font"><?php echo $return['name']; ?></td>
      </tr>
      <tr>
       <td colspan="2" class="white_back">
       <a class="button contrast" href="<?php echo $return['href']; ?>"><?php echo $button_view; ?></a>
		</td>
      </tr>
    </tbody>
   </table>
 </div>
    <?php } ?>
      <div class="text-right"><?php echo $pagination; ?></div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>