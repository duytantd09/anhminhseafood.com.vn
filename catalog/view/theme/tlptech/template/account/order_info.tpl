<?php echo $header; ?>
<?php echo $content_top; ?>
<div class="bg-light breadcrumb-w">
  <div class="container">
    <nav aria-label="breadcrumb">
     <ol class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li class="breadcrumb-item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ol>
  </nav>
</div>
</div>
<div class="container my-account">
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-lg-9 col-md-9 col-sm-8'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1><?php echo $heading_title; ?></h1>
      
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th colspan="2"><?php echo $text_order_detail; ?></th>
            </tr>
          </thead>
          <tbody>
            <tr>              
              <td>
                <?php if ($invoice_no) { ?>
                <?php echo $text_invoice_no; ?> <?php echo $invoice_no; ?><br />
                <?php } ?>
                <?php echo $text_order_id; ?> #<?php echo $order_id; ?><br />
                <?php echo $text_date_added; ?> <?php echo $date_added; ?>
              </td>
              <td>
                <?php if ($payment_method) { ?>
                <?php echo $text_payment_method; ?> <?php echo $payment_method; ?><br />
                <?php } ?>
                <?php if ($shipping_method) { ?>
                <?php echo $text_shipping_method; ?> <?php echo $shipping_method; ?>
                <?php } ?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th><?php echo $text_payment_address; ?></th>
              <?php if ($shipping_address) { ?>
              <th><?php echo $text_shipping_address; ?></th>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><?php echo $payment_address; ?></td>
              <?php if ($shipping_address) { ?>
              <td><?php echo $shipping_address; ?></td>
              <?php } ?>
            </tr>
          </tbody>
        </table>
      </div>


      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th><?php echo $column_name; ?></th>
              <th><?php echo $column_model; ?></th>
              <th><?php echo $column_quantity; ?></th>
              <th><?php echo $column_price; ?></th>
              <th><?php echo $column_total; ?></th>

            </tr>
          </thead>
          <tbody>
            <?php foreach ($products as $product) { ?>
            <tr>


              <td><?php echo $product['name']; ?>
                <?php foreach ($product['option'] as $option) { ?>
                <br />
                &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                <?php } ?>
                <br />
                <a class="return_link btn btn-outline-secondary" href="<?php echo $product['return']; ?>"><i class="fa fa-reply"></i> <?php echo $button_return; ?></a>&nbsp;&nbsp;
                <a class="return_link btn btn-outline-secondary" href="<?php echo $product['reorder']; ?>"><i class="fa fa-shopping-cart"></i> <?php echo $button_reorder; ?></a>
              </td>
              <td><?php echo $product['model']; ?></td>
              <td><?php echo $product['quantity']; ?></td>
              <td><?php echo $product['price']; ?></td>
              <td><?php echo $product['total']; ?></td>

            </tr>
            <?php } ?>
            <?php foreach ($vouchers as $voucher) { ?>
            <tr>
              <td><?php echo $voucher['description']; ?></td>
              <td></td>
              <td>1</td>
              <td><?php echo $voucher['amount']; ?></td>
              <td><?php echo $voucher['amount']; ?></td>

            </tr>
            <?php } ?>

            <?php foreach ($totals as $total) { ?>
            <tr>
              <td colspan="3"></td>
              <td><?php echo $total['title']; ?>:</td>
              <td><?php echo $total['text']; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <?php if ($comment) { ?>
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th><?php echo $text_comment; ?></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><?php echo $comment; ?></td>
            </tr>
          </tbody>
        </table>
      </div>
      <?php } ?>

      <?php if ($histories) { ?>
      <h3><?php echo $text_history; ?></h3>

      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th><?php echo $column_date_added; ?></th>
              <th><?php echo $column_status; ?></th>
              <th><?php echo $column_comment; ?></th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($histories as $history) { ?>
            <tr>
              <td><?php echo $history['date_added']; ?></td>
              <td><?php echo $history['status']; ?></td>
              <td><?php echo $history['comment']; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>

      <?php } ?>
      <div class="buttons clearfix">
        <div class="float-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?>
    </div>
    <?php echo $column_right; ?>
  </div>
</div>
<?php echo $footer; ?>