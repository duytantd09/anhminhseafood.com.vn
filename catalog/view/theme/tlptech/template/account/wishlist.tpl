<?php echo $header; ?>
<?php echo $content_top; ?>
<div class="bg-light breadcrumb-w">
  <div class="container">
    <nav aria-label="breadcrumb">
     <ol class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li class="breadcrumb-item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ol>
  </nav>
</div>
</div>
<div class="container my-account">
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-lg-9 col-md-9 col-sm-8'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1><?php echo $heading_title; ?></h1>
      <?php if ($products) { ?>
      <div class="cart-info table-responsive ">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th class="image "><?php echo $column_image; ?></th>
              <th class="name"><?php echo $column_name; ?></th>
              <th class="model "><?php echo $column_model; ?></th>
              <th class="stock "><?php echo $column_stock; ?></th>
              <th class="price "><?php echo $column_price; ?></th>
              <th class="action"><?php echo $column_action; ?></th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($products as $product) { ?>
            <tr>
              <td class="image" title="<?php echo $column_image; ?>"><?php if ($product['thumb']) { ?>
                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
                <?php } ?></td>
                <td class="name" title="<?php echo $column_name; ?>">
                  <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                  <div class="desktop_hide">
                    <div class="up_to_mobile">
                      <small><?php echo $column_model; ?>: <?php echo $product['model']; ?></small>
                      <small><?php echo $column_stock; ?>: <?php echo $product['stock']; ?></small>
                      <?php if ($product['price']) { ?>
                      <?php if (!$product['special']) { ?>
                      <small><?php echo $column_price; ?>: <?php echo $product['price']; ?></small><br />
                      <?php } else { ?>
                      <small><?php echo $column_price; ?>: <s><?php echo $product['price']; ?></s> <b><?php echo $product['special']; ?></b></small><br />
                      <?php } ?>
                      <?php } ?>
                    </div>
                  </div>
                </td>
                <td class="model " title="<?php echo $column_model; ?>"><?php echo $product['model']; ?></td>
                <td class="stock " title="<?php echo $column_stock; ?>"><?php echo $product['stock']; ?></td>
                <td class="price " title="<?php echo $column_price; ?>"><?php if ($product['price']) { ?>
                  <div class="price">
                    <?php if (!$product['special']) { ?>
                    <?php echo $product['price']; ?>
                    <?php } else { ?>
                    <span class="price-old"><?php echo $product['special']; ?></span> <span class="price"><?php echo $product['price']; ?></span> 
                    <?php } ?>
                  </div>
                  <?php } ?></td>
                  <td class="action text-center" title="<?php echo $column_action; ?>">
                    <button onclick="cart.add('<?php echo $product['product_id']; ?>')" class="btn btn-primary mb-1 btn-sm"><i class="fa fa-shopping-cart"></i> <?php echo $button_cart; ?></button>
                    <a href="<?php echo $product['remove']; ?>" class="btn btn-secondary btn-sm"><i class="fa fa-times"></i><?php echo $button_remove; ?></a>
                  </tr>
                </tbody>
                <?php } ?>
              </table>
            </div>
            <?php } else { ?>
            <p><?php echo $text_empty; ?></p>
            <?php } ?>
            <div class="buttons clearfix">
              <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
            </div>
            <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
          </div>
          <?php echo $footer; ?> 