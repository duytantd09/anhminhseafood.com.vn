<?php echo $header; ?>

<?php echo $content_top; ?>
<div class="bg-light breadcrumb-w">
  <div class="container">
    <nav aria-label="breadcrumb">
     <ol class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li class="breadcrumb-item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ol>
  </nav>
</div>
</div>
<div class="container">
  <div class="wt_layout_dk">
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-lg-12 col-md-12 col-sm-12'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <div id="social_login_content_holder"></div>
      <h1 class="text-center mb-3"><?php echo $heading_title; ?></h1>
      <div class="row">
        <div class="col-md-4 d-none d-md-block"></div>
        <div class="col-md-4 col-12">
          <div class="border p-3 rounded mb-3">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="icon icon-Users"></i></span>
                </div>
                <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
              </div>

              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="icon icon-OpenedLock"></i></span>
                </div>
                <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
              </div>

              <div class="clearfix mb-3">
                <a href="<?php echo $forgotten; ?>" class="float-right login-forgotten"><?php echo $text_forgotten; ?></a>
                <a href="<?php echo $register; ?>" class="float-left register"><?php echo $text_register; ?></a>
              </div>

              <input type="submit" value="<?php echo $button_login; ?>" class="btn btn-secondary btn-block mb-2" />

              <div class="text-center">
                <div class="fb-login-button" scope="public_profile,email" data-max-rows="1" data-size="large" data-button-type="login_with" onlogin="checkLoginState();" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false" ></div>
              </div>

              <?php if ($redirect) { ?>
              <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
              <?php } ?>
            </form>
          </div>
        </div>
      </div>
      <?php echo $content_bottom; ?></div>
    </div>
  </div>
</div>
  <script>
// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
  console.log('statusChangeCallback');
  console.log(response);
  // The response object is returned with a status field that lets the
  // app know the current login status of the person.
  // Full docs on the response object can be found in the documentation
  // for FB.getLoginStatus().
  if (response.status === 'connected') {
    // Logged into your app and Facebook.
    testAPI();
  } else {
    // The person is not logged into your app or we are unable to tell.
    document.getElementById('status').innerHTML = 'Please log ' +
    'into this app.';
  }
}

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
}

window.fbAsyncInit = function() {
  FB.init({
    appId      : '<?php echo $config_appId;?>',
  cookie     : true,  // enable cookies to allow the server to access 
                      // the session
  xfbml      : true,  // parse social plugins on this page
  version    : 'v2.8' // use graph api version 2.8
});

};

// Load the SDK asynchronously
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "https://connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function testAPI() {
 FB.api('/me', {fields: "email,first_name,last_name"}, function(response) {
  $.ajax({
    url: '<?php echo HTTP_SERVER;?>index.php?route=account/login/fblogin',
    data : 'email_address='+response.email+'&fname='+response.first_name+'&lname='+response.last_name+'&login_via=facebook',
    type : 'post',
    success: function(result) {
      if(result == 'success'){
        window.location.reload();
      } else if(result == 'registered'){
        window.location.reload();
      } else{
        $('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>Error: Please try again later or login with password</div>').insertBefore($('div.account-login'));
      }

    }
  });
});
}
</script>
<?php echo $footer; ?>