<?php echo $header; ?>
<div id="content">
  <div class="box-breadcrumb column column-margin-20px">
      <div class="container ">
        <ol class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
        </ol>
      </div>
    </div>


<?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div class="content <?php echo $class; ?>">
    	<div class="container shop-system">
    		<div class="column column-margin-10px">
    			<div class="title-category-pro">
    				<h1><?php echo $heading_title; ?></h1>
    			</div>
    		</div>
    		<div class="clearfix">
    			<div id="tabs" class="tabs">
                    <nav>
                        <ul>
                           <?php foreach($locals as $local) { ?>
                            <li><h2><a href="#section-<?php echo $local['id']; ?>"><?php echo $local['name']; ?></a></h2></li>
                           <?php } ?>
                        </ul>
                    </nav>
                    <div class="content">
                    <?php foreach($locals as $local) { ?>
                        <section id="section-<?php echo $local['id']; ?>">
                            <?php foreach($local['childrens'] as $children) { ?>
                            <div class="box-heading"><?php echo $children['name']; ?></div>
                            <div class="row">
                                <?php foreach( $children['locations'] as $location) { ?>
                                <div class="col-lg-4 col-md-4 col-sm-6 store-ro">
                                    <a href="<?php echo $location['href']; ?>"><img src="<?php echo $location['image']; ?>" alt="<?php echo $location['name']; ?>" /></a>
                                    <div class="all-nd">
                                        <h3><a href="<?php echo $location['href']; ?>"><strong><?php echo $location['name']; ?></strong></a></h3>
                                        <p><?php echo $location['address']; ?></p>
                                        <p>Điện thoại: <?php echo $location['telephone']; ?></p>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <?php } ?>
                        </section>
                    <?php } ?>
                    </div>
                </div>
    		</div>	
    	</div>
   </div>
</div>
<script type="text/javascript" src="catalog/view/theme/tlptech/js/cbpFWTabs.js"></script>

<script>
    new CBPFWTabs( document.getElementById( 'tabs' ) );
</script> 	

<?php echo $footer; ?>