<?php echo $header; ?><?php echo $content_top; ?>
<div class="bg-light breadcrumb-w">
  <div class="container">
    <nav aria-label="breadcrumb">
     <ol class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li class="breadcrumb-item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ol>
  </nav>
</div>
</div>
<div class="container">
    <div class="div-information">
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-lg-9 col-md-9 col-sm-8'; ?>
            <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="info-page-content <?php echo $class; ?>">

              <div class="home-titlel"><h1><?php echo $heading_title; ?></h1></div>
              <?php echo $description; ?>
              <?php echo $content_bottom; ?>
              <?php if ($show_form == 1) {?>
              <hr/>
              <div class="gray-bg">
                <div class="row" id="form-information">
                    <div class="col-sm-12"><h2><?php echo $text_contact;?></h2></div>
                    <div class="form-group col-sm-12 required">
                        <label class="control-label" for="input-name">Tên bạn</label>
                        <input type="text" name="name" value="" id="input-name" class="form-control">
                    </div>

                    <div class="form-group col-sm-12 required">
                        <label class="control-label" for="input-email">Số điện thoại</label>
                        <input type="text" name="telephone" value="" id="input-number" class="form-control">
                    </div>

                    <div class="form-group col-sm-12 required">
                        <label class="control-label" for="input-email">Địa chỉ email</label>
                        <input type="text" name="email" value="" id="input-email" class="form-control">
                    </div>
                    <div class="form-group col-sm-12 required">
                        <label class="control-label" for="input-enquiry">Yêu cầu</label>
                        <textarea name="enquiry" rows="5" id="input-enquiry" class="form-control"></textarea>
                    </div>
                    <div class="vertical-captcha">
                    </div>

                    <div class="col-sm-12 text-left">
                        <input class="btn btn-primary" type="button" id="button-send" name="button_send_report" value="Gửi">
                    </div>
                </div>
            </div>
            <script type="text/javascript"><!--
                $('#form-information #button-send').on('click', function() {
                    $.ajax({
                        url: 'index.php?route=information/contact/validateWhosesale&information_id=<?php echo $information_id;?>',
                        type: 'post',
                        data: $('#form-information textarea[name=\'enquiry\'], #form-information input[type=\'text\']'),
                        dataType: 'json',
                        beforeSend: function() {
                            $('#form-information button[name=\'button_send_report\']').button('loading');
                        },
                        complete: function() {
                            $('#form-information button[name=\'button_send_report\']').button('reset');
                        },
                        success: function(json) {
                            $('.alert, .text-danger, .error').remove();
                            if (json['error']) {
                                if (json['error']['telephone']) {
                                    $('#form-information input[name=\'telephone\']').after('<div class="text-danger example">' + json['error']['telephone'] + '</div>');
                                }
                                if (json['error']['name']) {
                                    $('#form-information input[name=\'name\']').after('<div class="text-danger example">' + json['error']['name'] + '</div>');
                                }
                                if (json['error']['email']) {
                                    $('#form-information input[name=\'email\']').after('<div class="text-danger example">' + json['error']['email'] + '</div>');
                                }
                                if (json['error']['enquiry']) {
                                    $('#form-information textarea[name=\'enquiry\']').after('<div class="error">' + json['error']['enquiry'] + '</div>');
                                }
                            } else {
                                $('#form-information input[name=\'name\']').val('');
                                $('#form-information input[name=\'email\']').val('');
                                $('#form-information input[name=\'telephone\']').val('');
                                $('#form-information textarea[name=\'enquiry\']').val('');
                                $('#form-information h2').after('<div class="alert alert-success">' + json['success'] + '</div>');
                            }
                        }
                    });
                });
                //--></script>
                <br/>
                <?php }?>
            </div>
            <?php echo $column_right; ?></div>
    </div>
</div>
    <?php echo $footer; ?>