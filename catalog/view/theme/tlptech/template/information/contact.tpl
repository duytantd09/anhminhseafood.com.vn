<?php echo $header; ?><?php echo $content_top; ?>
<div class="bg-light breadcrumb-w">
  <div class="container">
    <nav aria-label="breadcrumb">
       <ol class="breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
          <li class="breadcrumb-item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
          <?php } ?>
      </ol>
  </nav>
</div>
</div>
<div class="container">
    <div class="sitemap-bacg">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <h5><?php echo $store; ?></h5>
                <div class="border p-3 rounded">
                    <p><strong>Email</strong> <?php echo $config_email; ?></p>
                    <p><strong><?php echo $text_telephone; ?></strong> <?php echo $config_telephone; ?></p>
                    <p><strong><?php echo $text_address; ?></strong> <?php echo $address; ?></p>
                </div>
                <br>
                <div class="map-google">
                    <?php if ($geocode) { ?>
                    <?php echo $geocode; ?>
                    <?php } ?>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <h5>  Vui lòng điền thông tin liên hệ</h5>
                <div class="border rounded mb-3 p-3 bg-light">
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="row">
                        <div class="col-md-6 com-sm-12 col-12">
                            <div class="form-group required">
                                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                                <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control" />
                                <?php if ($error_name) { ?>
                                <div class="text-danger"><?php echo $error_name; ?></div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="col-md-6 com-sm-12 col-12">
                            <div class="form-group required">
                                <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                                <input type="text" name="email" value="<?php echo $email; ?>" id="input-number" class="form-control " />
                                <?php if ($error_email) { ?>
                                <div class="text-danger"><?php echo $error_email; ?></div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="col-md-6 com-sm-12 col-12">
                            <div class="form-group required">
                                <label class="control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
                                <input type="text" name="telephone" value="<?php echo $telephone; ?>" id="input-telephone" class="form-control " />
                                <?php if ($error_telephone) { ?>
                                <div class="text-danger"><?php echo $error_telephone; ?></div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="col-md-6 com-sm-12 col-12">
                            <div class="form-group required ">
                                <label class="control-label" for="input-name">Tên công ty</label>
                                <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control " />
                                <?php if ($error_name) { ?>
                                <div class="text-danger"><?php echo $error_name; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group required ">
                                <label class=" control-label" for="input-enquiry"><?php echo $entry_enquiry; ?></label>
                                <textarea name="enquiry" rows="5" id="input-enquiry" class="form-control "><?php echo $enquiry; ?></textarea>
                                <?php if ($error_enquiry) { ?>
                                <div class="text-danger"><?php echo $error_enquiry; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-6 com-sm-12 col-12">
                            <div class="vertical-captcha">
                                <?php echo $captcha; ?>
                            </div>
                        </div>
                        <div class="col-md-6 com-sm-12 col-12  text-right form-group">
                            <input class="btn btn-primary btn-color" type="submit" value="Hủy" />
                            <input class="btn btn-primary" type="submit" value="<?php echo $button_submit; ?> " />
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <br>
    </div>
</div>
<?php echo $footer; ?>