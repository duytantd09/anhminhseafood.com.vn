<?php echo $header; ?><?php echo $content_top; ?>
<div class="bg-light breadcrumb-w">
  <div class="container">
    <nav aria-label="breadcrumb">
     <ol class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li class="breadcrumb-item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ol>
  </nav>
</div>
</div>
<div class="container">
  <div class="information-tt">
    <h1><?php echo $heading_title; ?></h1>
    <div class="mainposts-nd"><?php echo $description; ?></div>
    <div class="blog_list re-order">
        <?php foreach ($categories as $blog) {?>
        <div class="blog-item py-5">
            <div class="row">
                <div class="image col-xl-4 col-lg-4 col-md-4 col-sm-16 col-12">
                    <div class="hover"><a href="<?php echo $blog['href']; ?>">
                        <a href="<?php echo $blog['href']; ?>"><img src="<?php echo $blog['image']; ?>" class="rounded shadow-sm" alt="<?php echo $blog['title']; ?>" title="<?php echo $blog['title']; ?>" /></a>
                    </div>
                </div>                
                <div class="blog_summary col-xl-8 col-lg-8 col-md-8 col-sm-16 col-12">
                    <a href="<?php echo $blog['href']; ?>"><h2 class="blog_title"><?php echo $blog['title']; ?></h2></a>
                    <p class="blog_description"><?php echo $blog['description']; ?></p>
                </div>
            </div>
        </div>

        <?php }?>
    </div>
  </div>
</div>
<?php echo $footer; ?>