<?php echo $header; ?>
<div id="content">
  <div class="box-breadcrumb column column-margin-20px">
      <div class="container">
        <ol class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
        </ol>
      </div>
    </div>

    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div class="container shop-system">
      <div id="content" class="<?php echo $class; ?>">
      <div class="row">  
          <?php echo $content_top; ?>
          <div class="box-heading"><?php echo $heading_title; ?></div>
              <div class="row">
               
                <div class="col-sm-6 margin-b">
                   <?php if ($image) { ?>
                   <div class="form-group"><img src="<?php echo $image; ?>" alt="<?php echo $store; ?>" title="<?php echo $store; ?>" /></div>
                   <?php } ?>
                	<span class="contrast_font"><?php echo $store; ?></span><br />
    			    <p><?php echo $address; ?></p>
						<?php echo $text_telephone; ?></span><br />
	    			<?php echo $telephone; ?><br />
	                <?php if ($fax) { ?>
	      			<br /><span class="contrast_font"><?php echo $text_fax; ?></span><br />
	      			<?php echo $fax; ?>
	      			<?php } ?>
	  
	                <?php if ($open) { ?>
	                <span class="contrast_font"><?php echo $text_open; ?></span><br />
	    			<?php echo html_entity_decode($open); ?>
	                <br />
	                <?php } ?>
	                <?php if ($comment) { ?>
	                <br /><span class="contrast_font"><?php echo $text_comment; ?></span><br />
	                <?php echo $comment; ?>
	                <?php } ?>
                </div>

	            <div class="col-sm-6 margin-b">
                 <?php if ($geocode) { ?>
		          <div class="contact_map">
		          <?php echo html_entity_decode($geocode, ENT_QUOTES, 'UTF-8'); ?>
		           </div>
		          <?php } ?>
       
              </div>
          </div>
          <?php echo html_entity_decode($description); ?>
          <?php echo $content_bottom; ?></div>
          </div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>