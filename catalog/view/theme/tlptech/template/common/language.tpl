<div class="language">
    <ul class="list-unstyled mb-0 language">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="language">
            <?php foreach ($languages as $language) { ?>
            <?php if ($language['code'] == $code) { ?>
            <li>
                <a class="current"><img src="<?php echo $language['image'];?>" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" /> <span> <?php echo $language['name']; ?></span></a>
            </li>
            <?php } else { ?>
            <li>
                <a href="<?php echo $language['code']; ?>">
                    <img src="<?php echo $language['image'];?>" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" /> <span> <?php echo $language['name']; ?></span>
                </a>
            </li>
            <?php } ?>
            <?php } ?>
            <input type="hidden" name="code" value="" />
            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
        </form>
    </ul>
</div>
