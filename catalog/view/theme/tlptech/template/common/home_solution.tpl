<div class="home-tab">
    <div class="col-sm-12 no-pa-right">
        <div class="home-blocks">
            <div class="blocks-title blue">
                <div  class="title">
                    <h2><span><i class="fa fa-lightbulb-o" aria-hidden="true"></i></span> <?php echo $text_solution; ?></h2>
                    <div id="triangle-right"></div>
                </div>
                <ul>
                    <?php foreach ($categories_top as $category_top) {?>
                    <li><a href="<?php echo $category_top['href']; ?>" title="<?php echo $category_top['name']; ?>"><?php echo $category_top['name']; ?></a></li>
                    <?php }?>
                </ul>
                <div style="clear: both;"></div>
            </div>
            <ul class="tab-doc col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <?php foreach ($categories as $category) { ?>
                <li class="<?php echo $category['active']; ?>"><a href="#solution-<?php echo $category['category_id']; ?>"  data-toggle="tab"><?php echo $category['name']; ?></a></li>
                <?php }?>
            </ul>

            <div class="tab-content col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <?php foreach ($categories as $category) { ?>

                <div id="solution-<?php echo $category['category_id']; ?>" class="tab-pane fade in <?php echo $category['active']; ?>">
                    <div class="row">
                        <div class="newest1 col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <?php 
                        $dem = 0;
                        foreach ($category['products'] as $product) { $dem++;
                        if($dem == 1){ ?>
                        <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><img src="<?php echo $product['thumb_hover']; ?>" alt="<?php echo $product['name']; ?>"></a>
                        <?php }}?>
                        </div>
                        <div class="product_4 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="newest2 col-lg-8 col-md-8 col-sm-12 col-xs-12 ">
                                    <?php 
                                    $dem = 0;
                                    foreach ($category['products'] as $product) { $dem++;
                                    if($dem == 2){ ?>
                                    <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><img src="<?php echo $product['thumb_hover_1']; ?>" alt="<?php echo $product['name']; ?>"></a>
                                    <?php }}?>
                                </div>
                                <?php 
                                $dem = 0;
                                foreach ($category['products'] as $product) { $dem++;
                                if($dem == 3){ ?>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 product-col">
                                    <div class="product-group">
                                        <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a>
                                        <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><h3 class="product-name"><?php echo $product['name']; ?></h3></a>

                                        <?php if ($product['price']) { ?>
                                          <div class="price">
                                            <?php if (!$product['special']) { ?>
                                            <?php echo $product['price']; ?>
                                            <?php } else { ?>
                                            <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
                                            <?php } ?>
                                            <?php if ($product['tax']) { ?>
                                            <br />
                                            <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                            <?php } ?>
                                          </div>
                                        <?php } ?>
                                        <div class="group-btn">
                                            <button class="btn favorite-btn" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" data-tooltip="<?php echo $button_wishlist; ?>"><i class="fa fa-heart" aria-hidden="true"></i></button>
                                            <button class="btn buy-btn" onclick="cart.buy('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" data-tooltip="<?php echo $button_buy; ?>"><?php echo $button_buy; ?></button>
                                            <button class="btn add-to-cart" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" data-tooltip="<?php echo $button_cart; ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <?php }}?>
                            </div>
                            <div class="row">
                                <?php 
                                $dem = 0;
                                foreach ($category['products'] as $product) { $dem++;
                                if($dem > 3 && $dem <= 6){ ?>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 product-col">
                                    <div class="product-group">
                                    <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a>
                                    <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><h3 class="product-name"><?php echo $product['name']; ?></h3></a>

                                    <?php if ($product['price']) { ?>
                                      <div class="price">
                                        <?php if (!$product['special']) { ?>
                                        <?php echo $product['price']; ?>
                                        <?php } else { ?>
                                        <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
                                        <?php } ?>
                                        <?php if ($product['tax']) { ?>
                                        <br />
                                        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                        <?php } ?>
                                      </div>
                                    <?php } ?>
                                    <div class="group-btn">
                                        <button class="btn favorite-btn" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" data-tooltip="<?php echo $button_wishlist; ?>"><i class="fa fa-heart" aria-hidden="true"></i></button>
                                        <button class="btn buy-btn" onclick="cart.buy('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" data-tooltip="<?php echo $button_buy; ?>"><?php echo $button_buy; ?></button>
                                        <button class="btn add-to-cart" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" data-tooltip="<?php echo $button_cart; ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                                </div>
                                <?php }}?>
                            </div>

                        </div>
                    </div>
                </div>
                <?php }?>
            </div>
            <script>
                $(document).ready(function () {
                    $(".tab-doc a").click(function () {
                        $(this).tab('show');
                    });
                });
            </script>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="tag">
            <p><span><i class="fa fa-tag" aria-hidden="true"></i></span><strong> <?php echo $text_keyword_search_popular; ?>: </strong>
                <span>
                <?php 
                $dem_tag = 0;
                foreach ($tags_all as $tags) { $dem_tag++;
                if($dem_tag <= 30){?>
                <a href="<?php echo $tags['href'];?>" title="<?php echo $tags['tag'];?>"><?php echo $tags['tag'];?></a>
                <?php }}?>
                </span>
            </p>
            <div style="clear: both;"></div>
        </div>
    </div>
    
</div>