<?php if($tlptech_header_login == 'enabled'){ ?>
<ul class="list-inline list-unstyled mb-0 login">
  <?php if (!$logged) { ?>
  <li class="list-inline-item">
    <a href="<?php echo $register; ?>" title="<?php echo $text_register; ?>"> <i class="icon icon-Pencil"></i> <span><?php echo $text_register; ?></span></a>
  </li>
  <li class="list-inline-item">
    <a href="<?php echo $login_link; ?>"> <i class="icon icon-User"></i> <span><?php echo $text_login; ?></span></a>
  </li>
  <?php } else { ?>
  <li class="list-inline-item">
    <a href="<?php echo $account; ?>"><i class="icon icon-User"></i> <?php echo $logged_name; ?></a> 
  </li>
  <li class="list-inline-item">
    <a href="<?php echo $logout_link; ?>"><i class="icon icon-Goto"></i> <?php echo $text_logout; ?></a>
  </li>
  <?php } ?>
</ul>
<?php } ?>