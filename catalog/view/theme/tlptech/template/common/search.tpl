
    <select name="search_category_id" id="search_category_id" class="custom-select" hidden>
        <option value='0'><?php echo $text_categories;?></option>
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['category_id'] == $category_id){?>
        <option value="<?php echo $category['category_id'];?>" selected><?php echo $category['name'];?></option>
        <?php } else {?>
        <option value="<?php echo $category['category_id'];?>"><?php echo $category['name'];?></option>
        <?php }}?>
    </select>
    <div id="search">
        <span class="button-search"><i class="icon icon-Search"></i></span>
        <input type="text" name="search" class="search_input form-control" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />
    </div>
