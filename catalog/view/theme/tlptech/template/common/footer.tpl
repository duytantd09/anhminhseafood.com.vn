<!-- 
<div class="modal fade" id="popup-page-load">
  <div class="modal-dialog modal-lg modal-dialog-centered img-modal " role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-7 col-md-7 col-sm-12 col-12">
            <img src="image/cache/catalog/banner/ms_banner_img1-cr-1200x500.jpg">
          </div>
          <div class="col-lg-5 col-md-5 col-sm-12 col-12 py-3 pr-5">
            Combo
            Ngôi nhà đẹp này là một sự kết hợp màu sắc hết sức cổ điển nhưng vẫn toát lên sự sang trọng, thanh lịch
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(window).on('load',function(){
    $('#popup-page-load').modal('show');
  });
</script> -->


<div class="float-icon-hotline">
  <ul>
    <li class="hotline_float_icon"><a id="messengerButton" href="tel:<?php echo $telephone_code;?>"><i class="fa fa-phone animated infinite tada"></i><span>Hotline: <?php echo $telephone_1;?></span></a></li>
    <li class="hotline_float_icon"><a target="_blank" rel="nofollow" id="messengerButton" href="//zalo.me/<?php echo $telephone_code;?>"><i class="fa fa-zalo animated infinite tada"></i><span>Nhắn tin qua Zalo</span></a></li>
    <li class="hotline_float_icon"><a target="_blank" rel="nofollow" id="messengerButton" href="https://www.messenger.com/t/2092490587717979"><i class="fa fa-messenger animated infinite tada"></i><span>Nhắn tin qua Facebook</span></a></li>
    <li><a id="back-to-top" href="#" class=" back-to-top" role="button"><i class="fa fa-angle-up" aria-hidden="true"></i></a></li>
  </ul>
</div>


<div class="fixed-social-network d-none">

  <a href="https://www.facebook.com/" class="fb-icon"><img src="image/catalog/icon-social/fb_601346.png" alt="facebook"> Facebook</a>
  
  
  <a href="https://www.instagram.com/" class="ins-icon"><img src="image/catalog/icon-social/instagram.png"> Instagram</a>
  
  
  <a href="https://www.youtube.com/" class="yt-icon"><img src="image/catalog/icon-social/youtube.png"> Youtube</a>
  
  
  <a href="https://twitter.com/" class="tw-icon"><img src="image/catalog/icon-social/twitter.png"> Twitter</a>
  
  
  <a href="https://plus.google.com/" class="gg-icon"><img src="image/catalog/icon-social/Google.png"> Google+</a>
  
  
  <a href="https://vn.linkedin.com/" class="linkedin-icon"><img src="image/catalog/icon-social/linkedIn_PNG21.png"> Linkedin</a>
  
</div>


<!--<div class="fix-phone-num d-none d-md-block"> <p><i class="Phone is-animating"></i> <span> <?php echo $telephone;?></span></p> </div>-->

<div class="top-footer">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-3 col-sm-12 col-12 icon text-center">
        <img src="catalog/view/theme/tlptech/image/IMG-EMAIL-2.png">
      </div>
      <div class="col-md-6 col-sm-12 col-12 form">
        <h6 class="mb-3">ĐĂNG KÝ NHẬN TIN KHUYẾN MÃI &amp; BÁO GIÁ TỪ CHÚNG TÔI</h6>
        <p>Chỉ cần nhập email và nhấn Đăng ký</p>
        <?php echo $module_newsletter; ?> 
        <p>Chúng tôi sẽ gửi cập nhật những tin khuyến mãi &amp; báo giá mới nhất đến bạn!</p> 
      </div>
      <div class="col-md-3 col-sm-12 col-12 cskh text-center">  
        <img src="catalog/view/theme/tlptech/image/chamsockhachhang.png">
        <div class="content-cskh h5 mt-3"> HOTLINE góp ý, than phiền: <b> <?php echo $telephone_1;?></b> </div>      
      </div>
    </div>
  </div>
</div>

<footer>
  <div id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-12 pt-5">
          <h5 class="heading-title"><?php echo $store;?></h5>
          <div class="name-foter">
            <ul class="list-unstyled">
              <li><?php echo $address;?></li>
              <li><b><?php echo $text_telephone; ?>: </b> <?php echo $telephone;?> <?php if(isset($telephone_1) && $telephone_1){echo ' - ' . $telephone_1;}?></li>
              <li><b>Email: </b> <?php echo $email;?> </li>
              <li><?php echo $config_comment;?> </li>
              
            </ul>
          </div>
          <br>
          <div>
            <h5 class="heading-title">Kết nối với chúng tôi</h5>
            <div><?php echo $socicals;?> </div>
          </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-12 pt-5 bdl">
          <div class="row">
            <div class="col-lg-1 d-none d-lg-block"></div>

            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
             <h5 class="heading-title"><?php echo $text_information; ?></h5>
             <ul class="list-unstyled">
              <?php foreach ($informations as $information) { ?>
              <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
              <?php } ?>
            </ul>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-12 col-12">
            <h5 class="heading-title"><?php echo $text_account; ?></h5>
            <ul class="list-unstyled">
              <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
              <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
              <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
            </ul>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-12">
            <h5 class="heading-title">FanPage</h5>
            <div id="fb-root"></div>
            <div class="fb-page" data-href="<?php echo $fax; ?>" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="<?php echo $fax; ?>"><a href="<?php echo $fax; ?>">Facebook</a></blockquote></div></div>
            <script>(function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.3&appId=371556406377065";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
          </div>
          <div id="powered" class="mt-5">
            <?php echo $powered; ?>
          </div>
        </div>      
      </div>
    </div>
  </div>
</div>
</footer>
<div class="wt_footer_support d-md-none">  
  <div class="footer_support clearfix"> 
    <ul class="ul"> 
      <li class="ft-btn ft-btn1">
        <a href="tel:<?php echo $telephone;?>" title="<?php echo $telephone;?>">
          <span><i class="fa fa-phone"></i>Gọi điện</span>
        </a>
      </li> 
      <li class="ft-btn ft-btn2">
        <a href="https://zalo.me/<?php echo $telephone;?>" title="<?php echo $telephone;?>">
          <span> <i class="fa fa-comments"></i> Zalo</span>
        </a>
      </li> 
      <li class="ft-btn ft-btn3">
        <a href="<?php echo $contact;?>">
          <span><i class="fa fa-map-marker"></i> Bản đồ</span>
        </a>
      </li> 
    </ul> 
  </div>  
</div>

<script  type="text/javascript" src="catalog/view/theme/tlptech/js/wow-js/wow.min.js"></script>
<script> new WOW().init(); </script>
<script defer="defer" type="text/javascript" src="catalog/view/theme/tlptech/js/colorbox/jquery.colorbox-min.js"></script>
<link href="catalog/view/theme/tlptech/js/colorbox/custom_colorbox.css" rel="stylesheet" type="text/css" />
<script  type="text/javascript" src="catalog/view/theme/tlptech/js/quickview.js"></script>
<?php echo $live_search; ?>
</body></html>