<<<<<<< HEAD
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title><?php echo $title; ?></title>
    <meta property="fb:app_id" content="<?php echo $config_appId;?>" />
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>" />
    <?php } ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <script src="catalog/view/theme/tlptech/js/jquery-2.1.1.min.js" type="text/javascript"></script>

    <script  src="catalog/view/theme/tlptech/js/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>

    <link href="catalog/view/theme/tlptech/js/bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <script  type="text/javascript" src="catalog/view/theme/tlptech/js/owlcarousel2/js/owl.carousel.min.js"></script>

    <link href="catalog/view/theme/tlptech/js/owlcarousel2/css/owl.carousel.min.css" rel="stylesheet" type="text/css" />

    <link href="catalog/view/theme/tlptech/js/owlcarousel2/css/owl.theme.default.min.css" rel="stylesheet" type="text/css" />

    <link href="catalog/view/theme/tlptech/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <link href="catalog/view/theme/tlptech/fonts/Stroke-Gap-Icons-Webfont/style.css" rel="stylesheet" type="text/css" />

    <script  type="text/javascript" src="catalog/view/theme/tlptech/js/tlptech_common.min.js"></script>
    
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/tlptech/css/main-style.min.css" />

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700" rel="stylesheet">
    <?php foreach ($scripts as $script) { ?>
    <script  type="text/javascript" src="<?php echo $script; ?>"></script>
    <?php } ?>
        <!--[if IE 8]>
        <link rel="stylesheet" type="text/css" href="catalog/view/theme/tlptech/stylesheet/ie8.css" />
    <![endif]-->
    <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
    <?php } ?>
</head>
<body class="<?php echo $class; ?>" >
    <div class="top-header d-none d-md-block">
        <div class="container">
            <div class="row">
                <div class="col-4">
                    <ul class="list-inline m-0">
                      <li class="list-inline-item"><i class="fa fa-envelope" aria-hidden="true"></i> <b>Email:</b> <?php echo $email;?></li>
                      <li class="list-inline-item"><i class="fa fa-phone" aria-hidden="true"></i> <b>Hotline:</b> <?php echo $telephone;?></li>
                  </ul>
              </div>
              <div class="col-8 text-right">
                <div class="d-inline-block"><?php echo $language; ?></div>
                <div class="d-inline-block px-2">--</div>
                <div class="d-inline-block"><?php echo $header_login; ?></div>
                <div class="d-inline-block pl-3"><?php echo $cart; ?></div>
            </div>
        </div>

    </div>
</div>
<div class="header">
    <div class="header-main">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 col-12 col-logo pr-0">
                    <div class="d-lg-none mobile_menu_trigger">
                        <!-- <i class="fa fa-bars"></i> -->
                        <div class="nav-icon"> <div></div> </div> <!--mobile bar icon-->
                    </div>
                    <div class="wt-logo d-inline-block">
                        <?php if ($logo) { ?>
                        <div class="logo">
                            <a href="<?php echo $home; ?>">
                                <?php if ($class == 'common-home'){?>
                                <h1><img src="<?php echo $logo;?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /> <span style="display:none"><?php echo $name; ?></span></h1>
                                <?php } else {?>
                                <img src="<?php echo $logo;?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" />
                                <?php } ?>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="col-xl-6 col-lg-6 col-menu d-none d-lg-block text-center">
                </div>
                <div class="col-xl-4 col-lg-4 col-menu d-none d-lg-block text-center">
                    <?php echo $search; ?>
                </div>
            </div>
        </div>

        <div class="container d-none d-lg-block">
            <div class="wapper-menu">
                <div id="menu">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="<?php echo $home; ?>">
                                <i class="fa fa-home" aria-hidden="true"></i>
                                Trang chủ
                            </a>
                        </li>

                        <?php if ($categories) { ?>
                        <?php foreach ($categories as $category_1) { ?>
                        <li class="list-inline-item full-width-dropdown-menu">
                            <a href="<?php echo $category_1['href']; ?>"><?php echo $category_1['name']; ?> <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            <?php if ($category_1['children']) { ?>
                            <div class="menu-drop-down">
                                <div class="row">
                                    <div class="col-sm-9">
                                        <ul class="row"> 
                                            <?php foreach ($category_1['children'] as $category_2) { ?>
                                            <li class="col-sm-4">
                                                <a href="<?php echo $category_2['href']; ?>" class="text-uppercase"><?php echo $category_2['name']; ?></a>
                                                <?php if ($category_2['children'] ) { ?>
                                                <ul>
                                                    <?php foreach ($category_2['children'] as $category_3) { ?>
                                                    <li>
                                                        <a href="<?php echo $category_3['href']; ?>"><i class="fa fa-angle-right" aria-hidden="true"></i> <?php echo $category_3['name']; ?></a>
                                                    </li>
                                                    <?php } ?>
                                                </ul>
                                                <?php } ?>
                                            </li><?php }?>
                                        </ul> 
                                    </div>
                                    <div class="col-sm-3">
                                        <a href="<?php echo $category_1['href']; ?>">
                                            <img src="<?php echo $category_1['thumb']; ?>">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <?php }?>
                        </li>
                        <?php }?>
                        <?php }?>

                        <?php foreach($categories_information as $category_information) { ?>
                        <li class="list-inline-item">
                            <a href="<?php echo $category_information['href']; ?>"><?php echo $category_information['name']; ?> <i class="fa fa-angle-down" aria-hidden="true"></i> </a>
                            <?php if($category_information['children']) { ?>
                            <div class="menu-drop-down">
                                <ul>
                                    <?php foreach($category_information['children'] as $blog) { ?>
                                    <li class="">
                                        <a href="<?php echo $blog['href']; ?>"><i class="fa fa-angle-right" aria-hidden="true"></i> <?php echo $blog['title']; ?></a>
                                    </li>
                                    <?php } ?>
                                </ul>  
                            </div>
                            <?php } ?>
                        </li>
                        <?php } ?>

                        <?php if($blogs){foreach($blogs as $blog_root) { ?>
                        <li class="list-inline-item">
                            <a href="<?php echo $blog_root['href']; ?>"><?php echo $blog_root['name']; ?> <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            <?php if($blog_root['children']) { ?>
                            <div class="menu-drop-down">
                                <ul>
                                    <?php foreach($blog_root['children'] as $blog) { ?>
                                    <li>
                                        <a href="<?php echo $blog['href']; ?>"><i class="fa fa-angle-right" aria-hidden="true"></i> <?php echo $blog['name']; ?></a>
                                    </li>
                                    <?php } ?>
                                </ul>  
                            </div>
                            <?php } ?>
                        </li>
                        <?php }} ?>

                        <?php if($contact){ ?>
                        <li class="list-inline-item">
                            <a href="<?php echo $contact; ?>">
                                <?php echo $text_contact; ?></a></li>
                                <?php } ?>
                            </ul>
                        </div> <!-- #menu ends -->
                    </div>
                </div><!--wapper menu ends-->
            </div><!-- hedaer-main end-->
            <div class="mobile_menu_wrapper" >
                <div class="mobile_menu">
                    <?php echo $header_login; ?>
                    <?php echo $language; ?>
                    
                    <br>
                    <h6><b><?php echo $tlptech_text_mobile_menu; ?></b></h6>
                    <ul class="m_menu list-unstyled px-0 py-0">
                        <?php if ($categories) { ?>
                        <?php foreach ($categories as $category_1) { ?>
                        <li><a href="<?php echo $category_1['href']; ?>" ><?php echo $category_1['name']; ?></a>
                            <?php if ($category_1['children']) { ?>
                            <span class="plus"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>
                            <ul class="list-unstyled px-0 py-0 mx-0 my-0">
                                <?php foreach ($category_1['children'] as $category_2) { ?>
                                <li>
                                    <a href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?></a>
                                    <?php if ($category_2['children']) { ?>
                                    <span class="plus"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>
                                    <ul class="list-unstyled px-0 py-0 mx-0 my-0">
                                        <?php foreach ($category_2['children'] as $category_3) { ?>
                                        <li><a href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                    <?php } ?>
                                </li>
                                <?php } ?>
                            </ul>
                            <?php } ?>
                        </li>
                        <?php } ?>
                        <?php } ?>
                        <?php foreach($blogs as $blog_root) { ?>
                        <li><a href="<?php echo $blog_root['href']; ?>"><?php echo $blog_root['name']; ?></a>
                            <?php if($blog_root['children']) { ?>
                            <span class="plus"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>
                            <ul>
                                <?php foreach($blog_root['children'] as $blog) { ?>
                                <li class="column level2">
                                    <a href="<?php echo $blog['href']; ?>"><?php echo $blog['name']; ?></a>
                                </li>
                                <?php } ?>
                            </ul> 
                            <?php } ?>
                        </li>
                        <?php } ?>
                    </ul>

                </div>
            </div>

        </div> <!-- header ends -->

=======
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title><?php echo $title; ?></title>
    <meta property="fb:app_id" content="<?php echo $config_appId;?>" />
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>" />
    <?php } ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <script src="catalog/view/theme/tlptech/js/jquery-2.1.1.min.js" type="text/javascript"></script>

    <script  src="catalog/view/theme/tlptech/js/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>

    <link href="catalog/view/theme/tlptech/js/bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <script  type="text/javascript" src="catalog/view/theme/tlptech/js/owlcarousel2/js/owl.carousel.min.js"></script>

    <link href="catalog/view/theme/tlptech/js/owlcarousel2/css/owl.carousel.min.css" rel="stylesheet" type="text/css" />

    <link href="catalog/view/theme/tlptech/js/owlcarousel2/css/owl.theme.default.min.css" rel="stylesheet" type="text/css" />

    <link href="catalog/view/theme/tlptech/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <link href="catalog/view/theme/tlptech/fonts/Stroke-Gap-Icons-Webfont/style.css" rel="stylesheet" type="text/css" />

    <script  type="text/javascript" src="catalog/view/theme/tlptech/js/tlptech_common.min.js"></script>
    
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/tlptech/css/main-style.min.css" />

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700" rel="stylesheet">
    
    <?php foreach ($scripts as $script) { ?>
    <script  type="text/javascript" src="<?php echo $script; ?>"></script>
    <?php } ?>
        <!--[if IE 8]>
        <link rel="stylesheet" type="text/css" href="catalog/view/theme/tlptech/stylesheet/ie8.css" />
    <![endif]-->
    <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
    <?php } ?>
</head>
<body class="<?php echo $class; ?>" >
    <div class="header">
        <div class="top-header d-none d-md-block">
            <div class="container">
                <div class="row">
                    <div class="col-4">
                        <ul class="list-inline m-0">
                          <li class="list-inline-item"><i class="fa fa-envelope" aria-hidden="true"></i> <b>Email:</b> <?php echo $email;?></li>
                          <li class="list-inline-item"><i class="fa fa-phone" aria-hidden="true"></i> <b>Hotline:</b> <?php echo $telephone;?></li>
                      </ul>
                  </div>
                  <div class="col-8 text-right">
                    <div class="d-inline-block"><?php echo $language; ?></div>
                    <div class="d-inline-block px-2">--</div>
                    <div class="d-inline-block"><?php echo $header_login; ?></div>
                    <div class="d-inline-block pl-3"><?php echo $cart; ?></div>
                </div>
            </div>

        </div>
    </div>

    <div class="header-main">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 col-12 col-logo pr-0">
                    <div class="d-lg-none mobile_menu_trigger">
                        <!-- <i class="fa fa-bars"></i> -->
                        <div class="nav-icon"> <div></div> </div> <!--mobile bar icon-->
                    </div>
                    <div class="wt-logo d-inline-block">
                        <?php if ($logo) { ?>
                        <div class="logo">
                            <a href="<?php echo $home; ?>">
                                <?php if ($class == 'common-home'){?>
                                <h1><img src="<?php echo $logo;?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /> <span style="display:none"><?php echo $name; ?></span></h1>
                                <?php } else {?>
                                <img src="<?php echo $logo;?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" />
                                <?php } ?>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="col-xl-6 col-lg-6 col-menu d-none d-lg-block text-center">
                </div>
                <div class="col-xl-4 col-lg-4 col-menu d-none d-lg-block text-center">
                    <?php echo $search; ?>
                </div>
            </div>
        </div>

        <div class="container d-none d-lg-block">
            <div class="wapper-menu">
                <div id="menu">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="<?php echo $home; ?>">
                                <i class="fa fa-home" aria-hidden="true"></i>
                                Trang chủ
                            </a>
                        </li>

                        <?php if ($categories) { ?>
                        <?php foreach ($categories as $category_1) { ?>
                        <li class="list-inline-item full-width-dropdown-menu">
                            <a href="<?php echo $category_1['href']; ?>"><?php echo $category_1['name']; ?> <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            <?php if ($category_1['children']) { ?>
                            <div class="menu-drop-down">
                                <div class="row">
                                    <div class="col-sm-9">
                                        <ul class="row"> 
                                            <?php foreach ($category_1['children'] as $category_2) { ?>
                                            <li class="col-sm-4">
                                                <a href="<?php echo $category_2['href']; ?>" class="text-uppercase"><?php echo $category_2['name']; ?></a>
                                                <?php if ($category_2['children'] ) { ?>
                                                <ul>
                                                    <?php foreach ($category_2['children'] as $category_3) { ?>
                                                    <li>
                                                        <a href="<?php echo $category_3['href']; ?>"><i class="fa fa-angle-right" aria-hidden="true"></i> <?php echo $category_3['name']; ?></a>
                                                    </li>
                                                    <?php } ?>
                                                </ul>
                                                <?php } ?>
                                            </li><?php }?>
                                        </ul> 
                                    </div>
                                    <div class="col-sm-3">
                                        <a href="<?php echo $category_1['href']; ?>">
                                            <img src="<?php echo $category_1['thumb']; ?>">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <?php }?>
                        </li>
                        <?php }?>
                        <?php }?>

                        <?php foreach($categories_information as $category_information) { ?>
                        <li class="list-inline-item">
                            <a href="<?php echo $category_information['href']; ?>"><?php echo $category_information['name']; ?> <i class="fa fa-angle-down" aria-hidden="true"></i> </a>
                            <?php if($category_information['children']) { ?>
                            <div class="menu-drop-down">
                                <ul>
                                    <?php foreach($category_information['children'] as $blog) { ?>
                                    <li class="">
                                        <a href="<?php echo $blog['href']; ?>"><i class="fa fa-angle-right" aria-hidden="true"></i> <?php echo $blog['title']; ?></a>
                                    </li>
                                    <?php } ?>
                                </ul>  
                            </div>
                            <?php } ?>
                        </li>
                        <?php } ?>

                        <?php if($blogs){foreach($blogs as $blog_root) { ?>
                        <li class="list-inline-item">
                            <a href="<?php echo $blog_root['href']; ?>"><?php echo $blog_root['name']; ?> <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            <?php if($blog_root['children']) { ?>
                            <div class="menu-drop-down">
                                <ul>
                                    <?php foreach($blog_root['children'] as $blog) { ?>
                                    <li>
                                        <a href="<?php echo $blog['href']; ?>"><i class="fa fa-angle-right" aria-hidden="true"></i> <?php echo $blog['name']; ?></a>
                                    </li>
                                    <?php } ?>
                                </ul>  
                            </div>
                            <?php } ?>
                        </li>
                        <?php }} ?>

                        <?php if($contact){ ?>
                        <li class="list-inline-item">
                            <a href="<?php echo $contact; ?>">
                                <?php echo $text_contact; ?></a></li>
                                <?php } ?>
                            </ul>
                        </div> <!-- #menu ends -->
                    </div>
                </div><!--wapper menu ends-->
            </div><!-- hedaer-main end-->
            <div class="mobile_menu_wrapper" >
                <div class="mobile_menu">
                    <?php echo $header_login; ?>
                    <?php echo $language; ?>
                    
                    <br>
                    <h6><b><?php echo $tlptech_text_mobile_menu; ?></b></h6>
                    <ul class="m_menu list-unstyled px-0 py-0">
                        <?php if ($categories) { ?>
                        <?php foreach ($categories as $category_1) { ?>
                        <li><a href="<?php echo $category_1['href']; ?>" ><?php echo $category_1['name']; ?></a>
                            <?php if ($category_1['children']) { ?>
                            <span class="plus"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>
                            <ul class="list-unstyled px-0 py-0 mx-0 my-0">
                                <?php foreach ($category_1['children'] as $category_2) { ?>
                                <li>
                                    <a href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?></a>
                                    <?php if ($category_2['children']) { ?>
                                    <span class="plus"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>
                                    <ul class="list-unstyled px-0 py-0 mx-0 my-0">
                                        <?php foreach ($category_2['children'] as $category_3) { ?>
                                        <li><a href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                    <?php } ?>
                                </li>
                                <?php } ?>
                            </ul>
                            <?php } ?>
                        </li>
                        <?php } ?>
                        <?php } ?>
                        <?php foreach($blogs as $blog_root) { ?>
                        <li><a href="<?php echo $blog_root['href']; ?>"><?php echo $blog_root['name']; ?></a>
                            <?php if($blog_root['children']) { ?>
                            <span class="plus"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>
                            <ul>
                                <?php foreach($blog_root['children'] as $blog) { ?>
                                <li class="column level2">
                                    <a href="<?php echo $blog['href']; ?>"><?php echo $blog['name']; ?></a>
                                </li>
                                <?php } ?>
                            </ul> 
                            <?php } ?>
                        </li>
                        <?php } ?>
                    </ul>

                </div>
            </div>

        </div> <!-- header ends -->

>>>>>>> a8cc293e8ba7c9b8984ce6d875dd2fb88725492e
        <div id="notification" class="container"></div>