<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title><?php echo $title; ?></title>
    <meta property="fb:app_id" content="<?php echo $config_appId;?>" />
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>" />
    <?php } ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <?php if (isset($propertys) && $propertys) { ?>
      <?php foreach ($propertys as $property) { ?>
      <meta property="og:<?php echo $property['title']; ?>" content="<?php echo $property['content']; ?>" />
      <?php }?>
      <meta property="og:type" content="website" />
    <?php } ?>
    <script src="catalog/view/theme/tlptech/js/jquery-2.1.1.min.js" type="text/javascript"></script>

    <script  src="catalog/view/theme/tlptech/js/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>

    <link href="catalog/view/theme/tlptech/js/bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <script  type="text/javascript" src="catalog/view/theme/tlptech/js/owlcarousel2/js/owl.carousel.min.js"></script>

    <link href="catalog/view/theme/tlptech/js/owlcarousel2/css/owl.carousel.min.css" rel="stylesheet" type="text/css" />

    <link href="catalog/view/theme/tlptech/js/owlcarousel2/css/owl.theme.default.min.css" rel="stylesheet" type="text/css" />

    <link href="catalog/view/theme/tlptech/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <link href="catalog/view/theme/tlptech/fonts/Stroke-Gap-Icons-Webfont/style.css" rel="stylesheet" type="text/css" />

    <script  type="text/javascript" src="catalog/view/theme/tlptech/js/tlptech_common.min.js"></script>
    
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/tlptech/stylesheet/style.min.css" />
    
    <?php foreach ($scripts as $script) { ?>
    <script  type="text/javascript" src="<?php echo $script; ?>"></script>
    <?php } ?>
        <!--[if IE 8]>
        <link rel="stylesheet" type="text/css" href="catalog/view/theme/tlptech/stylesheet/ie8.css" />
    <![endif]-->
    <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
    <?php } ?>
</head>
<body class="<?php echo $class; ?>" >
    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-6 col-sm-8 col-10 pr-0">
                    <ul class="list-inline m-0">
                        <li class="list-inline-item"><i class="fa fa-envelope d-none d-md-inline-block" aria-hidden="true"></i> <b>Email:</b> <?php echo $email;?></li>
                        <li class="list-inline-item"><i class="fa fa-phone d-none d-md-inline-block" aria-hidden="true"></i> <b>Hotline:</b> <?php echo $telephone;?></li>
                    </ul>
                </div>
                <div class="col-xl-7 col-lg-7 col-md-6 col-sm-4 col-2 text-right">
                    <div class="d-none d-lg-inline-block"><?php echo $language; ?></div>
                    <div class="d-none d-lg-inline-blockpx-2">--</div>
                    <div class="d-none d-lg-inline-block pr-3"><?php echo $header_login; ?></div>
                    <div class="d-inline-block "><?php echo $cart; ?></div>
                </div>
            </div>

        </div>
    </div>
    <div class="header">
        <div class="header-main">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 col-6 col-logo pr-0">
                        <div class="d-lg-none mobile_menu_trigger">
                            <!-- <i class="fa fa-bars"></i> -->
                            <div class="nav-icon"> <div></div> </div> <!--mobile bar icon-->
                        </div>
                        <div class="wt-logo d-inline-block">
                            <?php if ($logo) { ?>
                            <div class="logo">
                                <a href="<?php echo $home; ?>">
                                    <?php if ($class == 'common-home'){?>
                                    <h1><img src="<?php echo $logo;?>" style="max-height:70px" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /> <span style="display:none"><?php echo $name; ?></span></h1>
                                    <?php } else {?>
                                    <img src="<?php echo $logo;?>" style="max-height:70px" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" />
                                    <?php } ?>
                                </a>
                            </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="col-xl-6 col-lg-6 col-menu d-none d-lg-block text-center">
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6 text-center">
                        <?php echo $search; ?>
                    </div>
                </div>
            </div>

            <div class="container d-none d-lg-block">
                <div class="wapper-menu">
                    <div id="menu">
                        <ul class="list-inline">
                            <li class="list-inline-item active">
                                <a href="<?php echo $home; ?>">
                                    Trang chủ
                                </a>
                            </li>
                            <?php if ($categories) { ?>
                            <?php foreach ($categories as $category_1) { ?>
                            <li class="list-inline-item full-width-dropdown-menu">
                                <a href="<?php echo $category_1['href']; ?>"><?php echo $category_1['name']; ?> <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                <?php if ($category_1['children']) { ?>
                                <div class="menu-drop-down">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <ul class="row"> 
                                                <?php foreach ($category_1['children'] as $category_2) { ?>
                                                <li class="col-sm-4">
                                                    <a href="<?php echo $category_2['href']; ?>" class="text-uppercase"><?php echo $category_2['name']; ?></a>
                                                    <?php if ($category_2['children'] ) { ?>
                                                    <ul>
                                                        <?php foreach ($category_2['children'] as $category_3) { ?>
                                                        <li>
                                                            <a href="<?php echo $category_3['href']; ?>"><i class="fa fa-angle-right" aria-hidden="true"></i> <?php echo $category_3['name']; ?></a>
                                                        </li>
                                                        <?php } ?>
                                                    </ul>
                                                    <?php } ?>
                                                </li><?php }?>
                                            </ul> 
                                        </div>
                                        <div class="col-sm-3">
                                            <a href="<?php echo $category_1['href']; ?>">
                                                <img src="<?php echo $category_1['thumb']; ?>">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                            </li>
                            <?php }?>
                            <?php }?>
                            <?php if ($categories_information) { ?>
                            <?php foreach($categories_information as $category_information) { ?>
                            <li class="list-inline-item">
                                <a href="<?php echo $category_information['href']; ?>"><?php echo $category_information['name']; ?> <i class="fa fa-angle-down" aria-hidden="true"></i> </a>
                                <?php if($category_information['children']) { ?>
                                <div class="menu-drop-down">
                                    <ul>
                                        <?php foreach($category_information['children'] as $blog) { ?>
                                        <li class="">
                                            <a href="<?php echo $blog['href']; ?>"><i class="fa fa-angle-right" aria-hidden="true"></i> <?php echo $blog['title']; ?></a>
                                        </li>
                                        <?php } ?>
                                    </ul>  
                                </div>
                                <?php } ?>
                            </li>
                            <?php } ?><?php } ?>

                            <?php if($blogs){foreach($blogs as $blog_root) { ?>
                            <li class="list-inline-item">
                                <a href="<?php echo $blog_root['href']; ?>"><?php echo $blog_root['name']; ?> <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                <?php if($blog_root['children']) { ?>
                                <div class="menu-drop-down">
                                    <ul>
                                        <?php foreach($blog_root['children'] as $blog) { ?>
                                        <li>
                                            <a href="<?php echo $blog['href']; ?>"><i class="fa fa-angle-right" aria-hidden="true"></i> <?php echo $blog['name']; ?></a>
                                        </li>
                                        <?php } ?>
                                    </ul>  
                                </div>
                                <?php } ?>
                            </li>
                            <?php }} ?>

                            <?php if($contact){ ?>
                            <li class="list-inline-item">
                                <a href="<?php echo $contact; ?>">
                                    <?php echo $text_contact; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </div> <!-- #menu ends -->
                        </div>
                    </div><!--wapper menu ends-->
                </div><!-- hedaer-main end-->
                <div class="mobile_menu_wrapper" >
                    <div class="mobile_menu">
                        <?php echo $header_login; ?>
                        <?php echo $language; ?>
                        <h6 class="title-m-menu"><?php echo $tlptech_text_mobile_menu; ?></h6>
                        <ul class="m_menu list-unstyled px-0 py-0">
                            <?php if ($categories) { ?>
                            <?php foreach ($categories as $category_1) { ?>
                            <li>
                                <?php if ($category_1['children']) { ?>
                                <div class="d-flex p-link-d">
                                    <a class="flex-grow-1" href="<?php echo $category_1['href']; ?>" ><?php echo $category_1['name']; ?></a>
                                    <span class="plus align-self-center"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>
                                </div>
                                <ul class="list-unstyled px-0 py-0 mx-0 my-0">
                                    <?php foreach ($category_1['children'] as $category_2) { ?>
                                    <li>
                                        <?php if ($category_2['children']) { ?>
                                        <div class="d-flex p-link-d">
                                            <a class="flex-grow-1" href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?></a>
                                            <span class="plus align-self-center"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>
                                        </div>
                                        <ul class="list-unstyled p-0 m-0">
                                            <?php foreach ($category_2['children'] as $category_3) { ?>
                                            <li><a href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a></li>
                                            <?php } ?>
                                        </ul>
                                        <?php }else{ ?>
                                        <a href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?></a>
                                        <?php } ?>
                                    </li>
                                    <?php } ?>
                                </ul>
                                <?php }else{ ?>
                                <a href="<?php echo $category_1['href']; ?>" ><?php echo $category_1['name']; ?></a>
                                <?php } ?>
                            </li>
                            <?php } ?>
                            <?php } ?>
                            <?php foreach($blogs as $blog_root) { ?>
                            <li>
                                <?php if($blog_root['children']) { ?>
                                <div class="d-flex p-link-d">
                                    <a class="flex-grow-1" href="<?php echo $blog_root['href']; ?>"><?php echo $blog_root['name']; ?></a>
                                    <span class="plus align-self-center"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>
                                </div>
                                <ul class="list-unstyled p-0 m-0">
                                    <?php foreach($blog_root['children'] as $blog) { ?>
                                    <li class="column level2">
                                        <a href="<?php echo $blog['href']; ?>"><?php echo $blog['name']; ?></a>
                                    </li>
                                    <?php } ?>
                                </ul> 
                                <?php }else{ ?>
                                <a href="<?php echo $blog_root['href']; ?>"><?php echo $blog_root['name']; ?></a>
                                <?php } ?>
                            </li>
                            <?php } ?>
                        </ul>

                    </div>
                </div>

            </div> <!-- header ends -->

            <div id="notification" class="container"></div>