<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/tlptech/stylesheet/stylesheet.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/tlptech/stylesheet/grid/<?php echo $tlptech_max_width; ?>.css" />
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<?php if($tlptech_use_responsive == 'enabled'){ ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/tlptech/stylesheet/responsive.css" />
<?php } ?>
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<?php if ($direction == 'rtl') { ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/tlptech/stylesheet/rtl.css" />
<?php } ?>
<script src="catalog/view/theme/tlptech/js/jquery.bxslider.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/tlptech/stylesheet/jquery.bxslider.css" />
<script type="text/javascript" src="catalog/view/theme/tlptech/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="catalog/view/theme/tlptech/js/tlptech_common.js"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
<!--[if IE 8]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/tlptech/stylesheet/ie8.css" />
<![endif]-->
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
<?php echo $tlptech_styles; ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/tlptech/stylesheet/style.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/tlptech/stylesheet/style_media.css" />
</head>
<body class="<?php echo $class; ?>">
<div class="outer_container <?php echo $tlptech_default_product_style; ?> <?php if($tlptech_use_custom){ echo $tlptech_container_layout; } ?> <?php echo $tlptech_use_breadcrumb; ?>">
<div class="header_wrapper 
<?php echo $tlptech_menu_sticky; ?> 
<?php echo $tlptech_menu_border; ?> 
<?php echo $tlptech_header_style; ?>">
<div class="header_top_line_wrapper">
<div class="header_top_line container">
<?php if($tlptech_header_style == 'header1'){ ?>
    <?php } else { ?>
    <div class="drop_downs_wrapper">
    <?php echo $header_login; ?>
	
  	<?php echo $currency; ?>
    </div>
    <?php } ?>
  <div class="promo_message"><?php echo $tlptech_top_promo_message; ?></div>
  <div class="links contrast_font">
  <a href="<?php echo $account; ?>"><?php echo $text_account; ?></a>
  <a href="<?php echo $wishlist; ?>" id="wishlist-total"><?php echo $text_wishlist; ?></a>
  <a href="<?php echo $shopping_cart; ?>"><?php echo $text_shopping_cart; ?></a>
  <a href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a>
  </div>
  <div class="clearfix"></div>
</div>
</div>
<div class="container header">
<div class="header_main">
  <div class="header_right"> 
  <?php if ($logo) { ?>
  <div class="logo"><a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a></div>
  <?php } ?>
 <?php if($tlptech_header_style == 'header3') { ?>
 <?php } else { ?>
 <?php if($tlptech_header_search == 'enabled') { ?>
 <?php echo $search; ?>
 <?php } ?>
 <?php } ?>
 <div class="mobile_clear"></div>
  <!-- Position for header login, lang, curr, in the header main -->
	<?php if($tlptech_header_style == 'header1'){ ?>
    <?php echo $header_login; ?>
  	<?php echo $currency; ?>
    <?php } ?>
  <?php if($tlptech_header_style == 'header2') { ?>
    	<div class="shortcuts_wrapper">
       	<?php echo $header_wishlist_compare; ?>
        <?php if ($tlptech_header_cart) { echo $cart; } ?>
        </div>
    	<?php } else { ?>
    <?php } ?>
  <?php if($tlptech_header_style == 'header3'){ ?>
    <?php } else { ?>
      </div> <!-- header_right ends -->
	</div> <!-- header ends -->
    <?php } ?>

<div class="menu_wrapper">
<div class="container menu_border"></div>
<div class="container menu_holder">
<div id="menu">
<?php if($tlptech_header_style == 'header2') { ?>
    	<?php } else { ?>
        <div class="shortcuts_wrapper">
        <?php if($tlptech_header_style == 'header3') { ?>
        <?php echo $language; ?>
 <div class="search-holder">
 <?php echo $search; ?>
 </div>
 <?php } ?>
        
        <?php echo $header_wishlist_compare; ?>
        <?php if ($tlptech_header_cart) { echo $cart; } ?>
        </div>
 <?php } ?>
<a class="mobile_menu_trigger up_to_tablet"><i class="fa fa-bars"></i> <?php echo $tlptech_text_mobile_menu; ?></a>
  <ul class="only_desktop">
	<li class="home only_desktop <?php echo $tlptech_show_home_icon; ?>"><a href="<?php echo $home; ?>"><?php echo $text_home; ?></a></li>
       <?php if ($categories) { ?>
       <?php foreach ($categories as $category_1) { ?>
        <?php if ($category_1['category_1_id'] == $category_1_id) { ?>
		<li class="col<?php echo $category_1['column']; ?> current"><a href="<?php echo $category_1['href']; ?>" ><?php echo $category_1['name']; ?><i class="fa fa-sort-desc"></i></a>
         <?php } else { ?>
         <li class="col<?php echo $category_1['column']; ?>"><a href="<?php echo $category_1['href']; ?>" ><?php echo $category_1['name']; ?><i class="fa fa-sort-desc"></i></a>
         <?php } ?>
          <?php if ($category_1['children']) { ?>
          <div class="menu_drop_down" style="width: <?php echo ((($category_1['column']) * (195)) + (10)); ?>px">
          <div class="wrapper">
          <ul><?php foreach ($category_1['children'] as $category_2) { ?>
          <li class="column level2">
            <a href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?><i class="fa fa-caret-right"></i></a>
            <?php if($tlptech_menu_mega_second_thumb == 'enabled' && $category_2['thumb']) { ?>
          <a href="<?php echo $category_2['href']; ?>" class="sub_thumb"><img src="<?php echo $category_2['thumb']; ?>" alt="<?php echo $category_2['name']; ?>"/></a>
          <?php } ?>
              <?php if ($category_2['children']) { ?>
              <div class="third">
              <ul>
               <?php foreach ($category_2['children'] as $category_3) { ?>
               <li><a href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a></li>
               <?php } ?>
              </ul>
              </div>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
          </div><!-- wrapper ends -->
          </div>
          <?php } ?>
        </li>
        <?php } ?>
        <?php } ?>
        <?php foreach($blogs as $blog_root) { ?>
        <li class="col1"><a href="<?php echo $blog_root['href']; ?>" class="parent"><?php echo $blog_root['name']; ?><i class="fa fa-sort-desc"></i></a>
          <?php if($blog_root['children']) { ?>
            <div class="menu_drop_down" style="width: 205px">
                <ul>
                  <?php foreach($blog_root['children'] as $blog) { ?>
                    <li class="column level2">
                        <a href="<?php echo $blog['href']; ?>"><?php echo $blog['name']; ?></a>
                    </li>
                    <?php } ?>
                 </ul>  
            </div>
            <?php } ?>
        </li>
        <?php } ?>
        <li><i class="fa fa-caret-right"></i><a href="<?php echo $about_us; ?>"><?php echo $text_about_us; ?></a></li>
        <li><i class="fa fa-caret-right"></i><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
        <?php if($tlptech_custom_menu_block == 'enabled'){ ?>
		<li class="withsubs custom_block"><a><?php echo $tlptech_custom_menu_block_title; ?><i class="fa fa-sort-desc"></i></a>
        <div class="menu_drop_down" style="width:<?php echo $tlptech_menu_block_width; ?>px">
        <?php echo $tlptech_menu_custom_block_content; ?>
        </div></li>
		<?php } ?>
        <?php if($tlptech_custom_menu_title1){ ?>
		<li><a href="<?php echo $tlptech_custom_menu_url1 ?>"><?php echo $tlptech_custom_menu_title1; ?></a></li>
        <?php } ?>
        <?php if($tlptech_custom_menu_title2){ ?>
		<li><a href="<?php echo $tlptech_custom_menu_url2 ?>"><?php echo $tlptech_custom_menu_title2; ?></a></li>
        <?php } ?>
        <?php if($tlptech_custom_menu_title3){ ?>
		<li><a href="<?php echo $tlptech_custom_menu_url3; ?>"><?php echo $tlptech_custom_menu_title3; ?></a></li>
        <?php } ?>
        <?php if($tlptech_custom_menu_title4){ ?>
		<li><a href="<?php echo $tlptech_custom_menu_url4; ?>"><?php echo $tlptech_custom_menu_title4; ?></a></li>
        <?php } ?>
        <?php if($tlptech_custom_menu_title5){ ?>
		<li><a href="<?php echo $tlptech_custom_menu_url5; ?>"><?php echo $tlptech_custom_menu_title5; ?></a></li>
        <?php } ?>
      </ul>
    </div> <!-- menu_holder ends -->
</div> <!-- menu ends -->
</div> <!-- menu_wrapper ends -->

<?php if($tlptech_header_style == 'header3'){ ?>
      </div> <!-- header_right ends -->
	</div> <!-- header ends -->
    <?php } else { ?><?php } ?>
<div class="clearfix"></div>
<div class="mobile_menu_wrapper">

<div class="mobile_menu">

	<ul>
    <?php if ($categories) { ?>
       <?php foreach ($categories as $category_1) { ?>
         <li><a href="<?php echo $category_1['href']; ?>" ><?php echo $category_1['name']; ?></a>
          <?php if ($category_1['children']) { ?>
          <span class="plus"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>
          <ul>
            <?php foreach ($category_1['children'] as $category_2) { ?>
            <li>
            <a href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?></a>
              <?php if ($category_2['children']) { ?>
              <span class="plus"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>
              <ul>
                <?php foreach ($category_2['children'] as $category_3) { ?>
                <li><a href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
          <?php } ?>
        </li>
        <?php } ?>
        <?php } ?>
        <?php if($tlptech_custom_menu_title1){ ?>
		<li><a href="<?php echo $tlptech_custom_menu_url1; ?>"><?php echo $tlptech_custom_menu_title1; ?></a></li>
        <?php } ?>
        <?php if($tlptech_custom_menu_title2){ ?>
		<li><a href="<?php echo $tlptech_custom_menu_url2; ?>"><?php echo $tlptech_custom_menu_title2; ?></a></li>
        <?php } ?>
        <?php if($tlptech_custom_menu_title3){ ?>
		<li><a href="<?php echo $tlptech_custom_menu_url3; ?>"><?php echo $tlptech_custom_menu_title3; ?></a></li>
        <?php } ?>
        <?php if($tlptech_custom_menu_title4){ ?>
		<li><a href="<?php echo $tlptech_custom_menu_url4; ?>"><?php echo $tlptech_custom_menu_title4; ?></a></li>
        <?php } ?>
        <?php if($tlptech_custom_menu_title5){ ?>
		<li><a href="<?php echo $tlptech_custom_menu_url5; ?>"><?php echo $tlptech_custom_menu_title5; ?></a></li>
        <?php } ?>
       <!-- bolgs--> 
       <?php if ($blogs) { ?>
       <?php foreach($blogs as $blog_root) { ?>
         <li><a href="<?php echo $blog_root['href']; ?>" ><?php echo $blog_root['name']; ?></a>
          <?php if($blog_root['children']) { ?>
          <span class="plus"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>
          <ul>
            <?php foreach($blog_root['children'] as $blog) { ?>
            <li>
            <a href="<?php echo $blog['href']; ?>"><?php echo $blog['name']; ?></a>
            </li>
            <?php } ?>
          </ul>
          <?php } ?>
        </li>
        <?php } ?>
        <?php } ?>
        <!-- end bolgs --> 
        <li><a href="<?php echo $about_us; ?>"><?php echo $text_about_us; ?></a></li>
        <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
      </ul>

</div>
</div>
</div> <!-- header_wrapper ends -->
</div> <!-- inner conainer ends -->
<div class="breadcrumb_wrapper"></div>
<div id="notification" class="container"></div>