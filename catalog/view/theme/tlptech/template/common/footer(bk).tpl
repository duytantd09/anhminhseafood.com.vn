<div class="fix-phone-num d-none d-md-block"> <p><i class="Phone is-animating"></i> <span> <?php echo $telephone;?></span></p> </div>

<div class="top-footer">
  <div class="container"><!-- container -->
    <div class="row">
      <div class="col-md-4 col-sm-12 col-12 suppfo">
        <div class="row">
          <div class="col-3">
            <img src="catalog/view/theme/tlptech/image/@.png">
          </div>
          <div class="col-9 align-self-center">
           <p>Mail: <a href="mailto:<?php echo $email;?>"><?php echo $email;?></a></p>
         </div> 
       </div>
     </div>
     <div class="col-md-4 col-sm-12 col-12 suppfo">
      <div class="row">
        <div class="col-3">
          <img src="catalog/view/theme/tlptech/image/icon_phone.png">
        </div>
        <div class="col-9 align-self-center">
          <p >Hotline: <a href="tel:(+84) <?php echo $telephone;?>"><?php echo $telephone;?></a></p>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-12 col-12 suppfo ">
      <div class="row">
        <div class="col-3">
          <img src="catalog/view/theme/tlptech/image/icon_skype.png">
        </div>
        <div class="col-9 align-self-center">          
          <p>Kinh doanh: <a href="skype:<?php echo $email;?>?chat"><?php echo $telephone;?></a></p>
          <p>Kỹ thuật: <a href="skype:<?php echo $email;?>?chat"><?php echo $telephone_1;?></a></p>
        </div>   
      </div>         
    </div>
  </div>
</div><!-- #container -->
</div>

<footer>
  <div id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
          <h5 class="heading-title"><?php echo $store;?></h5>
          <div class="name-foter">
            <ul class="list-unstyled">
              <li><?php echo $address;?></li>
              <li><b>Điện thoại: </b> <?php echo $telephone;?></li>
              <li><b>Email: </b> <?php echo $email;?> </li>
            </ul>
          </div>
        </div>


        <?php if ($informations) { ?>
        <div class="col-lg-2 col-md-6 col-sm-6 col-12">
          <h5 class="heading-title">Chính sách</h5>
          <ul class="list-unstyled">
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
            <?php foreach ($informations as $information) { ?>
            <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
          <h5 class="heading-title">Đăng ký nhận Email</h5>
          <?php echo $module_newsletter; ?>  
          <div><?php echo $socicals;?> </div>
        </div>
        <?php } ?>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
          <h5 class="heading-title">Facebook</h5>
          <div id="fb-root"></div>
          <div class="fb-page" data-href="<?php echo $fax; ?>" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="<?php echo $fax; ?>"><a href="<?php echo $fax; ?>">Facebook</a></blockquote></div></div>
          <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.3&appId=371556406377065";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));</script>
        </div>                 

      </div>
    </div>
  </div>

  <div class="bottom-footer text-center">
    <div class="container">
      <div id="powered">
        <?php echo $powered; ?>
      </div>
    </div>
  </div>    
</footer>
<div class="wt_footer_support"> 
  <div class="container"> 
    <div class="footer_support clearfix"> 
      <ul class="ul"> 
        <li>
          <a href="tel:<?php echo $telephone;?>" title="<?php echo $telephone;?>">
            <i class="fa fa-phone" aria-hidden="true"></i><span>Gọi điện</span>
          </a>
        </li> 
        <li>
          <a href="sms:<?php echo $telephone;?>" title="<?php echo $telephone;?>">
            <i class="fa fa-comments" aria-hidden="true"></i><span>Gửi SMS</span>
          </a>
        </li> 
        <li>
          <a href="<?php echo $contact;?>">
            <i class="fa fa-map-marker" aria-hidden="true"></i><span>Bản đồ</span>
          </a>
        </li> 
      </ul> 
    </div> 
  </div> 
</div>
<a id="back-to-top" href="#" class="btn btn-outline-primary btn back-to-top" role="button"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
<script  type="text/javascript" src="catalog/view/theme/tlptech/js/wow-js/wow.min.js"></script>
<script> new WOW().init(); </script>
<script defer="defer" type="text/javascript" src="catalog/view/theme/tlptech/js/colorbox/jquery.colorbox-min.js"></script>
<link href="catalog/view/theme/tlptech/js/colorbox/custom_colorbox.css" rel="stylesheet" type="text/css" />
<?php echo $live_search; ?>
</body></html>