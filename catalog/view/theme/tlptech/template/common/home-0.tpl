<?php echo $header; ?>
<script type="text/javascript">
	$("li.home").addClass("current");
	$(".breadcrumb_wrapper").hide();
</script>
<div class="container">
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-9"><?php echo $home_top_top; ?></div>
    </div>
</div>
<?php echo $home_top_center; ?>
<?php echo $content_top; ?>
<div class="banner_home_2">
    <?php foreach ($banners as $banner) { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" style="width:100%;" />
    <?php } ?>
    <div class="banner_home_2_content">
        <div class="container">
            <div class="wrapper_background"><?php echo $home_top_right; ?></div>
        </div>
    </div>
</div>

<div class="home_top_wrapper">
<?php echo $home_top_left; ?>
</div>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> homepage">
    
     <?php echo $content_bottom_half; ?>
     <?php echo $content_bottom; ?>
     </div>
    <?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?> 