<?php echo $header; ?>
<?php echo $content_top; ?>
<?php echo $home_top_top; ?>
<?php echo $home_function; ?>
<?php foreach($banners as $banner){?>
<div class="home-contact" style="background-image: linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)),url('<?php echo $banner['image'];?>')">
	<div class="container">
		<div class="row">
			<div class="col-xl-3 col-lg-3 col-md-1 d-none d-md-block"></div>
			<div class="col-xl-6 col-lg-6 col-md-10 col-sm-12 col-12">
				<div class="home-section-content text-center">
					<div class="module-title text-center">
						<h3 class="title text-white"><?php echo $banner['title'];?></h3>
					</div>

					<div class="home-section-body">
						<div class="contact-content"><?php echo $banner['description'];?></div>
						<br>
						<div class="contact-btn">
							<a href="<?php echo $banner['link'];?>" class="btn btn-primary">Liên hệ</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php }?>
<?php echo $content_bottom; ?>
<?php echo $footer; ?> 