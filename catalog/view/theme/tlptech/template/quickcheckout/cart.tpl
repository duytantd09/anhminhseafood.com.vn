<div class="checkout-heading box-heading"><?php echo $text_cart; ?></div>
<?php if ($error_warning) { ?>
<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
  <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<div class="table-responsive">
  <table class="quickcheckout-cart table table-bordered table-sm">
   <thead>
    <tr>
      <th class="image "><?php echo $text_image; ?></th>
      <th class="name"><?php echo $text_name; ?></th>
      <th class="quantity"><?php echo $text_quantity; ?></th>
      <th class="unit-price "><?php echo $text_price; ?></th>
      <th class="total"><?php echo $text_total; ?></th>
    </tr>
  </thead>
  <?php if ($products || $vouchers) { ?>
  <tbody>
    <?php foreach ($products as $product) { ?>
    <tr>
      <td class="image " title="<?php echo $text_image; ?>">
        <?php if ($product['thumb']) { ?>
        <a href="<?php echo $product['href']; ?>">
          <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" />
        </a>
        <?php } ?>
      </td>
      <td class="name" title="<?php echo $text_name; ?>">
        <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
        <?php if (!$product['stock']) { ?>
        <span class="text-danger">***</span>
        <?php } ?>
        <div>
          <?php foreach ($product['option'] as $option) { ?>
          <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
          <?php } ?>

          <?php if ($product['recurring']) { ?>
          <small><?php echo $text_recurring_item; ?>: <?php echo $product['recurring']; ?></small>
          <?php } ?>
        </div>
      </td>
      <td class="quantity" title="<?php echo $text_quantity; ?>">
        <?php if ($edit_cart) { ?>
        <div class="input-group">
          <input type="text" name="quantity[<?php echo $product['cart_id']; ?>]" " value="<?php echo $product['quantity']; ?>" class="form-control"/>
          <div class="input-group-append">
            <a data-tooltip="<?php echo $button_update; ?>" class="button-update btn btn-outline-secondary"><i class="fa fa-refresh"></i></a>
            <a href="<?php echo $product['cart_id']; ?>" data-tooltip="<?php echo $button_remove; ?>" class="button-remove btn btn-outline-secondary" data-remove="<?php echo $product['cart_id']; ?>"><i class="fa fa-times"></i></a>
          </div>
        </div>
      </div>
      <?php } else { ?>
      x&nbsp;<?php echo $product['quantity']; ?>
      <?php } ?></td>
      <td class="unit-price " title="<?php echo $text_price; ?>"><?php echo $product['price']; ?></td>
      <td class="total" title="<?php echo $text_total; ?>"><?php echo $product['total']; ?></td>
    </tr>
    <?php } ?>
    <?php foreach ($vouchers as $voucher) { ?>
    <tr>
      <td class="image " title="<?php echo $text_image; ?>"></td>
      <td class="name" title="<?php echo $text_name; ?>"><?php echo $voucher['description']; ?></td>
      <td class="quantity" title="<?php echo $text_quantity; ?>">x&nbsp;1</td>
      <td class="price " title="<?php echo $text_price; ?>"><?php echo $voucher['amount']; ?></td>
      <td class="total" title="<?php echo $text_total; ?>"><?php echo $voucher['amount']; ?></td>
    </tr>
  </tbody>
  <?php } ?>
</table>
</div>
<div class="table-responsive">
  <table class="quickcheckout-cart table table-bordered table-sm">
    <tbody>
      <?php foreach ($totals as $total) { ?>
      <tr>
        <td colspan="4" class="text-right"><?php echo $total['title']; ?>:</td>
        <td "><?php echo $total['text']; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php } ?>
