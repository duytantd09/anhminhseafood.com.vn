<?php if ($coupon_module) { ?>
<div id="coupon-heading" class="dropdown-toggle font-weight-bold"><?php echo $entry_coupon; ?></div>

<div class="input-group mb-3" id="coupon-content">
  <input type="text" class="form-control" name="coupon" value="">
  <div class="input-group-append">
    <button class="btn btn-outline-secondary" type="button"  id="button-coupon"><?php echo $text_use_coupon; ?></button>
  </div>
</div>
<?php } ?>
<?php if ($voucher_module) { ?>
<div id="voucher-heading" class="dropdown-toggle font-weight-bold"><?php echo $entry_voucher; ?></div>

<div class="input-group mb-3" id="voucher-content">
  <input type="text" class="form-control" name="voucher" value="">
  <div class="input-group-append">
    <button class="btn btn-outline-secondary" type="button" id="button-voucher" ><?php echo $text_use_voucher; ?></button>
  </div>
</div>
<?php } ?>
<?php if ($reward_module && $reward) { ?>
<div id="reward-heading" class="dropdown-toggle font-weight-bold"><?php echo $entry_reward; ?></div>

<div class="input-group mb-3" id="reward-content">
  <input type="text" class="form-control" name="reward" value="" >
  <div class="input-group-append">
    <button class="btn btn-outline-secondary" type="button" id="button-reward" ><?php echo $text_use_reward; ?></button>
  </div>
</div>
<?php } ?>

<script type="text/javascript"><!--
  $('#coupon-heading').on('click', function() {
    if($('#coupon-content').is(':visible')){
      $('#coupon-content').slideUp('slow');
    } else {
      $('#coupon-content').slideDown('slow');
    };
  });

  $('#voucher-heading').on('click', function() {
    if($('#voucher-content').is(':visible')){
      $('#voucher-content').slideUp('slow');
    } else {
      $('#voucher-content').slideDown('slow');
    };
  });

  $('#reward-heading').on('click', function() {
    if($('#reward-content').is(':visible')){
      $('#reward-content').slideUp('slow');
    } else {
      $('#reward-content').slideDown('slow');
    };
  });
  //--></script>