var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');

// Compiles SCSS files from /scss into /css
gulp.task('sass', function() {
  return gulp.src('scss_dacsanbinhdinh.tlptech.vn/main-style.scss')
    .pipe(sass())
    .pipe(gulp.dest('stylesheet'))
});

// Minify compiled CSS
gulp.task('minify-css', ['sass'], function() {
  return gulp.src('stylesheet/style.css')
    .pipe(cleanCSS({
      compatibility: 'ie8'
    }))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('stylesheet'))
});

// Minify custom JS
gulp.task('minify-js', function() {
  return gulp.src('js/tlptech_common.js')
    .pipe(uglify())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('js'))
});

// Default task
gulp.task('default', ['sass', 'minify-css', 'minify-js']);

// Dev task 
gulp.task('dev', ['sass', 'minify-css', 'minify-js'], function() {
  gulp.watch('scss_dacsanbinhdinh.tlptech.vn/**/*.scss', ['sass']);
  gulp.watch('stylesheet/*.css', ['minify-css']);
  gulp.watch('js/*.js', ['minify-js']);
});
