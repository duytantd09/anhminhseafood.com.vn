<form id="alepaysubmit" name="alepaysubmit" action="<?php echo $action; ?>" method="POST">
  <div class="pull-right">
      Select one method
      <select name="alepay_method" style="margin-right: 10px;">
        <?php foreach ($payment_methods as $key => $value) { ?>
        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
        <?php } ?>
      </select>
    </div>
</form>
<div class="buttons">
    <div class="pull-right">
        <input type="submit" value="<?php echo $button_confirm; ?>" id="button-confirm" class="btn btn-primary" />
    </div>
</div>
<script type="text/javascript"><!--
    $('#button-confirm').bind('click', function() {
      $.ajax({
        type: "POST",
        url: $('#alepaysubmit').attr('action'),
        data: $('#alepaysubmit').serialize(), // serializes the form's elements.
		
        success: function(data){
		alert (data);
          var data = $.parseJSON(data);
          if (data.errorCode === '000') { // thanh cong
            if (data.checkoutUrl) { // neu alepay tra ve link thanh toan
              if (data.popup === '1') // bat popup alepay
                openAlepayPopup(data.checkoutUrl);
              else
                location.href = data.checkoutUrl;
            } else { // redirect sang alepay hoac sang trang success
              location.href = data.redirectUrl;
            }
          } else { // co loi
            alert(data.errorDescription);
          }
        }
      });
    });
    
    function openAlepayPopup(url) {
        this.popup = document.createElement('div');
        this.iframe = document.createElement("iframe");
        this.popup.style.opacity = 1;
        this.popup.id = "sc-popup-TTTG";
        this.popup.style.width = "100%";
        this.popup.style.minHeight = "500px";
        var iframeStyle =
                "z-index: 2147483647; display: block; background: rgba(0, 0, 0, 0.004) none repeat scroll 0% 0%;"
                + "border: 0px none transparent;    overflow-x: hidden;"
                + "overflow-y: auto;    visibility: visible;    margin: 0px;    padding: 0px;"
                + "position: fixed;    left: 0px;    top: 0px;"
                + "width: 100%;"
                + "height: 100%;";
        this.iframe.setAttribute('style', iframeStyle);
        this.iframe.border = 0;
        this.iframe.scrolling = "no";
        this.iframe.allowTransparency = "true";
        this.iframe.width = "100%";
        this.iframe.src = url;
        this.popup.appendChild(this.iframe);
        document.body.appendChild(this.popup);
        this.popup.className = "sc-popup-wrap";
    }
    
//--></script>

